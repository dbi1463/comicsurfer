/* ZipBookReaderTest.java created on 2012/12/15
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static org.junit.Assert.*;
import static tw.funymph.commons.file.FileUtilities.ZIP_FILE_EXTENSION;
import static tw.funymph.commons.io.IOConstants.URI_SCHEME_FILE;

import java.io.File;
import java.net.URI;

import org.junit.Before;
import org.junit.Test;

import tw.funymph.commons.zip.UTF8ZipFileFactory;

/** The test cases for {@link ZipBookReader}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class ZipBookReaderTest {

	private File correctTarget;
	private File incorrectTarget;
	private URI unsupportTarget;
	private ZipBookReader testee;

	@Before
	public void setUp() throws Exception {
		correctTarget = new File("data/zip/book.zip");
		incorrectTarget = new File("data/folder-folder");
		unsupportTarget = new URI("http://somewhere.com/abc");
		testee = new ZipBookReader(new UTF8ZipFileFactory());
	}

	@Test
	public void testCanRead() {
		assertTrue(testee.canRead(correctTarget.toURI()));
		assertFalse(testee.canRead(incorrectTarget.toURI()));
		assertFalse(testee.canRead(unsupportTarget));
	}

	@Test
	public void testRead() {
		assertNotNull(testee.read(correctTarget.toURI()));
		assertNull(testee.read(incorrectTarget.toURI()));
		assertNull(testee.read(unsupportTarget));
	}

	@Test
	public void testGetInformation() {
		assertEquals(ZIP_FILE_EXTENSION, testee.getExtension());
		assertFalse(testee.isFolderReader());
		assertEquals(ZipBookReader.DESCRIPTION, testee.getDescription());
		assertEquals(1, testee.getSupportedSchemes().length);
		assertEquals(URI_SCHEME_FILE, testee.getSupportedSchemes()[0]);
	}
}
