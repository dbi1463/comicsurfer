/* ZipEntryPageTest.java created on 2012/12/15
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static org.junit.Assert.*;
import static tw.funymph.commons.io.IOCloser.close;

import java.io.File;
import java.io.InputStream;
import java.util.zip.ZipFile;

import org.junit.Before;
import org.junit.Test;

import tw.funymph.commons.zip.UTF8ZipFileFactory;
import tw.funymph.commons.zip.ZipFileFactory;

/** The test cases for {@link ZipEntryPage}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class ZipEntryPageTest {

	private ZipFileFactory factory;

	@Before
	public void setUp() throws Exception {
		factory = new UTF8ZipFileFactory();
	}

	@Test
	public void testGetContent() {
		ZipFile file = factory.getZipFile(new File("data/zip/book.zip"));
		ZipEntryPage page = new ZipEntryPage(file, file.getEntry("page1.png"));
		InputStream stream1 = null;
		InputStream stream2 = null;
		assertNotNull((stream1 = page.getContent()));
		assertNotNull((stream2 = page.getContent()));
		assertFalse(stream1 == stream2);
		close(stream1);
		close(stream2);

		page = new ZipEntryPage(file, file.getEntry("non-existing.png"));
		assertNull(page.getContent());
	}

	@Test
	public void testClose() {
		ZipFile file = factory.getZipFile(new File("data/zip/book.zip"));
		ZipEntryPage page = new ZipEntryPage(file, file.getEntry("page1.png"));
		InputStream stream = null;
		assertNotNull((stream = page.getContent()));
		close(stream);

		page.close();
		assertNull(page.getContent());
	}
}
