/* FolderBookTest.java created on 2012/12/15
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

/** The test cases for {@link FolderBook}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class FolderBookTest {

	@Test
	public void testOpen() {
		FolderBook book = new FolderBook(new File("data/folder"));
		assertEquals(0, book.getPageCount());

		book.open();
		assertEquals(7, book.getPageCount());

		// Re-open without closing
		book.open();
		assertEquals(7, book.getPageCount());

		book.close();
		assertEquals(0, book.getPageCount());

		book = new FolderBook(null);
		book.open();
		assertEquals(0, book.getPageCount());
		book.close();

		book = new FolderBook(new File("non-existing"));
		book.open();
		assertEquals(0, book.getPageCount());
		book.close();
	}

	@Test
	public void testGetPage() {
		FolderBook book = new FolderBook(new File("data/folder"));
		book.open();
		int pageCount = book.getPageCount();
		assertEquals(7, pageCount);

		int index = 0;
		for(; index < pageCount; index++) {
			assertNotNull(book.getPage(index));
		}
		assertNull(book.getPage(-1));
		assertNull(book.getPage(index));
	}
}
