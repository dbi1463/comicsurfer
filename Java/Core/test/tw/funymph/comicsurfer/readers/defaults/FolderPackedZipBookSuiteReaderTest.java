/* FolderPackedZipBookSuiteReaderTest.java created on 2012/12/15
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static org.junit.Assert.*;
import static tw.funymph.comicsurfer.readers.defaults.FolderPackedZipBookSuiteReader.DESCRIPTION;
import static tw.funymph.commons.io.IOConstants.URI_SCHEME_FILE;

import java.io.File;
import java.net.URI;

import org.junit.Before;
import org.junit.Test;

import tw.funymph.commons.zip.UTF8ZipFileFactory;

/** The test cases for {@link FolderPackedZipBookSuiteReader}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class FolderPackedZipBookSuiteReaderTest {

	private File correctTarget;
	private File incorrectTarget;
	private File noImageTarget;
	private URI unsupportTarget;
	private FolderPackedZipBookSuiteReader testee;

	@Before
	public void setUp() throws Exception {
		correctTarget = new File("data/folder-zip");
		incorrectTarget = new File("data/folder-folder");
		unsupportTarget = new URI("http://somewhere.com/abc");
		noImageTarget = new File("data/folder-zip-no-images");
		testee = new FolderPackedZipBookSuiteReader(new UTF8ZipFileFactory());
	}

	@Test
	public void testGetInformation() {
		assertNull(testee.getExtension());
		assertTrue(testee.isFolderReader());
		assertEquals(DESCRIPTION, testee.getDescription());
		assertEquals(1, testee.getSupportedSchemes().length);
		assertEquals(URI_SCHEME_FILE, testee.getSupportedSchemes()[0]);
	}

	@Test
	public void testCanRead() {
		assertNotNull(testee.read(correctTarget.toURI()));
		assertNull(testee.read(incorrectTarget.toURI()));
		assertNull(testee.read(noImageTarget.toURI()));
		assertNull(testee.read(unsupportTarget));
	}

	@Test
	public void testRead() {
		assertTrue(testee.canRead(correctTarget.toURI()));
		assertFalse(testee.canRead(incorrectTarget.toURI()));
		assertFalse(testee.canRead(noImageTarget.toURI()));
		assertFalse(testee.canRead(unsupportTarget));
		assertFalse(testee.canRead(new File("data/folder/page1.png").toURI()));
	}
}
