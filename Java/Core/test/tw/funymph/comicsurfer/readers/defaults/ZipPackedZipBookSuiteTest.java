/* ZipPackedZipBookSuiteTest.java created on 2012/12/16
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import tw.funymph.comicsurfer.core.ComicBook;
import tw.funymph.commons.zip.UTF8ZipFileFactory;
import tw.funymph.commons.zip.ZipFileFactory;

/** The test cases for {@link ZipPackedZipBookSuite}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class ZipPackedZipBookSuiteTest {

	private ZipPackedZipBookSuite testee;

	@Before
	public void setUp() throws Exception {
		File file = new File("data/zip-zip/zip-zip.zip");
		ZipFileFactory factory = new UTF8ZipFileFactory();
		testee = new ZipPackedZipBookSuite(factory.getZipFile(file), file.toURI(), factory);
		assertEquals(0, testee.getComicBookCount());
	}

	@After
	public void tearDown() {
		testee.close();
	}

	@Test
	public void testOpen() {
		testee.open();
		int bookCount = testee.getComicBookCount();
		assertEquals(3, bookCount);

		int index = 0;
		int[] pageCounts = new int[] {6, 4, 3};
		for(; index < bookCount; index++) {
			ComicBook book = testee.getComicBook(index);
			assertNotNull(book);
			book.open();
			assertEquals(pageCounts[index], book.getPageCount());
			book.close();
		}
		assertNull(testee.getComicBook(-1));
		assertNull(testee.getComicBook(index));
	}

	@Test
	public void testClose() {
	}

	@Test
	public void testSetName() {
	}

	@Test
	public void testGetComicBook() {
	}
}
