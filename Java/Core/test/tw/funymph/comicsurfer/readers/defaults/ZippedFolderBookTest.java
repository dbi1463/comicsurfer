/* ZippedFolderBookTest.java created on 2012/12/16
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static org.junit.Assert.*;

import java.io.File;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.junit.Before;
import org.junit.Test;

import tw.funymph.commons.zip.UTF8ZipFileFactory;
import tw.funymph.commons.zip.ZipFileFactory;

/** The test cases for {@link ZippedFolderBook}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class ZippedFolderBookTest {

	private ZipFileFactory factory;

	@Before
	public void setUp() throws Exception {
		factory = new UTF8ZipFileFactory();
	}

	@Test
	public void testOpen() {
		ZipFile root = factory.getZipFile(new File("data/zip-folder/zip-folder.zip"));
		ZipEntry folder = root.getEntry("book1/");
		ZippedFolderBook book = new ZippedFolderBook(root, folder);
		assertEquals(0, book.getPageCount());

		book.open();
		assertEquals(4, book.getPageCount());

		// Re-open without closing
		book.open();
		assertEquals(4, book.getPageCount());

		book.close();
		assertEquals(0, book.getPageCount());

		book = new ZippedFolderBook(root, root.getEntry("book1/page1.png"));
		book.open();
		assertEquals(0, book.getPageCount());
		book.close();

		book = new ZippedFolderBook(null, null);
		book.open();
		assertEquals(0, book.getPageCount());
		book.close();
	}

	@Test
	public void testGetPage() {
		ZipFile root = factory.getZipFile(new File("data/zip-folder/zip-folder.zip"));
		ZipEntry folder = root.getEntry("book1/");
		ZippedFolderBook book = new ZippedFolderBook(root, folder);
		book.open();
		int pageCount = book.getPageCount();
		assertEquals(4, pageCount);

		int index = 0;
		for(; index < pageCount; index++) {
			assertNotNull(book.getPage(index));
		}
		assertNull(book.getPage(-1));
		assertNull(book.getPage(index));
	}
}
