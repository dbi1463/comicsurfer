/* ZipPackedFolderBookSuiteTest.java created on 2012/12/15
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import tw.funymph.comicsurfer.core.ComicBook;
import tw.funymph.commons.zip.UTF8ZipFileFactory;
import tw.funymph.commons.zip.ZipFileFactory;

/** The test cases for {@link ZipPackedFolderBookSuite}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class ZipPackedFolderBookSuiteTest {

	private ZipPackedFolderBookSuite testee;

	@Before
	public void setUp() throws Exception {
		File file = new File("data/zip-folder/zip-folder.zip");
		ZipFileFactory factory = new UTF8ZipFileFactory();
		testee = new ZipPackedFolderBookSuite(factory.getZipFile(file), file.getName(), file.toURI());
		assertEquals(0, testee.getComicBookCount());
	}

	@After
	public void tearDown() {
		testee.close();
	}

	@Test
	public void testOpen() {
		testee.open();
		int bookCount = testee.getComicBookCount();
		assertEquals(2, bookCount);

		int index = 0;
		int[] pageCounts = new int[] {4, 3};
		for(; index < bookCount; index++) {
			ComicBook book = testee.getComicBook(index);
			assertNotNull(book);
			book.open();
			assertEquals(pageCounts[index], book.getPageCount());
			book.close();
		}
		assertNull(testee.getComicBook(-1));
		assertNull(testee.getComicBook(index));
	}

	@Test
	public void testClose() {
		testee.open();
		assertEquals(2, testee.getComicBookCount());

		testee.close();
		assertEquals(0, testee.getComicBookCount());
	}

	@Test
	public void testGetInformation() {
		assertEquals("zip-folder.zip", testee.getSuiteName());
		assertEquals(new File("data/zip-folder/zip-folder.zip").toURI(), testee.getSuitePath());
	}
}
