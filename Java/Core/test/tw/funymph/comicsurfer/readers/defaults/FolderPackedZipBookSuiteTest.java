/* FolderPackedZipBookSuiteTest.java created on 2012/12/15
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import tw.funymph.comicsurfer.core.ComicBook;
import tw.funymph.commons.zip.UTF8ZipFileFactory;
import tw.funymph.commons.zip.ZipFileFactory;

/** The test cases for {@link FolderPackedZipBookSuite}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class FolderPackedZipBookSuiteTest {

	private FolderPackedZipBookSuite testee;

	@Before
	public void setUp() throws Exception {
		ZipFileFactory factory = new UTF8ZipFileFactory();
		testee = new FolderPackedZipBookSuite(new File("data/folder-zip"), factory);
		assertEquals(0, testee.getComicBookCount());
	}

	@After
	public void tearDown() {
		testee.close();
	}

	@Test
	public void testOpen() {
		testee.open();
		int bookCount = testee.getComicBookCount();
		assertEquals(2, bookCount);

		int index = 0;
		int[] pageCounts = new int[] {5, 4};
		for(; index < bookCount; index++) {
			ComicBook book = testee.getComicBook(index);
			assertNotNull(book);
			book.open();
			assertEquals(pageCounts[index], book.getPageCount());
			book.close();
		}
		assertNull(testee.getComicBook(-1));
		assertNull(testee.getComicBook(index));
	}

	@Test
	public void testClose() {
		testee.open();
		assertEquals(2, testee.getComicBookCount());

		testee.close();
		assertEquals(0, testee.getComicBookCount());
	}

	@Test
	public void testGetInformation() {
		assertEquals("folder-zip", testee.getSuiteName());
		assertEquals(new File("data/folder-zip").toURI(), testee.getSuitePath());
	}
}
