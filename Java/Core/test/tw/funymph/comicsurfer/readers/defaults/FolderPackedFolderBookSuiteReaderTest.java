/* FolderPackedFolderBookSuiteReader.java created on 2012/12/15
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static org.junit.Assert.*;

import java.io.File;
import java.net.URI;

import org.junit.Before;
import org.junit.Test;

import tw.funymph.commons.io.IOConstants;

/** The test cases for {@link FolderPackedFolderBookSuiteReader}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class FolderPackedFolderBookSuiteReaderTest {

	private File correctTarget;
	private File incorrectTarget;
	private URI unsupportTarget;
	private FolderPackedFolderBookSuiteReader testee;

	@Before
	public void setUp() throws Exception {
		correctTarget = new File("data/folder-folder");
		incorrectTarget = new File("data/folder");
		unsupportTarget = new URI("http://somewhere.com/abc");
		testee = new FolderPackedFolderBookSuiteReader();
	}

	@Test
	public void testCanRead() {
		assertTrue(testee.canRead(correctTarget.toURI()));
		assertFalse(testee.canRead(incorrectTarget.toURI()));
		assertFalse(testee.canRead(unsupportTarget));
		assertFalse(testee.canRead(new File("data/folder/page1.png").toURI()));
	}

	@Test
	public void testRead() {
		assertNotNull(testee.read(correctTarget.toURI()));
		assertNull(testee.read(incorrectTarget.toURI()));
		assertNull(testee.read(unsupportTarget));
	}

	@Test
	public void testGetInformation() {
		assertNull(testee.getExtension());
		assertTrue(testee.isFolderReader());
		assertEquals(FolderPackedFolderBookSuiteReader.DESCRIPTION, testee.getDescription());
		assertEquals(1, testee.getSupportedSchemes().length);
		assertEquals(IOConstants.URI_SCHEME_FILE, testee.getSupportedSchemes()[0]);
	}
}
