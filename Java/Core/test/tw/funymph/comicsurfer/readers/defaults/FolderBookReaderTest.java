/* FolderBookReaderTest.java created on 2012/12/15
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.URI;

import org.junit.Before;
import org.junit.Test;

import tw.funymph.commons.io.IOConstants;

/** The test cases for {@link FolderBookReader}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class FolderBookReaderTest {

	private File correctTarget;
	private File incorrectTarget;
	private URI unsupportTarget;
	private FolderBookReader testee;

	@Before
	public void setUp() throws Exception {
		correctTarget = new File("data/folder");
		incorrectTarget = new File("data/folder-folder");
		unsupportTarget = new URI("http://somewhere.com/abc");
		testee = new FolderBookReader();
	}

	@Test
	public void testGetInformation() {
		assertNull(testee.getExtension());
		assertTrue(testee.isFolderReader());
		assertEquals(FolderBookReader.DESCRIPTION, testee.getDescription());
		assertEquals(1, testee.getSupportedSchemes().length);
		assertEquals(IOConstants.URI_SCHEME_FILE, testee.getSupportedSchemes()[0]);
	}

	@Test
	public void testCanRead() {
		assertTrue(testee.canRead(correctTarget.toURI()));
		assertFalse(testee.canRead(incorrectTarget.toURI()));
		assertFalse(testee.canRead(unsupportTarget));
	}

	@Test
	public void testRead() {
		assertNotNull(testee.read(correctTarget.toURI()));
		assertNull(testee.read(incorrectTarget.toURI()));
		assertNull(testee.read(unsupportTarget));
	}
}
