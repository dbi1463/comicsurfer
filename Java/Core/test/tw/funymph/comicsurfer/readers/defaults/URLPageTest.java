/* URLPageTest.java created on 2012/12/15
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static tw.funymph.commons.io.IOCloser.close;

import java.io.File;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Test;

/** The test cases for {@link URLPage}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class URLPageTest {

	@Test
	public void testGetContent() throws MalformedURLException {
		URLPage page = new URLPage(new File("data/folder/page1.png").toURI().toURL());
		InputStream stream1 = null;
		InputStream stream2 = null;
		assertNotNull((stream1 = page.getContent()));
		assertNotNull((stream2 = page.getContent()));
		assertFalse(stream1 == stream2);
		close(stream1);
		close(stream2);

		page = new URLPage(new File("data/folder/page.png").toURI().toURL());
		assertNull(page.getContent());
	}

	@Test
	public void testClose() {
		URL url = null;
		try {
			url = new File("data/folder/page1.png").toURI().toURL();
		} catch (MalformedURLException e) {
			fail("It should not throw MalformedURLException.");
		}
		URLPage page = new URLPage(url);
		InputStream stream = null;
		assertNotNull((stream = page.getContent()));
		close(stream);

		page.close();
		assertNull(page.getContent());
	}
}
