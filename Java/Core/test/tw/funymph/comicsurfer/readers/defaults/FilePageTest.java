/* FilePageTest.java created on 2012/12/15
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static tw.funymph.commons.io.IOCloser.close;

import java.io.File;
import java.io.InputStream;

import org.junit.Test;

/** The test cases for {@link FilePage}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class FilePageTest {

	@Test
	public void testGetContent() {
		FilePage page = new FilePage(new File("data/folder/page1.png"));
		InputStream stream1 = null;
		InputStream stream2 = null;
		assertNotNull((stream1 = page.getContent()));
		assertNotNull((stream2 = page.getContent()));
		assertFalse(stream1 == stream2);
		close(stream1);
		close(stream2);

		page = new FilePage(new File("non-existing.png"));
		assertNull(page.getContent());
	}

	@Test
	public void testClose() {
		FilePage page = new FilePage(new File("data/folder/page1.png"));
		InputStream stream = null;
		assertNotNull((stream = page.getContent()));
		close(stream);

		page.close();
		assertNull(page.getContent());
	}
}
