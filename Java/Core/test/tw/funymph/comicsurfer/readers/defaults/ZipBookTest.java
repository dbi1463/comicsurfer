/* ZipBookTest.java created on 2012/12/15
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import tw.funymph.commons.zip.UTF8ZipFileFactory;
import tw.funymph.commons.zip.ZipFileFactory;

/** The test cases for {@link ZipBook}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class ZipBookTest {

	private ZipFileFactory factory;

	@Before
	public void setUp() throws Exception {
		factory = new UTF8ZipFileFactory();
	}

	@Test
	public void testOpen() {
		ZipBook book = new ZipBook(factory.getZipFile(new File("data/zip/book.zip")));
		assertEquals(0, book.getPageCount());

		book.open();
		assertEquals(7, book.getPageCount());

		// Re-open without closing
		book.open();
		assertEquals(7, book.getPageCount());

		book.close();
		assertEquals(0, book.getPageCount());

		book = new ZipBook(null);
		book.open();
		assertEquals(0, book.getPageCount());
		book.close();
	}

	@Test
	public void testGetPage() {
		ZipBook book = new ZipBook(factory.getZipFile(new File("data/zip/book.zip")));
		book.open();
		int pageCount = book.getPageCount();
		assertEquals(7, pageCount);

		int index = 0;
		for(; index < pageCount; index++) {
			assertNotNull(book.getPage(index));
		}
		assertNull(book.getPage(-1));
		assertNull(book.getPage(index));
	}
}
