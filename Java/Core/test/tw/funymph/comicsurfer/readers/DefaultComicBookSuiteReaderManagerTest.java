/* DefaultComicBookSuiteReaderManagerTest.java created on 2012/12/18
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.readers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;

import tw.funymph.comicsurfer.readers.defaults.FolderBookReader;
import tw.funymph.comicsurfer.readers.defaults.FolderPackedFolderBookSuite;
import tw.funymph.comicsurfer.readers.defaults.FolderPackedFolderBookSuiteReader;
import tw.funymph.comicsurfer.readers.defaults.FolderPackedZipBookSuiteReader;
import tw.funymph.comicsurfer.readers.defaults.ZipBookReader;
import tw.funymph.comicsurfer.readers.defaults.ZipPackedFolderBookSuite;
import tw.funymph.comicsurfer.readers.defaults.ZipPackedFolderBookSuiteReader;
import tw.funymph.comicsurfer.readers.defaults.ZipPackedZipBookSuite;
import tw.funymph.comicsurfer.readers.defaults.ZipPackedZipBookSuiteReader;

/** The test cases for {@link DefaultComicBookSuiteReaderManager}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class DefaultComicBookSuiteReaderManagerTest {

	private DefaultComicBookSuiteReaderManager testee;

	@Before
	public void setUp() throws Exception {
		testee = new DefaultComicBookSuiteReaderManager();
	}

	@Test
	public void testLoadThirdPartyReaders() throws URISyntaxException {
		assertNull(testee.getBestFitReader(new URI("http://www.8comic.com/html/197.html")));

		testee.loadThirdPartyReaders("data/no-existing");
		assertNull(testee.getBestFitReader(new URI("http://www.8comic.com/html/197.html")));

		testee.loadThirdPartyReaders("data/folder");
		assertNull(testee.getBestFitReader(new URI("http://www.8comic.com/html/197.html")));

		testee.loadThirdPartyReaders("data/readers");
		assertNotNull(testee.getBestFitReader(new URI("http://www.8comic.com/html/197.html")));

		// The manager will create the folder if it does not exist, thus
		// it needs to clear up the folder
		File file = new File("data/no-existing");
		if(file.exists()) {
			file.delete();
		}
	}

	@Test
	public void testRead() throws URISyntaxException {
		assertEquals(DefaultComicBookSuite.class, testee.read(new File("data/folder").toURI()).getClass());
		assertEquals(FolderPackedFolderBookSuite.class, testee.read(new File("data/folder-folder").toURI()).getClass());
		assertEquals(FolderPackedZipBookSuiteReader.class, testee.getBestFitReader(new File("data/folder-zip").toURI()).getClass());

		assertEquals(DefaultComicBookSuite.class, testee.read(new File("data/zip/book.zip").toURI()).getClass());
		assertEquals(ZipPackedFolderBookSuite.class, testee.read(new File("data/zip-folder/zip-folder.zip").toURI()).getClass());
		assertEquals(ZipPackedZipBookSuite.class, testee.read(new File("data/zip-zip/zip-zip.zip").toURI()).getClass());

		assertNull(testee.read(null));
		assertNull(testee.read(new URI("abc.com")));
		assertNull(testee.read(new URI("ftp://abc.com")));
		assertNull(testee.read(new File("data/folder-zip-no-images").toURI()));
		assertNull(testee.read(new URI("https://dl.dropbox.com/u/19418059/manual.cbsd")));
	}

	@Test
	public void testGetBestFitReader() throws URISyntaxException {
		assertEquals(FolderBookReader.class, testee.getBestFitReader(new File("data/folder").toURI()).getClass());
		assertEquals(FolderPackedFolderBookSuiteReader.class, testee.getBestFitReader(new File("data/folder-folder").toURI()).getClass());
		assertEquals(FolderPackedZipBookSuiteReader.class, testee.getBestFitReader(new File("data/folder-zip").toURI()).getClass());

		assertEquals(ZipBookReader.class, testee.getBestFitReader(new File("data/zip/book.zip").toURI()).getClass());
		assertEquals(ZipPackedFolderBookSuiteReader.class, testee.getBestFitReader(new File("data/zip-folder/zip-folder.zip").toURI()).getClass());
		assertEquals(ZipPackedZipBookSuiteReader.class, testee.getBestFitReader(new File("data/zip-zip/zip-zip.zip").toURI()).getClass());

		assertNull(testee.getBestFitReader(null));
		assertNull(testee.getBestFitReader(new URI("abc.com")));
		assertNull(testee.getBestFitReader(new URI("ftp://abc.com")));
		assertNull(testee.getBestFitReader(new File("data/folder-zip-no-images").toURI()));
		assertNull(testee.getBestFitReader(new URI("https://dl.dropbox.com/u/19418059/manual.cbsd")));
	}
}
