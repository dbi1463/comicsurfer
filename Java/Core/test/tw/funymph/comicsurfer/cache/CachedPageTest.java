/* CachedPageTest.java created on 2012/8/20
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.Assert.assertNull;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import tw.funymph.comicsurfer.core.SimplePage;
import tw.funymph.comicsurfer.utilities.ExceptionalInputStream;

/**
 * An unit test class that tests the functionalities of {@link CachedPage} class.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.5
 */
public class CachedPageTest {

	private byte[] testData;
	private CachedPage testee;
	private SimplePage mockPage;
	private SimplePage nullPage;
	private ByteArrayInputStream mockInputStream;

	@Before
	public void setUp() throws Exception {
		Random random = new Random();
		testData = new byte[(random.nextInt(2560) + 1)];
		for(int index = 0; index < testData.length; index++) {
			testData[index] = (byte)random.nextInt(128);
		}
		mockInputStream = new ByteArrayInputStream(testData);
		nullPage = new SimplePage(null, 1);
		mockPage = new SimplePage(mockInputStream);
		testee = new CachedPage(mockPage, 0);
		testee.open();
	}

	@Test
	public void testBasicInfomration() {
		CachedPage anotherCache = new CachedPage(nullPage, 0);
		assertEquals(Cacheable.CONTENT_TYPE_PAGE, testee.getContentType());
		assertEquals(0, testee.getIndex());
		assertEquals(1, anotherCache.getIndex());
	}

	@Test
	public void testIsReady() {
		assertFalse(testee.isReady());
		assertSuccessCache();
		assertTrue(testee.isReady());
	}

	@Test
	public void testGetContent() {
		assertSuccessCache();
		assertTrue(testee.isReady());
		InputStream content = testee.getContent();
		byte read = 0;
		int index = 0;
		try {
			while((read = (byte)content.read()) != -1) {
				assertEquals(testData[index], read);
				index++;
			}
			assertEquals(testData.length, index);
		} catch (IOException e) {
			fail("IO Exception occurred.");
		}
	}

	@Test
	public void testClose() {
		assertSuccessCache();
		assertTrue(testee.isReady());
		testee.close();
		assertTrue(testee.isReady());
	}

	@Test
	public void testCacheContent() {
		assertSuccessCache();
		assertEquals(testData.length, testee.cache.length);
		assertTrue(testee.isReady());
		assertEquals(testData.length, testee.cache.length);
		for(int index = 0; index < testData.length; index++) {
			assertEquals(testData[index], testee.cache[index]);
		}
	}

	@Test
	public void testClearCache() {
		assertSuccessCache();
		testee.cache(null);
		assertTrue(testee.isReady());
		testee.clear();
		assertFalse(testee.isReady());
		testee.clear();
		assertFalse(testee.isReady());
	}

	@Test
	public void testCacheOnNullPage() {
		testee = new CachedPage(nullPage, 0);
		assertFalse(testee.isReady());
		assertNullExceptionOnCache();
		assertTrue(testee.isReady());
		assertNull(testee.getContent());
	}

	@Test
	public void testCacheTime() {
		long startTime = System.currentTimeMillis();
		assertSuccessCache();
		long elapsedTime = System.currentTimeMillis() - startTime;
		// The cache time should be less than 500 milliseconds.
		assertTrue((elapsedTime < 500));
	}

	@Test
	public void testIOExceptionOnCacheContent() {
		testee = new CachedPage(new SimplePage(new ExceptionalInputStream()), 0);
		assertFalse(testee.isReady());
		testee.cache(null);
		assertTrue(testee.isReady());
	}

	private void assertSuccessCache() {
		try {
			testee.cache(null);
		} catch (Exception e) {
			fail("No exception should occur");
		}
	}

	private void assertNullExceptionOnCache() {
		try {
			testee.cache(null);
		} catch (NullPointerException e) {
			
		} catch (Exception e) {
			fail("No other exceptions should occur");
		}
	}
}
