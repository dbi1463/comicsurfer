/* CacheTaskTest.java created on 2013/7/14
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.cache;

//import static org.junit.Assert.*;

import org.junit.Test;

import tw.funymph.comicsurfer.mock.MockCacheContext;

/**
 * The class tests the functionalities of {@link CacheTask}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public class CacheTaskTest {

	@Test
	public void testReportCacheFailed() {
		MockCacheContext context = new MockCacheContext();
		Cacheable page = new CachedPage(null, 0);
		CacheTask task = new FailedTask(page);
		context.addCacheTask(task);
	}

	/**
	 * The class always throw a runtime exception during {@link #performTask()}.
	 *
	 * @author Pin-Ying Tu
	 * @version 3.0
	 * @since 3.0
	 */
	private class FailedTask extends CacheTask {

		/**
		 * Construct a <code>FailedTask</code> instance.
		 * 
		 * @param content the context need cache
		 */
		public FailedTask(Cacheable content) {
			super(content);
		}

		@Override
		protected void performTask() {
			throw new RuntimeException("The error should be reported");
		}
	}	
}
