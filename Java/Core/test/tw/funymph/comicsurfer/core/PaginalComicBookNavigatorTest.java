/* PaginalComicBookNavigatorTest.java created on 2012/8/23
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import tw.funymph.comicsurfer.core.paginal.PaginalComicBookNavigator;
import tw.funymph.comicsurfer.mock.MockComicBookSuite;
import tw.funymph.comicsurfer.mock.MockLastViewedRecordInformer;
import tw.funymph.comicsurfer.mock.MockNavigationEventListener;

/**
 * An unit test class that tests the functionalities of {@link PaginalComicBookNavigatorTest} class.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.5
 */
public class PaginalComicBookNavigatorTest {

	private PaginalComicBookNavigator testee;

	private MockComicBookSuite books;
	private MockNavigationEventListener listener;
	private MockLastViewedRecordInformer informer;

	@Before
	public void setUp() throws Exception {
		informer = new MockLastViewedRecordInformer();
		testee = new PaginalComicBookNavigator(informer);
		listener = new MockNavigationEventListener();
		testee.addNavigationEventListener(listener);
		books = new MockComicBookSuite(new int[] {20, 35, 29, 36, 41});
		informer.addRecord(books.getSuiteName(), 1, 8, 0);
		listener.assertInitialValues();
	}

	@After
	public void dearDown() {
		testee.closeComicBook();
	}

	@Test
	public void testOpenComicBook() {
		testee.openComicBook(books);
		assertEquals(books.getSuiteName(), testee.getCurrentSuite().getSuiteName());
		assertTrue(listener.isSuiteOpened());
		assertFalse(listener.isSuiteClosed());
		assertTrue(listener.isPageChanged());
		assertTrue(listener.isVolumeChanged());
		assertEquals(1, listener.getVolumeIndex());
		assertEquals(8, listener.getPageIndex());
		assertEquals(35, listener.getPageCount());
		assertEquals(5, listener.getVolumeCount());
		assertTrue(testee.hasNextPage());
		assertTrue(testee.hasPreviousPage());
		assertTrue(testee.hasNextVolume());
		assertTrue(testee.hasPreviousVolume());

		// Open the same book again: the navigator should do nothing and all indices are not changed
		listener.reset();
		testee.openComicBook(books);
		listener.assertInitialValues();
		assertEquals(1, testee.getCurrentVolumeIndex());
		assertEquals(8, testee.getCurrentPageIndex());
		assertEquals(35, testee.getPageCount());
		assertEquals(5, testee.getVolumeCount());
		assertTrue(testee.hasNextPage());
		assertTrue(testee.hasPreviousPage());
		assertTrue(testee.hasNextVolume());
		assertTrue(testee.hasPreviousVolume());

		// Open another book
		MockComicBookSuite anotherSuite = new MockComicBookSuite("Another Suite", "Somewhereelse", new int[] {10, 20});
		listener.reset();
		testee.openComicBook(anotherSuite);
		assertTrue(listener.isSuiteOpened());
		assertTrue(listener.isSuiteClosed());
		assertTrue(listener.isPageChanged());
		assertTrue(listener.isVolumeChanged());
		assertEquals(0, testee.getCurrentVolumeIndex());
		assertEquals(0, testee.getCurrentPageIndex());

		// Open a null suite when a book is still opened
		listener.reset();
		testee.openComicBook(null);
		assertFalse(listener.isSuiteOpened());
		assertFalse(listener.isSuiteClosed());
		assertFalse(listener.isPageChanged());
		assertFalse(listener.isVolumeChanged());
		assertTrue(listener.isOpenFailed());
		assertEquals(0, testee.getCurrentVolumeIndex());
		assertEquals(0, testee.getCurrentPageIndex());
	}

	@Test
	public void testCloseComicBook() {
		testee.openComicBook(books);
		listener.reset();
		testee.closeComicBook();
		assertEquals(-1, testee.getCurrentVolumeIndex());
		assertEquals(-1, testee.getCurrentPageIndex());
		assertEquals(0, testee.getPageCount());
		assertEquals(0, testee.getVolumeCount());
		assertFalse(testee.hasNextPage());
		assertFalse(testee.hasPreviousPage());
		assertFalse(testee.hasNextVolume());
		assertFalse(testee.hasPreviousVolume());
	}

	@Test
	public void testBack() {
		assertFalse(testee.canGoBack());
		testee.openComicBook(books);
		assertFalse(testee.canGoBack());
		testee.back();
		assertEquals(1, testee.getCurrentVolumeIndex());
		assertEquals(8, testee.getCurrentPageIndex());
		testee.nextVolume();
		assertEquals(2, testee.getCurrentVolumeIndex());
		assertEquals(0, testee.getCurrentPageIndex());
		assertTrue(testee.canGoBack());
		testee.back();
		assertEquals(1, testee.getCurrentVolumeIndex());
		assertEquals(8, testee.getCurrentPageIndex());
		testee.previousVolume(false);
		assertEquals(0, testee.getCurrentVolumeIndex());
		assertEquals(0, testee.getCurrentPageIndex());
		testee.back();
		assertEquals(1, testee.getCurrentVolumeIndex());
		assertEquals(8, testee.getCurrentPageIndex());
	}

	@Test
	public void testNextPage() {
		testee.openComicBook(books);
		testee.nextPage();
		assertEquals(1, testee.getCurrentVolumeIndex());
		assertEquals(9, testee.getCurrentPageIndex());
		testee.jumpToPage(34);
		assertTrue(testee.hasNextPage());
		testee.nextPage();
		assertEquals(2, testee.getCurrentVolumeIndex());
		assertEquals(0, testee.getCurrentPageIndex());
	}

	@Test
	public void testNextVolume() {
		testee.openComicBook(books);
		testee.nextVolume();
		assertEquals(2, testee.getCurrentVolumeIndex());
		assertEquals(0, testee.getCurrentPageIndex());
		assertTrue(testee.canGoBack());
		testee.jumpToVolume(4);
		assertEquals(4, testee.getCurrentVolumeIndex());
		assertEquals(0, testee.getCurrentPageIndex());
		assertFalse(testee.hasNextVolume());
		testee.nextVolume();
		assertEquals(4, testee.getCurrentVolumeIndex());
		assertEquals(0, testee.getCurrentPageIndex());
	}

	@Test
	public void testPreviousPage() {
		testee.openComicBook(books);
		assertTrue(testee.hasPreviousPage());
		testee.previousPage();
		assertEquals(1, testee.getCurrentVolumeIndex());
		assertEquals(7, testee.getCurrentPageIndex());
		assertTrue(testee.canGoBack());
		testee.jumpToPage(0);
		assertTrue(testee.hasPreviousPage());
		testee.previousPage();
		assertEquals(0, testee.getCurrentVolumeIndex());
		assertEquals(19, testee.getCurrentPageIndex());
	}

	@Test
	public void testPreviousVolume() {
		testee.openComicBook(books);
		testee.previousVolume(true);
		assertEquals(0, testee.getCurrentVolumeIndex());
		assertEquals(19, testee.getCurrentPageIndex());
		assertFalse(testee.hasPreviousVolume());
		testee.previousVolume(false);
		assertEquals(0, testee.getCurrentVolumeIndex());
		assertEquals(19, testee.getCurrentPageIndex());
	}

	@Test
	public void testCurrentPage() {
		assertNull(testee.currentPage());
		testee.openComicBook(books);
		assertNotNull(testee.currentPage());
	}

	@Test
	public void testJumpToPage() {
		listener.reset();
		testee.jumpToPage(5);
		assertFalse(listener.isPageChanged());

		testee.openComicBook(books);
		listener.reset();
		testee.jumpToPage(-1);
		assertFalse(listener.isPageChanged());

		listener.reset();
		testee.jumpToPage(40);
		assertFalse(listener.isPageChanged());

		listener.reset();
		testee.jumpToPage(8);
		assertFalse(listener.isPageChanged());

		listener.reset();
		testee.jumpToPage(9);
		assertTrue(listener.isPageChanged());
	}

	@Test
	public void testJumpToVolume() {
		listener.reset();
		testee.jumpToVolume(2);
		assertFalse(listener.isVolumeChanged());

		testee.openComicBook(books);

		listener.reset();
		testee.jumpToVolume(1);
		assertFalse(listener.isVolumeChanged());

		listener.reset();
		testee.jumpToVolume(-1);
		assertFalse(listener.isVolumeChanged());

		listener.reset();
		testee.jumpToVolume(10);
		assertFalse(listener.isVolumeChanged());

		listener.reset();
		testee.jumpToVolume(3);
		assertTrue(listener.isVolumeChanged());
	}

	@Test
	public void testRemoveNavigationEventListener() {
		testee.openComicBook(books);
		assertTrue(listener.isSuiteOpened());

		listener.reset();
		testee.removeNavigationEventListener(listener);
		testee.nextPage();
		assertFalse(listener.isPageChanged());
	}
}
