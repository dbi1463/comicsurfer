/* PaginalComicBookDisplayControllerTest.java created on 2012/12/2
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.core;

import static org.junit.Assert.*;
import static tw.funymph.comicsurfer.core.ImageDisplayModel.ACTUAL_IMAGE_SIZE;
import static tw.funymph.comicsurfer.core.ImageDisplayModel.FIT_WINDOW_SIZE;
import static tw.funymph.comicsurfer.core.MotionController.ACCESSORY_ACTION;
import static tw.funymph.comicsurfer.core.MotionController.PRIMARY_ACTION;

import org.junit.Before;
import org.junit.Test;

import tw.funymph.comicsurfer.core.paginal.PaginalComicBookDisplayController;
import tw.funymph.comicsurfer.mock.MockImageDisplayModelListener;
import tw.funymph.comicsurfer.mock.MockNavigator;
import tw.funymph.comicsurfer.mock.MockTrackMouseInformer;

/**
 * The test cases for {@link PaginalComicBookDisplayController}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class PaginalComicBookDisplayControllerTest {

	private MockNavigator navigator;
	private MockTrackMouseInformer informer;
	private PaginalComicBookDisplayController testee;
	private MockImageDisplayModelListener listener;

	@Before
	public void setUp() throws Exception {
		navigator = new MockNavigator();
		informer = new MockTrackMouseInformer();
		listener = new MockImageDisplayModelListener();
		testee = new PaginalComicBookDisplayController(navigator, informer);
		testee.addImageDisplayModelListener(listener);
		testee.setDisplayMode(ACTUAL_IMAGE_SIZE);
		testee.setImageSize(1600, 1200);
		testee.setWindowSize(800, 600);
	}

	@Test
	public void testMoved() {
		testee.moved(110, 120);
		assertEquals(0, testee.getMotionTrack().length);

		testee.moved(10, 20);
		assertEquals(-800, testee.getImageX());
		assertEquals(-600, testee.getImageY());
	}

	@Test
	public void testMotion() {
		listener.reset();
		testee.pressed(10, 20, PRIMARY_ACTION);
		assertEquals(0, testee.getMotionTrack().length);
		assertFalse(listener.isModelUpdated());

		testee.dragged(50, 60);
		assertEquals(0, testee.getMotionTrack().length);
		assertFalse(listener.isModelUpdated());

		testee.released(20, 30, ACCESSORY_ACTION);
		assertEquals(0, testee.getMotionTrack().length);
		assertFalse(listener.isModelUpdated());

		testee.pressed(10, 20, ACCESSORY_ACTION);
		assertEquals(1, testee.getMotionTrack().length);
		MotionTrackPoint point = testee.getMotionTrack()[0];
		assertEquals(10, point.getX());
		assertEquals(20, point.getY());
		assertTrue(listener.isModelUpdated());

		listener.reset();
		testee.dragged(15, 25);
		assertEquals(2, testee.getMotionTrack().length);
		assertTrue(listener.isModelUpdated());

		listener.reset();
		testee.released(50, 30, ACCESSORY_ACTION);
		assertEquals(0, testee.getMotionTrack().length);
		assertTrue(listener.isModelUpdated());

		navigator.reset();
		testee.pressed(30, 30, ACCESSORY_ACTION);
		testee.released(30, 30, ACCESSORY_ACTION);
		assertFalse(navigator.isNextPageInvoked());
		assertFalse(navigator.isNextVolumeInvoked());
		assertFalse(navigator.isPreviousPageInvoked());
		assertFalse(navigator.isPreviousVolumeInvoked());
	}

	@Test
	public void testNextPage() {
		navigator.reset();
		testee.pressed(20, 130, ACCESSORY_ACTION);
		testee.released(120, 100, ACCESSORY_ACTION);
		assertTrue(navigator.isNextPageInvoked());
		assertFalse(navigator.isNextVolumeInvoked());
		assertFalse(navigator.isPreviousPageInvoked());
		assertFalse(navigator.isPreviousVolumeInvoked());
	}

	@Test
	public void testNextVolume() {
		navigator.reset();
		testee.pressed(20, 30, ACCESSORY_ACTION);
		testee.released(30, 100, ACCESSORY_ACTION);
		assertFalse(navigator.isNextPageInvoked());
		assertTrue(navigator.isNextVolumeInvoked());
		assertFalse(navigator.isPreviousPageInvoked());
		assertFalse(navigator.isPreviousVolumeInvoked());
	}

	@Test
	public void testPreviousPage() {
		navigator.reset();
		testee.pressed(100, 30, ACCESSORY_ACTION);
		testee.released(20, 40, ACCESSORY_ACTION);
		assertFalse(navigator.isNextPageInvoked());
		assertFalse(navigator.isNextVolumeInvoked());
		assertTrue(navigator.isPreviousPageInvoked());
		assertFalse(navigator.isPreviousVolumeInvoked());
	}

	@Test
	public void testPreviousVolume() {
		navigator.reset();
		testee.pressed(100, 130, ACCESSORY_ACTION);
		testee.released(20, 30, ACCESSORY_ACTION);
		assertFalse(navigator.isNextPageInvoked());
		assertFalse(navigator.isNextVolumeInvoked());
		assertFalse(navigator.isPreviousPageInvoked());
		assertTrue(navigator.isPreviousVolumeInvoked());
	}

	@Test
	public void testSetDisplayMode() {
		listener.reset();
		testee.setDisplayMode(ACTUAL_IMAGE_SIZE);
		assertFalse(listener.isModelUpdated());

		listener.reset();
		testee.setDisplayMode(FIT_WINDOW_SIZE);
		assertTrue(listener.isModelUpdated());
		assertEquals(0.5, testee.getDisplayRatio(), 0.1d);
		assertEquals(FIT_WINDOW_SIZE, testee.getDisplayMode());

		// Image is 16:9, the screen is 4:3
		testee.setImageSize(1600, 900);
		listener.reset();
		testee.setDisplayMode(ImageDisplayModel.FIT_WINDOW_HEIGHT);
		assertTrue(listener.isModelUpdated());
		assertEquals(0.667, testee.getDisplayRatio(), 0.1);

		testee.setWindowSize(1024, 768);
		listener.reset();
		testee.setDisplayMode(ImageDisplayModel.FIT_WINDOW_WIDTH);
		assertTrue(listener.isModelUpdated());
		assertEquals(0.64, testee.getDisplayRatio(), 0.1);
	}

	@Test
	public void testMotionInReversedDirection() {
		informer.setTrackMouse(false);
		testee.moved(300, 400);
		assertEquals(-800, testee.getImageX());
		assertEquals(0, testee.getImageY());

		testee.moved(300, 500);
		assertEquals(-800, testee.getImageX());
		assertEquals(-600, testee.getImageY());

		testee.moved(200, 400);
		assertEquals(0, testee.getImageX());
		assertEquals(0, testee.getImageY());

		testee.moved(200, 500);
		assertEquals(0, testee.getImageX());
		assertEquals(-600, testee.getImageY());
	}

	@Test
	public void testOnlySurfX() {
		testee.setImageSize(900, 500);
		testee.moved(300, 400);
		assertEquals(-100, testee.getImageX());
		assertEquals(50, testee.getImageY());

		testee.moved(300, 500);
		assertEquals(-100, testee.getImageX());
		assertEquals(50, testee.getImageY());

		testee.moved(200, 400);
		assertEquals(-100, testee.getImageX());
		assertEquals(50, testee.getImageY());

		testee.moved(200, 500);
		assertEquals(-100, testee.getImageX());
		assertEquals(50, testee.getImageY());
	}

	@Test
	public void testNoNeedSurf() {
		informer.setTrackMouse(false);
		testee.setImageSize(640, 480);
		testee.moved(300, 400);
		assertEquals(80, testee.getImageX());
		assertEquals(60, testee.getImageY());

		testee.moved(300, 500);
		assertEquals(80, testee.getImageX());
		assertEquals(60, testee.getImageY());

		testee.moved(200, 400);
		assertEquals(80, testee.getImageX());
		assertEquals(60, testee.getImageY());

		testee.moved(200, 500);
		assertEquals(80, testee.getImageX());
		assertEquals(60, testee.getImageY());
	}

	@Test
	public void testOnlyNeedSurfY() {
		informer.setTrackMouse(false);
		testee.setImageSize(700, 650);
		testee.moved(300, 400);
		assertEquals(50, testee.getImageX());
		assertEquals(0, testee.getImageY());

		testee.moved(300, 450);
		assertEquals(50, testee.getImageX());
		assertEquals(-50, testee.getImageY());

		testee.moved(200, 400);
		assertEquals(50, testee.getImageX());
		assertEquals(0, testee.getImageY());

		testee.moved(200, 600);
		assertEquals(50, testee.getImageX());
		assertEquals(-50, testee.getImageY());
	}

	@Test
	public void testClicked() {
		listener.reset();
		navigator.reset();
		testee.clicked(33, 44, PRIMARY_ACTION, 1);
		assertFalse(listener.isModelUpdated());
		assertFalse(navigator.isNextPageInvoked());
		assertFalse(navigator.isNextVolumeInvoked());
		assertFalse(navigator.isPreviousPageInvoked());
		assertFalse(navigator.isPreviousVolumeInvoked());
	}

	@Test
	public void testRemoveImageDisplayModelListener() {
		listener.reset();
		testee.removeImageDisplayModelListener(listener);
		testee.setDisplayMode(FIT_WINDOW_SIZE);
		assertFalse(listener.isModelUpdated());
	}
}
