/* ComicBookSuiteComparatorTest.java created on 2012/12/4
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.core;

import static org.junit.Assert.*;

import org.junit.Test;

import tw.funymph.comicsurfer.core.ComicBookSuiteComparator;
import tw.funymph.comicsurfer.mock.MockComicBookSuite;

/**
 * The test cases for {@link ComicBookSuiteComparator}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class ComicBookSuiteComparatorTest {

	@Test
	public void testCompare() {
		ComicBookSuiteComparator testee = new ComicBookSuiteComparator();

		assertEquals(-1, testee.compare(null, null));
		assertEquals(-1, testee.compare(null, new MockComicBookSuite(new int[] {})));
		assertEquals(-1, testee.compare(new MockComicBookSuite(new int[] {}), null));

		assertEquals(-1, testee.compare(new MockComicBookSuite(null, "abc", new int[] {}), new MockComicBookSuite(null, "abc", new int[] {})));
		assertEquals(-1, testee.compare(new MockComicBookSuite(null, "abc", new int[] {}), new MockComicBookSuite("abc", "abc", new int[] {})));
		assertEquals(-1, testee.compare(new MockComicBookSuite("abc", "abc", new int[] {}), new MockComicBookSuite(null, "abc", new int[] {})));

		assertEquals(-1, testee.compare(new MockComicBookSuite("abc", null, new int[] {}), new MockComicBookSuite("abc", null, new int[] {})));
		assertEquals(-1, testee.compare(new MockComicBookSuite("abc", "abc", new int[] {}), new MockComicBookSuite("abc", null, new int[] {})));
		assertEquals(-1, testee.compare(new MockComicBookSuite("abc", null, new int[] {}), new MockComicBookSuite("abc", "abc", new int[] {})));

		assertEquals(0, testee.compare(new MockComicBookSuite(new int[] {}), new MockComicBookSuite(new int[] {})));
		assertTrue(testee.compare(new MockComicBookSuite("123", "123", new int[] {}), new MockComicBookSuite("abc", "abc", new int[] {})) < 0);
	}
}
