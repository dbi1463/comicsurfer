/* DefaultNotificationCenterTest.java created on 2013/7/21
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import tw.funymph.comicsurfer.mock.MockNotificationReceiver;

/**
 * The test cases for the {@link DefaultNotificationCenter}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public class DefaultNotificationCenterTest {

	@Test
	public void testGetInstance() {
		NotificationCenter notificationCenter = DefaultNotificationCenter.getInstance();
		assertNotNull(notificationCenter);
		assertEquals(notificationCenter, DefaultNotificationCenter.getInstance());
	}

	@Test
	public void testSubscribeNotification() {
		NotificationCenter notificationCenter = DefaultNotificationCenter.getInstance();
		MockNotificationReceiver receiver = new MockNotificationReceiver();
		notificationCenter.subscribe("abc", receiver);
		notificationCenter.publish(this, "abcd", null);
		receiver.assetNotNotified();
		notificationCenter.publish(this, "abc", null);
		receiver.assertNotifiedWithMessage("abc");

		// Subscribe the same receiver with the same message twice.
		receiver.reset();
		notificationCenter.subscribe("abc", receiver);
		notificationCenter.publish(this, "abc", null);
		receiver.assertNotifiedWithMessage("abc");
		assertEquals(1, receiver.getNotificationCount());
	}

	@Test
	public void testUnsubscribeNamedNotification() {
		NotificationCenter notificationCenter = DefaultNotificationCenter.getInstance();
		MockNotificationReceiver receiver = new MockNotificationReceiver();
		notificationCenter.subscribe("abc", receiver);
		notificationCenter.subscribe("abcd", receiver);
		// Assert all receiver are registered correctly
		notificationCenter.publish(this, "abcd", null);
		receiver.assertNotifiedWithMessage("abcd");
		receiver.reset();
		notificationCenter.publish(this, "abc", null);
		receiver.assertNotifiedWithMessage("abc");

		// Remove the receiver for "abc" message
		receiver.reset();
		notificationCenter.publish(this, "abc", null);
		receiver.assertNotifiedWithMessage("abc");

		// Remove the receiver for non-existing "xyz" message
		receiver.reset();
		notificationCenter.unsubscribe("xyz", receiver);
		notificationCenter.publish(this, "abc", null);
		receiver.assertNotifiedWithMessage("abc");

		receiver.reset();
		notificationCenter.unsubscribe("abc", receiver);
		notificationCenter.publish(this, "abc", null);
		receiver.assetNotNotified();
	}

	@Test
	public void testUnsubscribeNotification() {
		NotificationCenter notificationCenter = DefaultNotificationCenter.getInstance();
		MockNotificationReceiver receiver = new MockNotificationReceiver();
		notificationCenter.subscribe("abc", receiver);
		notificationCenter.subscribe("abcd", receiver);
		// Assert all receiver are registered correctly
		notificationCenter.publish(this, "abcd", null);
		receiver.assertNotifiedWithMessage("abcd");
		receiver.reset();
		notificationCenter.publish(this, "abc", null);
		receiver.assertNotifiedWithMessage("abc");

		receiver.reset();
		notificationCenter.unsubscribe(receiver);
		notificationCenter.publish(this, "abcd", null);
		receiver.assetNotNotified();
		notificationCenter.publish(this, "abcd", null);
		receiver.assetNotNotified();
	}

	@Test
	public void testPublishNotification() {
		NotificationCenter notificationCenter = DefaultNotificationCenter.getInstance();
		MockNotificationReceiver receiver = new MockNotificationReceiver();
		notificationCenter.subscribe("abc", receiver);
		Map<String, Object> data = new HashMap<String, Object>();
		long timestamp1 = System.currentTimeMillis();
		notificationCenter.publish(this, "abc", data);
		long timestamp2 = System.currentTimeMillis();

		receiver.assertNotifiedWithMessage("abc");
		Notification receivedNotification = receiver.getReceivedNotification();
		assertEquals(this, receivedNotification.getSender());
		assertEquals(data.size(), receivedNotification.getData().size());
		assertTrue(timestamp1 <= receivedNotification.getTimestamp());
		assertTrue(timestamp2 >= receivedNotification.getTimestamp());
	}
}
