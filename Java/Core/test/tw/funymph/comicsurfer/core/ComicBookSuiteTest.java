/* ComicBookSuiteTest.java created on Jan 8, 2012
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import tw.funymph.comicsurfer.mock.MockComicBook;
import tw.funymph.comicsurfer.readers.DefaultComicBookSuite;

/**
 * An unit test class that tests the functionalities of {@link ComicBookSuite} class.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.4
 */
public class ComicBookSuiteTest {

	private static final int BOOK1_PAGES = 8;
	private static final int BOOK2_PAGES = 9;

	private static final String NAME = "Mock Suite";
	private static final String PATH = "/path/to/suite";

	private MockComicBook book1;
	private MockComicBook book2;
	private DefaultComicBookSuite suite;

	@Before
	public void setUp() {
		book1 = new MockComicBook(BOOK1_PAGES);
		book2 = new MockComicBook(BOOK2_PAGES);
		suite = new DefaultComicBookSuite(NAME, new File(PATH).toURI());
		suite.addComicBook(book1);
		suite.addComicBook(book2);	
	}

	@Test
	public void testGetComicBookCount() {
		assertEquals(2, suite.getComicBookCount());
	}

	@Test
	public void testGetComicBook() {
		assertEquals(book1, suite.getComicBook(0));
		assertEquals(book2, suite.getComicBook(1));

		// Assert the non-existing book
		assertNull(suite.getComicBook(-1));
		assertNull(suite.getComicBook(3));
	}

	@Test
	public void testAddComicBook() {
		assertEquals(2, suite.getComicBookCount());
		MockComicBook book3 = new MockComicBook(10);
		suite.addComicBook(book3);
		assertEquals(3, suite.getComicBookCount());
	}

	@Test
	public void testGetSuiteName() {
		assertEquals(NAME, suite.getSuiteName());
	}

	@Test
	public void testGetSuitePath() {
		assertEquals(new File(PATH).toURI(), suite.getSuitePath());
	}

	@Test
	public void testClose() {
		assertEquals(2, suite.getComicBookCount());
		suite.close();
		assertEquals(0, suite.getComicBookCount());
	}
}
