/* ImageDisplayModelListenerListTest.java created on 2012/8/21
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.core.events;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import tw.funymph.comicsurfer.core.ImageDisplayModel;
import tw.funymph.comicsurfer.core.paginal.PaginalComicBookDisplayController;

/**
 * An unit test class that tests the functionalities of {@link ImageDisplayModelListenerList} class.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.5
 */
public class ImageDisplayModelListenerListTest implements ImageDisplayModelListener {

	private PaginalComicBookDisplayController mockEventSource;
	private ImageDisplayModelListenerList testee;

	private boolean notified = false;
	private ImageDisplayModel displayModel;

	@Before
	public void setUp() throws Exception {
		testee = new ImageDisplayModelListenerList(mockEventSource);
		testee.addImageDisplayModelListener(this);
	}

	@Test
	public void testAddImageDisplayModelListener() {
		assertEquals(1, testee.listeners.size());

		testee.addImageDisplayModelListener(this);
		assertEquals(1, testee.listeners.size());

		testee.addImageDisplayModelListener(null);
		assertEquals(1, testee.listeners.size());
	}

	@Test
	public void testRemoveImageDisplayModelListener() {
		assertEquals(1, testee.listeners.size());
		testee.removeImageDisplayModelListener(this);
		assertEquals(0, testee.listeners.size());
	}

	@Test
	public void testFireImageDisplayModelUpdatedEvent() {
		assertFalse(notified);
		assertNull(displayModel);
		testee.fireImageDisplayModelUpdatedEvent();
		assertTrue(notified);
		assertEquals(mockEventSource, displayModel);
	}

	@Override
	public void modelUpdated(ImageDisplayModel model) {
		notified = true;
		displayModel = model;
	}
}
