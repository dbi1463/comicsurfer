/* NavigationEventListenerListTest.java created on 2012/8/21
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.core.events;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import tw.funymph.comicsurfer.core.NavigationModel;
import tw.funymph.comicsurfer.core.paginal.PaginalComicBookNavigator;
import tw.funymph.comicsurfer.mock.MockNavigationEventListener;

/**
 * An unit test class that tests the functionalities of {@link NavigationEventListenerList} class.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.5
 */
public class NavigationEventListenerListTest {

	private NavigationModel navigator;
	private NavigationEventListenerList testee;
	private MockNavigationEventListener listener;

	@Before
	public void setUp() throws Exception {
		navigator = new PaginalComicBookNavigator(null);
		listener = new MockNavigationEventListener();
		testee = new NavigationEventListenerList(navigator);
		testee.addComicSurferEventListener(listener);
		listener.assertInitialValues();
	}

	@Test
	public void testAddComicSurferEventListener() {
		assertEquals(1, testee.listeners.size());

		testee.addComicSurferEventListener(null);
		assertEquals(1, testee.listeners.size());

		testee.addComicSurferEventListener(listener);
		assertEquals(1, testee.listeners.size());
	}

	@Test
	public void testRemoveComicSurferEventListener() {
		assertEquals(1, testee.listeners.size());

		testee.removeComicSurferEventListener(null);
		assertEquals(1, testee.listeners.size());

		testee.removeComicSurferEventListener(listener);
		assertEquals(0, testee.listeners.size());
	}

	@Test
	public void testFirePageChangedEvent() {
		testee.firePageChangedEvent();

		assertFalse(listener.isSuiteOpened());
		assertFalse(listener.isSuiteClosed());
		assertTrue(listener.isPageChanged());
		assertFalse(listener.isVolumeChanged());
	}

	@Test
	public void testFireSuiteOpenedEvent() {
		testee.fireSuiteOpenedEvent();

		assertTrue(listener.isSuiteOpened());
		assertFalse(listener.isSuiteClosed());
		assertFalse(listener.isPageChanged());
		assertFalse(listener.isVolumeChanged());
	}

	@Test
	public void testFireSuiteClosedEvent() {
		testee.fireSuiteClosedEvent();

		assertFalse(listener.isSuiteOpened());
		assertTrue(listener.isSuiteClosed());
		assertFalse(listener.isPageChanged());
		assertFalse(listener.isVolumeChanged());
	}

	@Test
	public void testFireVolumeChangedEvent() {
		testee.fireVolumeChangedEvent();

		assertFalse(listener.isSuiteOpened());
		assertFalse(listener.isSuiteClosed());
		assertFalse(listener.isPageChanged());
		assertTrue(listener.isVolumeChanged());
	}
}
