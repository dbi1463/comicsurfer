/* SimplePage.java created on 2012/8/19
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

import static java.lang.System.out;

import java.io.IOException;
import java.io.InputStream;

/**
 * A simple mock page.
 *
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.5
 */
public class SimplePage implements Page {

	private int pageIndex;
	private InputStream content;

	/**
	 * Construct <code>SimplePage</code> with the specified content input stream.
	 * 
	 * @param stream the specified content
	 */
	public SimplePage(InputStream stream) {
		this(stream, UNDEFINED_INDEX);
	}

	/**
	 * Construct <code>SimplePage</code> with the specified content input stream.
	 * 
	 * @param stream the specified content
	 */
	public SimplePage(InputStream stream, int index) {
		content = stream;
		pageIndex = index;
	}

	public boolean isReady() {
		return content != null;
	}

	@Override
	public InputStream getContent() {
		return content;
	}

	@Override
	public void open() { }

	@Override
	public void close() {
		try {
			if(content != null) {
				content.close();
			}
		} catch (IOException e) {
			out.println("[SimplePage] Error occurs on closing the input strea of the current page.");
		}
	}

	@Override
	public int getIndex() {
		return pageIndex;
	}
}
