/* SimplePageTest.java created on 2012/8/20
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.core;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;

import org.junit.Before;
import org.junit.Test;

import tw.funymph.comicsurfer.utilities.ExceptionalInputStream;

/** An unit test class that tests the functionalities of {@link SimplePage} class.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.5
 */
public class SimplePageTest {

	private SimplePage nullPage;
	private SimplePage simplePage;
	private SimplePage exceptionalPage;

	private ByteArrayInputStream dataStream;
	private ExceptionalInputStream exceptionalStream;

	@Before
	public void setUp() throws Exception {
		byte[] data = new byte[128];
		nullPage = new SimplePage(null);
		simplePage = new SimplePage(dataStream = new ByteArrayInputStream(data));
		exceptionalPage = new SimplePage(exceptionalStream = new ExceptionalInputStream());
	}

	@Test
	public void testIsReady() {
		assertFalse(nullPage.isReady());
		assertTrue(simplePage.isReady());
		assertTrue(exceptionalPage.isReady());
	}

	@Test
	public void testGetContent() {
		assertNull(nullPage.getContent());
		assertEquals(dataStream, simplePage.getContent());
	}

	@Test
	public void testClose() {
		exceptionalPage.close();
		assertTrue(exceptionalStream.isClosed());
	}
}
