/* LanguageChangeListenerList.java created on 2012/8/21
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.language;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import tw.funymph.comicsurfer.language.LanguageChangeListenerList;
import tw.funymph.comicsurfer.mock.MockLanguageChangeListener;
import tw.funymph.comicsurfer.mock.MockLazyLanguageManager;

/** An unit test class that tests the functionalities of {@link LanguageChangeListenerList} class.
 * 
 * @author Pin-Ying Tu
 * @version 1.5
 * @since 1.5
 */
public class LanguageChangeListenerListTest {

	private MockLazyLanguageManager manager;
	private MockLanguageChangeListener listener;

	private LanguageChangeListenerList testee;

	@Before
	public void setUp() throws Exception {
		manager = new MockLazyLanguageManager();
		listener = new MockLanguageChangeListener();
		testee = new LanguageChangeListenerList(manager);
		testee.addLanguageChangeListener(listener);
	}

	@Test
	public void testAddLanguageChangeListener() {
		assertEquals(1, testee.listeners.size());

		testee.addLanguageChangeListener(null);
		assertEquals(1, testee.listeners.size());

		testee.addLanguageChangeListener(listener);
		assertEquals(1, testee.listeners.size());

		testee.addLanguageChangeListener(new MockLanguageChangeListener());
		assertEquals(2, testee.listeners.size());
	}

	@Test
	public void testRemoveLanguageChangeListener() {
		assertEquals(1, testee.listeners.size());

		testee.removeLanguageChangeListener(null);
		assertEquals(1, testee.listeners.size());

		testee.removeLanguageChangeListener(new MockLanguageChangeListener());
		assertEquals(1, testee.listeners.size());

		testee.removeLanguageChangeListener(listener);
		assertEquals(0, testee.listeners.size());
	}

	@Test
	public void testFireLanguageChangedEvent() {
		MockLanguageChangeListener anotherListener = new MockLanguageChangeListener();
		assertEquals(0, listener.getNotifiedTimes());
		testee.fireLanguageChangedEvent();
		assertEquals(1, listener.getNotifiedTimes());

		testee.addLanguageChangeListener(anotherListener);
		testee.fireLanguageChangedEvent();
		assertEquals(2, listener.getNotifiedTimes());
		assertEquals(1, anotherListener.getNotifiedTimes());
	}
}
