/* LazyLanguageManagerTest.java created on Jan 8, 2012
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.language;

import org.junit.Before;
import org.junit.Test;

import tw.funymph.comicsurfer.mock.MockLanguageChangeListener;
import tw.funymph.comicsurfer.mock.MockLazyLanguageManager;

import static org.junit.Assert.assertEquals;
import static tw.funymph.comicsurfer.mock.MockLazyLanguageManager.*;

/** The test cases for the functionalities of {@link LazyLanguageManager} class.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.4
 */
public class LazyLanguageManagerTest {

	private MockLazyLanguageManager manager;
	private MockLanguageChangeListener listener;

	@Before
	public void setUp() {
		manager = new MockLazyLanguageManager();
		listener = new MockLanguageChangeListener();
		manager.addLanguageChangeListener(listener);
	}

	@Test
	public void testGetLocalizedText() {
		String VIEW = "View";
		assertEquals(ENGLISH_VALUE_1, manager.getLocalizedText(KEY1));
		assertEquals(ENGLISH_VALUE_2, manager.getLocalizedText(KEY2));
		assertEquals(ENGLISH_VALUE_3, manager.getLocalizedText(KEY3));
		assertEquals(VIEW, manager.getLocalizedText(VIEW));

		manager.setCurrentLanguage(LANGUAGE2);
		assertEquals(REVERSED_ENGLISH_VALUE_1, manager.getLocalizedText(KEY1));
		assertEquals(REVERSED_ENGLISH_VALUE_2, manager.getLocalizedText(KEY2));
		assertEquals(REVERSED_ENGLISH_VALUE_3, manager.getLocalizedText(KEY3));
		assertEquals(VIEW, manager.getLocalizedText(VIEW));
	}

	@Test
	public void testSetCurrentLanguage() {
		assertEquals(0, listener.getNotifiedTimes());
		assertEquals(LANGUAGE1, manager.getCurrentLanguage());

		// The same language name
		manager.setCurrentLanguage(LANGUAGE1);
		assertEquals(0, listener.getNotifiedTimes());
		assertEquals(LANGUAGE1, manager.getCurrentLanguage());

		// Change to the second language
		manager.setCurrentLanguage(LANGUAGE2);
		assertEquals(1, listener.getNotifiedTimes());
		assertEquals(LANGUAGE2, manager.getCurrentLanguage());

		// Try to change to a unexisting language
		manager.setCurrentLanguage("Chinese");
		assertEquals(1, listener.getNotifiedTimes());
		assertEquals(LANGUAGE2, manager.getCurrentLanguage());
	}

	@Test
	public void testAddLanguageChangeListener() {
		assertEquals(0, listener.getNotifiedTimes());
		manager.setCurrentLanguage(LANGUAGE2);
		assertEquals(LANGUAGE2, listener.getLanguageName());
		assertEquals(1, listener.getNotifiedTimes());
	}

	@Test
	public void testRemoveLanguageChangeListener() {
		assertEquals(0, listener.getNotifiedTimes());
		manager.setCurrentLanguage(MockLazyLanguageManager.LANGUAGE2);
		assertEquals(1, listener.getNotifiedTimes());
		manager.removeLanguageChangeListener(listener);
		manager.setCurrentLanguage(MockLazyLanguageManager.LANGUAGE1);
		assertEquals(1, listener.getNotifiedTimes());
	}
}
