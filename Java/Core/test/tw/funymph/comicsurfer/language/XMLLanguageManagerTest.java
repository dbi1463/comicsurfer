/* Test.java created on 2012/12/20
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.language;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/** The test cases for {@link XMLLanguageManager}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class XMLLanguageManagerTest {

	private XMLLanguageManager testee;

	@Before
	public void setUp() {
		testee = new XMLLanguageManager();
		testee.initialize("data");
		assertEquals(2, testee.getAvailableLanguages().size());
	}

	@Test
	public void testSetCurrentLanguage() {
		testee.setCurrentLanguage("English, United States");
		int index = 1;
		for(; index <= 3; index++) {
			String key = "Key" + index;
			String expected = "Key " + index + " Value";
			assertEquals(expected, testee.getLocalizedText(key));
		}
		String key = "Key" + index;
		assertEquals(key, testee.getLocalizedText(key));

		testee.setCurrentLanguage("正體中文, 台灣");
		for(index = 1; index <= 2; index++) {
			key = "Key" + index;
			String expected = "鍵值" + index;
			assertEquals(expected, testee.getLocalizedText(key));
		}
		assertEquals("Key 3 Value", testee.getLocalizedText("Key3"));
		assertEquals("Key4", testee.getLocalizedText("Key4"));

		testee.setCurrentLanguage("Unknown Language");
		for(index = 1; index <= 2; index++) {
			key = "Key" + index;
			String expected = "鍵值" + index;
			assertEquals(expected, testee.getLocalizedText(key));
		}
		assertEquals("Key 3 Value", testee.getLocalizedText("Key3"));
		assertEquals("Key4", testee.getLocalizedText("Key4"));
	}
}
