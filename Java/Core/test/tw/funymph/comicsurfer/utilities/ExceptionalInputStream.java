/* ExceptionalInputStream.java created on 2012/8/20
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.utilities;

import java.io.IOException;
import java.io.InputStream;

/** A class that always throws an IOException on reading.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.5
 */
public class ExceptionalInputStream extends InputStream {

	private boolean closed = false;

	public boolean isClosed() {
		return closed;
	}

	@Override
	public int read() throws IOException {
		throw new IOException();
	}

	@Override
	public int read(byte[] buffer) throws IOException {
		throw new IOException();
	}

	@Override
	public int read(byte[] buffer, int offet, int length) throws IOException {
		throw new IOException();
	}

	@Override
	public void close() throws IOException {
		closed = true;
		throw new IOException();
	}
}
