/* PreferencesListenerListTest.java created on 2012/8/21
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.preferences;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static tw.funymph.comicsurfer.preferences.Preferences.KEY_BACKGROUND_COLOR;
import static tw.funymph.comicsurfer.preferences.Preferences.KEY_DISPLAY_MODE;
import static tw.funymph.comicsurfer.preferences.Preferences.KEY_LANGUAGE;

import org.junit.Test;

import tw.funymph.comicsurfer.mock.MockPreferences;
import tw.funymph.comicsurfer.mock.MockPreferencesListener;

/**
 * The test cases for {@link PreferencesListenerList}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.5
 */
public class PreferencesListenerListTest {

	@Test
	public void testFirePreferencesChangedEvent() {
		// Initialize
		MockPreferencesListener listener = new MockPreferencesListener();
		PreferencesListenerList testee = new PreferencesListenerList(new MockPreferences());
		testee.addPreferencesListener(listener);
		testee.addPreferencesListener(null);

		testee.firePreferencesChangedEvent(KEY_BACKGROUND_COLOR);
		assertTrue(listener.isNotified());
		assertEquals(KEY_BACKGROUND_COLOR, listener.getChangedPreferenceKey());

		listener.reset();
		assertFalse(listener.isNotified());
		testee.addPreferencesListener(listener);
		testee.firePreferencesChangedEvent(KEY_DISPLAY_MODE);
		assertTrue(listener.isNotified());
		assertEquals(1, listener.getNotifiedCount());
		assertEquals(KEY_DISPLAY_MODE, listener.getChangedPreferenceKey());

		MockPreferencesListener anotherListener = new MockPreferencesListener();
		listener.reset();
		testee.removePreferencesListener(listener);
		testee.addPreferencesListener(anotherListener);
		testee.firePreferencesChangedEvent(KEY_LANGUAGE);
		assertTrue(anotherListener.isNotified());
		assertFalse(listener.isNotified());
	}
}
