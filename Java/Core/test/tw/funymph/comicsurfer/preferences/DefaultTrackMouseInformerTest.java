/* MockTrackMouseInformerTest.java created on 2012/12/15
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.preferences;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import tw.funymph.comicsurfer.mock.MockPreferences;

/** The test cases for {@link DefaultTrackMouseInformer}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class DefaultTrackMouseInformerTest {

	private MockPreferences preferences;
	private DefaultTrackMouseInformer testee;

	@Before
	public void setUp() throws Exception {
		preferences = new MockPreferences();
		preferences.setPreference(Preferences.KEY_TRACK_MOUSE, String.valueOf(true));
		testee = new DefaultTrackMouseInformer(preferences);
	}

	@Test
	public void testIsTrackMouse() {
		assertTrue(testee.isTrackMouse());
		preferences.setPreference(Preferences.KEY_TRACK_MOUSE, String.valueOf(false));
		assertFalse(testee.isTrackMouse());
	}
}
