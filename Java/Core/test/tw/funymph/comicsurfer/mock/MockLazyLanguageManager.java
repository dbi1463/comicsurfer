/* MockLanguageManager.java created on Jan 8, 2012
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.mock;

import java.util.Hashtable;
import java.util.Set;

import tw.funymph.comicsurfer.language.LazyLanguageManager;

/** The mock object for {@link LazyLanguageManager}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.4
 */
public class MockLazyLanguageManager extends LazyLanguageManager {

	public static final String LANGUAGE1 = "English";
	public static final String LANGUAGE2 = "ReversedEnglish";

	public static final String KEY1 = "Key1";
	public static final String KEY2 = "Key2";
	public static final String KEY3 = "Key3";

	public static final String ENGLISH_VALUE_1 = "File";
	public static final String ENGLISH_VALUE_2 = "Edit";
	public static final String ENGLISH_VALUE_3 = "Help";

	public static final String REVERSED_ENGLISH_VALUE_1 = "eliF";
	public static final String REVERSED_ENGLISH_VALUE_2 = "tidE";
	public static final String REVERSED_ENGLISH_VALUE_3 = "pleH";

	private Hashtable<String, String> englishTexts;
	private Hashtable<String, String> reversedEnglishTexts;

	private Hashtable<String, Hashtable<String, String>> languages;

	public MockLazyLanguageManager() {
		englishTexts = new Hashtable<String, String>();
		englishTexts.put(KEY1, ENGLISH_VALUE_1);
		englishTexts.put(KEY2, ENGLISH_VALUE_2);
		englishTexts.put(KEY3, ENGLISH_VALUE_3);

		reversedEnglishTexts = new Hashtable<String, String>();
		reversedEnglishTexts.put(KEY1, REVERSED_ENGLISH_VALUE_1);
		reversedEnglishTexts.put(KEY2, REVERSED_ENGLISH_VALUE_2);
		reversedEnglishTexts.put(KEY3, REVERSED_ENGLISH_VALUE_3);

		languages = new Hashtable<String, Hashtable<String, String>>();
		languages.put(LANGUAGE1, englishTexts);
		languages.put(LANGUAGE2, reversedEnglishTexts);

		currentLanguage = LANGUAGE1;
		texts.putAll(englishTexts);
	}

	@Override
	public boolean loadLanguage(String name) {
		if(languages.containsKey(name)) {
			texts.putAll(languages.get(name));
			return true;
		}
		return false;
	}

	@Override
	public Set<String> getAvailableLanguages() {
		return languages.keySet();
	}

	@Override
	public void initialize(String path) {}
}
