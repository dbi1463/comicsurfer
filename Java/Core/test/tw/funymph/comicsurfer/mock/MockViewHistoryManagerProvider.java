/* MockViewHistoryManagerProvider.java created on 2013/7/14
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.mock;

import tw.funymph.comicsurfer.history.ViewHistory;
import tw.funymph.comicsurfer.history.ViewHistoryManager;
import tw.funymph.comicsurfer.history.ViewHistoryManagerProvider;

/**
 * The class mock the {@link ViewHistoryManagerProvider} for testing.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public class MockViewHistoryManagerProvider implements ViewHistoryManagerProvider, ViewHistoryManager {

	private boolean historySaved;
	private boolean historyLoaded;
	private boolean useSingleton;

	private String historyPath;
	private ViewHistory updatedHistory;

	@Override
	public ViewHistoryManager getViewHistoryManager(boolean singleton) {
		reset();
		useSingleton = singleton;
		return this;
	}

	public void reset() {
		historyPath = null;
		updatedHistory = null;
		historySaved = false;
		historyLoaded = false;
		useSingleton = false;
	}

	@Override
	public void loadHistory(ViewHistory history) {
		historyLoaded = true;
		updatedHistory = history;
	}

	@Override
	public void saveHistory(ViewHistory history) {
		historySaved = true;
		updatedHistory = history;
	}

	@Override
	public void setPath(String path) {
		historyPath = path;
	}

	public String getHistoryPath() {
		return historyPath;
	}

	public boolean isHistorySaved() {
		return historySaved;
	}

	public boolean isHistoryLoaded() {
		return historyLoaded;
	}

	public boolean isSingletonUsed() {
		return useSingleton;
	}

	public ViewHistory getUpdatedHistory() {
		return updatedHistory;
	}
}
