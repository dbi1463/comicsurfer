/* MockNavigator.java created on 2012/12/2
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.mock;

import tw.funymph.comicsurfer.core.ComicBookSuite;
import tw.funymph.comicsurfer.core.Navigable;
import tw.funymph.comicsurfer.core.Page;
import tw.funymph.comicsurfer.core.events.NavigationEventListener;

/** The mock object for {@link Naivgable} that records which method
 * has been invoked.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class MockNavigator implements Navigable {

	private boolean nextPageInvoked;
	private boolean nextVolumeInvoked;
	private boolean previousPageInvoked;
	private boolean previousVolumeInvoked;
	private boolean previousVolumeWithLastPage;

	private int _jumpPage;
	private int _jumpVolume;

	public MockNavigator() {
		reset();
	}

	public void reset() {
		nextPageInvoked = false;
		nextVolumeInvoked = false;
		previousPageInvoked = false;
		previousVolumeInvoked = false;
		previousVolumeWithLastPage = false;
	}

	public boolean isNextPageInvoked() {
		return nextPageInvoked;
	}

	public boolean isNextVolumeInvoked() {
		return nextVolumeInvoked;
	}

	public boolean isPreviousPageInvoked() {
		return previousPageInvoked;
	}

	public boolean isPreviousVolumeInvoked() {
		return previousVolumeInvoked;
	}

	public boolean isPreviousVolumeWithLastPage() {
		return previousVolumeWithLastPage;
	}

	public int getJumpPageIndex() {
		return _jumpPage;
	}

	public int getJumpVolumeIndex() {
		return _jumpVolume;
	}

	@Override
	public void closeComicBook() {

	}

	@Override
	public void openComicBook(ComicBookSuite books) {

	}

	@Override
	public void back() {

	}

	@Override
	public void nextPage() {
		nextPageInvoked = true;
	}

	@Override
	public void nextVolume() {
		nextVolumeInvoked = true;
	}

	@Override
	public void previousPage() {
		previousPageInvoked = true;
	}

	@Override
	public void previousVolume(boolean gotoLastPage) {
		previousVolumeInvoked = true;
		previousVolumeWithLastPage = gotoLastPage;
	}

	@Override
	public Page currentPage() {
		return null;
	}

	@Override
	public void jumpToPage(int index) {
		_jumpPage = index;
	}

	@Override
	public void jumpToVolume(int index) {
		_jumpVolume = index;
	}

	@Override
	public void addNavigationEventListener(NavigationEventListener listener) {}

	@Override
	public void removeNavigationEventListener(NavigationEventListener listener) {}
}
