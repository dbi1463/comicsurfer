/* MockComicBook.java created on Jan 8, 2012
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.mock;

import java.io.ByteArrayInputStream;

import tw.funymph.comicsurfer.core.ComicBook;
import tw.funymph.comicsurfer.core.Page;
import tw.funymph.comicsurfer.core.SimplePage;

/**
 * The mock object of {@link ComicBook}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.4
 */
public class MockComicBook implements ComicBook {

	private static final int DEFAULT_BUFFER_SIZE = 64;

	private boolean opened;
	private Page[] pages;

	public MockComicBook(int pageCount) {
		opened = false;
		pages = new Page[pageCount];
		for(int i = 0; i < pageCount; i++) {
			pages[i] = new SimplePage(new ByteArrayInputStream(new byte[DEFAULT_BUFFER_SIZE]));
		}
	}

	public MockComicBook(Page[] mockPages) {
		pages = mockPages;
		opened = false;
	}

	@Override
	public void open() {
		opened = true;
	}

	@Override
	public void close() {
		opened = false;
	}

	@Override
	public int getPageCount() {
		return opened? pages.length : 0;
	}

	@Override
	public Page getPage(int index) {
		return opened? pages[index] : null;
	}

	public Page[] getPages() {
		return pages;
	}

	@Override
	public int getIndex() {
		return UNDEFINED_INDEX;
	}
}
