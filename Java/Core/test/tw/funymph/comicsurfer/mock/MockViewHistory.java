/* MockViewHistory.java created on 2012/9/26
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.mock;

import java.net.URI;
import java.util.List;

import tw.funymph.comicsurfer.history.ViewHistory;
import tw.funymph.comicsurfer.history.ViewHistoryListener;
import tw.funymph.comicsurfer.history.ViewRecord;

/** The mock object for {@link ViewHistory}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class MockViewHistory implements ViewHistory {

	private String name;
	private URI path;

	private int page;
	private int volume;
	private int count;
	private int block;

	public MockViewHistory() {
		count = 0;
	}

	public MockViewHistory(String suiteName, URI suiteURI, int volumeIndex, int pageIndex) {
		path = suiteURI;
		count = 1;
		name = suiteName;
		page = pageIndex;
		volume = volumeIndex;
	}

	@Override
	public int getRecordCount() {
		return count;
	}

	@Override
	public ViewRecord getRecord(String suiteName) {
		return suiteName.equals(name)? new ViewRecord(name, path, volume, page, block) : null;
	}

	@Override
	public List<ViewRecord> getRecords() {
		return null;
	}

	@Override
	public void setLastViewRecord(String suiteName, URI suitePath, int set, int media, int position) {
		count += ((name != null && name.equals(suiteName))? 0 : 1);
		name = suiteName;
		path = suitePath;
		page = media;
		volume = set;
		block = position;
	}

	@Override
	public void setMaximumRecordCapacity(int capacity) {}

	@Override
	public void addViewHistoryListener(ViewHistoryListener listener) {}

	@Override
	public void removeViewHistoryListener(ViewHistoryListener listener) {}

	@Override
	public ViewRecord getLastRecord() {
		return null;
	}

	@Override
	public int getMaximumRecordCapacity() {
		return 0;
	}

	@Override
	public ViewRecord getRecord(int index) {
		return null;
	}
}
