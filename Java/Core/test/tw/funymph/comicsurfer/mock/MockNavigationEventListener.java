/* MockNavigationEventListener.java created on 2012/8/23
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.mock;

import static org.junit.Assert.assertFalse;

import tw.funymph.comicsurfer.core.events.NavigationEvent;
import tw.funymph.comicsurfer.core.events.NavigationEventListener;

/** The mock object for {@link NavigationEventListener}.
 * 
 * @author Pin-Ying Tu
 * @version 1.5
 * @since 1.5
 */
public class MockNavigationEventListener implements NavigationEventListener {

	private int pageIndex;
	private int pageCount;
	private int volumeIndex;
	private int volumeCount;

	private boolean suiteClosed;
	private boolean suiteOpened;
	private boolean pageChanged;
	private boolean suiteOpening;
	private boolean volumeChanged;
	private boolean volumeChanging;
	private boolean openFailed;
	private boolean suiteClosing;

	public MockNavigationEventListener() {
		reset();
	}

	public void reset() {
		suiteClosed = false;
		suiteOpened = false;
		pageChanged = false;
		suiteOpening = false;
		volumeChanged = false;
		volumeChanging = false;

		pageIndex = -1;
		pageCount = -1;
		volumeIndex = -1;
		volumeCount = -1;
	}

	public boolean isSuiteClosed() {
		return suiteClosed;
	}

	public boolean isSuiteOpened() {
		return suiteOpened;
	}

	public boolean isPageChanged() {
		return pageChanged;
	}

	public boolean isVolumeChanged() {
		return volumeChanged;
	}

	public boolean isVolumeChanging() {
		return volumeChanging;
	}

	public boolean isOpenFailed() {
		return openFailed;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public int getPageCount() {
		return pageCount;
	}

	public int getVolumeIndex() {
		return volumeIndex;
	}

	public int getVolumeCount() {
		return volumeCount;
	}

	public boolean isSuiteClosing() {
		return suiteClosing;
	}

	public void assertInitialValues() {
		assertFalse(suiteOpened);
		assertFalse(suiteClosed);
		assertFalse(pageChanged);
		assertFalse(suiteOpening);
		assertFalse(volumeChanged);
		assertFalse(volumeChanging);
	}

	@Override
	public void pageChanged(NavigationEvent event) {
		pageChanged = true;
		updateIndexAndCount(event);
	}

	@Override
	public void volumeChanged(NavigationEvent event) {
		volumeChanged = true;
		updateIndexAndCount(event);
	}

	@Override
	public void suiteClosed(NavigationEvent event) {
		suiteClosed = true;
		updateIndexAndCount(event);
	}

	@Override
	public void suiteOpening(NavigationEvent event) {
		suiteOpening = true;
		updateIndexAndCount(event);
	}

	@Override
	public void suiteOpened(NavigationEvent event) {
		suiteOpened = true;
		updateIndexAndCount(event);
	}

	@Override
	public void volumeChanging(NavigationEvent event) {
		volumeChanging = true;
		updateIndexAndCount(event);
	}

	@Override
	public void suiteClosing(NavigationEvent event) {
		suiteClosing = true;
		updateIndexAndCount(event);
	}

	@Override
	public void suiteOpenFailed() {
		openFailed = true;
	}

	private void updateIndexAndCount(NavigationEvent event) {
		pageCount = event.getPageCount();
		pageIndex = event.getCurrentPageIndex();
		volumeCount = event.getVolumeCount();
		volumeIndex = event.getCurrentVolumeIndex();
	}
}
