/* MockLastViewedRecordInformer.java created on 2012/12/1
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.mock;

import java.util.Hashtable;
import java.util.Map;

import tw.funymph.comicsurfer.core.LastViewedComicBookInformer;

/** The mock object for {@link LastViewRecordInformer}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class MockLastViewedRecordInformer implements LastViewedComicBookInformer {

	private Map<String, Integer> volumes;
	private Map<String, Integer> pages;
	private Map<String, Integer> positions;

	public MockLastViewedRecordInformer() {
		volumes = new Hashtable<String, Integer>();
		pages = new Hashtable<String, Integer>();
		positions = new Hashtable<String, Integer>();
	}

	public void addRecord(String name, int volume, int page, int position) {
		volumes.put(name, volume);
		pages.put(name, page);
		positions.put(name, position);
	}

	@Override
	public int getLastViewedVolume(String name) {
		return volumes.containsKey(name)? volumes.get(name) : 0;
	}

	@Override
	public int getLastViewedPage(String name) {
		return pages.containsKey(name)? pages.get(name) : 0;
	}

	@Override
	public int getLastViewedBlock(String name) {
		return positions.containsKey(name)? positions.get(name) : 0;
	}
}
