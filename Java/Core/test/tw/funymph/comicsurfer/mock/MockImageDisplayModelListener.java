/* MockImageDisplayModelListener.java created on 2012/12/2
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.mock;

import tw.funymph.comicsurfer.core.ImageDisplayModel;
import tw.funymph.comicsurfer.core.events.ImageDisplayModelListener;

/** The mock object for {@link ImageDisplayModelListener}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class MockImageDisplayModelListener implements ImageDisplayModelListener {

	private boolean modelUpdated;
	private ImageDisplayModel eventSource;

	public MockImageDisplayModelListener() {
		reset();
	}

	public void reset() {
		eventSource = null;
		modelUpdated = false;
	}

	public boolean isModelUpdated() {
		return modelUpdated;
	}

	public ImageDisplayModel getEventSource() {
		return eventSource;
	}

	@Override
	public void modelUpdated(ImageDisplayModel model) {
		modelUpdated = true;
		eventSource = model;
	}
}
