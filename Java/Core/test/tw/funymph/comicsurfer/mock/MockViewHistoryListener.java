/* MockViewHistoryListener.java created on 2012/12/19
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.mock;

import static org.junit.Assert.*;

import tw.funymph.comicsurfer.history.ViewHistory;
import tw.funymph.comicsurfer.history.ViewHistoryListener;

/** The mock object for {@link ViewHistoryListener} that records whether
 * the listener has been notified or not.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class MockViewHistoryListener implements ViewHistoryListener {

	private boolean viewHistoryUpdated;
	private int updateCount;
	private ViewHistory updatedHistory;

	public MockViewHistoryListener() {
		reset();
	}

	public void assertInitialConditions() {
		assertNull(updatedHistory);
		assertEquals(0, updateCount);
		assertFalse(viewHistoryUpdated);
	}

	public void reset() {
		updateCount = 0;
		updatedHistory = null;
		viewHistoryUpdated = false;
	}

	public boolean isViewHistoryUpdated() {
		return viewHistoryUpdated;
	}

	public ViewHistory getUpdateViewHistory() {
		return updatedHistory;
	}

	public int getUpdateCount() {
		return updateCount;
	}

	@Override
	public void viewHistoryUpdated(ViewHistory history) {
		updateCount++;
		updatedHistory = history;
		viewHistoryUpdated = true;
	}
}
