/* MockLanguageChangeListener.java created on Jan 8, 2012
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.mock;

import tw.funymph.comicsurfer.language.LanguageChangeListener;
import tw.funymph.comicsurfer.language.LocalizedTextProvider;

/** The mock object for {@link LanguageChangeListner}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.4
 */
public class MockLanguageChangeListener implements LanguageChangeListener {

	private int notifiedTimes;
	private String lastLanguageName;

	public MockLanguageChangeListener() {
		notifiedTimes = 0;
	}

	@Override
	public void languageChanged(LocalizedTextProvider source) {
		notifiedTimes++;
		lastLanguageName = source.getCurrentLanguage();
	}

	public int getNotifiedTimes() {
		return notifiedTimes;
	}

	public String getLanguageName() {
		return lastLanguageName;
	}
}
