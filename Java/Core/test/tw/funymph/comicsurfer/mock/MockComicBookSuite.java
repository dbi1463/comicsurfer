/* MockComicBookSuite.java created on 2012/8/23
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.mock;

import java.net.URI;
import java.net.URISyntaxException;

import tw.funymph.comicsurfer.core.ComicBook;
import tw.funymph.comicsurfer.core.ComicBookSuite;

/** The mock object for {@link ComicBookSuite}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.5
 */
public class MockComicBookSuite implements ComicBookSuite {

	private boolean closed;

	private String name;
	private URI path;
	private MockComicBook[] bookes;

	public MockComicBookSuite(int[] pageCounts) {
		this("Mock Comic Book Suite", "file:/I:/MockSuite/", pageCounts);
	}

	public MockComicBookSuite(String suiteName, String suitePath, int[] pageCounts) {
		name = suiteName;
		try {
			path = (suitePath != null)? new URI(suitePath) : null;
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		bookes = new MockComicBook[pageCounts.length];
		for(int index = 0; index < pageCounts.length; index++) {
			bookes[index] = new MockComicBook(pageCounts[index]);
		}
	}

	@Override
	public int getComicBookCount() {
		return bookes.length;
	}

	@Override
	public ComicBook getComicBook(int index) {
		return bookes[index];
	}

	@Override
	public String getSuiteName() {
		return name;
	}

	@Override
	public URI getSuitePath() {
		return path;
	}

	@Override
	public void open() {
	}

	@Override
	public void close() {
		closed = true;
	}

	public boolean isClosed() {
		return closed;
	}
}
