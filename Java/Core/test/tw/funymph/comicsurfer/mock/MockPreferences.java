/* MockPreferences.java created on 2012/8/20
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.mock;

import tw.funymph.comicsurfer.preferences.Preferences;
import tw.funymph.comicsurfer.preferences.PreferencesListener;

/** A mock preferences for testing {@link Preferences}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.5
 */
public class MockPreferences implements Preferences {

	private String key;
	private String value;

	public MockPreferences() {
		reset();
	}

	public String getLastSetKey() {
		return key;
	}

	public String getLastSetValue() {
		return value;
	}

	public void reset() {
		key = null;
		value = null;
	}

	@Override
	public void setPreference(String newKey, String newValue) {
		key = newKey;
		value = newValue;
	}

	@Override
	public String getPreference(String key) {
		return value;
	}

	@Override
	public String[] getPreferenceKeys() {
		return new String[] { key };
	}

	@Override
	public void addPreferencesListener(PreferencesListener listener) {}

	@Override
	public void removePreferencesListener(PreferencesListener listener) {}
}
