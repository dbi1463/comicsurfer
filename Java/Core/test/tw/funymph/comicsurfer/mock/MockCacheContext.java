/* MockCacheContext.java created on 2013/7/14
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.mock;

import java.util.Map;

import tw.funymph.comicsurfer.cache.CacheContext;
import tw.funymph.comicsurfer.cache.CacheTask;

/**
 * The class mock the {@link CacheContext} for testing.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public class MockCacheContext implements CacheContext {

	private Map<String, Object> failedReport;

	@Override
	public void addCacheTask(CacheTask task) {
		task.setCacheContext(this);
		task.run();
	}

	@Override
	public void reportCacheFailed(Map<String, Object> report) {
		failedReport = report;
	}

	public Map<String, Object> getFailedReport() {
		return failedReport;
	}
}
