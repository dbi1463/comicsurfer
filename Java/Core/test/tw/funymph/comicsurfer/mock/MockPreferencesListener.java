/* MockPreferencesListener.java created on 2012/8/20
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.mock;

import tw.funymph.comicsurfer.preferences.Preferences;
import tw.funymph.comicsurfer.preferences.PreferencesListener;

/**
 * A mock preferences listener.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.5
 */
public class MockPreferencesListener implements PreferencesListener {

	private int notifiedCount;

	private String notifiedKey;
	private Preferences source;

	public MockPreferencesListener() {
		reset();
	}

	public void reset() {
		source = null;
		notifiedKey = null;
		notifiedCount = 0;
	}

	public boolean isNotified() {
		return notifiedCount > 0;
	}

	public int getNotifiedCount() {
		return notifiedCount;
	}

	public Preferences getEventSource() {
		return source;
	}

	public String getChangedPreferenceKey() {
		return notifiedKey;
	}

	@Override
	public void preferencesChanged(String key, Preferences preferences) {
		notifiedKey = key;
		notifiedCount++;
		source = preferences;
	}
}
