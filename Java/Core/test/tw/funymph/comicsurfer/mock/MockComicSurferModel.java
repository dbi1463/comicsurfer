/* MockComicSurferModel.java created on 2012/8/20
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.mock;

import tw.funymph.comicsurfer.ComicSurferModel;
import tw.funymph.comicsurfer.core.*;
import tw.funymph.comicsurfer.core.slide.SlideShowController;
import tw.funymph.comicsurfer.history.ViewHistory;
import tw.funymph.comicsurfer.language.*;
import tw.funymph.comicsurfer.preferences.Preferences;
import tw.funymph.comicsurfer.readers.ComicBookSuiteReaderManager;

/**
 * The mock object for {@link ComicSurferModel}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.5
 */
public class MockComicSurferModel implements ComicSurferModel {

	private ViewHistory history;
	private Preferences preferences;
	private ImageDisplayModel displayModel;
	private NavigationModel navigationModel;
	private LanguageManager languageManager;
	private MotionController motionController;

	public MockComicSurferModel() {
		languageManager = new MockLazyLanguageManager();
		history = new MockViewHistory();
	}

	public void setImageDisplayModel(ImageDisplayModel model) {
		displayModel = model;
	}

	public void setNavigationModel(NavigationModel model) {
		navigationModel = model;
	}

	public void setLanguageManager(LanguageManager manager) {
		languageManager = manager;
	}

	public void setMotionController(MotionController controller) {
		motionController = controller;
	}

	public void setPreferences(Preferences thePreferences) {
		preferences = thePreferences;
	}

	@Override
	public void initialize() {}

	@Override
	public void exit() {}

	@Override
	public NavigationModel getNavigator() {
		return navigationModel;
	}

	@Override
	public ImageDisplayModel getDisplayModel() {
		return displayModel;
	}

	@Override
	public MotionController getMotionController() {
		return motionController;
	}

	@Override
	public LanguageManager getLanguageManager() {
		return languageManager;
	}

	@Override
	public Preferences getPreferences() {
		return preferences;
	}

	@Override
	public ComicBookSuiteReaderManager getReaderManager() {
		return null;
	}

	@Override
	public ViewHistory getViewHistory() {
		return history;
	}

	@Override
	public SlideShowController getSlideShowController() {
		return null;
	}
}
