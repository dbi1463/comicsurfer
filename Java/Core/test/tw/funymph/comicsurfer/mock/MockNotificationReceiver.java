/* MockNotificationReceiver.java created on 2013/7/21
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.mock;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import tw.funymph.comicsurfer.core.Notification;
import tw.funymph.comicsurfer.core.NotificationReceiver;

/**
 * A mock object of {@link NotificationReceiver}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public class MockNotificationReceiver implements NotificationReceiver {

	private int notificationCount;
	private Notification receivedNotification;

	public MockNotificationReceiver() {
		reset();
	}

	@Override
	public void notificationReceived(Notification notification) {
		notificationCount++;
		receivedNotification = notification;
	}

	public void reset() {
		notificationCount = 0;
		receivedNotification = null;
	}

	public void assetNotNotified() {
		assertEquals(0, notificationCount);
		assertNull(receivedNotification);
	}

	public void assertNotifiedWithMessage(String message) {
		assertTrue(notificationCount > 0);
		assertEquals(message, receivedNotification.getMessage());
	}

	public Notification getReceivedNotification() {
		return receivedNotification;
	}

	public int getNotificationCount() {
		return notificationCount;
	}
}
