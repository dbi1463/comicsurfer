/* MockTrackMouseInformer.java created on 2012/12/2
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.mock;

import tw.funymph.comicsurfer.core.TrackMouseInformer;

/** The mock object for {@link TrackMouseInformer} that always return
 * the predefined value.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class MockTrackMouseInformer implements TrackMouseInformer {

	private boolean trackMouse;

	public MockTrackMouseInformer() {
		this(true);
	}

	public MockTrackMouseInformer(boolean trackMouse) {
		setTrackMouse(trackMouse);
	}

	public void setTrackMouse(boolean whetherTrackMouse) {
		trackMouse = whetherTrackMouse;
	}

	@Override
	public boolean isTrackMouse() {
		return trackMouse;
	}
}
