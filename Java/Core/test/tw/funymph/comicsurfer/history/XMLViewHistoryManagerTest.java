/* XMLViewHistoryManagerTest.java created on 2012/12/19
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.history;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Test;

/** The test cases for {@link XMLViewHistoryManager}.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class XMLViewHistoryManagerTest {

	@Test
	public void testLoadHistory() {
		RecentViewHistory history = new RecentViewHistory(2);
		XMLViewHistoryManager testee = new XMLViewHistoryManager();
		testee.loadHistory(history);
		assertEquals(0, history.getRecordCount());
		assertEquals(2, history.getMaximumRecordCapacity());

		history.setMaximumRecordCapacity(10);
		testee = new XMLViewHistoryManager("data/history.xml");
		testee.loadHistory(history);
		assertEquals(3, history.getRecordCount());

		ViewRecord record = history.getRecord("Photos");
		assertEquals(2, record.getMediaSetIndex());
		assertEquals(0, record.getMediaIndex());
		assertEquals("file:/D:/Dropbox/Photos/", history.getRecord("Photos").getURI().toString());
	}

	@Test
	public void testLoadOldVersionHistory() {
		RecentViewHistory history = new RecentViewHistory(2);
		XMLViewHistoryManager testee = new XMLViewHistoryManager();
		testee.loadHistory(history);
		assertEquals(0, history.getRecordCount());
		assertEquals(2, history.getMaximumRecordCapacity());

		testee = new XMLViewHistoryManager("data/old-history.xml");
		history.setMaximumRecordCapacity(10);
		testee.loadHistory(history);
		assertEquals(3, history.getRecordCount());

		ViewRecord record = history.getRecord("Photos");
		assertEquals(2, record.getMediaSetIndex());
		assertEquals(0, record.getMediaIndex());
		assertEquals("file:/D:/Dropbox/Photos/", history.getRecord("Photos").getURI().toString());
	}

	@Test
	public void testSaveHistory() throws URISyntaxException {
		RecentViewHistory history1 = new RecentViewHistory(2);
		history1.setLastViewRecord("abc", new URI("abc"), 2, 4, 0);
		String filename = "bin/needDelete.xml";
		File file = new File(filename);
		if(file.exists()) {
			file.deleteOnExit();
		}
		XMLViewHistoryManager testee = new XMLViewHistoryManager(filename);
		testee.saveHistory(history1);

		RecentViewHistory history2 = new RecentViewHistory(2);
		testee.loadHistory(history2);
		int count = history1.getRecordCount();
		assertEquals(count, history2.getRecordCount());
		for(int index = 0; index < count; index++) {
			ViewRecord record1 = history1.getRecord(index);
			ViewRecord record2 = history2.getRecord(index);
			assertEquals(record1.getURI(), record2.getURI());
			assertEquals(record1.getName(), record2.getName());
			assertEquals(record1.getMediaIndex(), record2.getMediaIndex());
			assertEquals(record1.getMediaSetIndex(), record2.getMediaSetIndex());
		}
	}
}
