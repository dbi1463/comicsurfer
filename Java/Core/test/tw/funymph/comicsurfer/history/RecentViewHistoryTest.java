/* RecentViewHistoryTest.java created on 2012/12/19
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.history;

import static org.junit.Assert.*;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;

import tw.funymph.comicsurfer.mock.MockViewHistoryListener;

/** The test cases for {@link RecentViewHistory}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.5
 */
public class RecentViewHistoryTest {

	private RecentViewHistory testee;
	private MockViewHistoryListener listener;

	@Before
	public void setUp() throws Exception {
		testee = new RecentViewHistory(3);
		assertEquals(3, testee.getMaximumRecordCapacity());
	}

	@Test
	public void testSetMaximumRecordCapacity() throws URISyntaxException {
		assertEquals(0, testee.getRecordCount());
		assertNull(testee.getLastRecord());
		assertNull(testee.getRecord(1));
		assertNull(testee.getRecord("abc"));

		testee.setLastViewRecord("abc", new URI("abc"), 1, 3, 0);
		assertEquals(1, testee.getRecordCount());
		ViewRecord record = testee.getLastRecord();
		assertNotNull(record);
		assertEquals(1, testee.getRecords().size());
		assertEquals(record, testee.getRecord(0));
		assertEquals(record, testee.getRecord("abc"));
		record = testee.getRecord("abc");
		assertEquals(1, record.getMediaSetIndex());
		assertEquals(3, record.getMediaIndex());
		record = testee.getRecord("abcd");
		assertNull(record);

		testee.setLastViewRecord("abc", new URI("abc"), 14, 34, 0);
		assertEquals(1, testee.getRecordCount());
		record = testee.getRecord("abc");
		assertEquals(14, record.getMediaSetIndex());
		assertEquals(34, record.getMediaIndex());

		testee.setLastViewRecord("abcd", new URI("abcd"), 11, 13, 0);
		assertEquals(2, testee.getRecordCount());
		record = testee.getLastRecord();
		assertNotNull(record);
		assertEquals(record, testee.getRecord(1));
		assertEquals(record, testee.getRecord("abcd"));
		record = testee.getRecord("abc");
		assertEquals(14, record.getMediaSetIndex());
		assertEquals(34, record.getMediaIndex());
		record = testee.getRecord("abcd");
		assertEquals(11, record.getMediaSetIndex());
		assertEquals(13, record.getMediaIndex());

		testee.setLastViewRecord("ab", new URI("ab"), 11, 31, 0);
		assertEquals(3, testee.getRecordCount());

		testee.setLastViewRecord("abcde", new URI("abcde"), 12, 32, 0);
		assertEquals(3, testee.getRecordCount());

		testee.setMaximumRecordCapacity(2);
		assertEquals(2, testee.getRecordCount());
		assertEquals(2, testee.getMaximumRecordCapacity());

		testee.setMaximumRecordCapacity(10);
		assertEquals(2, testee.getRecordCount());
		assertEquals(10, testee.getMaximumRecordCapacity());

		testee.setLastViewRecord("abcdef", new URI("abcdef"), 13, 33, 0);
		assertEquals(3, testee.getRecordCount());
	}

	@Test
	public void testAddViewHistoryListener() throws URISyntaxException {
		listener = new MockViewHistoryListener();
		testee.setLastViewRecord("abc", new URI("abc"), 1, 3, 0);
		assertFalse(listener.isViewHistoryUpdated());

		testee.addViewHistoryListener(listener);
		testee.setLastViewRecord("abcd", new URI("abcd"), 11, 13, 0);
		assertTrue(listener.isViewHistoryUpdated());
		listener.reset();

		testee.removeViewHistoryListener(listener);
		testee.setLastViewRecord("ab", new URI("ab"), 11, 31, 0);
		assertFalse(listener.isViewHistoryUpdated());
	}
}
