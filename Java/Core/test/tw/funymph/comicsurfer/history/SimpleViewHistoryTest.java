/* SimpleViewHistoryTest.java created on 2013/7/14
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.history;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * The class test the functionalities of {@link SimpleViewHistory}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public class SimpleViewHistoryTest {

	private SimpleViewHistory testee;

	@Before
	public void setUp() {
		testee = new SimpleViewHistory();
		testee.setLastViewRecord("abc", null, 1, 2, 3);
		assertEquals(1, testee.getMaximumRecordCapacity());
		testee.setLastViewRecord("abcd", null, 4, 5, 6);
		assertEquals(2, testee.getMaximumRecordCapacity());
	}

	@Test
	public void testGetMaximumRecordCapacity() {
		testee.setLastViewRecord("abcde", null, 7, 8, 9);
		assertEquals(3, testee.getMaximumRecordCapacity());
		testee.setLastViewRecord("abcdef", null, 10, 11, 12);
		assertEquals(4, testee.getMaximumRecordCapacity());
	}

	@Test
	public void testGetRecordByIndex() {
		ViewRecord record = testee.getRecord(1);
		assertEquals(4, record.getMediaSetIndex());
		assertEquals(5, record.getMediaIndex());
		assertEquals(6, record.getPosition());
	}

	@Test
	public void testSetMaximumRecordCapacity() {
		try {
			testee.setMaximumRecordCapacity(10);
			assertTrue(false);
		}
		catch(Exception e) {
			assertTrue(true);
			assertEquals(UnsupportedOperationException.class, e.getClass());
		}
		assertEquals(2, testee.getMaximumRecordCapacity());
	}

	@Test
	public void testSetReference() {
		SimpleViewHistory reference = new SimpleViewHistory();
		assertNull(testee.getRecord("abcdef"));
		testee.setReference(reference);
		assertNull(testee.getRecord("abcdef"));
		reference.setLastViewRecord("abcdef", null, 10, 11, 12);
		assertNotNull(testee.getRecord("abcdef"));

		String name = "new record in both histories";
		testee.setLastViewRecord(name, null, 3, 5, 7);
		assertNotNull(testee.getRecord(name));
		assertNotNull(reference.getRecord(name));
		ViewRecord record = reference.getRecord(name);
		assertEquals(3, record.getMediaSetIndex());
		assertEquals(5, record.getMediaIndex());
		assertEquals(7, record.getPosition());
	}
}
