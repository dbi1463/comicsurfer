/* SimpleLastViewedComicBookInformerTest.java created on 2013/7/14
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.history;

import static org.junit.Assert.*;

import org.junit.Test;

import tw.funymph.comicsurfer.mock.MockViewHistory;

/**
 * The class tests the functionalities of {@link SimpleLastViewedComicBookInformer}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public class SimpleLastViewedComicBookInformerTest {

	@Test
	public void testGetRecord() {
		MockViewHistory history = new MockViewHistory();
		SimpleLastViewedComicBookInformer testee = new SimpleLastViewedComicBookInformer(history);
		history.setLastViewRecord("abc", null, 2, 32, 3);
		assertEquals(3, testee.getLastViewedBlock("abc"));
		assertEquals(32, testee.getLastViewedPage("abc"));
		assertEquals(2, testee.getLastViewedVolume("abc"));

		assertEquals(0, testee.getLastViewedBlock("abcd"));
		assertEquals(0, testee.getLastViewedPage("abcd"));
		assertEquals(0, testee.getLastViewedVolume("abcd"));
	}
}
