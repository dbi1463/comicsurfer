/* XMLViewHistoryManagerProvider.java created on 2013/7/14
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.history;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * The class tests the functionalities of {@link XMLViewHistoryManagerProvider}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public class XMLViewHistoryManagerProviderTests {

	@Test
	public void testGetSingletonViewHistoryManager() {
		XMLViewHistoryManagerProvider provider = new XMLViewHistoryManagerProvider();
		ViewHistoryManager manager = provider.getViewHistoryManager(true);
		assertEquals(XMLViewHistoryManager.class, manager.getClass());

		ViewHistoryManager anotherManager = provider.getViewHistoryManager(true);
		assertEquals(XMLViewHistoryManager.class, anotherManager.getClass());
		assertEquals(manager, anotherManager);
	}

	@Test
	public void testGetNonSingletonViewHistoryManager() {
		XMLViewHistoryManagerProvider provider = new XMLViewHistoryManagerProvider();
		ViewHistoryManager manager = provider.getViewHistoryManager(true);
		assertEquals(XMLViewHistoryManager.class, manager.getClass());

		ViewHistoryManager anotherManager = provider.getViewHistoryManager(false);
		assertEquals(XMLViewHistoryManager.class, anotherManager.getClass());
		assertNotSame(manager, anotherManager);

		ViewHistoryManager thirdManager = provider.getViewHistoryManager(false);
		assertEquals(XMLViewHistoryManager.class, anotherManager.getClass());
		assertNotSame(manager, thirdManager);
		assertNotSame(anotherManager, thirdManager);
	}
}
