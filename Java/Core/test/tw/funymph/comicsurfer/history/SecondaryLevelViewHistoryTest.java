/* SecondaryLevelViewHistoryTest.java created on 2013/7/14
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.history;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import tw.funymph.comicsurfer.mock.MockViewHistoryListener;
import tw.funymph.comicsurfer.mock.MockViewHistoryManagerProvider;

/**
 * The class test the functionalities of {@link SecondaryLevelViewHistory}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public class SecondaryLevelViewHistoryTest {

	private SecondaryLevelViewHistory testee;
	private MockViewHistoryManagerProvider provider;

	@Before
	public void setUp() {
		provider = new MockViewHistoryManagerProvider();
		testee = new SecondaryLevelViewHistory(provider);
		assertNull(testee.getLastRecord());
	}

	@Test
	public void testGetRecordByName() {
		assertNull(testee.getRecord("abc"));
		assertTrue(provider.isHistoryLoaded());
		assertFalse(provider.isSingletonUsed());
		provider.reset();
		assertNull(testee.getRecord("abc"));
		assertFalse(provider.isHistoryLoaded());
		assertFalse(provider.isSingletonUsed());

		provider.reset();
		testee.setLastViewRecord("abc", null, 1, 2, 3);
		assertNotNull(testee.getRecord("abc"));
		assertTrue(provider.isHistorySaved());
		assertFalse(provider.isSingletonUsed());
	}

	@Test
	public void testGetRecordCount() {
		try {
			testee.getRecordCount();
			assertTrue(false);
		}
		catch(Exception e) {
			assertEquals(UnsupportedOperationException.class, e.getClass());
		}
	}

	@Test
	public void testGetMaximumRecordCapacity() {
		try {
			testee.getMaximumRecordCapacity();
			assertTrue(false);
		}
		catch(Exception e) {
			assertEquals(UnsupportedOperationException.class, e.getClass());
		}
	}

	@Test
	public void testGetRecordByIndex() {
		try {
			testee.getRecord(1);
			assertTrue(false);
		}
		catch(Exception e) {
			assertEquals(UnsupportedOperationException.class, e.getClass());
		}
	}

	@Test
	public void testGetRecords() {
		try {
			testee.getRecords();
			assertTrue(false);
		}
		catch(Exception e) {
			assertEquals(UnsupportedOperationException.class, e.getClass());
		}
	}

	@Test
	public void testSetMaximumRecordCapacity() {
		try {
			testee.setMaximumRecordCapacity(3);
			assertTrue(false);
		}
		catch(Exception e) {
			assertEquals(UnsupportedOperationException.class, e.getClass());
		}
	}

	@Test
	public void testViewHistoryListener() {
		MockViewHistoryListener listener = new MockViewHistoryListener();
		listener.assertInitialConditions();
		testee.addViewHistoryListener(listener);
		testee.setLastViewRecord("abc", null, 1, 2, 3);
		assertNotNull(listener.getUpdateViewHistory());
		assertTrue(listener.isViewHistoryUpdated());
		assertEquals(1, listener.getUpdateCount());

		listener.reset();
		listener.assertInitialConditions();
		testee.addViewHistoryListener(listener);
		testee.setLastViewRecord("abcd", null, 1, 2, 3);
		assertNotNull(listener.getUpdateViewHistory());
		assertTrue(listener.isViewHistoryUpdated());
		assertEquals(1, listener.getUpdateCount());

		listener.reset();
		listener.assertInitialConditions();
		testee.removeViewHistoryListener(listener);
		testee.setLastViewRecord("abcde", null, 1, 2, 3);
		listener.assertInitialConditions();
	}
}
