/* LastViewedRecordUpdaterTest.java created on 2012/12/4
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.history;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static tw.funymph.comicsurfer.preferences.Preferences.KEY_LAST_OPEN_PATH;

import java.net.URI;

import org.junit.Before;
import org.junit.Test;

import tw.funymph.comicsurfer.core.events.NavigationEvent;
import tw.funymph.comicsurfer.history.LastViewedRecordUpdater;
import tw.funymph.comicsurfer.mock.MockPreferences;
import tw.funymph.comicsurfer.mock.MockViewHistory;

/**
 * The test cases for {@link LastViewedRecordUpdater}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class LastViewedRecordUpdaterTest {

	private static final String MOCK_SUITE_NAME = "mock suite";
	private static final String MOCK_SUITE_URI = "file:/I:/MockSuite/";

	private NavigationEvent fileEvent;
	private NavigationEvent urlEvent;
	private MockViewHistory history;
	private MockPreferences preferences;
	private LastViewedRecordUpdater testee;

	@Before
	public void setUp() throws Exception {
		history = new MockViewHistory();
		preferences = new MockPreferences();
		urlEvent = new NavigationEvent("123", new URI("http://123.com/123"), 1, 100, 2, 20);
		fileEvent = new NavigationEvent(MOCK_SUITE_NAME, new URI(MOCK_SUITE_URI), 10, 30, 2, 7);
		testee = new LastViewedRecordUpdater(history, preferences);
	}

	@Test
	public void testPageChanged() {
		testee.pageChanged(fileEvent);
		assertDoNothing();
	}

	@Test
	public void testVolumeChanging() {
		testee.volumeChanging(fileEvent);
		assertDoNothing();
	}

	@Test
	public void testVolumeChanged() {
		testee.volumeChanged(fileEvent);
		assertDoNothing();
	}

	@Test
	public void testSuiteClosing() {
		testee.suiteClosing(fileEvent);
		assertNull(preferences.getLastSetKey());
		assertNull(preferences.getLastSetValue());
		ViewRecord record = history.getRecord(MOCK_SUITE_NAME);
		assertEquals(2, record.getMediaSetIndex());
		assertEquals(10, record.getMediaIndex());
	}

	@Test
	public void testSuiteClosed() {
		testee.suiteClosed(fileEvent);
		assertDoNothing();
	}

	@Test
	public void testSuiteOpening() {
		testee.suiteOpening(fileEvent);
		assertDoNothing();
	}

	@Test
	public void testSuiteOpened() {
		testee.suiteOpened(fileEvent);
		assertEquals(KEY_LAST_OPEN_PATH, preferences.getLastSetKey());
		assertEquals(0, history.getRecordCount());
		ViewRecord record = history.getRecord(MOCK_SUITE_NAME);
		assertNull(record);

		preferences.reset();
		testee.suiteOpened(urlEvent);
		assertDoNothing();
	}

	@Test
	public void testSuiteOpenFailed() {
		testee.suiteOpenFailed();
		assertDoNothing();
	}

	private void assertDoNothing() {
		assertNull(preferences.getLastSetKey());
		assertNull(preferences.getLastSetValue());
		assertEquals(0, history.getRecordCount());
		ViewRecord record = history.getRecord(MOCK_SUITE_NAME);
		assertNull(record);
	}
}
