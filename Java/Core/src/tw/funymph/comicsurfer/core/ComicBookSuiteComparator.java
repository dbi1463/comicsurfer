/* ComicBookSuiteComparator.java created on 2012/8/23
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

import java.util.Comparator;

/** A class that is used to check whether two suites are equal or not.
 *
 * @author Pin-Ying Tu
 * @version 1.5
 * @since 1.5
 */
public class ComicBookSuiteComparator implements Comparator<ComicBookSuite> {

	@Override
	public int compare(ComicBookSuite suite1, ComicBookSuite suite2) {
		if(suite1 == null || suite2 == null) {
			return -1;
		}
		if(suite1.getSuiteName() == null || suite2.getSuiteName() == null) {
			return -1;
		}
		if(suite1.getSuitePath() == null || suite2.getSuitePath() == null) {
			return -1;
		}
		int nameComparedResult = suite1.getSuiteName().compareTo(suite2.getSuiteName());
		if(nameComparedResult == 0) {
			return suite1.getSuitePath().compareTo(suite2.getSuitePath());
		}
		return nameComparedResult;
	}
}
