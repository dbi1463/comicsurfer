/* Constants.java created on 2013/5/5
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

/**
 * All comic surfer system constants.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public interface ComicSurferConstants {

	/**
	 * Represent the page or comic book does not support the {@link Indexable#getIndex()} method. 
	 */
	public static final int UNDEFINED_INDEX = -1;

	public static final String VIEWER_SUGGESTION_PAGINAL = "ViewSuggestion.paginal";

	public static final String VIEWER_SUGGESTION_SLIDE_SHOW = "ViewSuggestion.slideShow";

	public static final String VIEWER_SUGGESTION_CONTINUOUS = "ViewSuggestion.continuous";
}
