/* ComicSurferViewController.java created on 2013/5/16
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

/**
 * Since Comic Surfer 3.0, each view has independent controller which provides
 * unique navigation way, motion controller, and image display model.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public interface ComicSurferViewController {

	public void enter();

	public void leave();

	/**
	 * Get the main comic book navigator that handles the navigation
	 * between comic books.
	 * 
	 * @return the main comic book navigator
	 */
	public NavigationModel getNavigator();

	/**
	 * Get the image display model that manages the display size of
	 * the images.
	 * 
	 * @return the image display model
	 */
	public ImageDisplayModel getDisplayModel();

	/**
	 * Get the motion controller that handles the user's input events.
	 * 
	 * @return the motion controller
	 */
	public MotionController getMotionController();

	/**
	 * Get the suggested viewer for the controller. The actually viewer will
	 * determined based on the suggestion by the UI at runtime.
	 * 
	 * @return the suggested viewer
	 */
	public String getViewerSuggestion();
}
