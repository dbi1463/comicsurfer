/* TrackMouseInformer.java created on 2012/11/17
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

import tw.funymph.comicsurfer.core.paginal.PaginalComicBookDisplayController;

/** A class can implement this interface to let {@link PaginalComicBookDisplayController}
 * know whether to track mouse in the same direction or not.
 *
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public interface TrackMouseInformer {

	/** Get whether the image should track mouse in the same
	 * direction or not.
	 * 
	 * @return true the image track mouse in the same direction
	 */
	public boolean isTrackMouse();
}
