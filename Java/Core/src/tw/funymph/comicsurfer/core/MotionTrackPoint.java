/* MotionTrackPoint.java created on Dec 14, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

/** A simple class to record the motion tracks
 *
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.3
 */
public class MotionTrackPoint {

	private int x;
	private int y;

	/** Contruct a <code>MotionTrack</code> instance for a point in
	 * the entrie track.
	 * 
	 * @param theX the x of the point
	 * @param theY the y of the point
	 */
	public MotionTrackPoint(int theX, int theY) {
		x = theX;
		y = theY;
	}

	/** Get the x of the point.
	 * 
	 * @return the x of the point
	 */
	public int getX() {
		return x;
	}

	/** Get the y of the point.
	 * 
	 * @return the y of the point
	 */
	public int getY() {
		return y;
	}
}
