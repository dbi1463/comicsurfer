/* NavigationModel.java created on Nov 29, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

/** A class can implement this interface to handles the navigation
 * and the presentation model when navigating a suite comic books.
 * This is the main interface for the comic book suite navigation
 * use case controller.
 *
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.3
 */
public interface NavigationModel extends Navigable {

	/** Get whether the current suite has the next page.
	 * 
	 * @return true if the current suite has the next page 
	 */
	public boolean hasNextPage();

	/** Get whether the current suite has the next volume.
	 * 
	 * @return true if the current suite has the next volume
	 */
	public boolean hasNextVolume();

	/** Get whether the current suite has the previous page.
	 * 
	 * @return true if the current suite has the previous page
	 */
	public boolean hasPreviousPage();

	/** Get whether the current suite has the previous volume.
	 * 
	 * @return true if the current suite has the previous page
	 */
	public boolean hasPreviousVolume();

	/** Get whether the model can go back to the previous viewed page.
	 * 
	 * @return true if the model can go back to the previous viewed page
	 */
	public boolean canGoBack();

	/** Get the index of the current page in the current volume.
	 * 
	 * @return the index of the current page
	 */
	public int getCurrentPageIndex();

	/** Get the index of the current volume in the suite.
	 * 
	 * @return the index of the current book (volume)
	 */
	public int getCurrentVolumeIndex();

	/** Get the amount of the pages in the current volume.
	 * 
	 * @return the page count of the current volume
	 */
	public int getPageCount();

	/** Get the amount of the volumes in the current opened comic
	 * book suite.
	 * 
	 * @return the volume count of the suite
	 */
	public int getVolumeCount();

	/** Get the current comic book suite.
	 * 
	 * @return the current viewing suite; null if no suite is opened
	 */
	public ComicBookSuite getCurrentSuite();
}
