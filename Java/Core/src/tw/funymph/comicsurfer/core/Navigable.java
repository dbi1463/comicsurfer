/* Navigable.java created on 2012/11/17
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

import tw.funymph.comicsurfer.core.events.NavigationEventListener;

/** A class can implement this interface to handle the core navigation
 * operations for comic book suite.
 *
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public interface Navigable {

	/** Close the current comic book suite.
	 */
	public void closeComicBook();

	/** Open the comic book suite from the specified URI.
	 * 
	 * @param uri the URI to opened.
	 */
	public void openComicBook(ComicBookSuite books);

	/** Go back to the previous viewed page.
	 */
	public void back();

	/** Change to the next page. Note that if the current page is the LAST page
	 * BUT the current book is NOT the last book in the suite, the method will
	 * CLOSE the current book and then OPEN the next book to get the FIRST page.
	 */
	public void nextPage();

	/** Open the next volume.
	 */
	public void nextVolume();

	/** Change the current page to the previous page. Note that if the current page
	 * is the FIRST page BUT the current book is NOT the first book in the suite,
	 * the method will CLOSE the current book and then OPEN the previous book and
	 * jump the LAST page.
	 */
	public void previousPage();

	/** Open the previous volume. If <code>gotoLastPage</code> is true, the method
	 * will change the current page to the last page after opening the previous
	 * volume; otherwise, the current page will be the first page.
	 * 
	 * @param gotoLastPage specify whether goto the last page
	 */
	public void previousVolume(boolean gotoLastPage);

	/** Get the input stream that can read the image of the current page. The current
	 * page can be changed by methods like {@link #jumpToPage(int)}, {@link #nextPage()},
	 * {@link #previousPage()}, etc.
	 * 
	 * @return the current page
	 */
	public Page currentPage();

	/** Jump to specific page index, i.e., 0 < index < {@link #getPageCount()}}.
	 * 
	 * @param index the specific page index
	 */
	public void jumpToPage(int index);

	/** Jump to the specified volume index (0 < index < {@link #getComicBookCount()}).
	 * 
	 * @param index the new volume index
	 */
	public void jumpToVolume(int index);

	/** Add the specific listener into the notification list. Note that an instance
	 * will be only added once.
	 * 
	 * @param listener the listener to be added
	 */
	public void addNavigationEventListener(NavigationEventListener listener);

	/** Remove the specific listener from the notification list.
	 * 
	 * @param listener the listener to be removed
	 */
	public void removeNavigationEventListener(NavigationEventListener listener);
}
