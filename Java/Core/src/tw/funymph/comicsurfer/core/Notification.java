/* Notification.java created on 2013/7/21
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

import java.util.Map;

/**
 * A class can implement this interface to wrap the sender, message, and data
 * as a notification.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public interface Notification {

	/**
	 * Get the extra data that the sender want to pass to the receiver.
	 * 
	 * @return the extra data to the receiver
	 */
	public Map<String, Object> getData();

	/**
	 * Get the sender who send this notification.
	 * 
	 * @return the sender of the notification
	 */
	public Object getSender();

	/**
	 * Get the message that also be the key to notify registered receivers.
	 * 
	 * @return the message
	 */
	public String getMessage();

	/**
	 * Get the timestamp of the notification.
	 * 
	 * @return the timestamp
	 */
	public long getTimestamp();
}
