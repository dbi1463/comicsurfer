/* ComicBook.java created on Oct 27, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

/**
 * A class can implement this interface to manage a set of images in
 * different media as a comic book and can be navigated.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public interface ComicBook extends Indexable {

	/**
	 * Open the comic book and set the current page to the first
	 * one ready for navigation. If there are a suite of comic books,
	 * each book will be opened when the user see it, and be closed
	 * when the user see another book.
	 */
	public void open();

	/**
	 * Close the book. Note that a closed book CAN be reopened again
	 * so that cannot release any resources that are needed for reopening.
	 */
	public void close();

	/**
	 * Get the number of the pages in the comic book. Note that this method
	 * can only be called after open().
	 * 
	 * @return the number of pages
	 */
	public int getPageCount();

	/**
	 * Get the input stream for the specified page index. Note that the given
	 * index must be in the range [0, {@link #getPageCount()})
	 *  
	 * @param index the specified index
	 * @return the page
	 */
	public Page getPage(int index);
}
