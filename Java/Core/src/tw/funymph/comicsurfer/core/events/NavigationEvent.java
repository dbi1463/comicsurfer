/* NavigationEvent.java created on 2012/12/14
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core.events;

import java.net.URI;

/**
 * The object that contains the necessary information of the last
 * navigation event.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class NavigationEvent {

	private int pageIndex;
	private int pageCount;
	private int volumeIndex;
	private int volumeCount;

	private URI suiteURI;
	private String suiteName;

	/**
	 * Construct a <code>NavigationEvent</code> instance with the initial values.
	 * 
	 * @param name the suite name when the event occurs
	 * @param uri the suite URI when the event occurs
	 * @param page the page index when the event occurs
	 * @param totalPages the total pages of the book when the event occurs
	 * @param volume the volume index when the event occurs
	 * @param totalVolumes the total volumes of the suite when the event occurs
	 */
	public NavigationEvent(String name, URI uri, int page, int totalPages, int volume, int totalVolumes) {
		suiteName = name;
		suiteURI = uri;
		pageIndex = page;
		pageCount = totalPages;
		volumeIndex = volume;
		volumeCount = totalVolumes;
	}

	/**
	 * Get the current page index.
	 * 
	 * @return the current page index
	 */
	public int getCurrentPageIndex() {
		return pageIndex;
	}

	/**
	 * Get the current volume index.
	 * 
	 * @return the current volume index
	 */
	public int getCurrentVolumeIndex() {
		return volumeIndex;
	}

	/**
	 * Get the number of the pages in the current viewed comic book.
	 * 
	 * @return the number of the pages
	 */
	public int getPageCount() {
		return pageCount;
	}

	/**
	 * Get the number of the volumes in the current view comic book suite.
	 * 
	 * @return the number of the volumes
	 */
	public int getVolumeCount() {
		return volumeCount;
	}

	/**
	 * Get the name of the comic book suite.
	 * 
	 * @return the name of the suite
	 */
	public String getSuiteName() {
		return suiteName;
	}

	/**
	 * Get the path of the comic book suite.
	 * 
	 * @return the path of the suite
	 */
	public URI getSuitePath() {
		return suiteURI;
	}
}
