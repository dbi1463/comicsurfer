/* ImageDisplayModelListener.java created on Dec 5, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core.events;

import tw.funymph.comicsurfer.core.ImageDisplayModel;

/**
 * A class can implement this interface to receive and handle the
 * image display model updated events.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.3
 */
public interface ImageDisplayModelListener {

	/**
	 * Invoked when the image display model is updated.
	 * 
	 * @param model the event source
	 */
	public void modelUpdated(ImageDisplayModel model);
}
