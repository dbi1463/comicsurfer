/* ImageDisplayModelListenerList.java created on Dec 5, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core.events;

import java.util.ArrayList;

import tw.funymph.comicsurfer.core.ImageDisplayModel;

/**
 * A class that manages the <code>ImageDisplayModelListener</code> instances
 * and fire events to the registered listeners.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.3
 */
public class ImageDisplayModelListenerList {

	private ImageDisplayModel eventSource;
	ArrayList<ImageDisplayModelListener> listeners;

	/**
	 * Construct a <code>ImageDisplayModelListenerList</code> instance.
	 * 
	 * @param model the event source
	 */
	public ImageDisplayModelListenerList(ImageDisplayModel displayModel) {
		eventSource = displayModel;
		listeners = new ArrayList<ImageDisplayModelListener>();
	}

	/**
	 * Add the specific listener into the notification list. Note that the
	 * same instance will only be added once.
	 * 
	 * @param listener the listener to be added
	 */
	public void addImageDisplayModelListener(ImageDisplayModelListener listener) {
		if(listener != null && !listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	/**
	 * Remove the specific listener from the notification list.
	 * 
	 * @param listener the listener to be removed
	 */
	public void removeImageDisplayModelListener(ImageDisplayModelListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Fire an event that the image display model is updated to all registered listeners.
	 */
	public void fireImageDisplayModelUpdatedEvent() {
		for(ImageDisplayModelListener listener : listeners) {
			listener.modelUpdated(eventSource);
		}
	}
}
