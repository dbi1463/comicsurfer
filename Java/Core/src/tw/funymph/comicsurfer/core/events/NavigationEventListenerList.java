/* NavigationEventListenerList.java created on Nov 29, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core.events;

import java.net.URI;
import java.util.ArrayList;

import tw.funymph.comicsurfer.core.ComicBookSuite;
import tw.funymph.comicsurfer.core.NavigationModel;

/**
 * A class that manages the <code>NavigationEventListener</code> instances
 * and fire events to the registered listeners.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.3
 */
public class NavigationEventListenerList {

	private NavigationModel eventSource;
	ArrayList<NavigationEventListener> listeners;

	/**
	 * Construct a <code>NavigationEventList</code> instance.
	 * 
	 * @param navigationModel the event source
	 */
	public NavigationEventListenerList(NavigationModel navigationModel) {
		eventSource = navigationModel;
		listeners = new ArrayList<NavigationEventListener>();
	}

	/**
	 * Add the specific listener into the notification list. Note that an instance
	 * will be only added once.
	 * 
	 * @param listener the listener to be added
	 */
	public void addComicSurferEventListener(NavigationEventListener listener) {
		if(listener != null && !listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	/**
	 * Remove the specific listener from the notification list.
	 * 
	 * @param listener the listener to be removed
	 */
	public void removeComicSurferEventListener(NavigationEventListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Fire an event that the current page has been changed to all registered listeners.
	 */
	public void firePageChangedEvent() {
		NavigationEvent event = createEvent();
		for(NavigationEventListener listener : listeners) {
			listener.pageChanged(event);
		}
	}

	/**
	 * Fire an event that a new suite is on opening to all registered listeners.
	 */
	public void fireSuiteOpeningEvent() {
		NavigationEvent event = createEvent();
		for(NavigationEventListener listener : listeners) {
			listener.suiteOpening(event);
		}
	}

	/**
	 * Fire an event that a new suite has been opened to all registered listeners.
	 */
	public void fireSuiteOpenedEvent() {
		NavigationEvent event = createEvent();
		for(NavigationEventListener listener : listeners) {
			listener.suiteOpened(event);
		}
	}

	/**
	 * Fire an event that the navigator is unable to open the URI to all registered listeners.
	 */
	public void fireSuiteOpenFailedEvent() {
		for(NavigationEventListener listener : listeners) {
			listener.suiteOpenFailed();
		}
	}

	/**
	 * Fire an event that the current suite has been closed to all registered listeners.
	 */
	public void fireSuiteClosedEvent() {
		NavigationEvent event = createEvent();
		for(NavigationEventListener listener : listeners) {
			listener.suiteClosed(event);
		}
	}

	/**
	 * Fire an event that the current suite is on closing to all registered listeners.
	 */
	public void fireSuiteClosingEvent() {
		NavigationEvent event = createEvent();
		for(NavigationEventListener listener : listeners) {
			listener.suiteClosing(event);
		}
	}

	/**
	 * Fire an event that the current volume is on changing to all registered listeners.
	 */
	public void fireVolumeChangingEvent() {
		NavigationEvent event = createEvent();
		for(NavigationEventListener listener : listeners) {
			listener.volumeChanging(event);
		}
	}

	/**
	 * Fire an event that the current volume has been changed to all registered listeners.
	 */
	public void fireVolumeChangedEvent() {
		NavigationEvent event = createEvent();
		for(NavigationEventListener listener : listeners) {
			listener.volumeChanged(event);
		}
	}

	/**
	 * Create the needed event instance.
	 * 
	 * @return the event instance
	 */
	private NavigationEvent createEvent() {
		ComicBookSuite suite = eventSource.getCurrentSuite();
		String name = (suite != null)? suite.getSuiteName() : "";
		URI path = (suite != null)? suite.getSuitePath() : null;
		int pageIndex = eventSource.getCurrentPageIndex();
		int pageCount = eventSource.getPageCount();
		int volumeIndex = eventSource.getCurrentVolumeIndex();
		int volumeCount = eventSource.getVolumeCount();
		return new NavigationEvent(name, path, pageIndex, pageCount, volumeIndex, volumeCount);
	}
}
