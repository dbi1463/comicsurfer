/* NavigationEventListenerAdapter.java created on 2013/1/6
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.core.events;

/**
 * A simple class that provides the default implementation (do nothing) of
 * {@link NavigationEventListener} for the other classes that only want handle
 * a few events not all.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class NavigationEventListenerAdapter implements NavigationEventListener {

	@Override
	public void pageChanged(NavigationEvent event) {}

	@Override
	public void volumeChanging(NavigationEvent event) {}

	@Override
	public void volumeChanged(NavigationEvent event) {}

	@Override
	public void suiteClosing(NavigationEvent event) {}

	@Override
	public void suiteClosed(NavigationEvent event) {}

	@Override
	public void suiteOpening(NavigationEvent event) {}

	@Override
	public void suiteOpened(NavigationEvent event) {}

	@Override
	public void suiteOpenFailed() {}
}
