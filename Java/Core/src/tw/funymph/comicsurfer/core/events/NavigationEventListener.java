/* NavigationEventListener.java created on Nov 29, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core.events;

/**
 * A class can implement the interface to receive and handle the
 * comic books navigation events.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public interface NavigationEventListener {

	/**
	 * Invoked when the current page is changed.
	 * 
	 * @param event the event source
	 */
	public void pageChanged(NavigationEvent event);

	/**
	 * Invoked when the current volume is on changing.
	 *
	 * @param event the event source
	 */
	public void volumeChanging(NavigationEvent event);

	/**
	 * Invoked when the current volume is changed.
	 *
	 * @param event the event source
	 */
	public void volumeChanged(NavigationEvent event);

	/**
	 * Invoked when the current comic book suite is on closing.
	 * 
	 * @param event the event source
	 */
	public void suiteClosing(NavigationEvent event);

	/**
	 * Invoked when the current comic book suite is closed.
	 * 
	 * @param event the event source
	 */
	public void suiteClosed(NavigationEvent event);

	/**
	 * Invoked when a new comic book suite is on opening.
	 * 
	 * @param event the event source
	 */
	public void suiteOpening(NavigationEvent event);

	/**
	 * Invoked when a new comic book suite is opened.
	 * 
	 * @param event the event source
	 */
	public void suiteOpened(NavigationEvent event);

	/**
	 * Invoked when the navigator is failed to open the suite.
	 */
	public void suiteOpenFailed();
}
