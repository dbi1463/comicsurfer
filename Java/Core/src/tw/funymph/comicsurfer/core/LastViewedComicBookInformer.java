/* LastViewedRecordInformer.java created on 2012/11/17
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

/**
 * A class can implement this interface to offer the last viewed
 * record information about the given comic book suite.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public interface LastViewedComicBookInformer {

	/**
	 * Get the index of the last viewed volume.
	 * 
	 * @param name the name of the comic book suite
	 * @return the index of the last viewed volume
	 */
	public int getLastViewedVolume(String name);

	/**
	 * Get the index of the last viewed page.
	 * 
	 * @param name the name of the comic book suite
	 * @return the index of the last viewed page
	 */
	public int getLastViewedPage(String name);

	/**
	 * Get the block in the last viewed page.
	 * 
	 * @param name the name of the comic book suite
	 * @return the block in the last viewed page
	 */
	public int getLastViewedBlock(String name);
}