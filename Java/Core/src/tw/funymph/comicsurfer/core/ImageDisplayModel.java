/* ImageDisplayModel.java created on Dec 2, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

import tw.funymph.comicsurfer.core.events.ImageDisplayModelListener;

/** A class can implements this interface to control the display
 * of images in four display modes.
 *
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.3
 */
public interface ImageDisplayModel {

	/** The image is displayed in full actual size.
	 */
	public static final int ACTUAL_IMAGE_SIZE = 0;

	/** The image is scaled to fit the window width only if
	 * the image size is larger than the window size.
	 */
	public static final int FIT_WINDOW_WIDTH = 1;

	/** The image is scaled to fit the window height only if
	 * the image size is larger than the window size.
	 */
	public static final int FIT_WINDOW_HEIGHT = 2;

	/** The image is scaled to fit the window width or height
	 * (better quality is selected automatically) only if the
	 * image size is larger than the window size.
	 */
	public static final int FIT_WINDOW_SIZE = 3;

	/** This mode is used only for slide show.
	 */
	public static final int SLIDE_SHOW_MODE = FIT_WINDOW_SIZE;

	/** Set the display mode. The display mode is one of the followings:
	 * <ol>
	 * <li>{@link #ACTUAL_IMAGE_SIZE}</li>
	 * <li>{@link #FIT_WINDOW_WIDTH}</li>
	 * <li>{@link #FIT_WINDOW_HEIGHT}</li>
	 * <li>{@link #FIT_WINDOW_SIZE}</li>
	 * <li>{@link #SLIDE_SHOW_MODE}</li>
	 * </ol>
	 * 
	 * @param newMode the new display mode
	 */
	public void setDisplayMode(int newMode);

	/** Set the new window size. This method will changes to inner status
	 * and fire model changed events.
	 * 
	 * @param width the width of the window
	 * @param height the height of the window
	 */
	public void setWindowSize(int width, int height);

	/** Set the new image size. This method will changes to inner status
	 * and fire model changed events.
	 * 
	 * @param width the width of the image
	 * @param height the height of the image
	 */
	public void setImageSize(int width, int height);

	/** Get the x location of the image.
	 * 
	 * @return the x location
	 */
	public int getImageX();

	/** Get the y location of the image.
	 * 
	 * @return the y location
	 */
	public int getImageY();

	/** Get the current display mode.
	 * 
	 * @return the display mode
	 */
	public int getDisplayMode();

	/** Get the image display ratio based on the display mode, image size,
	 * and the window size.
	 * 
	 * @return the image display ratio
	 */
	public double getDisplayRatio();

	/** Add the specific listener into the notification list. Note that the
	 * same instance will only be added once.
	 * 
	 * @param listener the listener to be added
	 */
	public void addImageDisplayModelListener(ImageDisplayModelListener listener);

	/** Remove the specific listener from the notification list.
	 * 
	 * @param listener the listener to be removed
	 */
	public void removeImageDisplayModelListener(ImageDisplayModelListener listener);
}
