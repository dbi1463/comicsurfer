/* ReferenceAttachable.java created on 2013/7/14
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

/**
 * A class/interface can implement/extends this interface to add the
 * ability of attach the reference of the same type.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public interface ReferenceAttachable<Reference> {

	/**
	 * Set the reference.
	 * 
	 * @param reference the reference
	 */
	public void setReference(Reference reference);
}
