/* SlideShowController.java created on 2013/1/6
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core.slide;

/** The controller to start/stop the slide show.
 *
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public interface SlideShowController {

	/** Start the slide show.
	 */
	public void startSlideShow();

	/** Stop the slide show.
	 */
	public void stopSlideShow();

	/** Check whether the system is in the slide show mode.
	 * 
	 * @return true is the system is in the slide show mode
	 */
	public boolean isSlideShowStarted();
}
