/* ComicBookSuite.java created on 2012/6/16
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

import java.net.URI;

/** A class can implement this interface to manage a suite of comic books.
 *
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.0
 */
public interface ComicBookSuite {

	/** Open the comic book suite -- prepare any resources that are needed
	 * to open the requested comic book, and the information for navigation,
	 * i.e., the number of comic books in the suite.
	 */
	public void open();

	/** Close the suite of comic books and release all resources.
	 */
	public void close();

	/** Get the number of the comic books in the suite.
	 * 
	 * @return the number of the comic books in the suite
	 */
	public int getComicBookCount();

	/** Get a specific comic book.
	 * 
	 * @param index the index of the specific book
	 * @return the specific comic book
	 */
	public ComicBook getComicBook(int index);

	/** Get the name of the comic book suite.
	 * 
	 * @return the name of the comic book suite
	 */
	public String getSuiteName();

	/** Get the URI that contains the comic book suite.
	 * 
	 * @return the URI that contains the comic book suite
	 */
	public URI getSuitePath();
}
