/* NotificationReceiver.java created on 2013/7/21
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

/**
 * A class can implement this interface and register itself as a notification
 * receiver to receive the notification.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public interface NotificationReceiver {

	/**
	 * Invoke when the registered notification is published.
	 * 
	 * @param notification the published notification
	 */
	public void notificationReceived(Notification notification);
}
