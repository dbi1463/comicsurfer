/* DefaultNotificationCenter.java created on 2013/7/21
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The default notification center of the {@link NotificationCenter}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public class DefaultNotificationCenter implements NotificationCenter {

	private static DefaultNotificationCenter instance;

	private Map<String, List<NotificationReceiver>> categoriedReceivers;

	/**
	 * Get the singleton instance of the default notification center.
	 * 
	 * @return the singleton instance
	 */
	public static DefaultNotificationCenter getInstance() {
		if(instance == null) {
			instance = new DefaultNotificationCenter();
		}
		return instance;
	}

	/**
	 * The private default constructor to prevent the creation of multiple instances.
	 */
	private DefaultNotificationCenter() {
		categoriedReceivers = new HashMap<String, List<NotificationReceiver>>();
	}

	@Override
	public void subscribe(String message, NotificationReceiver receiver) {
		List<NotificationReceiver> notificationList = getNotificationList(message);
		if(!notificationList.contains(receiver)) {
			notificationList.add(receiver);
		}
	}

	@Override
	public void unsubscribe(NotificationReceiver receiver) {
		Collection<List<NotificationReceiver>> allReceivers = categoriedReceivers.values();
		for(List<NotificationReceiver> receivers : allReceivers) {
			receivers.remove(receiver);
		}
	}

	@Override
	public void unsubscribe(String message, NotificationReceiver receiver) {
		getNotificationList(message).remove(receiver);
	}

	@Override
	public void publish(Notification notification) {
		List<NotificationReceiver> notificationList = getNotificationList(notification.getMessage());
		for(NotificationReceiver receiver : notificationList) {
			receiver.notificationReceived(notification);
		}
	}

	@Override
	public void publish(Object sender, String message, Map<String, Object> data) {
		publish(new DefaultNotification(sender, message, data));
	}

	/**
	 * Get the notification list for the message.
	 * 
	 * @param message the message
	 * @return the notification list
	 */
	protected List<NotificationReceiver> getNotificationList(String message) {
		if(!categoriedReceivers.containsKey(message)) {
			categoriedReceivers.put(message, new ArrayList<NotificationReceiver>());
		}
		return categoriedReceivers.get(message);
	}
}
