/* Indexable.java created on 2013/5/5
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

/**
 * The object that can be indexed should implement this interface to offer
 * the necessary information for indexing.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public interface Indexable extends ComicSurferConstants {

	/**
	 * Get the index of this instance in its container. If the instance does
	 * not support index, please return {@link ComicSurferConstants#UNDEFINED_INDEX}.
	 * 
	 * @return the index of this instance
	 */
	public int getIndex();
}
