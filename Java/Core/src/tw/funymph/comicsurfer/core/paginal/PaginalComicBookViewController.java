/* PaginalComicBookViewController.java created on 2013/5/16
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core.paginal;

import static tw.funymph.comicsurfer.core.ComicSurferConstants.VIEWER_SUGGESTION_PAGINAL;

import tw.funymph.comicsurfer.core.ComicSurferViewController;
import tw.funymph.comicsurfer.core.ImageDisplayModel;
import tw.funymph.comicsurfer.core.LastViewedComicBookInformer;
import tw.funymph.comicsurfer.core.MotionController;
import tw.funymph.comicsurfer.core.NavigationModel;
import tw.funymph.comicsurfer.core.TrackMouseInformer;

/**
 * The view controller for the paginal view.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public class PaginalComicBookViewController implements ComicSurferViewController {

	private NavigationModel navigator;
	private PaginalComicBookDisplayController displayController;
	
	public PaginalComicBookViewController(LastViewedComicBookInformer viewHistory, TrackMouseInformer trackMouse) {
		navigator = new PaginalComicBookNavigator(viewHistory);
		displayController = new PaginalComicBookDisplayController(navigator, trackMouse);
	}

	@Override
	public NavigationModel getNavigator() {
		return navigator;
	}

	@Override
	public ImageDisplayModel getDisplayModel() {
		return displayController;
	}

	@Override
	public MotionController getMotionController() {
		return displayController;
	}

	@Override
	public String getViewerSuggestion() {
		return VIEWER_SUGGESTION_PAGINAL;
	}

	@Override
	public void enter() { }

	@Override
	public void leave() { }
}
