/* PaginalComicBookDisplayController.java created on Dec 2, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core.paginal;

import static java.lang.Math.abs;
import static java.lang.Math.ceil;
import static java.lang.Math.min;

import java.util.ArrayList;

import tw.funymph.comicsurfer.core.ImageDisplayModel;
import tw.funymph.comicsurfer.core.MotionController;
import tw.funymph.comicsurfer.core.MotionTrackPoint;
import tw.funymph.comicsurfer.core.Navigable;
import tw.funymph.comicsurfer.core.TrackMouseInformer;
import tw.funymph.comicsurfer.core.events.ImageDisplayModelListener;
import tw.funymph.comicsurfer.core.events.ImageDisplayModelListenerList;

/**
 * A class that implements both the {@link ImageDisplayModel} and
 * {@link MotionController} to control the comic book display on
 * the screen.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.3
 */
public class PaginalComicBookDisplayController implements ImageDisplayModel, MotionController {

	/**
	 * The default display ratio, i.e., 1.0 for actual image size.
	 */
	private static final double DEFAULT_RATIO = 1.0;

	/**
	 * The default maximum motion range (96 x 96) to go around the entire image.
	 */
	private static final int MAXIMUM_MOTION = 96;

	private int mode;

	private int x;
	private int y;
	private int screenWidth;
	private int screenHeight;
	private int imageWidth;
	private int imageHeight;
	private int scaledImageWidth;
	private int scaledImageHeight;

	private int pressedX;
	private int pressedY;
	private int surfingStartX;
	private int surfingStartY;

	private double ratio;
	private double verticalSpeed;
	private double horizontalSpeed;

	private boolean needSurfingX;
	private boolean needSurfingY;
	private boolean surfingStarted;
	private boolean rightButtonPressed;

	private Navigable navigator;
	private TrackMouseInformer informer;
	private ArrayList<MotionTrackPoint> points;
	private ImageDisplayModelListenerList listeners;

	/**
	 * Construct a <code>ComicBookDisplayController</code> instances with
	 * the data source that offers the user preferences data and navigator.
	 * 
	 * @param theNavigator the data source.
	 * @param theInformer the track mouse information source
	 */
	public PaginalComicBookDisplayController(Navigable theNavigator, TrackMouseInformer theInformer) {
		informer = theInformer;
		navigator = theNavigator;
		mode = ACTUAL_IMAGE_SIZE;
		points = new ArrayList<MotionTrackPoint>();
		listeners = new ImageDisplayModelListenerList(this);
	}

	@Override
	public void moved(int x, int y) {
		if(!surfingStarted) {
			surfingStarted = true;
		}
		else {
			int movedX = (int)ceil((x - surfingStartX) * horizontalSpeed);
			int movedY = (int)ceil((y - surfingStartY) * verticalSpeed);
			calculateLocation(movedX, movedY);
		}
		surfingStartX = x;
		surfingStartY = y;
	}

	@Override
	public void pressed(int x, int y, int action) {
		rightButtonPressed = (action == ACCESSORY_ACTION);
		if(rightButtonPressed) {
			pressedX = x;
			pressedY = y;
			points.add(new MotionTrackPoint(x, y));
			listeners.fireImageDisplayModelUpdatedEvent();
		}
	}

	@Override
	public void released(int x, int y, int action) {
		if(rightButtonPressed) {
			rightButtonPressed = false;
			int diffX = x - pressedX;
			int diffY = y - pressedY;
			points.clear();
			listeners.fireImageDisplayModelUpdatedEvent();
			if(abs(diffX) > abs(diffY)) {
				if(diffX > 0) {
					navigator.nextPage();
				}
				else {
					navigator.previousPage();
				}
			}
			else if(abs(diffX) < abs(diffY)) {
				if(diffY > 0) {
					navigator.nextVolume();
				}
				else {
					navigator.previousVolume(false);
				}
			}
		}
	}

	@Override
	public void setDisplayMode(int newMode) {
		if(mode != newMode) {
			mode = newMode;
			evaluateSurfingRequirement();
			calculateLocation(0, 0);
		}
	}

	@Override
	public void setWindowSize(int width, int height) {
		screenWidth = width;
		screenHeight = height;
		evaluateSurfingRequirement();
		calculateLocation(0, 0);
	}

	@Override
	public void setImageSize(int width, int height) {
		surfingStarted = false;
		imageWidth = width;
		imageHeight = height;
		evaluateSurfingRequirement();
		calculateLocation(0, 0);
	}

	@Override
	public int getImageX() {
		return x;
	}

	@Override
	public int getImageY() {
		return y;
	}

	@Override
	public int getDisplayMode() {
		return mode;
	}

	@Override
	public double getDisplayRatio() {
		return ratio;
	}

	@Override
	public void addImageDisplayModelListener(ImageDisplayModelListener listener) {
		listeners.addImageDisplayModelListener(listener);
	}

	@Override
	public void removeImageDisplayModelListener(ImageDisplayModelListener listener) {
		listeners.removeImageDisplayModelListener(listener);
	}

	@Override
	public void dragged(int x, int y) {
		if(rightButtonPressed) {
			points.add(new MotionTrackPoint(x, y));
			listeners.fireImageDisplayModelUpdatedEvent();
		}
	}

	@Override
	public void clicked(int x, int y, int action, int count) {}

	@Override
	public MotionTrackPoint[] getMotionTrack() {
		return points.toArray(new MotionTrackPoint[0]);
	}

	/**
	 * Evaluate the surfing requirements including speed, scaled image size, etc.
	 */
	private void evaluateSurfingRequirement() {
		calculateDisplayRatio();
		scaledImageWidth = (int)(imageWidth * ratio);
		scaledImageHeight = (int)(imageHeight * ratio);
		int diffX = scaledImageWidth - screenWidth;
		int diffY = scaledImageHeight - screenHeight;
		needSurfingX = diffX > 0;
		needSurfingY = diffY > 0;
		horizontalSpeed = abs(diffX) > MAXIMUM_MOTION? (abs((double)diffX)) / (double)MAXIMUM_MOTION : 1.0;
		verticalSpeed = abs(diffY) > MAXIMUM_MOTION? (abs((double)diffY)) / (double)MAXIMUM_MOTION : 1.0;
	}

	/**
	 * Calculate the new location of the displayed image based on
	 * the mouse movement. 
	 * 
	 * @param movedX the horizontal mouse movement
	 * @param movedY the vertical mouse movement
	 */
	private void calculateLocation(int movedX, int movedY) {
		int diffWidth = screenWidth - scaledImageWidth;
		int diffHeight = screenHeight - scaledImageHeight;
		if(surfingStarted) {
			boolean trackMouse = informer.isTrackMouse();
			int newX = trackMouse? x + movedX : x - movedX;
			int newY = trackMouse? y + movedY : y - movedY;
			x = needSurfingX? ((newX < diffWidth)? diffWidth : ((newX > 0)? 0 : newX)) : abs(diffWidth) / 2;
			y = needSurfingY? ((newY < diffHeight)? diffHeight : ((newY > 0)? 0 : newY)) : abs(diffHeight) / 2;
		}
		else {
			x = needSurfingX? diffWidth : abs(diffWidth) / 2;
			y = needSurfingY? 0 : abs(diffHeight) / 2;
		}
		listeners.fireImageDisplayModelUpdatedEvent();
	}

	/**
	 * Calculate the display ratio based on the display mode, screen
	 * size and image size.
	 */
	private void calculateDisplayRatio() {
		double widthRatio = (double)screenWidth / (double)imageWidth;
		double heightRatio = (double)screenHeight / (double)imageHeight;
		switch(mode) {
		case FIT_WINDOW_HEIGHT:
			ratio = min(heightRatio, DEFAULT_RATIO);
			break;
		case FIT_WINDOW_WIDTH:
			ratio = min(widthRatio, DEFAULT_RATIO);
			break;
		case FIT_WINDOW_SIZE:
			ratio = min(min(heightRatio, widthRatio), DEFAULT_RATIO);
			break;
		case ACTUAL_IMAGE_SIZE:
		default:
			ratio = DEFAULT_RATIO;
		}
	}
}
