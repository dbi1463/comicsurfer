/* PaginalComicBookNavigator.java created on Dec 2, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core.paginal;

import tw.funymph.comicsurfer.core.ComicBook;
import tw.funymph.comicsurfer.core.ComicBookSuite;
import tw.funymph.comicsurfer.core.LastViewedComicBookInformer;
import tw.funymph.comicsurfer.core.NavigationModel;
import tw.funymph.comicsurfer.core.Page;
import tw.funymph.comicsurfer.core.events.NavigationEventListener;
import tw.funymph.comicsurfer.core.events.NavigationEventListenerList;

/**
 * The implementation of <code>NavigationModel</code> interface to support
 * paginal navigation.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.3
 */
public class PaginalComicBookNavigator implements NavigationModel {

	private int currentPageIndex;
	private int currentVolumeIndex;
	private int previousPageIndex;
	private int previousVolumeIndex;

	private boolean canGoBack;

	private Page currentPage;
	private ComicBook currentBook;
	private ComicBookSuite bookSuite;
	private LastViewedComicBookInformer informer;
	private NavigationEventListenerList listeners;

	/**
	 * Construct a <code>PaginalComicBookNavigator</code> instance with
	 * the data source of the user's preferences and other information.
	 * 
	 * @param theInformer the last view record informer.
	 */
	public PaginalComicBookNavigator(LastViewedComicBookInformer theInformer) {
		informer = theInformer;
		listeners = new NavigationEventListenerList(this);
		reset();
	}

	@Override
	public void openComicBook(ComicBookSuite booksToOpen) {
		if(booksToOpen == null) {
			listeners.fireSuiteOpenFailedEvent();
			return;
		}
		else {
			if(isSameBookSuite(booksToOpen)) {
				return;
			}
			listeners.fireSuiteOpeningEvent();
			closeComicBook();
			openComicBookToLastViewLocation(booksToOpen);
			listeners.fireSuiteOpenedEvent();
		}
	}

	@Override
	public void closeComicBook() {
		if(currentBook != null) {
			listeners.fireSuiteClosingEvent();
			reset();
			listeners.fireSuiteClosedEvent();
		}
	}

	@Override
	public boolean hasNextPage() {
		return hasNexPageInCurrentVolume()? true : hasNextVolume();
	}

	@Override
	public boolean hasNextVolume() {
		return (bookSuite == null)? false : currentVolumeIndex < (bookSuite.getComicBookCount() - 1);
	}

	@Override
	public boolean hasPreviousPage() {
		return hasPreviousPageInCurrentBook()? true : hasPreviousVolume();
	}

	@Override
	public boolean hasPreviousVolume() {
		return currentVolumeIndex > 0;
	}

	@Override
	public boolean canGoBack() {
		return canGoBack;
	}

	@Override
	public int getCurrentPageIndex() {
		return currentPageIndex;
	}

	@Override
	public int getCurrentVolumeIndex() {
		return currentVolumeIndex;
	}

	@Override
	public int getPageCount() {
		return (currentBook != null)? currentBook.getPageCount() : 0;
	}

	@Override
	public int getVolumeCount() {
		return (bookSuite != null)? bookSuite.getComicBookCount() : 0;
	}

	@Override
	public ComicBookSuite getCurrentSuite() {
		return bookSuite;
	}

	@Override
	public void back() {
		if(canGoBack()) {
			// Save the page indexes because they will be changed in
			// jumpToVolume() and jumpToPage() methods
			int reservedPreviousPageIndex = previousPageIndex;
			int reservedCurrentPageIndex = currentPageIndex;
			jumpToVolume(previousVolumeIndex);
			jumpToPage(reservedPreviousPageIndex);
			previousPageIndex = reservedCurrentPageIndex;
		}
	}

	@Override
	public void nextPage() {
		if(hasNexPageInCurrentVolume()) {
			canGoBack = true;
			jumpToPage(currentPageIndex + 1);
		}
		else {
			nextVolume();
		}
	}

	@Override
	public void nextVolume() {
		if(hasNextVolume()) {
			canGoBack = true;
			jumpToVolume(currentVolumeIndex + 1);
		}
	}

	@Override
	public void previousPage() {
		if(hasPreviousPageInCurrentBook()) {
			canGoBack = true;
			jumpToPage(currentPageIndex - 1);
		}
		else {
			previousVolume(true);
		}
	}

	@Override
	public void previousVolume(boolean gotoLastPage) {
		if(hasPreviousVolume()) {
			canGoBack = true;
			jumpToVolume(currentVolumeIndex - 1);
			if(gotoLastPage) {
				openPage(currentBook.getPageCount() - 1);
			}
		}
	}

	@Override
	public Page currentPage() {
		return currentPage;
	}

	@Override
	public void jumpToPage(int index) {
		if(currentBook != null) {
			if(index >= 0 && index < currentBook.getPageCount() && currentPageIndex != index) {
				previousVolumeIndex = currentVolumeIndex;
				openPage(index);
			}
		}
	}

	@Override
	public void jumpToVolume(int index) {
		jumpToPageOnVolume(index, 0);
	}

	@Override
	public void addNavigationEventListener(NavigationEventListener listener) {
		listeners.addComicSurferEventListener(listener);
	}

	@Override
	public void removeNavigationEventListener(NavigationEventListener listener) {
		listeners.removeComicSurferEventListener(listener);
	}

	/**
	 * Check whether the given books suite is the same as the current viewing book
	 * suite (if it exists).
	 * 
	 * @param anotherSuite the book suite to be checked
	 * @return true if the given book suite is the same as the current viewing book suite
	 */
	private boolean isSameBookSuite(ComicBookSuite anotherSuite) {
		return bookSuite != null && bookSuite.getSuitePath().equals(anotherSuite.getSuitePath());
	}

	/**
	 * Open the read in comic book suite to last viewed location if
	 * the location exists in the view history.
	 * 
	 * @param suiteToOpen the read in comic book suite
	 */
	private void openComicBookToLastViewLocation(ComicBookSuite suiteToOpen) {
		bookSuite = suiteToOpen;
		bookSuite.open();
		int page = informer.getLastViewedPage(bookSuite.getSuiteName());
		int volume = informer.getLastViewedVolume(bookSuite.getSuiteName());
		bookSuite.getComicBook(volume).open();
		jumpToPageOnVolume(volume, page);
		previousPageIndex = page;
		previousVolumeIndex = volume;
	}

	/**
	 * Jump to the specific volume and page.
	 * 
	 * @param volume the volume to be opened
	 * @param page the page to be opened
	 */
	private void jumpToPageOnVolume(int volume, int page) {
		if(bookSuite != null) {
			if(volume >= 0 && volume < bookSuite.getComicBookCount() && currentVolumeIndex != volume) {
				if(currentBook != null) {
					currentBook.close();
				}
				previousVolumeIndex = currentVolumeIndex;
				currentVolumeIndex = volume;
				currentBook = bookSuite.getComicBook(currentVolumeIndex);
				listeners.fireVolumeChangingEvent();
				currentBook.open();
				listeners.fireVolumeChangedEvent();
				openPage(page);
			}
		}
	}

	/**
	 * Get whether the current volume has the next page.
	 * 
	 * @return true if the current volume has the next page
	 */
	private boolean hasNexPageInCurrentVolume() {
		return (currentBook == null)? false : currentPageIndex < (currentBook.getPageCount() - 1);
	}

	/**
	 * Get whether the current volume has the previous page.
	 * 
	 * @return true if the current volume has the previous page
	 */
	private boolean hasPreviousPageInCurrentBook() {
		return (currentBook == null)? false : (currentPageIndex > 0);
	}

	/**
	 * Open the specified page
	 * 
	 * @param index the page index
	 */
	private void openPage(int index) {
		previousPageIndex = currentPageIndex;
		currentPageIndex = index;
		currentPage = currentBook.getPage(currentPageIndex);
		listeners.firePageChangedEvent();
	}

	/**
	 * Reset all status of the navigator.
	 */
	private void reset() {
		if(bookSuite != null) {
			bookSuite.close();
			bookSuite = null;
		}
		bookSuite = null;
		currentBook = null;
		currentPage = null;
		currentPageIndex = -1;
		currentVolumeIndex = -1;
		canGoBack = false;
	}
}
