/* NotificationCenter.java created on 2013/7/21
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

import java.util.Map;

/**
 * A class can implement this interface as a notification center to manage
 * (1) the registration of receivers and (2) notification to the registered
 * receivers.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public interface NotificationCenter {

	/**
	 * Add the given receiver as the subscriber of the message.
	 *  
	 * @param message the interested message
	 * @param receiver the notification receiver
	 */
	public void subscribe(String message, NotificationReceiver receiver);

	/**
	 * Remove the given receiver from all subscribed message notification
	 * list.
	 * 
	 * @param receiver the receiver to be removed
	 */
	public void unsubscribe(NotificationReceiver receiver);

	/**
	 * Remove the given receiver from the specified subscribed message
	 * notification list.
	 * 
	 * @param message the name of the message notification list
	 * @param receiver the receiver to be removed
	 */
	public void unsubscribe(String message, NotificationReceiver receiver);

	/**
	 * Publish the notification to all registered receivers.
	 * 
	 * @param notification the notification to be published
	 */
	public void publish(Notification notification);

	/**
	 * Publish the given data as a notification to all registered receivers.
	 * 
	 * @param sender the message sender
	 * @param message the message
	 * @param data the extra data
	 */
	public void publish(Object sender, String message, Map<String, Object> data);
}
