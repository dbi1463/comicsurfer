/* MotionController.java created on Dec 2, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

/** A class can implement this interface as an adapter to handle the pointer
 * motion events for different platforms.
 *
 * @author Pin-Ying Tu
 * @version 1.3
 * @since 1.3
 */
public interface MotionController {

	/** There is no action bound with the motion event.
	 */
	public static final int NO_ACTION = 0;

	/** The motion event is bound with a primary action.
	 * In the desktop PC, the primary action is the mouse left button pressed.
	 * In the device with a touch panel, the primary action is the touches with one finger.
	 */
	public static final int PRIMARY_ACTION = 1;

	/** The motion event is bound with a accessory action.
	 * In the desktop PC, the primary action is the mouse right button pressed.
	 * In the device with a touch panel, the primary action is the touches with two finger.
	 */
	public static final int ACCESSORY_ACTION = 2;

	/** Invoked when the pointer is moved.
	 * 
	 * @param x the new x location of the pointer
	 * @param y the new y location of the pointer
	 */
	public void moved(int x, int y);

	/** Invoked when the pointer is dragged.
	 * 
	 * @param x the new x location of the pointer
	 * @param y the new y location of the pointer
	 */
	public void dragged(int x, int y);

	/** Invoked when the pointer is pressed.
	 * 
	 * @param x the new x location of the pointer
	 * @param y the new y location of the pointer
	 * @param action the bound action
	 */
	public void pressed(int x, int y, int action);

	/** Invoked when the pointer is released.
	 * 
	 * @param x the new x location of the pointer
	 * @param y the new y location of the pointer
	 * @param action the bound action
	 */
	public void released(int x, int y, int action);

	/** Invoked when the pointer is clicked.
	 * 
	 * @param x the new x location of the pointer
	 * @param y the new y location of the pointer
	 * @param action the bound action
	 * @param count the amount of the clicks
	 */
	public void clicked(int x, int y, int action, int count);

	/** Get the track points for the last motion.
	 * 
	 * @return the track points
	 */
	public MotionTrackPoint[] getMotionTrack();
}
