/* Page.java created on 2012/8/19
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

import java.io.InputStream;

/**
 * A class can implement this interface to offer the functionalities that
 * a comic book page should have.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.5
 */
public interface Page extends Indexable {

	/**
	 * Before invoking {@link #getContent()}, the {@link ComicBook} will invoke this
	 * method to offer a page a chance to prepare any resource or information that are
	 * needed by {@link #getContent()}. Please note that this method will be invoked
	 * once on its container ({@link ComicBook}) opening.
	 */
	public void open();

	/**
	 * Close the page and release the resource. This method will be invoked when its
	 * container ({@link ComicBook}) is closed.
	 */
	public void close();

	/**
	 * Get the input stream from which UI can read the image content. Note that the
	 * UI will close the input stream after reading the image content. Therefore, this
	 * method CANNOT always return the same instance. Each time the return instance
	 * must be the available status.
	 * 
	 * @return the content input stream
	 */
	public InputStream getContent();
}
