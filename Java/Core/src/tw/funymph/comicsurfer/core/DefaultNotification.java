/* DefaultNotification.java created on 2013/7/21
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

import static java.lang.System.currentTimeMillis;

import java.util.HashMap;
import java.util.Map;

/**
 * The default implementation of the {@link Notification}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public class DefaultNotification implements Notification {

	private Map<String, Object> data;
	private Object sender;
	private String message;
	private long timestamp;

	public DefaultNotification(Object theSender, String theMessage, Map<String, Object> theData) {
		sender = theSender;
		message = theMessage;
		data = new HashMap<String, Object>();
		if(theData != null) {
			data.putAll(theData);
		}
		timestamp = currentTimeMillis();
	}

	@Override
	public Map<String, Object> getData() {
		return data;
	}

	@Override
	public Object getSender() {
		return sender;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public long getTimestamp() {
		return timestamp;
	}
}
