/* ReaderJarFileFilter.java created on 2012/9/20
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.core;

import static java.lang.Class.forName;
import static java.lang.String.format;
import static java.lang.System.out;
import static tw.funymph.commons.file.FileUtilities.JAR_FILE_EXTENSION;

import java.io.File;
import java.io.FileFilter;
import java.lang.reflect.Constructor;
import java.net.URLClassLoader;
import java.util.Map;

import tw.funymph.comicsurfer.readers.ComicBookSuiteReader;
import tw.funymph.comicsurfer.readers.ComicBookSuiteReaderFactory;
import tw.funymph.commons.file.FileExtensionFilter;
import tw.funymph.commons.runtime.RuntimeJarFileLoader;
import tw.funymph.commons.xml.AbstractXMLDecoderTreeNode;
import tw.funymph.commons.xml.XMLDecoder;

/**
 * This class uses the Java Reflection API to load third-party readers from the
 * specified path.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class ThirdPartyReaderLoader implements FileFilter {

	private static final String ELEMENT_LOADER = "reader";
	private static final String ATTRIBUTE_NAME = "factory";
	private static final String ATTRIBUTE_PACKAGE = "package";
	private static final String READER_DEFINITION = "reader.xml";

	private XMLDecoder decoder;

	/**
	 * Construct a <code>ThirdPartyReaderLoader</code> instance.
	 */
	public ThirdPartyReaderLoader() {
		decoder = new XMLDecoder();
	}

	@Override
	public boolean accept(File folder) {
		return getReaderDefinition(folder) != null;
	}

	/**
	 * Load the reader from the specified folder.
	 * 
	 * @param readerFolder the folder that contains third-party readers
	 * @return all created readers
	 */
	public ComicBookSuiteReader[] loadThridPartyReaders(File readerFolder) {
		File definition = getReaderDefinition(readerFolder);
		if (definition != null) {
			URLClassLoader classLoader = prepareClassLoader(readerFolder);
			DefinitionDecoderTreeRootNode rootNode = new DefinitionDecoderTreeRootNode(classLoader);
			decoder.decode(rootNode, definition);
			return rootNode.getReaders();
		}
		return null;
	}

	/**
	 * Get the reader definition file from the specified folder.
	 * 
	 * @param folder the folder that may contains the definition file
	 * @return the definition file; null means the folder does not have the definition file
	 */
	private File getReaderDefinition(File folder) {
		if (folder.isDirectory()) {
			File[] files = folder.listFiles();
			for (File file : files) {
				if (READER_DEFINITION.equalsIgnoreCase(file.getName())) {
					return file;
				}
			}
		}
		return null;
	}

	/**
	 * Prepare the class loader for the third-party readers.
	 * 
	 * @param readerFolder the folder that contains library for the third-party readers
	 * @return a class loader
	 */
	private URLClassLoader prepareClassLoader(File readerFolder) {
		RuntimeJarFileLoader classLoader = new RuntimeJarFileLoader();
		File[] libraries = readerFolder.listFiles(new FileExtensionFilter(JAR_FILE_EXTENSION));
		if (libraries != null) {
			for (File library : libraries) {
				classLoader.addJarFile(library);
			}
		}
		return classLoader;
	}

	/**
	 * The class that really uses the Java Reflection API and the information
	 * from the definition file to create readers.
	 * 
	 * @author Pin-Ying Tu
	 * @version 3.0
	 * @since 2.1
	 */
	private class DefinitionDecoderTreeRootNode extends AbstractXMLDecoderTreeNode {

		private static final String FULL_CLASSNAME_TEMPLATE = "%1s.%2s";
		private static final String LOAD_ERROR_MSG_TEMPLATE = "[ThirdPartyReaderLoader] Unable to load the reader: %2s";

		private URLClassLoader classLoader;
		private ComicBookSuiteReader[] readers;

		/**
		 * Construct a <code>DefinitionDecoderTreeRootNode</code> instance with
		 * the specified class loader. The class loader will be the entry to
		 * create objects on using the Java Reflection API.
		 * 
		 * @param loader the class loader
		 */
		public DefinitionDecoderTreeRootNode(URLClassLoader loader) {
			super(ELEMENT_LOADER);
			classLoader = loader;
		}

		/**
		 * Get the created loaders.
		 * 
		 * @return the created loaders
		 */
		public ComicBookSuiteReader[] getReaders() {
			return readers;
		}

		@Override
		public void processAttributes(Map<String, String> attributes) {
			String className = format(FULL_CLASSNAME_TEMPLATE, attributes.get(ATTRIBUTE_PACKAGE), attributes.get(ATTRIBUTE_NAME));
			try {
				Class<?>[] parameterTypes = {};
				Object[] parameters = {};
				Constructor<?> constructor = forName(className, true, classLoader).getConstructor(parameterTypes);
				Object object = constructor.newInstance(parameters);
				if (object instanceof ComicBookSuiteReaderFactory) {
					readers = ((ComicBookSuiteReaderFactory) object).createReaders();
				}
			} catch (Exception e) {
				out.println(format(LOAD_ERROR_MSG_TEMPLATE, className));
			}
		}
	}
}
