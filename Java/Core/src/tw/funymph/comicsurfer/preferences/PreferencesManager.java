/* PreferencesManager.java created on Dec 6, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.preferences;

/**
 * A class can implement this interface to offer the abilities to
 * load and save user preferences from and into storage in different
 * platforms.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.3
 */
public interface PreferencesManager {

	/**
	 * Load the user preferences from the storage.
	 * 
	 * @param preferences the container to keep user preferences
	 */
	public void loadPreferences(Preferences preferences);

	/**
	 * Save the user preferences back into the storage.
	 * 
	 * @param preferences the user preferences to be saved
	 */
	public void savePreferences(Preferences preferences);
}
