/* PreferencesListener.java created on Dec 6, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.preferences;

/**
 * A class can implement this interface to receive and handle the
 * user preferences changed events.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.3
 */
public interface PreferencesListener {

	/**
	 * Invoked when the interested user preferences are changed.
	 * 
	 * @param key the key of the changed preference
	 * @param preferences the changed user preferences
	 */
	public void preferencesChanged(String key, Preferences preferences);
}
