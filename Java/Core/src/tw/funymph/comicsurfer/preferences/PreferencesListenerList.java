/* PreferencesListenerList.java created on Dec 6, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.preferences;

import java.util.ArrayList;

/**
 * A class that manages the <code>PreferencesListener</code> instances
 * and fire events to the registered listeners.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.3
 */
public class PreferencesListenerList {

	private Preferences eventSource;
	private ArrayList<PreferencesListener> listeners;

	/**
	 * Construct a <code>PreferencesListenerList</code> instance with
	 * the event source (preferences). 
	 * 
	 * @param preferences the event source
	 */
	public PreferencesListenerList(Preferences preferences) {
		eventSource = preferences;
		listeners = new ArrayList<PreferencesListener>();
	}

	/**
	 * Add the specific listener into the list.
	 * 
	 * @param listener the listener to be added
	 */
	public void addPreferencesListener(PreferencesListener listener) {
		if(listener != null && !listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	/**
	 * Remove the specific listener from the list.
	 * 
	 * @param listener the listener to be removed
	 */
	public void removePreferencesListener(PreferencesListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Fire an event that the preferences has been changed to all registered listeners.
	 * 
	 * @param key the key of the changed preference
	 */
	public void firePreferencesChangedEvent(String key) {
		for(PreferencesListener listener : listeners) {
			listener.preferencesChanged(key, eventSource);
		}
	}
}
