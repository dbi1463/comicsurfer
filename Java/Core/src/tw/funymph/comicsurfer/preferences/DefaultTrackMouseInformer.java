/* DefaultTrackMouseInformer.java created on 2012/11/17
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.preferences;

import static java.lang.Boolean.parseBoolean;
import static tw.funymph.comicsurfer.preferences.Preferences.KEY_TRACK_MOUSE;
import tw.funymph.comicsurfer.core.TrackMouseInformer;

/** The default implementation of {@link TrackMouseInformer}.
 *
 * @author Pin-Ying Tu
 * @version 2.1 
 * @since 2.1
 */
public class DefaultTrackMouseInformer implements TrackMouseInformer {

	private Preferences informationSource;

	/** Construct a <code>DefaultTrackMouseInformer</code> with the real
	 * information source: {@link Preferences}.
	 * 
	 * @param preferences the information source
	 */
	public DefaultTrackMouseInformer(Preferences preferences) {
		informationSource = preferences;
	}

	@Override
	public boolean isTrackMouse() {
		return parseBoolean(informationSource.getPreference(KEY_TRACK_MOUSE));
	}
}
