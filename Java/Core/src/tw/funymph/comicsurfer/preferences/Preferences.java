/* Preferences.java created on Dec 6, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.preferences;

/**
 * A class can implement this interface to provide a container for the
 * user preferences.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.3
 */
public interface Preferences {

	public static final String KEY_LANGUAGE = "language";
	public static final String KEY_TRACK_MOUSE = "track-mouse";
	public static final String KEY_DISPLAY_MODE = "display-mode";
	public static final String KEY_LAST_OPEN_PATH = "last-open-path";
	public static final String KEY_SLIDESHOW_SPEED = "slideshow-speed";
	public static final String KEY_BACKGROUND_COLOR = "background-color";

	/**
	 * Set the preference value.
	 * 
	 * @param key the key of the preference
	 * @param value the new value of the preference
	 */
	public void setPreference(String key, String value);

	/**
	 * Get the preference value.
	 * 
	 * @param key the key of the preference
	 * @return the preference value
	 */
	public String getPreference(String key);

	/**
	 * Get all preference keys.
	 * 
	 * @return all preference keys
	 */
	public String[] getPreferenceKeys();

	/**
	 * Add the specific listener into the list.
	 * 
	 * @param listener the listener to be added
	 */
	public void addPreferencesListener(PreferencesListener listener);

	/**
	 * Remove the specific listener from the list.
	 * 
	 * @param listener the listener to be removed
	 */
	public void removePreferencesListener(PreferencesListener listener);
}
