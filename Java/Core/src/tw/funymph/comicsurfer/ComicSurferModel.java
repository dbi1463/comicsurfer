/* ComicSurferModel.java created on Dec 2, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer;

import tw.funymph.comicsurfer.core.ImageDisplayModel;
import tw.funymph.comicsurfer.core.MotionController;
import tw.funymph.comicsurfer.core.NavigationModel;
import tw.funymph.comicsurfer.core.slide.SlideShowController;
import tw.funymph.comicsurfer.history.ViewHistory;
import tw.funymph.comicsurfer.preferences.Preferences;
import tw.funymph.comicsurfer.readers.ComicBookSuiteReaderManager;
import tw.funymph.comicsurfer.language.LanguageManager;

/**
 * A class can implement this interface to manage all the business
 * logics of the ComicSurfer for different platforms. This is also a
 * coordinator to maintain the status synchronization between all
 * sub-components.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.3
 */
public interface ComicSurferModel {

	/** Initialize all sub-components.
	 */
	public void initialize();

	/** Close the application and release all allocated resources.
	 */
	public void exit();

	/** Get the main comic book navigator that handles the navigation
	 * between comic books.
	 * 
	 * @return the main comic book navigator
	 */
	public NavigationModel getNavigator();

	/** Get the image display model that manages the display size of
	 * the images.
	 * 
	 * @return the image display model
	 */
	public ImageDisplayModel getDisplayModel();

	/** Get the motion controller that handles the user's input events.
	 * 
	 * @return the motion controller
	 */
	public MotionController getMotionController();

	/** Get the language manager that manages the display text of all
	 * available languages. 
	 * 
	 * @return the language manager
	 */
	public LanguageManager getLanguageManager();

	/** Get the user preferences.
	 * 
	 * @return the user preferences
	 */
	public Preferences getPreferences();

	/** Get the view history.
	 * 
	 * @return the view history
	 */
	public ViewHistory getViewHistory();

	/** Get the comic book suite readers manager.
	 * 
	 * @return the comic book suite readers manager
	 */
	public ComicBookSuiteReaderManager getReaderManager();

	/** Get the slide show controller.
	 * 
	 * @return the slide show controller
	 */
	public SlideShowController getSlideShowController();
}
