/* PreferencesCompatibilityDecodeNode.java created on 2012/9/27
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.compatibility;

import tw.funymph.comicsurfer.preferences.Preferences;
import tw.funymph.commons.xml.AbstractXMLDecoderTreeNode;

/** This class is designed for compatible with the old preference format.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class PreferencesV1DecoderNode extends AbstractXMLDecoderTreeNode {

	private Preferences preferences;

	/** Construct a <code>PreferencesV1DecoderNode</code> instance with
	 * the name and the preference container.
	 * 
	 * @param name the name of the XML node
	 * @param preferences the preference container
	 */
	public PreferencesV1DecoderNode(String name, Preferences thePreferences) {
		super(name);
		preferences = thePreferences;
	}

	@Override
	public void processContent(String content) {
		preferences.setPreference(getName(), content);
	}
}
