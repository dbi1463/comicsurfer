/* ViewRecordV1DecoderNode.java created on 2013/7/14
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.compatibility;

import static java.lang.Integer.parseInt;
import static tw.funymph.commons.assertion.AssertUtilities.isNotBlank;
import static tw.funymph.commons.assertion.AssertUtilities.isNotNull;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import tw.funymph.comicsurfer.history.ViewHistory;
import tw.funymph.commons.xml.AbstractXMLDecoderTreeNode;

/**
 * The leaf node to decode/encode information between the XML file and
 * the view record. This is kept for compatibility.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class ViewRecordV1DecoderNode extends AbstractXMLDecoderTreeNode {

	private static final String ELEMENT_RECORD = "record";

	private static final String ATTRIBUTE_URI = "uri";
	private static final String ATTRIBUTE_NAME = "name";
	private static final String ATTRIBUTE_PATH = "path";
	private static final String ATTRIBUTE_PAGE = "page";
	private static final String ATTRIBUTE_VOLUME = "volume";

	private ViewHistory viewHistory;

	/**
	 * Construct a <code>ViewRecordCodecNode</code> instance with the
	 * view history.
	 * 
	 * @param history the view history
	 */
	public ViewRecordV1DecoderNode(ViewHistory history) {
		super(ELEMENT_RECORD);
		viewHistory = history;
	}

	@Override
	public void processAttributes(Map<String, String> attributes) {
		String name = attributes.get(ATTRIBUTE_NAME);
		if(!attributes.containsKey(ATTRIBUTE_VOLUME)) {
			return;
		}
		int volume = parseInt(attributes.get(ATTRIBUTE_VOLUME));
		int page = parseInt(attributes.get(ATTRIBUTE_PAGE));
		URI path = attributes.containsKey(ATTRIBUTE_PATH)? new File(attributes.get(ATTRIBUTE_PATH)).toURI() : null;
		try {
			URI uri = attributes.containsKey(ATTRIBUTE_URI)? new URI(attributes.get(ATTRIBUTE_URI)) : path;
			if(isNotNull(uri) && isNotBlank(name)) {
				viewHistory.setLastViewRecord(name, uri, volume, page, 0);
			}
		} catch (URISyntaxException e) {
			System.out.println(e.getMessage());
		}
	}
}
