/* LastViewedRecord.java created on Dec 6, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.history;

import java.net.URI;

/**
 * A pure data object to record the last closed comic book.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.3
 */
public class ViewRecord {

	private int setIndex;
	private int position;
	private int mediaIndex;
	
	private URI uri;
	private String name;

	/**
	 * Construct a <code>ViewRecord</code> instance.
	 * 
	 * @param suiteName the name of the last viewed comic book suite
	 * @param suiteURI the URI (file, folder, or URL) of the last view comic book suite
	 * @param set the index of the last viewed media set
	 * @param media the index of the last viewed media
	 * @param pos the position in the last viewed meida
	 */
	public ViewRecord(String suiteName, URI suiteURI, int set, int media, int pos) {
		name = suiteName;
		uri = suiteURI;
		
		setIndex = set;
		position = pos;
		mediaIndex = media;
	}

	/**
	 * Get the name of the last viewed comic book suite.
	 * 
	 * @return the name of the last viewed comic book suite
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get the path of the last viewed comic book suite
	 * 
	 * @return the path of the last viewed comic book suite
	 */
	public URI getURI() {
		return uri;
	}

	/**
	 * Get the index of the last viewed volume.
	 * 
	 * @return the index of the last viewed volume
	 */
	public int getMediaSetIndex() {
		return setIndex;
	}

	/**
	 * Get the index of the last viewed page.
	 * 
	 * @return the index of the last viewed page
	 */
	public int getMediaIndex() {
		return mediaIndex;
	}

	/**
	 * Get the position in the last viewed media.
	 * 
	 * @return the position in the last viewed media
	 */
	public int getPosition() {
		return position;
	}
}
