/* RecentViewHistory.java created on 2012/9/26
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.history;

import java.util.Collection;

import tw.funymph.commons.collection.RecentHistory;

/**
 * The implementation of {@link ViewHistory} that uses {@link RecentHistory}
 * as the container to keep the recent <i>n</i> records.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.5
 */
public class RecentViewHistory extends AbstractViewHistory {

	private RecentHistory<ViewRecord> records;

	/**
	 * Construct a <code>RecentViewHistory</code> with the maximum
	 * capacity of records to be keep in the history.
	 * 
	 * @param capacity the maximum capacity
	 */
	public RecentViewHistory(int capacity) {
		records = new RecentHistory<ViewRecord>(capacity);
	}

	@Override
	public int getMaximumRecordCapacity() {
		return records.getMaximumRecentEventCapacity();
	}

	@Override
	public ViewRecord getRecord(int index) {
		return records.get(index);
	}

	@Override
	public void setMaximumRecordCapacity(int capacity) {
		records.setMaximumRecentEventCapacity(capacity);
	}

	@Override
	protected Collection<ViewRecord> getContainer() {
		return records;
	}
}
