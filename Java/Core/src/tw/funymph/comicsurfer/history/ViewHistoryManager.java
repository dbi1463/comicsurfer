/* ViewHistoryManager.java created on 2012/9/26
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.history;

/**
 * A class can implement this interface to load/save the view
 * history from/to different media (e.g., file or database).
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public interface ViewHistoryManager {

	/**
	 * Load the view records and add then into the container.
	 * 
	 * @param history the view record container
	 */
	public void loadHistory(ViewHistory history);

	/**
	 * Save the view records from the container into the storage.
	 * 
	 * @param history the history to be saved
	 */
	public void saveHistory(ViewHistory history);

	/**
	 * Set the path to write/read the view history.
	 * 
	 * @param path the path to write/read the view history
	 * @since 3.0
	 */
	public void setPath(String path);
}
