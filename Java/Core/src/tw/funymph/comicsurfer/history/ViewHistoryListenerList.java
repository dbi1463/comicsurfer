/* ViewHistoryListenerList.java created on 2012/9/26
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.history;

import java.util.LinkedList;

/**
 * A utility class that manage a collection of view history listeners
 * and notify all listeners.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class ViewHistoryListenerList {

	private ViewHistory viewHistory;
	private LinkedList<ViewHistoryListener> listeners;

	/**
	 * Construct a <code>ViewHistoryListenerList</code> instance with
	 * the event source.
	 * 
	 * @param history the event source
	 */
	public ViewHistoryListenerList(ViewHistory history) {
		viewHistory = history;
		listeners = new LinkedList<ViewHistoryListener>();
	}

	/**
	 * Notify all view history listeners that the interested view
	 * history is changed.
	 */
	public void notifyViewHistoryUpdated() {
		for(ViewHistoryListener listener : listeners) {
			listener.viewHistoryUpdated(viewHistory);
		}
	}

	/**
	 * Add the specific listener that wants to receive the view history
	 * changed events into the notification list.
	 * 
	 * @param listener the listener to be added
	 */
	public void addViewHistoryListener(ViewHistoryListener listener) {
		if(!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	/**
	 * Remove the specific listener from the notification list.
	 * 
	 * @param listener the listener to be removed
	 */
	public void removeViewHistoryListener(ViewHistoryListener listener) {
		listeners.remove(listener);
	}
}
