/* SimpleViewHistory.java created on 2013/6/2
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.history;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A simple implementation of {@link ViewHistory}. The difference between this
 * implementation and {@link RecentViewHistory} is that the {@link RecentViewHistory}
 * has the maximum capacity, and this implementation does not.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public class SimpleViewHistory extends AbstractViewHistory {

	private List<ViewRecord> records;

	/**
	 * Construct a <code>SimpleViewHistory</code> instance.
	 */
	public SimpleViewHistory() {
		records = new ArrayList<ViewRecord>();
	}

	@Override
	public int getMaximumRecordCapacity() {
		return records.size();
	}

	@Override
	public ViewRecord getRecord(int index) {
		return records.get(index);
	}

	@Override
	public void setMaximumRecordCapacity(int capacity) {
		throw new UnsupportedOperationException("The SimpleViewHistory does not provide the implementation");
	}

	@Override
	protected Collection<ViewRecord> getContainer() {
		return records;
	}
}
