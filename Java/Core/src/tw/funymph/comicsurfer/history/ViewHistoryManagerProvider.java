/* ViewHistoryManagerProvider.java created on 2013/6/2
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.history;

/**
 * A class can implement this interface as a factory to provide the instance
 * of {@link ViewHistoryManager}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public interface ViewHistoryManagerProvider {

	/**
	 * Get the instance of {@link ViewHistoryManager}.
	 * 
	 * @param singleton true to get the singleton of {@link ViewHistoryManager}
	 * @return the instance of {@link ViewHistoryManager}
	 */
	public ViewHistoryManager getViewHistoryManager(boolean singleton);
}
