/* ViewRecordV1CodecNode.java created on 2013/7/14
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.history;

import static java.lang.Integer.parseInt;
import static java.lang.String.valueOf;
import static tw.funymph.commons.assertion.AssertUtilities.isNotBlank;
import static tw.funymph.commons.assertion.AssertUtilities.isNotNull;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Hashtable;
import java.util.Map;

import tw.funymph.comicsurfer.history.ViewHistory;
import tw.funymph.comicsurfer.history.ViewRecord;
import tw.funymph.commons.xml.AbstractXMLCodecTreeNode;

/**
 * The leaf node to decode/encode information between the XML file and
 * the view record. This is kept for compatibility.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class ViewRecordCodecNode extends AbstractXMLCodecTreeNode {

	private static final String ELEMENT_RECORD = "record";

	private static final String ATTRIBUTE_URI = "uri";
	private static final String ATTRIBUTE_NAME = "name";
	private static final String ATTRIBUTE_PATH = "path";
	private static final String ATTRIBUTE_MEDIA = "media";
	private static final String ATTRIBUTE_MEDIA_SET = "media-set";
	private static final String ATTRIBUTE_POSITION = "position";

	private ViewHistory viewHistory;

	/**
	 * Construct a <code>ViewRecordCodecNode</code> instance with the
	 * view history.
	 * 
	 * @param history the view history
	 */
	public ViewRecordCodecNode(ViewHistory history) {
		super(ELEMENT_RECORD);
		viewHistory = history;
	}

	@Override
	public void processAttributes(Map<String, String> attributes) {
		String name = attributes.get(ATTRIBUTE_NAME);
		int mediaSet = parseInt(attributes.get(ATTRIBUTE_MEDIA_SET));
		int media = parseInt(attributes.get(ATTRIBUTE_MEDIA));
		int position = attributes.containsKey(ATTRIBUTE_POSITION)? parseInt(attributes.get(ATTRIBUTE_POSITION)) : 0;
		URI path = attributes.containsKey(ATTRIBUTE_PATH)? new File(attributes.get(ATTRIBUTE_PATH)).toURI() : null;
		try {
			URI uri = attributes.containsKey(ATTRIBUTE_URI)? new URI(attributes.get(ATTRIBUTE_URI)) : path;
			if(isNotNull(uri) && isNotBlank(name)) {
				viewHistory.setLastViewRecord(name, uri, mediaSet, media, position);
			}
		} catch (URISyntaxException e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public int getRepeatCount() {
		return viewHistory.getRecordCount();
	}

	@Override
	public Map<String, String> getAttributes(int index) {
		ViewRecord record = viewHistory.getRecord(index);
		Hashtable<String, String> attributes = new Hashtable<String, String>();
		attributes.put(ATTRIBUTE_NAME, record.getName());
		attributes.put(ATTRIBUTE_URI, record.getURI().toString());
		attributes.put(ATTRIBUTE_MEDIA_SET, valueOf(record.getMediaSetIndex()));
		attributes.put(ATTRIBUTE_MEDIA, valueOf(record.getMediaIndex()));
		attributes.put(ATTRIBUTE_POSITION, valueOf(record.getPosition()));
		return attributes;
	}
}
