/* XMLViewHistoryManager.java created on 2012/9/26
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.history;

import java.io.File;
import java.util.Hashtable;
import java.util.Map;

import tw.funymph.comicsurfer.compatibility.ViewRecordV1DecoderNode;
import tw.funymph.commons.file.FileUtilities;
import tw.funymph.commons.xml.AbstractXMLDecoderTreeNode;
import tw.funymph.commons.xml.AbstractXMLEncoderTreeNode;
import tw.funymph.commons.xml.XMLDecoder;
import tw.funymph.commons.xml.XMLEncoder;

/**
 * The implementation of {@link ViewHistoryManager} that use an XML
 * file to store the view records.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class XMLViewHistoryManager implements ViewHistoryManager {

	private static final String DEFAULT_PATH = "history.xml";

	private static final String ELEMENT_ROOT = "history";
	private static final String ATTRIBUTE_VERSION = "version";

	private static final int CURRENT_VERSION = 2;

	private String historyPath;

	private XMLEncoder encoder;
	private XMLDecoder decoder;

	/**
	 * Construct a <code>XMLViewHistoryManager</code> instance.
	 */
	public XMLViewHistoryManager() {
		this(DEFAULT_PATH);
	}

	/**
	 * Construct a <code>XMLViewHistoryManager</code> instance.
	 * 
	 * @param path the history path
	 */
	public XMLViewHistoryManager(String path) {
		setPath(path);
		encoder = new XMLEncoder();
		decoder = new XMLDecoder();
	}

	@Override
	public synchronized void setPath(String path) {
		historyPath = path;
	}

	@Override
	public synchronized void loadHistory(ViewHistory history) {
		File file = new File(historyPath);
		ViewHistoryDecoderTreeRootNode root = new ViewHistoryDecoderTreeRootNode(history);
		if(file.exists()) {
			decoder.decode(root, file);
		}
	}

	@Override
	public synchronized void saveHistory(ViewHistory history) {
		ViewHistoryEncoderTreeRootNode root = new ViewHistoryEncoderTreeRootNode(history);
		File file = new File(historyPath);
		File parentFolder = file.getParentFile();
		if(parentFolder != null && !parentFolder.exists()) {
			FileUtilities.makeFolderIfNotExist(file.getParentFile());
		}
		encoder.encode(root, file);
	}

	/**
	 * The root encoder of the view history encoder tree.
	 *
	 * @author Pin-Ying Tu
	 * @version 3.0
	 * @since 2.1
	 */
	private static class ViewHistoryEncoderTreeRootNode extends AbstractXMLEncoderTreeNode {

		private ViewHistory viewHistory;

		/** Construct a <code>ViewHistoryEncoderTreeRootNode</code> instance to encode
		 * the specified view history
		 * 
		 * @param history the view history to be encoded
		 */
		public ViewHistoryEncoderTreeRootNode(ViewHistory history) {
			super(ELEMENT_ROOT);
			viewHistory = history;
			addChildEncoder(new ViewRecordCodecNode(viewHistory));
		}

		@Override
		public Map<String, String> getAttributes(int index) {
			Hashtable<String, String> attributes = new Hashtable<String, String>();
			attributes.put(ATTRIBUTE_VERSION, String.valueOf(CURRENT_VERSION));
			return attributes;
		}
	}

	/**
	 * The root decoder of the view history XML decoder tree.
	 * 
	 * @author Pin-Ying Tu
	 * @version 3.0
	 * @since 2.1
	 */
	private static class ViewHistoryDecoderTreeRootNode extends AbstractXMLDecoderTreeNode {

		private ViewHistory viewHistory;

		/**
		 * Construct <code>ViewHistoryDecoderTreeRootNode</code> instance to
		 * decode the information for the view history
		 * 
		 * @param history the view record container
		 */
		public ViewHistoryDecoderTreeRootNode(ViewHistory history) {
			super(ELEMENT_ROOT);
			viewHistory = history;
			addChildNode(new ViewRecordCodecNode(viewHistory));
		}

		@Override
		public void processAttributes(Map<String, String> attributes) {
			if(!attributes.containsKey(ATTRIBUTE_VERSION)) {
				addChildNode(new ViewRecordV1DecoderNode(viewHistory));
			}
		}
	}
}
