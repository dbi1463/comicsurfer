/* LastViewedRecordUpdater.java created on 2012/11/17
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.history;

import static tw.funymph.comicsurfer.preferences.Preferences.KEY_LAST_OPEN_PATH;
import static tw.funymph.commons.io.IOConstants.URI_SCHEME_FILE;

import java.io.File;
import java.net.URI;

import tw.funymph.comicsurfer.core.events.NavigationEvent;
import tw.funymph.comicsurfer.core.events.NavigationEventListener;
import tw.funymph.comicsurfer.preferences.Preferences;

/**
 * A {@link NavigationEventListener} whose responsibility is to update
 * the last view record on comic book suite opened and closing.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class LastViewedRecordUpdater implements NavigationEventListener {

	private ViewHistory history;
	private Preferences preferences;

	/**
	 * Construct a <code>LastViewedRecordUpdater</code> instance with
	 * the update targets: ViewHistory and Preferences.
	 * 
	 * @param theHistory one of the update target
	 * @param thePreferences another update target
	 */
	public LastViewedRecordUpdater(ViewHistory theHistory, Preferences thePreferences) {
		history = theHistory;
		preferences = thePreferences;
	}

	@Override
	public void pageChanged(NavigationEvent event) {}

	@Override
	public void volumeChanging(NavigationEvent event) {}

	@Override
	public void volumeChanged(NavigationEvent event) {}

	@Override
	public void suiteClosing(NavigationEvent event) {
		int currentVolumeIndex = event.getCurrentVolumeIndex();
		int currentPageIndex = event.getCurrentPageIndex();
		history.setLastViewRecord(event.getSuiteName(), event.getSuitePath(), currentVolumeIndex, currentPageIndex, 0);
	}

	@Override
	public void suiteClosed(NavigationEvent event) {}

	@Override
	public void suiteOpening(NavigationEvent event) {}

	@Override
	public void suiteOpened(NavigationEvent event) {
		URI pathURI = event.getSuitePath();
		if(URI_SCHEME_FILE.equalsIgnoreCase(pathURI.getScheme())) {
			String path = new File(pathURI).getParentFile().getAbsolutePath();
			preferences.setPreference(KEY_LAST_OPEN_PATH, path);
		}
	}

	@Override
	public void suiteOpenFailed() {}
}
