/* ViewHistoryListener.java created on 2012/9/26
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.history;

/**
 * A class can implement this interface to receive the view history
 * changed events.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public interface ViewHistoryListener {

	/**
	 * Invoked when the view history is changed.
	 * 
	 * @param history the updated history
	 */
	public void viewHistoryUpdated(ViewHistory history);
}
