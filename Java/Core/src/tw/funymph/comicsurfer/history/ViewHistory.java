/* ViewHistory.java created on 2012/9/26
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.history;

import java.net.URI;
import java.util.Collection;

/**
 * A class can implement this interface to offer a container to
 * manage view records.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public interface ViewHistory {

	/**
	 * Get the number of records in the history
	 * 
	 * @return the number of records
	 */
	public int getRecordCount();

	/**
	 * Get the maximum capacity that the container can keep.
	 * 
	 * @return the maximum capacity
	 */
	public int getMaximumRecordCapacity();

	/**
	 * Get the last record.
	 * 
	 * @return the last record
	 */
	public ViewRecord getLastRecord();

	/**
	 * Get the record specified by the index.
	 * 
	 * @param index the index of the record in the history
	 * @return the specified record
	 */
	public ViewRecord getRecord(int index);

	/**
	 * Get the record specified by the comic book suite name
	 * 
	 * @param name the comic book suite name
	 * @return the specified record
	 */
	public ViewRecord getRecord(String name);

	/**
	 * Get all records.
	 * 
	 * @return All records
	 */
	public Collection<ViewRecord> getRecords();

	/**
	 * Set the index of the last viewed volume and page for the comic book
	 * suite specified by the name. This method should fire the preferences
	 * changed event to all registered listeners.
	 * 
	 * @param name the name of the comic book suite
	 * @param path the path of the comic book suite
	 * @param volume the index of the last viewed volume
	 * @param page the index of the last viewed page
	 */
	public void setLastViewRecord(String name, URI path, int volume, int page, int position);

	/**
	 * Set the maximum capacity. If the new capacity is smaller than the
	 * current capacity, the implementation can decide to drop records based
	 * on their owned algorithm.
	 * 
	 * @param capacity the maximum capacity
	 */
	public void setMaximumRecordCapacity(int capacity);

	/**
	 * Add the specific listener that wants to receive the view history
	 * changed events into the notification list.
	 * 
	 * @param listener the listener to be added
	 */
	public void addViewHistoryListener(ViewHistoryListener listener);

	/**
	 * Remove the specific listener from the notification list.
	 * 
	 * @param listener the listener to be removed
	 */
	public void removeViewHistoryListener(ViewHistoryListener listener);
}
