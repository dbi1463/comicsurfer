/* XMLViewHistoryManagerProvider.java created on 2013/6/2
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.history;

/**
 * The implementation of {@link ViewHistoryManagerProvider} to provide the
 * XML implementation of {@link ViewHistoryManager}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public class XMLViewHistoryManagerProvider implements ViewHistoryManagerProvider {

	private static ViewHistoryManager instance;

	/**
	 * Get the singleton instance of the {@link ViewHistoryManager}.
	 * 
	 * @return the singleton instance of the {@link ViewHistoryManager}
	 */
	private static ViewHistoryManager getInstance() {
		if(instance == null) {
			instance = new XMLViewHistoryManager();
		}
		return instance;
	}

	@Override
	public ViewHistoryManager getViewHistoryManager(boolean singleton) {
		if(singleton) {
			return getInstance();
		}
		return new XMLViewHistoryManager();
	}
}
