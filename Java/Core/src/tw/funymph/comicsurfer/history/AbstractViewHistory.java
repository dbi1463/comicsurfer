/* AbstractViewHistory.java created on 2013/6/2
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.history;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;

import tw.funymph.comicsurfer.core.ReferenceAttachable;

/**
 * The abstract class implements the most common functionalities of a {@link ViewHistory}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public abstract class AbstractViewHistory implements ViewHistory, ReferenceAttachable<ViewHistory> {

	protected ViewRecord lastRecord;
	protected ViewHistory historyReference;
	protected ViewHistoryListenerList listeners;

	/**
	 * Construct a <code>AbstractViewHistory</code> instance by it concrete classes.
	 */
	protected AbstractViewHistory() {
		listeners = new ViewHistoryListenerList(this);
	}

	@Override
	public int getRecordCount() {
		return getContainer().size();
	}

	@Override
	public ViewRecord getLastRecord() {
		return lastRecord;
	}

	@Override
	public ViewRecord getRecord(String name) {
		Collection<ViewRecord> records = getContainer();
		for(ViewRecord record : records) {
			if(record.getName().equals(name)) {
				return record;
			}
		}
		return (historyReference != null)? historyReference.getRecord(name) : null;
	}

	@Override
	public Collection<ViewRecord> getRecords() {
		return new ArrayList<ViewRecord>(getContainer());
	}

	@Override
	public void setLastViewRecord(String name, URI path, int set, int media, int position) {
		ViewRecord record = getRecord(name);
		if(record != null) {
			getContainer().remove(record);
		}
		getContainer().add(lastRecord = new ViewRecord(name, path, set, media, position));
		if(historyReference != null) {
			historyReference.setLastViewRecord(name, path, set, media, position);
		}
		listeners.notifyViewHistoryUpdated();
	}

	@Override
	public void addViewHistoryListener(ViewHistoryListener listener) {
		listeners.addViewHistoryListener(listener);
	}

	@Override
	public void removeViewHistoryListener(ViewHistoryListener listener) {
		listeners.removeViewHistoryListener(listener);
	}

	@Override
	public void setReference(ViewHistory reference) {
		historyReference = reference;
	}

	/**
	 * Get the real container that contains all the view records.
	 * 
	 * @return the container
	 */
	protected abstract Collection<ViewRecord> getContainer();
}
