/* SimpleLastViewedComicBookInformer.java created on 2013/7/14
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.history;

import tw.funymph.comicsurfer.core.LastViewedComicBookInformer;

/**
 * The class wrap the view history to provide a simple implementation
 * of {@link LastViewComicBookInformer}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public class SimpleLastViewedComicBookInformer implements LastViewedComicBookInformer {

	private ViewHistory history;

	/**
	 * Construct a <code>SimpleLastViewedComicBookInformer</code> instance
	 * by wrapping the view history.
	 * 
	 * @param source the view history source
	 */
	public SimpleLastViewedComicBookInformer(ViewHistory source) {
		history = source;
	}

	@Override
	public int getLastViewedVolume(String name) {
		ViewRecord record = history.getRecord(name);
		return (record != null)? record.getMediaSetIndex() : 0;
	}

	@Override
	public int getLastViewedPage(String name) {
		ViewRecord record = history.getRecord(name);
		return (record != null)? record.getMediaIndex() : 0;
	}

	@Override
	public int getLastViewedBlock(String name) {
		ViewRecord record = history.getRecord(name);
		return (record != null)? record.getPosition() : 0;
	}
}
