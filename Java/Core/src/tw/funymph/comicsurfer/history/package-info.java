/* package-info.java created on 2013/6/2
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */

/**
 * This package provides a set of classes to manage the view history.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
package tw.funymph.comicsurfer.history;
