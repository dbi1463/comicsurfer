/* SecondaryLevelViewHistory.java created on 2013/6/2
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.history;

import static tw.funymph.commons.codec.DigestUtilities.md5Hex;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The class implements both {@link ViewHistory} to provide a secondary-level
 * view history.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public class SecondaryLevelViewHistory implements ViewHistory {

	private static final String BASE_PATH = "histories";
	private static final String PATH_TEMPLATE = "%1s/%2s.xml";
	private static final String EXCEPTION_MESSAGE = "The secondary-level view history does not provides this implementation.";

	private String historiesPath;
	private ViewRecord lastRecord;
	private ViewHistoryListenerList listeners;
	private Map<String, ViewHistory> histories;
	private ViewHistoryManagerProvider managerProvider;

	/**
	 * Construct a <code>SecondaryLevelViewHistory</code> with the default path and
	 * the view history manager provider.
	 * 
	 * @param provider the view history manager provider
	 */
	public SecondaryLevelViewHistory(ViewHistoryManagerProvider provider) {
		this(BASE_PATH, provider);
	}

	/**
	 * Construct a <code>SecondaryLevelViewHistory</code> with the specified path and
	 * the view history manager provider.
	 * 
	 * @param base path the path to save/read view histories
	 * @param provider the view history manager provider
	 */
	public SecondaryLevelViewHistory(String basePath, ViewHistoryManagerProvider provider) {
		historiesPath = basePath;
		managerProvider = provider;
		listeners = new ViewHistoryListenerList(this);
		histories = new HashMap<String, ViewHistory>();
	}

	@Override
	public ViewRecord getLastRecord() {
		return lastRecord;
	}

	@Override
	public ViewRecord getRecord(String name) {
		return getViewHistory(name).getRecord(name);
	}

	@Override
	public void setLastViewRecord(String name, URI path, int set, int media, int position) {
		ViewHistory history = getViewHistory(name);
		history.setLastViewRecord(name, path, set, media, position);
		lastRecord = history.getLastRecord();
		listeners.notifyViewHistoryUpdated();
		saveHistory(name, history);
	}

	@Override
	public void addViewHistoryListener(ViewHistoryListener listener) {
		listeners.addViewHistoryListener(listener);
	}

	@Override
	public void removeViewHistoryListener(ViewHistoryListener listener) {
		listeners.removeViewHistoryListener(listener);
	}

	/**
	 * The secondary-level view history does not provides this implementation.
	 * 
	 * @throws UnsupportedOperationException
	 */
	@Override
	public int getRecordCount() {
		throw new UnsupportedOperationException(EXCEPTION_MESSAGE);
	}

	/**
	 * The secondary-level view history does not provides this implementation.
	 * 
	 * @throws UnsupportedOperationException
	 */
	@Override
	public int getMaximumRecordCapacity() {
		throw new UnsupportedOperationException(EXCEPTION_MESSAGE);
	}

	/**
	 * The secondary-level view history does not provides this implementation.
	 * 
	 * @throws UnsupportedOperationException
	 */
	@Override
	public ViewRecord getRecord(int index) {
		throw new UnsupportedOperationException(EXCEPTION_MESSAGE);
	}

	/**
	 * The secondary-level view history does not provides this implementation.
	 * 
	 * @throws UnsupportedOperationException
	 */
	@Override
	public List<ViewRecord> getRecords() {
		throw new UnsupportedOperationException(EXCEPTION_MESSAGE);
	}

	/**
	 * The secondary-level view history does not provides this implementation.
	 */
	@Override
	public void setMaximumRecordCapacity(int capacity) {
		throw new UnsupportedOperationException(EXCEPTION_MESSAGE);
	}

	private ViewHistory getViewHistory(String name) {
		String key = md5Hex(name);
		if(!histories.containsKey(key)) {
			ViewHistory history = loadHistory(key);
			histories.put(key, history);
		}
		return histories.get(key);
	}

	private ViewHistory loadHistory(String key) {
		ViewHistory history = new SimpleViewHistory();
		ViewHistoryManager manager = getHistoryManager(key);
		manager.loadHistory(history);
		return history;
	}

	private void saveHistory(String name, ViewHistory history) {
		String key = md5Hex(name);
		ViewHistoryManager manager = getHistoryManager(key);
		manager.saveHistory(history);
	}

	private ViewHistoryManager getHistoryManager(String key) {
		ViewHistoryManager manager = managerProvider.getViewHistoryManager(false);
		manager.setPath(String.format(PATH_TEMPLATE, historiesPath, key));
		return manager;
	}
}
