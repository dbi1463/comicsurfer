/* DefaultComicBook.java created on 2012/9/17
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers;

import java.util.ArrayList;

import tw.funymph.comicsurfer.core.Page;

/**
 * A simple implementation to offer a container to keep the pages in a comic book.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class DefaultComicBook extends AbstractComicBook {

	private ArrayList<Page> pages;

	/**
	 * Construct a <code>DefaultComicBook</code> instance.
	 */
	public DefaultComicBook() {
		pages = new ArrayList<Page>();
	}

	/**
	 * Add the page into the book.
	 * 
	 * @param page the page to be added
	 */
	public void addPage(Page page) {
		pages.add(page);
	}

	@Override
	public void open() {}

	@Override
	public void close() {
		// Do nothing because the comic book can be reopened.
	}

	@Override
	public int getPageCount() {
		return pages.size();
	}

	@Override
	public Page getPage(int index) {
		return pages.get(index);
	}
}
