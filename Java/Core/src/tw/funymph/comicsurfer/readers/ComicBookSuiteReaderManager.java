/* ComicBookSuiteReaderManager.java created on Oct 27, 2012
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers;

import java.net.URI;

import tw.funymph.comicsurfer.core.ComicBookSuite;

/**
 * A class can implement this interface to manage a collection of
 * comic book suite readers and determine the algorithm to choose
 * one of those readers to read the specified URI.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public interface ComicBookSuiteReaderManager {

	/**
	 * Add a {@link ComicBookSuiteReader} into the manager.
	 * 
	 * @param reader the reader to be added and managed
	 */
	public void addReader(ComicBookSuiteReader reader);

	/**
	 * Add all readers into the manager.
	 * 
	 * @param readers the readers to be added.
	 */
	public void addReaders(ComicBookSuiteReader[] readers);

	/**
	 * Load all third-party readers from the given path.
	 * 
	 * @param path the path that may contains third-party readers
	 */
	public void loadThirdPartyReaders(String path);

	/**
	 * Read a suite of comic books from the specified file with the reader that is
	 * gotten by {@link #getBestFitReader(URI)}.
	 * 
	 * @param uri the URI which contains a suite of comic books
	 * @return a suite of comic books
	 */
	public ComicBookSuite read(URI uri);

	/**
	 * Get a reader that can read the specified URI (file, folder, or URL).
	 * 
	 * @param uri the specified URI (file, folder, or URL)
	 * @return a reader that can read the URI
	 */
	public ComicBookSuiteReader getBestFitReader(URI uri);
}