/* AbstractPage.java created on 2012/9/17
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers;

import tw.funymph.comicsurfer.core.Page;

/**
 * A class that isolates the change of {@link Page} from the plug-in readers.
 * Any plug-in reader can use this class as the base to extend the ability of
 * managing pages from new resources. If the {@link Page} has any new methods,
 * this abstract class will offer the default implementation to avoid compatible
 * issues.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public abstract class AbstractPage implements Page {

	protected int index;

	/**
	 * Construct an instance by its children with the default page index
	 * {@link tw.funymph.comicsurfer.core.ComicSurferConstants#UNDEFINED_INDEX UNDEFINED_INDEX}.
	 */
	protected AbstractPage() {
		this(UNDEFINED_INDEX);
	}

	/**
	 * Construct an instance by its children with the specified index.
	 */
	protected AbstractPage(int theIndex) {
		index = theIndex;
	}

	@Override
	public int getIndex() {
		return index;
	}

	@Override
	public void open() {
		// For compatibility, AbstractComicBookSuite has to provide an empty implementation
		// for the existing third-party plug-in readers.
	}
}
