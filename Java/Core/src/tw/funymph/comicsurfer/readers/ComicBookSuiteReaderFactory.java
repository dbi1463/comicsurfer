/* ComicBookSuiteReaderFactory.java created on 2012/9/17
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers;

/**
 * Any reader plug-in should offer a class that implements this interface
 * to load all readers. The class will be instantiated at runtime by using
 * Java Reflection API, so the class must have a default constructor (has
 * no parameters). 
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public interface ComicBookSuiteReaderFactory {

	/**
	 * Load (instantiate) all readers that are provided by the plug-in package
	 * with all necessary resources.
	 * 
	 * @return all readers
	 */
	public ComicBookSuiteReader[] createReaders();
}
