/* DefaultComicBookSuiteReaderManager.java created on Nov 17, 2012
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import tw.funymph.comicsurfer.core.ComicBookSuite;
import tw.funymph.comicsurfer.core.ThirdPartyReaderLoader;
import tw.funymph.comicsurfer.readers.defaults.*;
import tw.funymph.commons.file.FileUtilities;
import tw.funymph.commons.zip.UTF8ZipFileFactory;
import tw.funymph.commons.zip.ZipFileFactory;

/**
 * A class that loads and manages all comic book suite readers.
 *
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.0
 */
public class DefaultComicBookSuiteReaderManager implements ComicBookSuiteReaderManager {

	private ThirdPartyReaderLoader readerLoader;
	private ArrayList<ComicBookSuiteReader> folderReaders;
	private Hashtable<String, List<ComicBookSuiteReader>> uriReaders;
	private Hashtable<String, List<ComicBookSuiteReader>> fileReaders;

	/**
	 * Construct a <code>DefaultComicBookSuiteReaderManager</code> instance with
	 * the <code>UTF8ZIPFileFactory</code> and default readers.
	 */
	public DefaultComicBookSuiteReaderManager() {
		this(new UTF8ZipFileFactory());
	}

	/**
	 * Construct a <code>DefaultComicBookSuiteReaderManager</code> instance with
	 * the specified ZIP file factory and default readers.
	 * 
	 * @param factory the factory is used to create <code>ZipFile</code>
	 */
	public DefaultComicBookSuiteReaderManager(ZipFileFactory factory) {
		readerLoader = new ThirdPartyReaderLoader();
		folderReaders = new ArrayList<ComicBookSuiteReader>();
		uriReaders = new Hashtable<String, List<ComicBookSuiteReader>>();
		fileReaders = new Hashtable<String, List<ComicBookSuiteReader>>();

		addReader(new FolderPackedFolderBookSuiteReader());
		addReader(new FolderPackedZipBookSuiteReader(factory));
		addReader(new ZipPackedFolderBookSuiteReader(factory));
		addReader(new ZipPackedZipBookSuiteReader(factory));
		addReader(new FolderBookReader());
		addReader(new ZipBookReader(factory));
	}

	@Override
	public void addReader(ComicBookSuiteReader reader) {
		String extension = reader.getExtension();
		String[] schemes = reader.getSupportedSchemes();
		if(reader.isFolderReader()) {
			folderReaders.add(reader);
		}
		else if(extension != null) {
			addReader(extension, reader, fileReaders);
		}
		else if(schemes != null) {
			for(String scheme : schemes) {
				addReader(scheme, reader, uriReaders);
			}
		}
	}

	@Override
	public void addReaders(ComicBookSuiteReader[] readers) {
		if(readers != null) {
			for(ComicBookSuiteReader reader : readers) {
				addReader(reader);
			}
		}
	}

	@Override
	public void loadThirdPartyReaders(String path) {
		File readersFolder = new File(path);
		if(readersFolder.exists()) {
			File[] readerFolders = readersFolder.listFiles(readerLoader);
			if(readerFolders != null) {
				for(File readerFolder : readerFolders) {
					loadThirdPartyReader(readerFolder);
				}
			}
		}
		else {
			readersFolder.mkdirs();
		}
	}

	@Override
	public ComicBookSuite read(URI uri) {
		return read(uri, getBestFitReader(uri));
	}

	@Override
	public ComicBookSuiteReader getBestFitReader(URI uri) {
		if(uri == null) {
			return null;
		}
		File file = FileUtilities.convertFrom(uri);
		List<ComicBookSuiteReader> readers = (file != null)? getFileReaderCandidates(file) : getURIReaderCandidates(uri);
		if(readers != null) {
			for(ComicBookSuiteReader reader : readers) {
				if(reader.canRead(uri)) {
					return reader;
				}
			}
		}
		return null;
	}

	/**
	 * Read a suite of comic books from the specified URI (file, folder, or URL).
	 * 
	 * @param uri the URI which contains a suite of comic books
	 * @param reader the reader
	 * @return a suite of comic books
	 */
	private ComicBookSuite read(URI uri, ComicBookSuiteReader reader) {
		return (uri != null && reader != null)? reader.read(uri) : null;
	}

	/**
	 * Add the reader into the collection.
	 * 
	 * @param key the key for the reader
	 * @param reader the reader to be added
	 * @param collection the collection
	 */
	private void addReader(String key, ComicBookSuiteReader reader, Hashtable<String, List<ComicBookSuiteReader>> collection) {
		if(collection.containsKey(key)) {
			collection.get(key).add(reader);
		}
		else {
			ArrayList<ComicBookSuiteReader> readers = new ArrayList<ComicBookSuiteReader>();
			readers.add(reader);
			collection.put(key, readers);
		}
	}

	/**
	 * Get candidate readers that can read comic book suite from the local storage (File).
	 * 
	 * @param file the local storage
	 * @return the candidate readers
	 */
	private List<ComicBookSuiteReader> getFileReaderCandidates(File file) {
		List<ComicBookSuiteReader> readers = null;
		if(file.isDirectory()) {
			readers = folderReaders;
		}
		else {
			String extension = FileUtilities.getExtension(file);
			if(extension != null && fileReaders.containsKey(extension)) {
				readers = fileReaders.get(extension);
			}
		}
		return readers;
	}

	/**
	 * Get candidate readers that can read comic book suite from the remote storage (URI).
	 * 
	 * @param uri the remote storage
	 * @return the candidate readers
	 */
	private List<ComicBookSuiteReader> getURIReaderCandidates(URI uri) {
		String scheme = uri.getScheme();
		if(scheme != null && uriReaders.containsKey(scheme)) {
			return uriReaders.get(scheme);
		}
		return null;
	}

	/**
	 * Load the readers from the specified folder.
	 * 
	 * @param readerFolder the folder that contains third-party readers
	 */
	private void loadThirdPartyReader(File readerFolder) {
		ComicBookSuiteReader[] readers = readerLoader.loadThridPartyReaders(readerFolder);
		if(readers != null) {
			addReaders(readers);
			System.out.println("    Found " + readers.length + " reader(s) from the \"" + readerFolder.getName() + "\" folder");
		}
	}
}
