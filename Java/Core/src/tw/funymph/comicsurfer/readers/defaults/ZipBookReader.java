/* ZippedBookReader.java created on Oct 29, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static tw.funymph.commons.file.FileUtilities.ZIP_FILE_EXTENSION;
import static tw.funymph.commons.file.FileUtilities.removeExtension;
import static tw.funymph.commons.zip.ZipUtilities.close;
import static tw.funymph.commons.zip.ZipUtilities.containsImage;

import java.io.File;
import java.util.zip.ZipFile;

import tw.funymph.comicsurfer.core.ComicBookSuite;
import tw.funymph.comicsurfer.readers.ComicBookSuiteFileReader;
import tw.funymph.comicsurfer.readers.DefaultComicBookSuite;
import tw.funymph.commons.zip.ZipFileFactory;

/**
 * A reader can read images from a compressed file.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public class ZipBookReader extends ComicBookSuiteFileReader {

	/**
	 * The description of the reader.
	 */
	static final String DESCRIPTION = "The zip file that contains only one book.";

	private ZipFileFactory zipFileFactory;

	/**
	 * Construct a <code>ZipBookReader</code> instance with the
	 * specified platform-dependent ZIP file factory.
	 * 
	 * @param factory the platform-dependent ZIP file factory
	 */
	public ZipBookReader(ZipFileFactory factory) {
		super(ZIP_FILE_EXTENSION);
		zipFileFactory = factory;
	}

	@Override
	public String getDescription() {
		return DESCRIPTION;
	}

	@Override
	protected boolean canReadFile(File file) {
		ZipFile zipFile = zipFileFactory.getZipFile(file);
		boolean result = containsImage(zipFile);
		close(zipFile);
		return result;
	}

	@Override
	protected ComicBookSuite readFile(File file) {
		ZipFile zipFile = zipFileFactory.getZipFile(file);
		if(containsImage(zipFile)) {
			DefaultComicBookSuite books = new DefaultComicBookSuite(removeExtension(file.getName()), file.toURI());
			books.addComicBook(new ZipBook(zipFile));
			return books;
		}
		return null;
	}
}
