/* FolderPackedZipBookSuiteReader.java created on Oct 30, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static tw.funymph.commons.file.FileUtilities.ZIP_FILE_EXTENSION;
import static tw.funymph.commons.zip.ZipUtilities.containsImage;

import java.io.File;

import tw.funymph.comicsurfer.core.ComicBookSuite;
import tw.funymph.comicsurfer.readers.ComicBookSuiteFileReader;
import tw.funymph.commons.file.FileExtensionFilter;
import tw.funymph.commons.zip.ZipFileFactory;

/**
 * A reader can read a suite of compressed comic books from a directory.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public class FolderPackedZipBookSuiteReader extends ComicBookSuiteFileReader {

	/**
	 * The description of the reader.
	 */
	static final String DESCRIPTION = "The directory that contains a suite of zipped books.";

	private ZipFileFactory zipFileFactory;

	/**
	 * Construct a <code>FolderPackedZipBookReader</code> instance with the
	 * specified platform-dependent ZIP file factory.
	 * 
	 * @param factory the platform-dependent ZIP file factory
	 */
	public FolderPackedZipBookSuiteReader(ZipFileFactory factory) {
		zipFileFactory = factory;
	}

	@Override
	public String getDescription() {
		return DESCRIPTION;
	}

	@Override
	protected boolean canReadFile(File file) {
		if(file.isDirectory()) {
			File zipfiles[] = file.listFiles(new FileExtensionFilter(ZIP_FILE_EXTENSION));
			for(File zipfile : zipfiles) {
				if(containsImage(zipFileFactory, zipfile)) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	protected ComicBookSuite readFile(File folder) {
		return canReadFile(folder)? new FolderPackedZipBookSuite(folder, zipFileFactory) : null;
	}
}
