/* ZipPackedFolderBookSuite.java created on 2012/12/1
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static java.util.Collections.sort;
import static tw.funymph.commons.file.FileUtilities.isImageFile;

import java.net.URI;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import tw.funymph.comicsurfer.readers.AbstractComicBookSuite;
import tw.funymph.commons.zip.ZipEntryComparator;
import tw.funymph.commons.zip.ZipUtilities;

/**
 * From 2.1, to improve the responsiveness, the creation of comic books is
 * moved from the reader to the suite itself. This class creates folder comic
 * books in a ZIP file.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class ZipPackedFolderBookSuite extends AbstractComicBookSuite {

	private ZipFile suiteZipFile;
	private List<ZipEntry> folders;
	private Map<String, Integer> pageCount;

	/**
	 * Construct a <code>ZipPackedFolderBookSuite</code> instance.
	 * 
	 * @param zipFile the ZIP file that contains folder books
	 * @param uri the URI of the ZIP file
	 */
	public ZipPackedFolderBookSuite(ZipFile zipFile, String name, URI uri) {
		super(name, uri);
		suiteZipFile = zipFile;
		folders = new LinkedList<ZipEntry>();
		pageCount = new Hashtable<String, Integer>();
	}

	@Override
	public synchronized void open() {
		scanZipFileStructure();
		sort(folders, new ZipEntryComparator());
		for(ZipEntry entry : folders) {
			String folder = entry.getName();
			if(pageCount.get(folder) > 0) {
				books.add(new ZippedFolderBook(suiteZipFile, entry));
			}
		}
		folders.clear();
		pageCount.clear();
	}

	@Override
	public synchronized void close() {
		super.close();
		ZipUtilities.close(suiteZipFile);
	}

	/**
	 * Scan the ZIP file structure to find images and folders.
	 */
	private void scanZipFileStructure() {
		Enumeration<? extends ZipEntry> entries = suiteZipFile.entries();
		while(entries.hasMoreElements()) {
			ZipEntry entry = entries.nextElement();
			if(entry.isDirectory()) {
				folders.add(entry);
				pageCount.put(entry.getName(), 0);
			}
			else if(isImageFile(entry.getName())) {
				for(int i = folders.size() - 1; i >= 0; i--) {
					String folder = folders.get(i).getName();
					if(entry.getName().startsWith(folder)) {
						pageCount.put(folder, (pageCount.get(folder) + 1));
					}
				}
			}
		}
	}
}
