/* FolderPackedFolderBookSuiteReader.java created on Oct 28, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static tw.funymph.commons.collection.Finder.found;

import java.io.File;

import tw.funymph.comicsurfer.core.ComicBookSuite;
import tw.funymph.comicsurfer.readers.ComicBookSuiteFileReader;
import tw.funymph.commons.file.ImageFolderFilter;

/**
 * A reader can read a suite of comic books from a directory.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public class FolderPackedFolderBookSuiteReader extends ComicBookSuiteFileReader {

	/**
	 * The description of the reader.
	 */
	static final String DESCRIPTION = "The directory that contains a suite of books.";

	/**
	 * Construct a <code>FolderPackedFolderBooksReader</code> instance.
	 */
	public FolderPackedFolderBookSuiteReader() {}

	@Override
	public String getDescription() {
		return DESCRIPTION;
	}

	@Override
	protected boolean canReadFile(File file) {
		return found(file.listFiles(), new ImageFolderFilter());
	}

	@Override
	protected ComicBookSuite readFile(File file) {
		return canReadFile(file)? new FolderPackedFolderBookSuite(file) : null;
	}
}
