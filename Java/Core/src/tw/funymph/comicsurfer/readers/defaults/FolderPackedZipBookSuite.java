/* FolderPackedZipBookSuite.java created on 2012/12/1
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static tw.funymph.commons.file.FileUtilities.ZIP_FILE_EXTENSION;
import static tw.funymph.commons.zip.ZipUtilities.containsImage;

import java.io.File;
import java.util.zip.ZipFile;

import tw.funymph.comicsurfer.readers.AbstractComicBookSuite;
import tw.funymph.commons.file.FileExtensionFilter;
import tw.funymph.commons.zip.ZipFileFactory;

/**
 * From 2.1, to improve the responsiveness, the creation of comic books is
 * moved from the reader to the suite itself. This class creates zipped comic
 * books in a folder.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class FolderPackedZipBookSuite extends AbstractComicBookSuite {

	private File suiteFolder;
	private ZipFileFactory zipFileFactory;

	/**
	 * Construct a <code>FolderPackedZipBookSuite</code> instance.
	 * 
	 * @param folder the folder that contains zipped book
	 * @param factory the factory to create a zip file
	 */
	public FolderPackedZipBookSuite(File folder, ZipFileFactory factory) {
		super(folder.getName(), folder.toURI());
		suiteFolder = folder;
		zipFileFactory = factory;
	}

	@Override
	public synchronized void open() {
		File[] files = suiteFolder.listFiles(new FileExtensionFilter(ZIP_FILE_EXTENSION));
		for(File file : files) {
			ZipFile zipfile = zipFileFactory.getZipFile(file);
			if(containsImage(zipfile)) {
				books.add(new ZipBook(zipfile));
			}
		}
	}
}
