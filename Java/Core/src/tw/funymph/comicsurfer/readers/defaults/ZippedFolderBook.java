/* ZippedFolderBook.java created on Oct 30, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static tw.funymph.commons.file.FileUtilities.isImageFile;

import java.util.Enumeration;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * A class that manages all the images of a comic book stored in
 * a zipped folder. Note that the images are loaded when actually
 * needed (lazy loading) and re-open a closed images may take some
 * time to re-scan the images in the ZIP file.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public class ZippedFolderBook extends ZipBook {

	private ZipEntry folderEntry;

	/**
	 * Construct a <code>ZippedFolderBook</code> instance.
	 * 
	 * @param root the file that contains the images of a comic book
	 * @param folder the root folder of a comic book
	 */
	public ZippedFolderBook(ZipFile root, ZipEntry folder) {
		super(root);
		folderEntry = (folder != null && folder.isDirectory())? folder : null;
	}

	@Override
	protected Vector<ZipEntry> scanImages() {
		Vector<ZipEntry> imageEntries = new Vector<ZipEntry>();
		if(folderEntry != null) {
			Enumeration<? extends ZipEntry> allEntries = root.entries();
			while(allEntries.hasMoreElements()) {
				ZipEntry entry = allEntries.nextElement();
				if(!entry.getName().equals(folderEntry.getName()) &&
					entry.getName().startsWith(folderEntry.getName()) &&
					isImageFile(entry.getName())) {
					imageEntries.add(entry);
				}
			}
		}
		return imageEntries;
	}

	@Override
	public synchronized void close() {
		closePages();
	}
}
