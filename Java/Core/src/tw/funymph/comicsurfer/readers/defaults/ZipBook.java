/* ZippedBook.java created on Oct 29, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import tw.funymph.comicsurfer.core.Page;
import tw.funymph.comicsurfer.readers.AbstractComicBook;
import tw.funymph.commons.file.FileUtilities;
import tw.funymph.commons.zip.ZipEntryComparator;
import tw.funymph.commons.zip.ZipUtilities;

/**
 * A class that manages all the images of a comic book stored in
 * a ZIP file. Note that the images are loaded when actually needed
 * (lazy loading) and re-open a closed images may take some time to
 * re-scan the images in the ZIP file.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public class ZipBook extends AbstractComicBook {

	protected ZipFile root;
	protected List<Page> pages;

	/**
	 * Construct a <code>ZipBook</code> instance.
	 * 
	 * @param file the file that contains images of a comic book
	 */
	public ZipBook(ZipFile file) {
		root = file;
		pages = new ArrayList<Page>();
	}

	@Override
	public synchronized void open() {
		if(root == null) {
			return;
		}
		closePages();
		Vector<ZipEntry> imageEntries = scanImages();
		Collections.sort(imageEntries, new ZipEntryComparator());
		for(ZipEntry imageEntry : imageEntries) {
			Page page = new ZipEntryPage(root, imageEntry);
			pages.add(page);
			page.open();
		}
	}

	@Override
	public synchronized void close() {
		closePages();
		ZipUtilities.close(root);
	}

	@Override
	public int getPageCount() {
		return pages.size();
	}

	@Override
	public Page getPage(int index) {
		if(index >= 0 && index < pages.size()) {
			return pages.get(index);
		}
		return null;
	}

	/**
	 * Close the opened pages.
	 */
	protected void closePages() {
		for(Page page : pages) {
			page.close();
		}
		pages.clear();
	}

	/**
	 * Scan the entries that are images.
	 * 
	 * @return the image entries
	 */
	protected Vector<ZipEntry> scanImages() {
		Vector<ZipEntry> imageEntries = new Vector<ZipEntry>();
		Enumeration<? extends ZipEntry> allEntries = root.entries();
		while(allEntries.hasMoreElements()) {
			ZipEntry entry = allEntries.nextElement();
			if(!entry.isDirectory() && FileUtilities.isImageFile(entry.getName())) {
				imageEntries.add(entry);
			}
		}
		return imageEntries;
	}
}
