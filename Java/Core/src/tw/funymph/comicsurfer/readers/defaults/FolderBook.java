/* FolderBook.java created on Oct 28, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static java.util.Arrays.sort;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import tw.funymph.comicsurfer.core.Page;
import tw.funymph.comicsurfer.readers.AbstractComicBook;
import tw.funymph.commons.file.FileComparator;
import tw.funymph.commons.file.ImageFileFilter;

/**
 * A class that manages all the images of a comic book stored in
 * a directory. Note that the images are loaded when actually needed
 * (lazy loading) and re-open a closed images may take some time to
 * re-scan the images in the directory.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public class FolderBook extends AbstractComicBook {

	private File bookRoot;
	private List<Page> pages;
	private ImageFileFilter filter;

	/**
	 * Construct a <code>DirectoryComicBook</code> instance to manage
	 * all the images in the directory.
	 * 
	 * @param directory the directory that contains all the images
	 */
	public FolderBook(File directory) {
		bookRoot = directory;
		pages = new ArrayList<Page>();
		filter = new ImageFileFilter();
	}

	@Override
	public synchronized void open() {
		if(bookRoot == null) {
			return;
		}
		close();
		File[] files = bookRoot.listFiles(filter);
		if(files != null) {
			sort(files, new FileComparator());
			for(File file : files) {
				Page page = new FilePage(file);
				pages.add(page);
				page.open();
			}
		}
	}

	@Override
	public synchronized void close() {
		for(Page page : pages) {
			page.close();
		}
		pages.clear();
	}

	@Override
	public int getPageCount() {
		return pages.size();
	}

	@Override
	public Page getPage(int index) {
		if(index >= 0 && index < pages.size()) {
			return pages.get(index);
		}
		return null;
	}
}
