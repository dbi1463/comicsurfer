/* URLPage.java created on 2012/9/19
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import tw.funymph.comicsurfer.readers.AbstractPage;

/**
 * A class to represent a page of an image file that is located on an URL.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class URLPage extends AbstractPage {

	private URL url;

	/**
	 * Construct a <code>URLPage</code> instance with the URL in string format.
	 * 
	 * @param theURL the source URL
	 */
	public URLPage(String theURL) {
		try {
			url = new URL(theURL);
		} catch (MalformedURLException e) {
			url = null;
			System.out.println("[URLPage] The input URL is incorrect!");
		}
	}

	/**
	 * Construct a <code>URLPage</code> instance with the URL.
	 * 
	 * @param theURL the source URL
	 */
	public URLPage(URL theURL) {
		url = theURL;
	}

	@Override
	public InputStream getContent() {
		InputStream content = null;
		try {
			content = url.openStream();
		} catch (Exception e) {
			content = null;
			System.out.println("[URLPage] The input URL is unavailable!");
		}
		return content;
	}

	@Override
	public void close() {
		url = null;
	}
}
