/* FolderBookReader.java created on Oct 28, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static tw.funymph.commons.file.FileUtilities.containsImages;

import java.io.File;

import tw.funymph.comicsurfer.core.ComicBookSuite;
import tw.funymph.comicsurfer.readers.ComicBookSuiteFileReader;
import tw.funymph.comicsurfer.readers.DefaultComicBookSuite;

/**
 * A reader that read images from a directory.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public class FolderBookReader extends ComicBookSuiteFileReader {

	/**
	 * The description of the reader.
	 */
	static final String DESCRIPTION = "The directory that contains only one book.";

	/**
	 * Construct a <code>FolderBookReader</code> instance.
	 */
	public FolderBookReader() {}

	@Override
	public String getDescription() {
		return DESCRIPTION;
	}

	@Override
	protected boolean canReadFile(File file) {
		return containsImages(file);
	}

	@Override
	protected ComicBookSuite readFile(File file) {
		if(canReadFile(file)) {
			DefaultComicBookSuite books = new DefaultComicBookSuite(file.getName(), file.toURI());
			books.addComicBook(new FolderBook(file));
			return books;
		}
		return null;
	}
}
