/* ZipPackedFolderBookSuiteReader.java created on Oct 30, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static tw.funymph.commons.file.FileUtilities.ZIP_FILE_EXTENSION;
import static tw.funymph.commons.file.FileUtilities.isImageFile;
import static tw.funymph.commons.zip.ZipUtilities.close;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import tw.funymph.comicsurfer.core.ComicBookSuite;
import tw.funymph.comicsurfer.readers.ComicBookSuiteFileReader;
import tw.funymph.commons.zip.ZipFileFactory;

/**
 * A reader can read a suite of comic books from folders that are compressed
 * in a zip file.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public class ZipPackedFolderBookSuiteReader extends ComicBookSuiteFileReader {

	/**
	 * The description of the reader.
	 */
	static final String DESCRIPTION = "The zip file that contains a suite of books organized by folders.";

	private ZipFileFactory zipFilefactory;

	/**
	 * Construct a <code>ZipPackedFolderBooksReader</code> instance with the
	 * specified platform-dependent ZIP file factory.
	 * 
	 * @param factory the platform-dependent ZIP file factory
	 */
	public ZipPackedFolderBookSuiteReader(ZipFileFactory factory) {
		super(ZIP_FILE_EXTENSION);
		zipFilefactory = factory;
	}

	@Override
	public String getDescription() {
		return DESCRIPTION;
	}

	@Override
	protected boolean canReadFile(File file) {
		ZipFile zipFile = zipFilefactory.getZipFile(file);
		if(zipFile == null) {
			return false;
		}
		boolean result = findImageInZippedFolder(zipFile);
		close(zipFile);
		return result;
	}

	@Override
	protected ComicBookSuite readFile(File file) {
		return canReadFile(file)? new ZipPackedFolderBookSuite(zipFilefactory.getZipFile(file), file.getName(), file.toURI()) : null; 
	}

	/**
	 * Check whether there is an image inside a folder that is in the ZIP file.
	 * 
	 * @param zipFile the ZIP file to check
	 * @return true if there is an image inside a folder that is in the ZIP file
	 */
	private boolean findImageInZippedFolder(ZipFile zipFile) {
		// Folder path cache
		List<String> folders = new ArrayList<String>();
		Enumeration<? extends ZipEntry> entries = zipFile.entries();
		while(entries.hasMoreElements()) {
			ZipEntry entry = entries.nextElement();
			if(entry.isDirectory()) {
				folders.add(entry.getName());
			}
			else if(isImageFile(entry.getName())) {
				// Usually, the entry is sorted and finding a folder in reversed order
				// would get a better performance
				for(int index = folders.size() - 1; index >= 0; index--) {
					if(entry.getName().startsWith(folders.get(index))) {
						return true;
					}
				}
			}
		}
		return false;
	}
}
