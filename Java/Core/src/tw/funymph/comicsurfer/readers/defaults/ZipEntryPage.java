/* ZipEntryPage.java created on 2012/9/3
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import tw.funymph.comicsurfer.readers.AbstractPage;

/**
 * A class to represent a page in a ZIP file.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.5
 */
public class ZipEntryPage extends AbstractPage {

	private ZipFile bookFile;
	private ZipEntry pageEntry;

	/** Construct a <code>ZipEntryPage</code> instance with the source ZIP file
	 * and the entry for the page.
	 * 
	 * @param file the source ZIP file
	 * @param entry the entry for the page
	 */
	public ZipEntryPage(ZipFile file, ZipEntry entry) {
		bookFile = file;
		pageEntry = entry;
	}

	@Override
	public InputStream getContent() {
		InputStream content = null;
		try {
			content = bookFile.getInputStream(pageEntry);
		} catch (Exception e) {
			content = null;
			System.out.println("[ZipEntryPage] Error occurs when reading the selected ZipEntry.");
		}
		return content;
	}

	@Override
	public void close() {
		bookFile = null;
		pageEntry = null;
	}
}
