/* FolderPackedFolderBookSuite.java created on 2012/12/1
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static tw.funymph.commons.file.FileUtilities.containsImages;

import java.io.File;

import tw.funymph.comicsurfer.readers.AbstractComicBookSuite;

/**
 * From 2.1, to improve the responsiveness, the creation of comic books is
 * moved from the reader to the suite itself. This class creates folder comic
 * books in a folder.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class FolderPackedFolderBookSuite extends AbstractComicBookSuite {

	private File suiteRoot;

	/**
	 * Construct a <code>FolderPackedFolderBookSuite</code> instance.
	 * 
	 * @param folder the folder that contains folder book
	 */
	public FolderPackedFolderBookSuite(File folder) {
		super(folder.getName(), folder.toURI());
		suiteRoot = folder;
	}

	@Override
	public synchronized void open() {
		File[] folders = suiteRoot.listFiles();
		for(File folder : folders) {
			if(folder.isDirectory() && containsImages(folder)) {
				books.add(new FolderBook(folder));
			}
		}
	}
}
