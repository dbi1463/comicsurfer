/* ZipPackedZipBookSuite.java created on 2012/12/1
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static tw.funymph.commons.file.FileUtilities.isZipFile;
import static tw.funymph.commons.file.FileUtilities.removeExtension;
import static tw.funymph.commons.zip.ZipUtilities.containsImage;

import java.net.URI;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import tw.funymph.comicsurfer.readers.AbstractComicBookSuite;
import tw.funymph.commons.zip.ZipFileFactory;
import tw.funymph.commons.zip.ZipUtilities;

/**
 * From 2.1, to improve the responsiveness, the creation of comic books is
 * moved from the reader to the suite itself. This class creates ZIP comic
 * books in a ZIP file.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class ZipPackedZipBookSuite extends AbstractComicBookSuite {

	private ZipFile suiteZipFile;
	private ZipFileFactory zipFileFactory;

	/**
	 * Construct a <code>ZipPackedZipBookSuite</code> instance.
	 * 
	 * @param zipFile the ZIP file that contains folder books
	 * @param uri the URI of the ZIP file
	 * @param factory the factory to create ZipFile instances.
	 */
	public ZipPackedZipBookSuite(ZipFile zipFile, URI uri, ZipFileFactory factory) {
		super(removeExtension(zipFile.getName()), uri);
		suiteZipFile = zipFile;
		zipFileFactory = factory;
	}

	@Override
	public synchronized void open() {
		Enumeration<? extends ZipEntry> entries = suiteZipFile.entries();
		while(entries.hasMoreElements()) {
			ZipEntry entry = entries.nextElement();
			if(isZipFile(entry.getName())) {
				ZipFile child = zipFileFactory.getZipFile(suiteZipFile, entry);
				if(child != null && containsImage(child)) {
					books.add(new ZipBook(child));
				}
			}
		}
	}

	@Override
	public synchronized void close() {
		super.close();
		ZipUtilities.close(suiteZipFile);
	}
}
