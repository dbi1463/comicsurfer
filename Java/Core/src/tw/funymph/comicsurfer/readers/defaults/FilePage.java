/* FilePage.java created on 2012/9/3
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import tw.funymph.comicsurfer.readers.AbstractPage;

/**
 * A class to represent a page of an image file.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.5
 */
public class FilePage extends AbstractPage {

	private File imageFile;

	/**
	 * Construct a <code>FilePage</code> instance with the source image file.
	 * 
	 * @param file the source image file
	 */
	public FilePage(File file) {
		imageFile = file;
	}

	@Override
	public InputStream getContent() {
		InputStream content = null;
		try {
			content = new FileInputStream(imageFile);
		} catch (Exception e) {
			content = null;
			System.out.println("[FilePage] The input file is unavailable!");
		}
		return content;
	}

	@Override
	public void close() {
		imageFile = null;
	}
}
