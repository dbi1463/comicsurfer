/* ZipPackedZipBookSuiteReader.java created on Oct 30, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers.defaults;

import static tw.funymph.commons.file.FileUtilities.ZIP_FILE_EXTENSION;
import static tw.funymph.commons.file.FileUtilities.isZipFile;
import static tw.funymph.commons.zip.ZipUtilities.close;
import static tw.funymph.commons.zip.ZipUtilities.containsImage;

import java.io.File;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import tw.funymph.comicsurfer.core.ComicBookSuite;
import tw.funymph.comicsurfer.readers.ComicBookSuiteFileReader;
import tw.funymph.commons.zip.ZipFileFactory;

/**
 * A reader can read a suite of compressed comic books from a compressed file.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public class ZipPackedZipBookSuiteReader extends ComicBookSuiteFileReader {

	/**
	 * The description of the reader.
	 */
	static final String DESCRIPTION = "The zip file that contains a suite of zipped book.";

	private ZipFileFactory zipFileFactory;

	/**
	 * Construct a <code>ZipPackedZipBooksReader</code> instance with the
	 * specified platform-dependent ZIP file factory.
	 * 
	 * @param factory the platform-dependent ZIP file factory
	 */
	public ZipPackedZipBookSuiteReader(ZipFileFactory factory) {
		super(ZIP_FILE_EXTENSION);
		zipFileFactory = factory;
	}

	@Override
	public String getDescription() {
		return DESCRIPTION;
	}

	@Override
	protected boolean canReadFile(File file) {
		ZipFile zipFile = zipFileFactory.getZipFile(file);
		if(zipFile == null) {
			return false;
		}
		boolean result = findImagesInZippedZipFile(zipFile);
		close(zipFile);
		return result;
	}

	@Override
	protected ComicBookSuite readFile(File file) {
		return canReadFile(file)? new ZipPackedZipBookSuite(zipFileFactory.getZipFile(file), file.toURI(), zipFileFactory) : null;
	}

	/**
	 * Check whether there is an image inside a ZIP file that is in the ZIP file.
	 * 
	 * @param zipFile the ZIP file to check
	 * @return true if there is an image inside a ZIP file that is in the ZIP file
	 */
	private boolean findImagesInZippedZipFile(ZipFile zipFile) {
		Enumeration<? extends ZipEntry> entries = zipFile.entries();
		while(entries.hasMoreElements()) {
			ZipEntry entry = entries.nextElement();
			if(isZipFile(entry.getName())) {
				ZipFile child = zipFileFactory.getZipFile(zipFile, entry);
				if(containsImage(child)) {
					return true;
				}
			}
		}
		return false;
	}
}
