/* ComicBookSuiteReader.java created on Oct 27, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers;

import java.net.URI;

import tw.funymph.comicsurfer.core.ComicBookSuite;

/**
 * A class can implement this interface to read the comic book suite
 * from a specific format and offer the information about the format.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public interface ComicBookSuiteReader {

	/**
	 * Get a description for the format.
	 * 
	 * @return the description for the format
	 */
	public String getDescription();

	/**
	 * Get the extension that the reader can read.
	 * 
	 * @return the extension that the read can read
	 */
	public String getExtension();

	/**
	 * Get the supported URI schemes.
	 * 
	 * @return the scheme that the reader support
	 */
	public String[] getSupportedSchemes();

	/**
	 * Indicate whether the reader can read from a folder or not.
	 * 
	 * @return true if the reader can read from a folder
	 */
	public boolean isFolderReader();

	/**
	 * Check whether the reader can read the specified file (folder) or not. 
	 * 
	 * @param uri the URI (file, folder, or URL) to read
	 * @return true if the reader can read the file
	 */
	public boolean canRead(URI uri);

	/**
	 * Read the comic book suite from the specified URL (file, folder, or URL).
	 * 
	 * @param uri the URI (file, folder, or URL) that contains a comic book suite
	 * @return the comic book suite; null if cannot read anything from the URI or any error occurred
	 */
	public ComicBookSuite read(URI uri);
}
