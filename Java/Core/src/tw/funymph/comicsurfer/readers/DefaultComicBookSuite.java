/* ComicBookSuite.java created on Oct 27, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers;

import java.net.URI;

import tw.funymph.comicsurfer.core.ComicBook;

/**
 * A class that offers a container to manage a suite of comic books.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public class DefaultComicBookSuite extends AbstractComicBookSuite {

	/**
	 * Construct a nameless container to manages a suite of comic books.
	 * 
	 * @param uri the URI that contains the comic books
	 */
	public DefaultComicBookSuite(URI uri) {
		super(uri);
	}

	/**
	 * Construct a container to manages a suite of comic books.
	 * 
	 * @param name the name of the suite
	 * @param uri the URI that contains the comic books
	 */
	public DefaultComicBookSuite(String name, URI uri) {
		super(uri);
		setName(name);
	}

	/**
	 * Add a new comic book (usually called by a comic book suite reader).
	 * 
	 * @param book the new comic book to be added
	 */
	public void addComicBook(ComicBook book) {
		books.add(book);
	}
}
