/* ComicBookSuiteFileReader.java created on Dec 8, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.readers;

import static tw.funymph.commons.file.FileUtilities.convertFrom;
import static tw.funymph.commons.io.IOConstants.URI_SCHEME_FILE;

import java.io.File;
import java.net.URI;

import tw.funymph.comicsurfer.core.ComicBookSuite;

/**
 * An abstract class that implements <code>ComicBookSuiteReader</code> interface
 * to provide protection from null exception of using the input file. A class can
 * extend this class and implement the hook methods to get a non-null input file.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.3
 */
public abstract class ComicBookSuiteFileReader extends AbstractComicBookSuiteReader {

	public static final String SUPPORTED_SCHEMES[] = { URI_SCHEME_FILE };

	protected String supportedExtension;
	protected boolean isFolderReader;

	/** Construct a folder reader instance by its children.
	 */
	protected ComicBookSuiteFileReader() {
		supportedExtension = null;
		isFolderReader = true;
	}

	/** Construct a file reader instance by its children.
	 * 
	 * @param extension the supported extension
	 */
	protected ComicBookSuiteFileReader(String extension) {
		supportedExtension = extension;
		isFolderReader = false;
	}

	@Override
	public String getExtension() {
		return supportedExtension;
	}

	@Override
	public String[] getSupportedSchemes() {
		return SUPPORTED_SCHEMES;
	}

	@Override
	public boolean canRead(URI uri) {
		File file = convertFrom(uri);
		if(file != null) {
			return canReadFile(file);
		}
		return false;
	}

	@Override
	public ComicBookSuite read(URI uri) {
		File file = convertFrom(uri);
		if(file != null) {
			return readFile(file);
		}
		return null;
	}

	@Override
	public boolean isFolderReader() {
		return isFolderReader;
	}

	/**
	 * A hook method is ONLY called when the input file of {@link #canRead(URI)}
	 * is not null.
	 * 
	 * @param file the file to read (always not null) 
	 * @return true if the reader can read the file
	 */
	protected abstract boolean canReadFile(File file);

	/**
	 * A hook method is ONLY called when the input file of {@link #read(URI)}
	 * is not null.
	 * 
	 * @param file the file to read (always not null)
	 * @return the comic book suite; null if cannot read anything from the file (folder)
	 */
	protected abstract ComicBookSuite readFile(File file);
}
