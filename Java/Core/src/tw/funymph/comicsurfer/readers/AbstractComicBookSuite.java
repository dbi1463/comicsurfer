/* AbstractComicBookSuite.java created on 2012/9/17
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import tw.funymph.comicsurfer.core.ComicBook;
import tw.funymph.comicsurfer.core.ComicBookSuite;

/**
 * A class that isolates the change of {@link ComicBookSuite} from the plug-in
 * readers. In most cases, the {@link DefaultComicBookSuite} is the good choice
 * to manage the books. However, if the plug-in reader needs more functionalities
 * than that {@link DefaultComicBookSuite} has, the reader can use this class as
 * the base to extend the ability of managing books from new resources. If the
 * {@link ComicBookSuite} has any new methods, this abstract class will offer
 * the default implementation to avoid compatible issues.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public abstract class AbstractComicBookSuite implements ComicBookSuite {

	protected URI uri;
	protected String name;

	protected List<ComicBook> books;

	/**
	 * Construct a nameless container to manages a suite of comic books.
	 * 
	 * @param suiteURI the URI that contains the comic books
	 */
	protected AbstractComicBookSuite(URI suiteURI) {
		this(null, suiteURI);
	}

	/**
	 * Construct a nameless container to manages a suite of comic books.
	 * 
	 * @param suiteURI the URI that contains the comic books
	 */
	protected AbstractComicBookSuite(String suiteName, URI suiteURI) {
		uri = suiteURI;
		name = suiteName;
		books = new ArrayList<ComicBook>();
	}

	@Override
	public void open() {
		// For compatibility, AbstractComicBookSuite has to provide an empty implementation
		// for the existing third-party plug-in readers.
	}

	@Override
	public synchronized void close() {
		for(ComicBook book : books) {
			book.close();
		}
		books.clear();
	}

	/**
	 * Set the new name of the suite.
	 * 
	 * @param newName the new name
	 */
	public void setName(String newName) {
		name = newName;
	}

	@Override
	public int getComicBookCount() {
		return books.size();
	}

	@Override
	public ComicBook getComicBook(int index) {
		if(index >= 0 && index < books.size()) {
			return books.get(index);
		}
		return null;
	}

	@Override
	public String getSuiteName() {
		return name;
	}

	@Override
	public URI getSuitePath() {
		return uri;
	}
}
