/* AbstractComicBookSuiteReader.java created on 2012/9/12
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.readers;

/**
 * A class that isolates the change of {@link ComicBookSuiteReader} from
 * the plug-in readers. Any plug-in reader can use this class as the base
 * to extend the ability of reading comic book suites from new resources.
 * If the {@link ComicBookSuiteReader} has any new methods, this abstract
 * class will offer the default implementation to avoid compatible issues.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public abstract class AbstractComicBookSuiteReader implements ComicBookSuiteReader {

}
