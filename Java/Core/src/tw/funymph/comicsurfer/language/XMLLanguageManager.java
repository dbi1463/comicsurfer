/* XMLLanguageManager.java created on April 5, 2012
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.language;

import static tw.funymph.commons.assertion.AssertUtilities.isNotBlank;

import java.io.File;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

import tw.funymph.commons.xml.AbstractXMLDecoderTreeNode;
import tw.funymph.commons.xml.XMLDecoder;

/**
 * A class that manages all available language files using the SAX API which
 * is faster and uses less memory than JDOM API.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.4
 */
public class XMLLanguageManager extends LazyLanguageManager {

	private static final String ELEMENT_TEXT = "text";
	private static final String ELEMENT_ROOT = "language";
	private static final String ATTRIBUTE_TEXT_KEY = "key";
	private static final String ATTRIBUTE_TEXT_VALUE = "value";
	private static final String ATTRIBUTE_LANGUAGE_NAME = "name";

	private XMLDecoder decoder;
	private Hashtable<String, File> languages;

	/**
	 * Construct a <code>SAXLanguageManager</code> instance with the initial
	 * language name.
	 */
	public XMLLanguageManager() {
		decoder = new XMLDecoder();
		languages = new Hashtable<String, File>();
	}

	@Override
	public void initialize(String path) {
		File[] files = new File(path).listFiles();
		for(File file : files) {
			String name = null;
			if(file.isFile() && (name = getLanguageName(file)) != null) {
				languages.put(name, file);
				System.out.println("    Found " + name + " from " + file.getName());
			}
		}
	}

	@Override
	public boolean loadLanguage(String name) {
		if(languages.containsKey(name)) {
			return loadLanguage(languages.get(name));
		}
		return false;
	}

	@Override
	public Set<String> getAvailableLanguages() {
		return languages.keySet();
	}

	/**
	 * Load the texts from the specified file.
	 * 
	 * @param file the file to load
	 */
	private boolean loadLanguage(File file) {
		LanguageLoader loader = new LanguageLoader();
		return decoder.decode(loader, file);
	}

	/**
	 * Get the name from the specified language name.
	 * 
	 * @param file the file to load
	 * @return the language name; null if the file is not a language definition file
	 */
	private String getLanguageName(File file) {
		LanguageDetector detector = new LanguageDetector();
		decoder.decode(detector, file);
		return detector.getLanguageName();
	}

	/**
	 * A class that detects the name and load texts from a language definition file.
	 *
	 * @author Pin-Ying Tu
	 * @version 3.0
	 * @since 1.4
	 */
	private class LanguageDetector extends AbstractXMLDecoderTreeNode {

		private String languageName;

		/**
		 * Construct a default <code>LanguageDetector</code> instance.
		 */
		public LanguageDetector() {
			super(ELEMENT_ROOT);
		}

		/**
		 * Get the detected language name.
		 * 
		 * @return the detected language name; null if the file is not a language definition file
		 */
		public String getLanguageName() {
			return languageName;
		}

		@Override
		public void processAttributes(Map<String, String> attributes) {
			languageName = attributes.get(ATTRIBUTE_LANGUAGE_NAME);
		}
	}

	/**
	 * A class that loads all localized texts from a language definition file.
	 *
	 * @author Pin-Ying Tu
	 * @version 3.0
	 * @since 1.4
	 */
	private class LanguageLoader extends AbstractXMLDecoderTreeNode {

		/**
		 * Construct a default <code>LanguageLoader</code> instance.
		 */
		public LanguageLoader() {
			super(ELEMENT_TEXT);
		}

		@Override
		public void processAttributes(Map<String, String> attributes) {
			String key = attributes.get(ATTRIBUTE_TEXT_KEY);
			String value = attributes.get(ATTRIBUTE_TEXT_VALUE);
			if(isNotBlank(key) && isNotBlank(value)) {
				texts.put(key, value);
			}
		}
	}
}
