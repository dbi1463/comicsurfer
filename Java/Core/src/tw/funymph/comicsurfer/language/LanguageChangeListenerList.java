/* LanguageChangeListenerList.java created on Dec 2, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.language;

import java.util.ArrayList;

/**
 * A class that manages the <code>LanguageChnageListener</code> instances
 * and fire events to the registered listeners.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.3
 */
public class LanguageChangeListenerList {

	private LanguageManager eventSource;
	ArrayList<LanguageChangeListener> listeners;

	/**
	 * Construct a <code>LanguageChangeListenerList</code> instance.
	 * 
	 * @param languageManager the event source
	 */
	public LanguageChangeListenerList(LanguageManager languageManager) {
		eventSource = languageManager;
		listeners = new ArrayList<LanguageChangeListener>();
	}

	/**
	 * Add the specific listener into the notification list. Note that
	 * the same instances will be only added once.
	 * 
	 * @param listener the listener to be added
	 */
	public void addLanguageChangeListener(LanguageChangeListener listener) {
		if(listener != null && !listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	/**
	 * Remove the specific listener from the notification list.
	 * 
	 * @param listener the listener to be removed
	 */
	public void removeLanguageChangeListener(LanguageChangeListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Fire an event that the current language is changed to all registered listeners.
	 */
	public void fireLanguageChangedEvent() {
		for(LanguageChangeListener listener : listeners) {
			listener.languageChanged(eventSource);
		}
	}
}
