/* LocalizedTextProvider.java created on 2012/12/15
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.language;

/**
 * The interface defines the methods that a localized text provider
 * should implement.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public interface LocalizedTextProvider {

	/**
	 * Get the current language name.
	 * 
	 * @return the name of the current language
	 */
	public String getCurrentLanguage();

	/**
	 * Get the localized text bound on the specific key.
	 * 
	 * @param key the key to get the localized text
	 * @return the localized text
	 */
	public String getLocalizedText(String key);
}
