/* LanguageManager.java created on Dec 2, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.language;

import java.util.Set;

/**
 * A class can implements this interface to manage the localized text of 
 * all available languages.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.3
 */
public interface LanguageManager extends LocalizedTextProvider {

	/**
	 * Initialize the manager by scanning the language definition files
	 * in the specified path.
	 * 
	 * @param path the path of language definition files
	 */
	public void initialize(String path);

	/**
	 * Load the localized texts for the language.
	 * 
	 * @param name the name of the language needed to load
	 * @return true if the localized texts are loaded correctly
	 */
	public boolean loadLanguage(String name);

	/**
	 * Set the current language. This method will fire a language
	 * changed event and notify all registered listeners if the
	 * new language name is existed.
	 * 
	 * @param name the new language name
	 */
	public void setCurrentLanguage(String name);

	/**
	 * Get the names of all available languages.
	 * 
	 * @return the names of all available languages 
	 */
	public Set<String> getAvailableLanguages();

	/**
	 * Add the specific listener into the notification list. Note that
	 * the same instances will be only added once.
	 * 
	 * @param listener the listener to be added
	 */
	public void addLanguageChangeListener(LanguageChangeListener listener);

	/**
	 * Remove the specific listener from the notification list.
	 * 
	 * @param listener the listener to be removed
	 */
	public void removeLanguageChangeListener(LanguageChangeListener listener);
}
