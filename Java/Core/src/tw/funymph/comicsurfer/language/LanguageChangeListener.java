/* LanguageChangeListener.java created on Oct 13, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.language;

/**
 * A class can implement the interface to receive and handle the language
 * changed events.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public interface LanguageChangeListener {

	/**
	 * Invoked when the language is changed.
	 * 
	 * @param source the localized text source
	 */
	public void languageChanged(LocalizedTextProvider source);
}
