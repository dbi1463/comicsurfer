/* LazyLanguageManager.java created on Dec 2, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.language;

import java.util.Hashtable;

/**
 * An abstract class that implements parts of methods defined in
 * the <code>LanguageManager</code> interface. The word lazy means
 * the localized texts of a specific language is loaded only when
 * it is really needed to reduce the memory usage. Note that there
 * is only one copy of localized texts for the current language.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.3
 */
public abstract class LazyLanguageManager implements LanguageManager {

	protected String currentLanguage;

	protected Hashtable<String, String> texts;
	protected LanguageChangeListenerList listeners;

	/**
	 * Construct a <code>LazyLanguageManager</code> instance by its
	 * inherited concrete classes.
	 */
	protected LazyLanguageManager() {
		texts = new Hashtable<String, String>();
		listeners = new LanguageChangeListenerList(this);
	}

	@Override
	public String getLocalizedText(String key) {
		if(texts.containsKey(key)) {
			return texts.get(key);
		}
		return key;
	}

	@Override
	public String getCurrentLanguage() {
		return currentLanguage;
	}

	@Override
	public void setCurrentLanguage(String languageName) {
		if(currentLanguage != languageName && loadLanguage(languageName)) {
			currentLanguage = languageName;
			listeners.fireLanguageChangedEvent();
		}
	}

	@Override
	public void addLanguageChangeListener(LanguageChangeListener listener) {
		listeners.addLanguageChangeListener(listener);
	}

	@Override
	public void removeLanguageChangeListener(LanguageChangeListener listener) {
		listeners.removeLanguageChangeListener(listener);
	}
}
