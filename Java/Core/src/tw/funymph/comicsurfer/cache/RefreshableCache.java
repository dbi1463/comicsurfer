/* RefreshableCache.java created on 2012/9/4
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.cache;

/**
 * A cache container can implement this interface to manage the cacheable
 * items with different cache replacement algorithms.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.5
 */
public interface RefreshableCache extends Cacheable {

	/**
	 * Refresh the container based on the cache replacement algorithm.
	 */
	public void refresh();
}
