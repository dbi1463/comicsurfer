/* CachedComicBook.java created on 2012/6/16
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.cache;

import java.util.ArrayList;
import java.util.List;

import tw.funymph.comicsurfer.core.ComicBook;
import tw.funymph.comicsurfer.core.Page;

/**
 * A class that manages a set of images with the cache ability.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.5
 */
public class CachedComicBook implements RefreshableCache, ComicBook {

	public static final int CACHE_WINDOW_SIZE = 4;

	private int index;

	private ComicBook book;
	private CacheContext cacheContext;
	private List<CachedPage> cachedPages;

	private volatile boolean cached;
	private volatile int currentIndex;

	/**
	 * Construct a <code>CachedComicBook</code> instance with the original
	 * comic book to be cached and the task queue.
	 * 
	 * @param aBook the original comic book to be cached
	 * @param theIndex the index of the comic book
	 * @param context the cache context
	 */
	public CachedComicBook(ComicBook aBook, int theIndex, CacheContext context) {
		book = aBook;
		index = theIndex;
		cached = false;
		currentIndex = 0;
		cacheContext = context;
		cachedPages = new ArrayList<CachedPage>();
	}

	@Override
	public int getIndex() {
		return (book.getIndex() == UNDEFINED_INDEX)? index : book.getIndex();
	}

	@Override
	public synchronized void open() {
		if(!cached) {
			cache(cacheContext);
		}
	}

	/**
	 * The resource management of the cached book is determined by
	 * the algorithm of its cache container, so that calling this
	 * method has no effect.
	 */
	@Override
	public void close() {}

	@Override
	public int getPageCount() {
		return book.getPageCount();
	}

	@Override
	public synchronized Page getPage(int index) {
		currentIndex = index;
		cacheContext.addCacheTask(new RefreshCacheAction(this));
		return cachedPages.get(index);
	}

	@Override
	public synchronized void cache(CacheContext context) {
		if(!cached) {
			book.open();
			for(int index = 0; index < book.getPageCount(); index++) {
				cachedPages.add(new CachedPage(book.getPage(index), index));
			}
			initialCache();
			cached = true;
		}
	}

	@Override
	public synchronized void clear() {
		if(cached) {
			for(CachedPage page : cachedPages) {
				page.clear();
			}
			book.close();
			cachedPages.clear();
			cached = false;
		}
	}

	@Override
	public synchronized boolean isCached() {
		return cached;
	}

	@Override
	public synchronized void refresh() {
		cachePages();
		clearLeastRecentlyUsedPages();
	}

	@Override
	public int getContentType() {
		return CONTENT_TYPE_BOOK;
	}

	/**
	 * The initial cache will cache the first three pages and the last two pages
	 * of the book.
	 */
	private void initialCache() {
		// This method will load the first three pages when the current index is 0.
		cachePages();
		
		// In that case, this instance is not the currently viewed book. So that the
		// user may viewed this instance on changing to the previous/next volume,
		// changing to the previous page at the head page of the next volume, or
		// changing to the next page at the last page of the previous volume.
		if(currentIndex == 0) {
			cacheContext.addCacheTask(new PrefetchAction(getCachedPage(book.getPageCount() - 1)));
			cacheContext.addCacheTask(new PrefetchAction(getCachedPage(book.getPageCount() - 2)));
		}
	}

	/**
	 * Cache the pages according to the current index.
	 */
	private void cachePages() {
		cacheContext.addCacheTask(new PrefetchAction(getCachedPage(currentIndex)));
		for(int index = (currentIndex - CACHE_WINDOW_SIZE); index <= (currentIndex + CACHE_WINDOW_SIZE); index++) {
			if(index >= 0 && index < book.getPageCount() && index != currentIndex) {
				cacheContext.addCacheTask(new PrefetchAction(getCachedPage(index)));
			}
		}
	}

	/**
	 * Clear the least recently used pages.
	 */
	private void clearLeastRecentlyUsedPages() {
		for(int index = 2; index < (cachedPages.size() - 2); index++) {
			if(index < (currentIndex - CACHE_WINDOW_SIZE) || index > (currentIndex + CACHE_WINDOW_SIZE)) {
				cachedPages.get(index).clear();
			}
		}
	}

	/**
	 * Get the cached page specified by the given index.
	 * 
	 * @param index the index to get
	 * @return the cached page
	 */
	private CachedPage getCachedPage(int index) {
		if(index >= 0 && index < cachedPages.size()) {
			return cachedPages.get(index);
		}
		return null;
	}
}
