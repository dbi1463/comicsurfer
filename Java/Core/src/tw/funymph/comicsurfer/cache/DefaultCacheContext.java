/* DefaultCacheContext.java created on 2013/7/22
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.cache;

import java.util.Map;

import tw.funymph.comicsurfer.core.DefaultNotificationCenter;
import tw.funymph.comicsurfer.core.NotificationCenter;
import tw.funymph.commons.thread.ThreadPool;

/**
 * The default implementation of {@link CacheContext}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 3.0
 */
public class DefaultCacheContext implements CacheContext {

	private ThreadPool threadPool;

	public DefaultCacheContext() {
		threadPool = new ThreadPool(10);
		threadPool.recruit();
	}

	@Override
	public void addCacheTask(CacheTask task) {
		threadPool.addTask(task);
	}

	@Override
	public void reportCacheFailed(Map<String, Object> report) {
		NotificationCenter notificationCenter = DefaultNotificationCenter.getInstance();
		notificationCenter.publish(this, "CacheFailed", report);
	}
}
