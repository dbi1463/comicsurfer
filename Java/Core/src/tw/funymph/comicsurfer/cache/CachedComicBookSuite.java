/* ComicBookSuiteCache.java created on 2012/6/16
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.cache;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import tw.funymph.comicsurfer.core.ComicBook;
import tw.funymph.comicsurfer.core.ComicBookSuite;

/**
 * A class that offers a container to manage a suite of comic books
 * with the cache ability.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.5
 */
public class CachedComicBookSuite implements RefreshableCache, ComicBookSuite {

	public static final int CACHE_WINDOW_SIZE = 2;

	private int currentBookIndex;

	private ComicBookSuite bookSuite;
	private CacheContext cacheContext;
	private List<CachedComicBook> cachedBooks;

	/**
	 * Construct a <code>CachedComicBookSuite</code> instance with the original comic
	 * book suite to be cached and the task queue.
	 * 
	 * @param suite the original comic book to be cached
	 */
	public CachedComicBookSuite(ComicBookSuite suite) {
		bookSuite = suite;
		cachedBooks = new ArrayList<CachedComicBook>();
		cacheContext = new DefaultCacheContext();
	}

	@Override
	public int getComicBookCount() {
		return bookSuite.getComicBookCount();
	}

	@Override
	public synchronized ComicBook getComicBook(int index) {
		if(index >= 0 && index < bookSuite.getComicBookCount()) {
			currentBookIndex = index;
			cacheContext.addCacheTask(new RefreshCacheAction(this));
			return cachedBooks.get(currentBookIndex);
		}
		return null;
	}

	@Override
	public String getSuiteName() {
		return bookSuite.getSuiteName();
	}

	@Override
	public URI getSuitePath() {
		return bookSuite.getSuitePath();
	}

	@Override
	public synchronized void open() {
		bookSuite.open();
		for(int index = 0; index < bookSuite.getComicBookCount(); index++) {
			cachedBooks.add(new CachedComicBook(bookSuite.getComicBook(index), index, cacheContext));
		}
	}

	@Override
	public synchronized void close() {
		clear();
		bookSuite.close();
	}

	@Override
	public synchronized boolean isCached() {
		return bookSuite.getComicBookCount() == cachedBooks.size();
	}

	@Override
	public synchronized void cache(CacheContext context) {}

	@Override
	public synchronized void clear() {
		for(CachedComicBook book : cachedBooks) {
			book.clear();
		}
		cachedBooks.clear();
	}

	@Override
	public synchronized void refresh() {
		cacheBooks();
		clearLeastRecentlyUsedBooks();
	}

	@Override
	public int getContentType() {
		return CONTENT_TYPE_SUITE;
	}

	/**
	 * Cache the comic books according to the current index.
	 */
	private void cacheBooks() {
		cacheContext.addCacheTask(new PrefetchAction(getCachedBook(currentBookIndex)));
		for(int index = (currentBookIndex - CACHE_WINDOW_SIZE); index <= (currentBookIndex + CACHE_WINDOW_SIZE); index++) {
			if(index >= 0 && index < bookSuite.getComicBookCount() && index != currentBookIndex) {
				cacheContext.addCacheTask(new PrefetchAction(getCachedBook(index)));
			}
		}
	}

	/**
	 * Clear the least recently used comic books.
	 */
	private void clearLeastRecentlyUsedBooks() {
		for(int index = 0; index < cachedBooks.size(); index++) {
			if(index < (currentBookIndex - CACHE_WINDOW_SIZE) || index > (currentBookIndex + CACHE_WINDOW_SIZE)) {
				cachedBooks.get(index).clear();
			}
		}
	}

	/**
	 * Get the cached book specified by the given index.
	 * 
	 * @param index the index to get
	 * @return the cached book
	 */
	private CachedComicBook getCachedBook(int index) {
		if(index >= 0 && index < cachedBooks.size()) {
			return cachedBooks.get(index);
		}
		return null;
	}
}
