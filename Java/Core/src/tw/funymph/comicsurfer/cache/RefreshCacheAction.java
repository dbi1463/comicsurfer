/* RefreshCacheAction.java created on 2012/9/4
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.cache;

/**
 * An action to execute the refresh method in independent threads.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.5
 */
public class RefreshCacheAction extends CacheTask {

	/**
	 * Construct <code>RefreshCacheAction</code> instance with the content to be refreshed.
	 * 
	 * @param content the content to be refreshed (book or suite).
	 */
	public RefreshCacheAction(RefreshableCache content) {
		super(content);
	}

	@Override
	public void performTask() {
		if(cacheableContent != null) {
			((RefreshableCache)cacheableContent).refresh();
		}
	}
}
