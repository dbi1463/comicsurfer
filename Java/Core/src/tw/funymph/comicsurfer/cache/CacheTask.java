/* CacheTask.java created on 2013/5/14
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.cache;

/**
 * The base class of all cache tasks. The class use Template method to
 * ensure that all errors will be reported to the cache context during
 * {@link #performTask()}.
 * 
 * @author Pin-Ying Tu
 * @version 3.0 
 * @since 3.0
 */
public abstract class CacheTask implements Runnable {

	protected CacheContext cacheContext;
	protected Cacheable cacheableContent;

	/**
	 * Construct a <code>CacheTask</code> by its concrete classes.
	 * 
	 * @param content the content to be cached
	 */
	protected CacheTask(Cacheable content) {
		cacheableContent = content;
	}

	/**
	 * Set the cache context.
	 * 
	 * @param context the context
	 */
	public void setCacheContext(CacheContext context) {
		cacheContext = context;
	}

	@Override
	public void run() {
		try {
			performTask();
		}
		catch(Throwable e) {
			cacheContext.reportCacheFailed(null);
		}
	}

	/**
	 * Perform the real task.
	 */
	protected abstract void performTask();
}
