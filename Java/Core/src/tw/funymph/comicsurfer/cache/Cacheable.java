/* Cacheable.java created on 2012/9/3
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.cache;

/**
 * A class can implements this interface to offer the ability to cache
 * the content from slowly I/O resource before it really used.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.5
 */
public interface Cacheable {

	public static final int CONTENT_TYPE_PAGE = 0;
	public static final int CONTENT_TYPE_BOOK = 1;
	public static final int CONTENT_TYPE_SUITE = 2;

	/**
	 * Indicate whether the resource is cached or not. 
	 * 
	 * @return true if the resource has been cached ready for use
	 */
	public boolean isCached();

	/**
	 * Cache the resource from the slowly I/O.
	 * 
	 * @param context the cache context
	 */
	public void cache(CacheContext context);

	/**
	 * Clear the cached content to release the allocated resource.
	 */
	public void clear();

	/**
	 * Get the content type of the cacheable content.
	 * 
	 * @return the content type
	 */
	public int getContentType();
}
