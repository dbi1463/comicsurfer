/* CacheContext.java created on 2013/5/14
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.cache;

import java.util.Map;

/**
 * A class can implement this interface to offer a whole picture and
 * the resources need by the cache mechanism.
 * 
 * @author Pin-Ying Tu
 * @version 3.0 
 * @since 3.0
 */
public interface CacheContext {

	/**
	 * Add the cache task.
	 * 
	 * @param task the task to perform cache
	 */
	public void addCacheTask(CacheTask task);

	/**
	 * Report the error message while failed to cache the specified content.
	 * 
	 * @param report the detailed error report
	 */
	public void reportCacheFailed(Map<String, Object> report);
}
