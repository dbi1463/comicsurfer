/* LoadPageAction.java created on 2012/9/3
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.cache;

/**
 * An action to execute the cache method in independent threads.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.5
 */
public class PrefetchAction extends CacheTask {

	/**
	 * Construct <code>PrefetchAction</code> instance with the content to be pre-fetched.
	 * 
	 * @param content the content to be pre-fetched (page or book).
	 */
	public PrefetchAction(Cacheable content) {
		super(content);
	}

	@Override
	public void performTask() {
		if(cacheableContent != null && !cacheableContent.isCached()) {
			cacheableContent.cache(cacheContext);
		}
	}
}
