/* CachedPage.java created on 2012/8/19
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.cache;

import static java.lang.System.gc;
import static tw.funymph.commons.io.IOUtilities.copy;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import tw.funymph.comicsurfer.core.Page;
import tw.funymph.commons.io.IOCloser;

/**
 * A class that handle the cache of a page.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.5
 */
public class CachedPage implements Cacheable, Page {

	private static final int BUFFER_SIZE = 4096;

	// Open the visibility for unit test
	byte[] cache;

	private int index;

	private boolean cached;
	private boolean cacheFailed;

	private Page page;

	/**
	 * Construct a <code>CachedPage</code> instance with the page to be cached.
	 * 
	 * @param thePage the page to be cached
	 * @param theIndex the index of the page
	 */
	public CachedPage(Page thePage, int theIndex) {
		page = thePage;
		index = theIndex;

		cache = null;
		cached = false;
		cacheFailed = false;
	}

	/**
	 * Check whether the cache is down (failed also means down) or not.
	 * 
	 * @return true if the cache is down
	 */
	public synchronized boolean isReady() {
		return cached || cacheFailed;
	}

	@Override
	public int getIndex() {
		return (page.getIndex() == UNDEFINED_INDEX)? index : page.getIndex();
	}

	@Override
	public synchronized InputStream getContent() {
		if(cacheFailed) {
			return page.getContent();
		}
		else {
			return new ByteArrayInputStream(cache);
		}
	}

	/**
	 * The resource management of the cached page is determined by
	 * the algorithm of its cache container, so that calling this
	 * method has no effect.
	 */
	@Override
	public void open() {}

	/**
	 * The resource management of the cached page is determined by
	 * the algorithm of its cache container, so that calling this
	 * method has no effect.
	 */
	@Override
	public void close() {}

	@Override
	public synchronized void cache(CacheContext context) {
		if(!cached) {
			InputStream source = page.getContent();
			ByteArrayOutputStream output = new ByteArrayOutputStream(BUFFER_SIZE);
			try {
				copy(source, output);
				cache = output.toByteArray();
				cached = true;
			} catch(Exception e) {
				cacheFailed = true;
				if(context != null) {
					context.reportCacheFailed(null);
				}
			} finally {
				IOCloser.close(source);
				IOCloser.close(output);
				output = null;
			}
			gc();
		}
	}

	@Override
	public synchronized void clear() {
		if(cached) {
			cache = null;
			cached = false;
		}
	}

	@Override
	public synchronized boolean isCached() {
		return cached;
	}

	@Override
	public int getContentType() {
		return CONTENT_TYPE_PAGE;
	}
}
