/* LanguageKeys.java created on 2012/10/16
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer;

/**
 * All language keys used in the Comic Surfer Desktop version.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public interface LanguageKeys {

	// The language keys used by the preferences
	public static final String LANG_KEY_PREFERENCES = "Preferences";
	public static final String LANG_KEY_BEHAVIOR = "Behavior";
	public static final String LANG_KEY_IMAGE_OPTIONS = "Image Options";
	public static final String LANG_KEY_DISPLAY_MODE = "Image display mode";
	public static final String LANG_KEY_SLIDESHOW_SPEED = "Slideshow speed";
	public static final String LANG_KEY_TRACK_MOUSE = "Image tracks mouse movement";
	public static final String LANG_KEY_DIALOG_OPTIONS = "Dialog Options";
	public static final String LANG_KEY_SHOW_OPEN_DIALOG = "Show open dialog on startup";
	public static final String LANG_KEY_APPEARANCE = "Appearance";
	public static final String LANG_KEY_COLORS = "Colors";
	public static final String LANG_KEY_BACKGROUND_COLOR = "Background Color";
	public static final String LANG_KEY_INTERNATIONALIZATION = "Internationalization";
	public static final String LANG_KEY_LANGUAGE = "Display Language";

	// The language keys used by the actions
	public static final String LANG_KEY_CLOSE = "Close";
	public static final String LANG_KEY_CLOSE_DESCRIPTION = "Close the book";
	public static final String LANG_KEY_ABOUT = "About";
	public static final String LANG_KEY_ABOUT_DESCRIPTION = "About Comic Surfer";
	public static final String LANG_KEY_EXIT = "Exit";
	public static final String LANG_KEY_EXIT_DESCRIPTION = "Exit Comic Surfer";
	public static final String LANG_KEY_GO_BACK = "Go Back";
	public static final String LANG_KEY_GO_BAKC_DESCRIPTION = "Go back to the previous viewed page";
	public static final String LANG_KEY_NEXT_PAGE = "Next Page";
	public static final String LANG_KEY_NEXT_PAGE_DESCRIPTION = "Show the next page";
	public static final String LANG_KEY_NEXT_VOLUME = "Next Volume";
	public static final String LANG_KEY_NEXT_VOLUME_DESCRIPTION = "Show the next volume";
	public static final String LANG_KEY_OPEN = "Open";
	public static final String LANG_KEY_OPEN_DESCRIPTION = "Open comic books";
	public static final String LANG_KEY_OPEN_FROM = "Open From";
	public static final String LANG_KEY_OPEN_FROM_DESCRIPTION = "Open comic books from";
	public static final String LANG_KEY_OPEN_FROM_DIALOG_TITLE = "Comic Book Location";
	public static final String LANG_KEY_OPEN_FROM_DIALOG_MSG = "Please enter the comic book location:";
	public static final String LANG_KEY_PREVIOUS_PAGE = "Previous Page";
	public static final String LANG_KEY_PREVIOUS_PAGE_DESCRIPTION = "Show the previous page";
	public static final String LANG_KEY_PREVIOUS_VOLUME = "Previous Volume";
	public static final String LANG_KEY_PREVIOUS_VOLUME_DESCRIPTION = "Show the previous volume";
	public static final String LANG_KEY_ACTUAL_IMAGE_SIZE_DESCRIPTION = "Display in actual image size";
	public static final String LANG_KEY_FIT_WINDOW_WIDTH = "Fit Window Width";
	public static final String LANG_KEY_FIT_WINDOW_SIZE = "Fit Window Size";
	public static final String LANG_KEY_FIT_WINDOW_HEIGHT = "Fit Window Height";
	public static final String LANG_KEY_ACTUAL_IMAGE_SIZE = "Actual Image Size";
	public static final String LANG_KEY_FIT_WINDOW_HEIGHT_DESCRIPTION = "Fit to the window height when the image is larger than the window";
	public static final String LANG_KEY_FIT_WINDOW_SIZE_DESCRIPTION = "Fit to the window width/height when the image is larger than the window";
	public static final String LANG_KEY_FIT_WINDOW_WIDTH_DESCRIPTION = "Fit to the window width when the image is larger than the window";
	public static final String LANG_KEY_PREFERENCES_DESCRIPTION = "Edit Preferences";
	public static final String LANG_KEY_UPDATE_COMIC_SURFER = "Update Comic Surfer";
	public static final String LANG_KEY_FILE_FILTER_DESCRIPTION = "Readable Comic Books";
	public static final String LANG_KEY_SLIDE_SHOW = "Slide Show";
	public static final String LANG_KEY_START_SLIDE_SHOW = "Start Slide Show";
	public static final String LANG_KEY_STOP_SLIDE_SHOW = "Stop Slide Show";

	// The language keys used by the menus
	public static final String LANG_KEY_FILE_MENU = "File";
	public static final String LANG_KEY_VIEW_MENU = "View";
	public static final String LANG_KEY_LANGUAGE_MENU = "Language";
	public static final String LANG_KEY_HELP_MENU = "Help";
	public static final String LANG_KEY_NEWS_MENU = "News";
	public static final String LANG_KEY_RECENT_VIEWED = "Recent Viewed";

	// The language keys used as the messages
	public static final String LANG_KEY_ERROR_MSG_ON_LOADING_IMAGE = "Unable to load the image";
	public static final String LANG_KEY_LOADING_MSG = "Loading...";
	public static final String LANG_KEY_UPDATE_COMPLETED = "Update completed.";
	public static final String LANG_KEY_TRY_AGAIN_LATER = "Update failed, please try again later.";
	public static final String LANG_KEY_RESTART_MESSAGE = "Update completed. Would you like to restart Comic Surfer?";
	public static final String LANG_KEY_UPDATING = "Updating...";
	public static final String LANG_KEY_BAKUPING = "Backuping...";
	public static final String LANG_KEY_DOWNLOADING = "Downloading...";
	public static final String LANG_KEY_COMPLETED = "Completed.";
	public static final String LANG_KEY_BACKUP_FAILED = "Failed to backup the current version. Update aborted.";
	public static final String LANG_KEY_DOWNLOAD_FAILED = "Failed to download the new version. Update aborted.";
	public static final String LANG_KEY_WAIT_UNCOMPLETED_UPDATE_OR_VERSION_CHECK = "An update or version check task is not completed. Please try update later.";
	public static final String LANG_KEY_CHECK_FAILED = "Unable to get the remote version information.";
}
