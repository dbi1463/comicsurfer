/* OSXSupport.java created on Dec 10, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer;

import static com.apple.eawt.Application.getApplication;
import static tw.funymph.comicsurfer.actions.SystemActions.ABOUT;
import static tw.funymph.comicsurfer.actions.SystemActions.EXIT;
import static tw.jsway.utilities.ImageUtilities.loadIcon;

import javax.swing.ActionMap;

import com.apple.eawt.AboutHandler;
import com.apple.eawt.AppEvent.AboutEvent;
import com.apple.eawt.AppEvent.QuitEvent;
import com.apple.eawt.Application;
import com.apple.eawt.QuitHandler;
import com.apple.eawt.QuitResponse;

/**
 * A class that supports Mac OS X specific options.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.3
 */
public class OSXSupport implements QuitHandler, AboutHandler  {

	private static final String DOCK_ICON_PATH = "icons/ComicSurferIcon_256.png";
	private static final String APPLYING_MAC_OS_X_SETTINGS_MESSAGE = "  Applying Mac OS X specific settings...";

	private ActionMap actions;

	/**
	 * Register Mac OS X specific handlers
	 * 
	 * @param actions the actions to adapt
	 */
	public static void registerHandlers(ActionMap actions) {
		System.out.println(APPLYING_MAC_OS_X_SETTINGS_MESSAGE);
		OSXSupport support = new OSXSupport(actions);
		Application app = getApplication();
		app.setAboutHandler(support);
		app.setDockIconImage(loadIcon(DOCK_ICON_PATH).getImage());
		app.setQuitHandler(support);
	}

	/**
	 * Construct a <code>OSXSupport</code> instance by its
	 * inherited concrete classes.
	 * 
	 * @param theActions the actions to adapt
	 */
	protected OSXSupport(ActionMap theActions) {
		actions = theActions;
	}

	@Override
	public void handleAbout(AboutEvent e) {
		actions.get(ABOUT).actionPerformed(null);
	}

	@Override
	public void handleQuitRequestWith(QuitEvent e, QuitResponse response) {
		actions.get(EXIT).actionPerformed(null);
	}
}
