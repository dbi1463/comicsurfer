/* ComicSurfer.java created on Oct 13, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer;

import static java.lang.Integer.parseInt;
import static java.lang.String.valueOf;
import static java.lang.System.exit;
import static java.lang.System.getProperty;
import static java.lang.System.out;
import static java.lang.System.setProperty;
import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;
import static java.util.Calendar.getInstance;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.showMessageDialog;
import static tw.funymph.comicsurfer.OSXSupport.registerHandlers;
import static tw.funymph.comicsurfer.actions.SystemActions.getActions;

import java.io.File;
import java.util.Calendar;

import javax.swing.ActionMap;
import javax.swing.JFrame;

import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;
import tw.funymph.comicsurfer.ui.ComicSurferFrame;
import tw.jsway.memory.MemoryUsageMonitor;

/**
 * The main program entry and system variables.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public class ComicSurfer {

	private static final String NAME_SEPARATOR = " ";
	private static final String SIZE_SEPARATOR = "x";
	private static final String VERSION_SPEARATOR = ".";
	private static final String KEY_VALUE_SEPARATOR = "=";

	private static final String BUILD_PREFIX = " Build ";
	private static final String VERSION_PREFIX = " Version ";

	// Property names
	private static final String OS_NAME_PROPERTY = "os.name";
	private static final String JVM_VERSION_PROPERTY = "java.version";
	private static final String USE_SCREEN_MENU_BAR_PROPERTY = "apple.laf.useScreenMenuBar";
	private static final String APPLICATION_NAME_PROPERTY = "com.apple.mrj.application.apple.menu.about.name";

	// Property values
	private static final String OS_X_PROPERTY_VALUE = "OS X";
	private static final String MINIMUM_MULTI_CHARSET_JVM_VERSION = "1.7";

	// Messages
	private static final String MAC_OS_X_DETECTED_MESSAGE = "Mac OS X environment is detected.";

	private static final String SIZE_ARGUMENT = "-size";
	private static final String TRANSLATE_ARGUMENT = "-translate";

	private static final double DEFAULT_RECYCLE_BOUNDARY = 0.5f;

	private static final int WIDTH_INDEX = 0;
	private static final int HEIGHT_INDEX = 1;
	private static final int VALUE_PART_INDEX = 1;

	public static final int MAJOR_VERSION = 3;
	public static final int MINOR_VERSION = 0;
	public static final int BUILD_VERSION = parseBuildVersion();
	public static final int PRODUCT_VERSION = 2013;

	public static final String PRODUCT_NAME = "Comic Surfer";
	public static final String SHORT_VERSION = MAJOR_VERSION + VERSION_SPEARATOR + MINOR_VERSION;
	public static final String FULL_VERSION = SHORT_VERSION + BUILD_PREFIX + BUILD_VERSION;
	public static final String FULL_PRODUCT_NAME = PRODUCT_NAME + NAME_SEPARATOR + PRODUCT_VERSION;
	public static final String FULL_PRODUCT_VERSION = PRODUCT_VERSION + VERSION_PREFIX + FULL_VERSION;
	public static final String FULL_VERSIONED_PRODUCT_NAME = PRODUCT_NAME + NAME_SEPARATOR + FULL_PRODUCT_VERSION;

	private static boolean isJVM7;
	private static boolean useMacStyle;
	private static ComicSurferFrame mainFrame;

	/**
	 * Get the main displayed frame.
	 * 
	 * @return the main frame
	 */
	public static ComicSurferFrame getMainFrame() {
		return mainFrame;
	}

	/**
	 * The main program entry
	 * 
	 * @param arguments the command line arguments
	 */
	public static void main(String[] arguments) {
		try {
			checkSystemEnvironment();
			initializeComicSurfer(arguments);
		}
		catch(Throwable e) {
			e.printStackTrace();
			showMessageDialog(null, e.getMessage(), "Unable to start " + PRODUCT_NAME, ERROR_MESSAGE);
			exit(-1);
		}
	}

	/**
	 * Check the system environment.
	 */
	private static void checkSystemEnvironment() {
		isJVM7 = getProperty(JVM_VERSION_PROPERTY).contains(MINIMUM_MULTI_CHARSET_JVM_VERSION);
		useMacStyle = getProperty(OS_NAME_PROPERTY).contains(OS_X_PROPERTY_VALUE);
		if(useMacStyle) {
			out.println(MAC_OS_X_DETECTED_MESSAGE);
			setProperty(USE_SCREEN_MENU_BAR_PROPERTY, valueOf(true));
			setProperty(APPLICATION_NAME_PROPERTY, PRODUCT_NAME);
		}
		out.println("Operating System: " + getProperty(OS_NAME_PROPERTY));
		out.println("Java Runtime Environment: " + getProperty(JVM_VERSION_PROPERTY));
	}

	/**
	 * Initialize the Comic Surfer
	 * 
	 * @param arguments the command arguments
	 */
	private static void initializeComicSurfer(String[] arguments) {
		out.println("Initializing " + FULL_VERSIONED_PRODUCT_NAME + "...");
		ComicSurferDesktopModel model = new ComicSurferDesktopModel(isJVM7);
		model.initialize();
		ActionMap actions = getActions(model);
		if(useMacStyle) {
			registerHandlers(actions);
		}
		MemoryUsageMonitor monitor = MemoryUsageMonitor.getInstance();
		out.println("  Enable periodic memory usage monitor (period: " + monitor.getMonitorPeriod() + " ms).");
		monitor.setAutoRecycle(true);
		monitor.setRecycleBoundary(DEFAULT_RECYCLE_BOUNDARY);
		out.println("  Enable auto-recycling (recycling when memory usage > " + monitor.getRecycleBoundary() + ").");
		mainFrame = new ComicSurferFrame(model, actions);
		try {
			parse(arguments, model, mainFrame);
		}
		catch(Exception e) {
			e.printStackTrace();
			help();
			return;
		}
		out.println("Initialization completed.");
		out.println("Show " + FULL_PRODUCT_NAME + ".");
		mainFrame.setVisible(true);
	}

	/**
	 * Parse the arguments and modify the program behavior if any legal
	 * argument is specified.
	 * 
	 * @param arguments the arguments
	 * @param model the <code>ComicSurferModel</code> instance
	 * @param frame the main frame
	 * @return true when every thing is normal, and needs to show the main window
	 */
	private static void parse(String[] arguments, ComicSurferDesktopModel model, JFrame frame) {
		boolean fileOpened = false;
		for(String argument : arguments) {
			String[] parts = argument.split(KEY_VALUE_SEPARATOR);
			if(argument.startsWith(SIZE_ARGUMENT)) {
				String[] sizes = parts[VALUE_PART_INDEX].split(SIZE_SEPARATOR);
				int width = parseInt(sizes[WIDTH_INDEX]);
				int height = parseInt(sizes[HEIGHT_INDEX]);
				frame.setSize(width, height);
				frame.setExtendedState(JFrame.NORMAL);
			}
			else if(argument.startsWith(TRANSLATE_ARGUMENT)) {
				XMLLanguageFileGenerator generator = new XMLLanguageFileGenerator();
				generator.generate("en-US", "English, United States", "");
			}
			else if(!fileOpened) {
				File file = new File(argument);
				if(file.exists()) {
					model.open(file.toURI());
					fileOpened = true;
				}
			}
		}
	}

	/**
	 * Show the help information.
	 */
	private static void help() {
		out.println("ComicSurfer [-size=WxH] [-translate=locale] [file to open]");
	}

	/**
	 * Parse the build version from the MANIFEST.MF in the JAR file. If the application
	 * is not opened from a JAR, the build version will be the opening date (for testing).
	 * 
	 * @return build version
	 */
	private static int parseBuildVersion() {
		String build = ComicSurfer.class.getPackage().getImplementationVersion();
		if(build != null) {
			return parseInt(build);
		}
		Calendar now = getInstance();
		return now.get(YEAR) * 10000 + (now.get(MONTH) + 1) * 100 + now.get(DAY_OF_MONTH);
	}
}
