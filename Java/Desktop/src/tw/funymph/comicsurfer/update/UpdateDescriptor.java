/* UpdateDescriptor.java created on 2012/9/7
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.update;

import static java.lang.System.out;
import static tw.funymph.comicsurfer.ComicSurfer.PRODUCT_NAME;

import java.util.Map;

import tw.funymph.commons.xml.AbstractXMLDecoderTreeNode;

/** The class defines the information about how to update the new version.
 * 
 * @author Pin-Ying Tu
 * @version 2.0
 * @since 2.0
 */
public class UpdateDescriptor extends AbstractXMLDecoderTreeNode {

	private static final String ELEMENT_ROOT = "product";
	private static final String ELEMENT_FILE = "file";
	private static final String ELEMENT_VERSION = "version";

	private static final String ATTRIBUTE_URL = "url";
	private static final String ATTRIBUTE_SIZE = "size";
	private static final String ATTRIBUTE_NAME = "name";
	private static final String ATTRIBUTE_MAJOR = "major";
	private static final String ATTRIBUTE_MINOR = "minor";
	private static final String ATTRIBUTE_BUILD = "build";

	private String newVersionPath;
	private String productName;

	private int fileSize;
	private int localMajorVersion;
	private int localMinorVersion;
	private int localBuildVersion;
	private int remoteMajorVersion;
	private int remoteMinorVersion;
	private int remoteBuildVersion;

	/** Construct a <code>UpdateDescriptor</code> instance with local version information.
	 * 
	 * @param majorVersion the local major version
	 * @param minorVersion the local minor version
	 * @param build the local build version
	 */
	public UpdateDescriptor(int majorVersion, int minorVersion, int build) {
		super(ELEMENT_ROOT);
		localMajorVersion = majorVersion;
		localMinorVersion = minorVersion;
		localBuildVersion = build;
		addChildNode(new FileInfoDecoder());
		addChildNode(new VersionInfoDecoder());
	}

	/** Get whether the remote server has a newer version.
	 * 
	 * @return true if the remote server has a newer version
	 */
	public boolean hasNewVersion() {
		if(remoteMajorVersion > localMajorVersion) {
			return true;
		}
		else if(remoteMajorVersion == localMajorVersion &&
				remoteMinorVersion > localMinorVersion) {
			return true;
		}
		else if(remoteMajorVersion == localMajorVersion &&
				remoteMinorVersion == localMinorVersion &&
				remoteBuildVersion > localBuildVersion) {
			return true;
		}
		return false;
	}

	/** Get the path to download the newer version
	 * 
	 * @return the path to download the newer version
	 */
	public String getNewVersionPath() {
		return newVersionPath;
	}

	/** Get the file size of the newer version
	 * 
	 * @return the file size of the newer version
	 */
	public int getFileSize() {
		return fileSize;
	}

	@Override
	public void processAttributes(Map<String, String> attributes) {
		productName = attributes.get(ATTRIBUTE_NAME);
	}

	@Override
	public void processContent(String content) {}

	/** Get whether the parsed version information is for the correct product.
	 * 
	 * @return true if the information is for the current product
	 */
	private boolean isCorrectProductName() {
		return productName != null? PRODUCT_NAME.equals(productName) : false;
	}

	/** The decoder to decode the version information from the remote XML stream.
	 * 
	 * @author Pin-Ying Tu
	 * @version 2.0
	 * @since 2.0
	 */
	private class VersionInfoDecoder extends AbstractXMLDecoderTreeNode {

		/** Construct a <code>VersionInfoDecoder</code> instance.
		 */
		public VersionInfoDecoder() {
			super(ELEMENT_VERSION);
		}

		@Override
		public void processAttributes(Map<String, String> attributes) {
			if(isCorrectProductName()) {
				remoteMajorVersion = Integer.parseInt(attributes.get(ATTRIBUTE_MAJOR));
				remoteMinorVersion = Integer.parseInt(attributes.get(ATTRIBUTE_MINOR));
				remoteBuildVersion = Integer.parseInt(attributes.get(ATTRIBUTE_BUILD));
				out.println("Found remote version: " + remoteMajorVersion + "." + remoteMinorVersion + " Build " + remoteBuildVersion);
			}
		}

		@Override
		public void processContent(String content) {}
	}

	/** The decoder to decode the file information from the remote XML stream.
	 * 
	 * @author Pin-Ying Tu
	 * @version 2.0
	 * @since 2.0
	 */
	private class FileInfoDecoder extends AbstractXMLDecoderTreeNode {

		/** Construct a <code>FileInfoDecoder</code> instance.
		 */
		public FileInfoDecoder() {
			super(ELEMENT_FILE);
		}

		@Override
		public void processAttributes(Map<String, String> attributes) {
			if(isCorrectProductName()) {
				newVersionPath = attributes.get(ATTRIBUTE_URL);
				fileSize = Integer.parseInt(attributes.get(ATTRIBUTE_SIZE));
			}
		}

		@Override
		public void processContent(String content) {}
	}
}
