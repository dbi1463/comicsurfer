/* VersionChecker.java created on 2012/9/7
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.update;

import java.net.URL;

import tw.funymph.comicsurfer.LanguageKeys;
import tw.funymph.commons.xml.XMLDecoder;

/** The helper class that actually check whether there is a new version
 * can be used to update. The visibility of the class is designed to let
 * the class can be used by {@link AsyncUpdateManager} only.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.0
 */
class VersionChecker implements Runnable {

	private static final int UNKNOWN_VERSION = -1;

	private static final String UPDATE_INFO_URL = "https://dl.dropbox.com/u/19418059/ComicSurfer/update.xml";

	private XMLDecoder decoder;
	private UpdateDescriptor updateDescriptor;
	private UpdateProgressListenerList listeners;

	private int localMajorVersion;
	private int lcoalMinorVersion;
	private int localBuildVersion;

	/** Construct a <code>VersionChecker</code> instance.
	 * 
	 * @param theListeners the listeners to be notified
	 */
	VersionChecker(UpdateProgressListenerList theListeners) {
		listeners = theListeners;
		decoder = new XMLDecoder();
		setVersionInfor(UNKNOWN_VERSION, UNKNOWN_VERSION, UNKNOWN_VERSION);
	}

	/** Set the local version information.
	 * 
	 * @param majorVersion the local major version
	 * @param minorVersion the local minor version
	 * @param build the local build version
	 */
	public void setVersionInfor(int majorVersion, int minorVersion, int build) {
		localMajorVersion = majorVersion;
		lcoalMinorVersion = minorVersion;
		localBuildVersion = build;
		updateDescriptor = null;
	}

	/** Get the update descriptor.
	 * 
	 * @param wait true will try to parse the version information if the descriptor is not existed
	 * @return the descriptor; null if failed to get the remote version information
	 */
	public UpdateDescriptor getUpdateDescriptor(boolean wait) {
		if(updateDescriptor == null && wait) {
			run();
		}
		return updateDescriptor;
	}

	@Override
	public void run() {
		try {
			parseVersionInfo();
			if(updateDescriptor != null) {
				listeners.fireCheckFinishedEvent(updateDescriptor);
			}
			else {
				listeners.fireErrorOccurredEvent(LanguageKeys.LANG_KEY_CHECK_FAILED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			listeners.fireErrorOccurredEvent(LanguageKeys.LANG_KEY_CHECK_FAILED);
		}
	}

	/** Parse the version information from the default URL.
	 * 
	 * @return the descriptor; null if failed
	 * @throws Exception if any error occurs
	 */
	private UpdateDescriptor parseVersionInfo() throws Exception {
		if(localMajorVersion == UNKNOWN_VERSION || lcoalMinorVersion == UNKNOWN_VERSION || localBuildVersion == UNKNOWN_VERSION) {
			updateDescriptor = null;
		}
		URL url = new URL(UPDATE_INFO_URL);
		UpdateDescriptor descriptor = new UpdateDescriptor(localMajorVersion, lcoalMinorVersion, localBuildVersion);
		decoder.decode(descriptor, url.openStream());
		updateDescriptor = descriptor;
		return updateDescriptor;
	}
}
