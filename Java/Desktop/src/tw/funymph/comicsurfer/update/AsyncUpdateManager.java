/* AsyncUpdateManager.java created on 2012/9/7
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.update;

/** The class can implements this interface to offer the ability
 * to update the software asynchronously.
 * 
 * @author Pin-Ying Tu
 * @version 2.0
 * @since 2.0
 */
public interface AsyncUpdateManager {

	/** Start to update the software. The method will return immediately and
	 * the progress or the status will be notified to the registered listeners.
	 */
	public void startUpdate();

	/** Start to check whether there is a new version. The method will return
	 * immediately and the progress or the status will be notified to the
	 * registered listeners.
	 * 
	 * @param majorVersion the current major version
	 * @param minorVersion the current minor version
	 * @param build the build number
	 */
	public void startVersionCheck(int majorVersion, int minorVersion, int build);

	/** Get whether a new version is available for download.
	 * 
	 * @return true if a new version is available.
	 */
	public boolean hasNewVersion();

	/** Add the listener into the notification list. All progress and status
	 * during the version check or software update will be notified to the
	 * registered listeners in the list. 
	 * 
	 * @param listener the listener
	 */
	public void addUpdateProgressListener(UpdateProgressListener listener);

	/** Remove the listener from the notification list.
	 * 
	 * @param listener the listener
	 */
	public void removeUpdateProgressListener(UpdateProgressListener listener);
}
