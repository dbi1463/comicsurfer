/* UpdateProgressListenerList.java created on 2012/9/7
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.update;

import java.util.LinkedList;
import java.util.List;

/** The helper class that manages the registered update progress listeners.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.0
 */
public class UpdateProgressListenerList {

	private List<UpdateProgressListener> listeners;

	/** Construct <code>UpdateProgressListenerList</code> instance.
	 */
	public UpdateProgressListenerList() {
		listeners = new LinkedList<UpdateProgressListener>();
	}

	/** Add the specified listener into the notification list.
	 * 
	 * @param listener the listener to be added
	 */
	public void addUpdateProgressListener(UpdateProgressListener listener) {
		if(!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	/** Remove the specified listener from the notification list.
	 * 
	 * @param listener the listener to be removed
	 */
	public void removeUpdateProgressListener(UpdateProgressListener listener) {
		listeners.remove(listener);
	}

	/** Fire an event that an error has occurred to all registered listeners.
	 * 
	 * @param message the error message
	 */
	public void fireErrorOccurredEvent(String message) {
		for(UpdateProgressListener listener : listeners) {
			listener.errorOccurred(message);
		}
	}

	/** Fire an event that the version check is finished to all registered listeners.
	 * 
	 * @param descriptor the update descriptor
	 */
	public void fireCheckFinishedEvent(UpdateDescriptor descriptor) {
		for(UpdateProgressListener listener : listeners) {
			listener.checkFinished(descriptor);
		}
	}

	/** Fire an event that the software update is finished to all registered listeners.
	 * 
	 * @param success the result of software update
	 */
	public void fireUpdateFinishedEvent(boolean success) {
		for(UpdateProgressListener listener : listeners) {
			listener.updateFinished(success);
		}
	}

	/** Fire an event that the progress is updated to all registered listeners.
	 * 
	 * @param info the information of the current progress
	 * @param extra the extra information
	 * @param start the progress value when the current task started
	 * @param current the current progress value
	 * @param end the progress value when the current task will end
	 */
	public void fireProgressUpdatedEvent(String info, String extra, int start, int current, int end) {
		for(UpdateProgressListener listener : listeners) {
			listener.progressUpdated(info, extra, start, current, end);
		}
	}
}
