/* SoftwareUpdater.java created on 2012/9/7
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.update;

import static tw.funymph.comicsurfer.ComicSurfer.*;
import static tw.funymph.comicsurfer.LanguageKeys.*;
import static tw.funymph.commons.file.FileUtilities.ZIP_FILE_EXTENSION;
import static tw.funymph.commons.file.FileUtilities.makeFolderIfNotExist;
import static tw.funymph.commons.io.IOUtilities.downloadFileTo;
import static tw.funymph.commons.io.IOUtilities.parseFilename;
import static tw.funymph.commons.zip.ZipUtilities.packFiles;
import static tw.funymph.commons.zip.ZipUtilities.unpack;

import java.io.File;
import java.io.FileFilter;
import java.text.NumberFormat;

import tw.funymph.commons.io.IOProgressListener;

/** The helper class that actually update the software. The visibility of the class
 * is designed to let the class can be used by {@link AsyncUpdateManager} only.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.0
 */
class SoftwareUpdater implements Runnable, FileFilter, IOProgressListener {

	private static final String PROGRAM_ROOT = ".";
	private static final String DEFAULT_BACKUP_PATH = "backup";
	private static final String DEFAULT_DOWNLOAD_PATH = "download";

	private static final String IGNORED_FOLDERS[] = { DEFAULT_DOWNLOAD_PATH, DEFAULT_BACKUP_PATH };

	private NumberFormat formatter;
	private UpdateDescriptor updateDescriptor;
	private UpdateProgressListenerList listeners;

	/** Construct a <code>SoftwareUpdater</code> instance.
	 * 
	 * @param theListeners the listeners to be notified
	 */
	SoftwareUpdater(UpdateProgressListenerList theListeners) {
		listeners = theListeners;
		formatter = NumberFormat.getInstance();
		formatter.setMaximumFractionDigits(1);
	}

	/** Set the update descriptor.
	 * 
	 * @param descriptor the new update descriptor
	 */
	public void setDescriptor(UpdateDescriptor descriptor) {
		updateDescriptor = descriptor;
	}

	/** Update the software.
	 * 
	 * @throws Exception when any error occurred during the software update
	 */
	public void update(UpdateDescriptor descriptor) throws Exception {
		if(descriptor != null && descriptor.hasNewVersion()) {
			listeners.fireProgressUpdatedEvent(LANG_KEY_DOWNLOADING, "0/3", 0, 0, 3);
			downloadFile();
			listeners.fireProgressUpdatedEvent(LANG_KEY_BAKUPING, "1/3", 0, 1, 3);
			backupFiles();
			listeners.fireProgressUpdatedEvent(LANG_KEY_UPDATING, "2/3", 0, 2, 3);
			replaceFiles();
			listeners.fireProgressUpdatedEvent(LANG_KEY_COMPLETED, "3/3", 0, 3, 3);
		}
	}

	@Override
	public void run() {
		try {
			update(updateDescriptor);
			listeners.fireUpdateFinishedEvent(true);
		} catch (Exception e) {
			e.printStackTrace();
			listeners.fireErrorOccurredEvent(e.getMessage());
		}
	}

	@Override
	public boolean accept(File file) {
		if(file.isDirectory()) {
			for(String ignored : IGNORED_FOLDERS) {
				if(ignored.equalsIgnoreCase(file.getName())) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public void errorOccurred(String message) {
		listeners.fireErrorOccurredEvent(message);		
	}

	@Override
	public void progressUpdated(int processed, double speed, int unit) {
		double speedInKB = speed / 1024;
		double speedInMB = speedInKB / 1024;
		String speedInfo = (speedInMB > 1.0)? (formatter.format(speedInMB) + "MB/s") : (formatter.format(speedInKB) + "KB/s");
		listeners.fireProgressUpdatedEvent(LANG_KEY_DOWNLOADING, speedInfo, 0, processed, updateDescriptor.getFileSize());
	}

	/** Download the file recorded in the update descriptor.
	 * 
	 * @return the download size in bytes
	 * @throws Exception if any error occurs
	 */
	private boolean downloadFile() throws Exception {
		makeFolderIfNotExist(new File(DEFAULT_DOWNLOAD_PATH));
		int size = downloadFileTo(updateDescriptor.getNewVersionPath(), DEFAULT_DOWNLOAD_PATH, this);
		if(size != updateDescriptor.getFileSize()) {
			throw new Exception(LANG_KEY_DOWNLOAD_FAILED);
		}
		return size == updateDescriptor.getFileSize();
	}

	/** Backup the current version.
	 * 
	 * @throws Exception if any error occurs
	 */
	private void backupFiles() throws Exception {
		makeFolderIfNotExist(new File(DEFAULT_BACKUP_PATH));
		File[] filesToBackup = new File(PROGRAM_ROOT).listFiles(this);
		String filename = MAJOR_VERSION + "." + MINOR_VERSION + "." + BUILD_VERSION + "." + ZIP_FILE_EXTENSION;
		packFiles(filesToBackup, DEFAULT_BACKUP_PATH, filename);
		if(!(new File(DEFAULT_BACKUP_PATH + File.separator + filename).exists())) {
			throw new Exception(LANG_KEY_BACKUP_FAILED);
		}
	}

	/** Replace the current version with the new version.
	 */
	private void replaceFiles() {
		String filename = parseFilename(updateDescriptor.getNewVersionPath());
		File source = new File(DEFAULT_DOWNLOAD_PATH + File.separator + filename);
		unpack(source, PROGRAM_ROOT);
	}
}
