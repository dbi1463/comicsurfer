/* UpdateProgressListener.java created on 2012/9/7
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.update;

/** The class which is interested in the progress or status of {@link AsyncUpdateManager}
 * can implement this interface and register to receive the events.
 * 
 * @author Pin-Ying Tu
 * @version 2.0
 * @since 2.0
 */
public interface UpdateProgressListener {

	/** Invoke when an error occurred on updating software or checking version.
	 * 
	 * @param message the error message
	 */
	public void errorOccurred(String message);

	/** Invoke when the version check is finished.
	 * 
	 * @param descriptor the update descriptor
	 */
	public void checkFinished(UpdateDescriptor descriptor);

	/** Invoke when the software update is finished.
	 * 
	 * @param success true means the update is succeeded.
	 */
	public void updateFinished(boolean success);

	/** Invoke when the progress is updated.
	 * 
	 * @param info the information of the current progress
	 * @param extra the extra information
	 * @param start the progress value when the current task started
	 * @param current the current progress value
	 * @param end the progress value when the current task will end
	 */
	public void progressUpdated(String info, String extra, int start, int current, int end);
}
