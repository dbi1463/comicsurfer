/* DefaultUpdateManager.java created on 2012/9/7
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.update;

import tw.funymph.comicsurfer.LanguageKeys;

/** The default implementation of {@link UpdateManager} and {@link AsyncUpdateManager}
 * to offer the ability to update software synchronous or asynchronously. Note that
 * the default update manager is a singleton to ensure only one thread to access version
 * check or software update at the same time.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.0
 */
public class DefaultUpdateManager implements UpdateManager, AsyncUpdateManager {

	private static DefaultUpdateManager updateManager;

	private Thread backgroundThread;
	private VersionChecker versionChecker;
	private SoftwareUpdater softwareUpdater;
	private UpdateProgressListenerList listeners;

	/** Get the default update manager (singleton).
	 * 
	 * @return the default update manager
	 */
	public static DefaultUpdateManager getDefaultManager() {
		if(updateManager == null) {
			updateManager = new DefaultUpdateManager();
		}
		return updateManager;
	}

	/** The construction of <code>DefaultUpdateManager</code> is disabled. Please
	 * use the {@link #getDefaultManager()} method to get the default manager.
	 */
	private DefaultUpdateManager() {
		listeners = new UpdateProgressListenerList();
		versionChecker = new VersionChecker(listeners);
		softwareUpdater = new SoftwareUpdater(listeners);
	}

	@Override
	public synchronized boolean update(UpdateDescriptor descriptor) {
		try {
			softwareUpdater.update(descriptor);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public synchronized UpdateDescriptor getUpdateDescriptor(int majorVersion, int minorVersion, int build) {
		UpdateDescriptor descriptor = null;
		try {
			versionChecker.setVersionInfor(majorVersion, minorVersion, build);
			descriptor = versionChecker.getUpdateDescriptor(true);
		} catch (Exception e) {
			descriptor = null;
		}
		return descriptor;
	}

	@Override
	public synchronized void startUpdate() {
		if(backgroundThread.isAlive()) {
			listeners.fireErrorOccurredEvent(LanguageKeys.LANG_KEY_WAIT_UNCOMPLETED_UPDATE_OR_VERSION_CHECK);
			return;
		}
		softwareUpdater.setDescriptor(versionChecker.getUpdateDescriptor(true));
		backgroundThread = new Thread(softwareUpdater);
		backgroundThread.start();
	}

	@Override
	public synchronized void startVersionCheck(int majorVersion, int minorVersion, int build) {
		versionChecker.setVersionInfor(majorVersion, minorVersion, build);
		backgroundThread = new Thread(versionChecker);
		backgroundThread.start();
	}

	@Override
	public void addUpdateProgressListener(UpdateProgressListener listener) {
		listeners.addUpdateProgressListener(listener);
	}

	@Override
	public void removeUpdateProgressListener(UpdateProgressListener listener) {
		listeners.removeUpdateProgressListener(listener);
	}

	@Override
	public boolean hasNewVersion() {
		UpdateDescriptor descriptor = versionChecker.getUpdateDescriptor(false);
		return descriptor != null? descriptor.hasNewVersion() : false;
	}
}
