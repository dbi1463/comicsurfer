/* UpdateManager.java created on 2012/9/7
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.update;

/** The class can implements this interface to offer the ability
 * to update the software synchronously.
 * 
 * @author Pin-Ting Tu
 * @version 2.0
 * @since 2.0
 */
public interface UpdateManager {

	/** Update the software. The method will be blocked until the update is
	 * succeeded or failed.
	 * 
	 * @return true if the update succeeded
	 */
	public boolean update(UpdateDescriptor descriptor);

	/** Get the update descriptor. The method will be blocked until the getting
	 * descriptor is succeeded or failed.
	 * 
	 * @param majorVersion the current major version
	 * @param minorVersion the current minor version
	 * @param build the build number
	 * @return the update descriptor
	 */
	public UpdateDescriptor getUpdateDescriptor(int majorVersion, int minorVersion, int build);
}
