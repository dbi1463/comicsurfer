/* PreferenceDialog.java created on 2012/9/25
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import tw.funymph.comicsurfer.actions.SystemAction;
import tw.funymph.comicsurfer.language.LanguageChangeListener;
import tw.funymph.comicsurfer.language.LocalizedTextProvider;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;
import tw.funymph.comicsurfer.model.ComicSurferDesktopPreferences;

import tw.jsway.property.CategorizedEditableProperties;
import tw.jsway.property.editor.CategorizedPropertiesDialogPanel;

import static tw.funymph.comicsurfer.model.ComicSurferDesktopPreferences.*;

/**
 * The GUI to edit the user's preferences.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class PreferenceDialog extends JDialog implements LanguageChangeListener {

	private static final int COLOR_PROPERTY_GROUP_INDEX = 0;
	private static final int IMAGE_PROPERTY_GROUP_INDEX = 0;
	private static final int DIALOG_PROPERTY_GROUP_INDEX = 1;

	private static final int SPACE = 5;
	private static final int DIALOG_WIDTH = 580;
	private static final int DIALOG_HEIGHT = 440;

	private static final int SELECTOR_WIDTH = 120;
	private static final int ON_OFF_SWITCH_WIDTH = 80;
	private static final int SLIDESHOW_EDITOR_WIDTH = 100;
	private static final int COLOR_BUTTON_WIDTH = 120;

	private JPanel buttons;
	private JPanel buttonsPanel;
	private ComicSurferDesktopModel ststemModel;
	private CategorizedEditableProperties editableProperties;
	private CategorizedPropertiesDialogPanel editablePropertiesPane;

	/**
	 * Show the preference dialog with the parent frame and the model.
	 * 
	 * @param parentFrame the parent frame
	 * @param model the model to get the preferences
	 */
	public static void showPreferenceDialog(JFrame parentFrame, ComicSurferDesktopModel model) {
		JDialog dialog = new PreferenceDialog(parentFrame, model);
		dialog.setVisible(true);
	}

	/**
	 * Construct a <code>PreferenceDialog</code> instance with the parent frame
	 * and the model.
	 * 
	 * @param parentFrame the parent frame
	 * @param model the model to get the preferences
	 */
	public PreferenceDialog(JFrame parentFrame, ComicSurferDesktopModel model) {
		super(parentFrame);
		ststemModel = model;
		ststemModel.getLanguageManager().addLanguageChangeListener(this);
		setTitle(ststemModel.getLanguageManager().getLocalizedText(LANG_KEY_PREFERENCES));
		initializeComponents((ComicSurferDesktopPreferences)model.getPreferences());

		setModal(true);
		setResizable(false);
		setSize(DIALOG_WIDTH, DIALOG_HEIGHT);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(parentFrame);
	}

	public void dispose() {
		editablePropertiesPane.detach();
		ststemModel.getLanguageManager().removeLanguageChangeListener(this);
		super.dispose();
	}

	@Override
	public void languageChanged(LocalizedTextProvider source) {
		setTitle(ststemModel.getLanguageManager().getLocalizedText(LANG_KEY_PREFERENCES));
	}

	/**
	 * Initialize all components with the preferences.
	 * 
	 * @param preferences the preferences to be edited
	 */
	private void initializeComponents(ComicSurferDesktopPreferences preferences) {
		buttons = new JPanel();
		buttons.setBorder(BorderFactory.createEmptyBorder(SPACE, SPACE, SPACE, SPACE));
		buttons.setLayout(new BoxLayout(buttons, BoxLayout.LINE_AXIS));
		buttons.add(Box.createHorizontalGlue());
		buttons.add(new JButton(new CloseDialogAction(ststemModel, this)));

		editableProperties = preferences.getCategorizedEditableProperties();
		editablePropertiesPane = new CategorizedPropertiesDialogPanel(editableProperties);
		editablePropertiesPane.setCategorySelectorWidth(SELECTOR_WIDTH);
		editablePropertiesPane.setEditorWidth(CATEGORY_BEHAVIOR, IMAGE_PROPERTY_GROUP_INDEX, 1, ON_OFF_SWITCH_WIDTH);
		editablePropertiesPane.setEditorWidth(CATEGORY_BEHAVIOR, IMAGE_PROPERTY_GROUP_INDEX, 2, SLIDESHOW_EDITOR_WIDTH);
		editablePropertiesPane.setEditorWidth(CATEGORY_BEHAVIOR, DIALOG_PROPERTY_GROUP_INDEX, 0, ON_OFF_SWITCH_WIDTH);
		editablePropertiesPane.setEditorWidth(CATEGORY_APPEARANCE, COLOR_PROPERTY_GROUP_INDEX, 0, COLOR_BUTTON_WIDTH);

		buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new BorderLayout());
		buttonsPanel.add(new JSeparator(JSeparator.HORIZONTAL), BorderLayout.PAGE_START);
		buttonsPanel.add(buttons, BorderLayout.CENTER);

		getContentPane().add(buttonsPanel, BorderLayout.SOUTH);
		getContentPane().add(editablePropertiesPane, BorderLayout.CENTER);
	}

	/**
	 * The action to close the dialog.
	 * 
	 * @author Pin-Ying Tu
	 * @version 3.0
	 * @since 2.1
	 */
	static class CloseDialogAction extends SystemAction {

		private JDialog dialog;

		/**
		 * Construct a <code></code> instance with the model to get the localized
		 * text, and the dialog to be closed.
		 * 
		 * @param model the model to get the localized text
		 * @param theDialog the dialog to be closed
		 */
		CloseDialogAction(ComicSurferDesktopModel model, JDialog theDialog) {
			super(model);
			dialog = theDialog;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			dialog.dispose();
		}

		@Override
		protected void updateDisplayedContents() {
			putValue(NAME, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_CLOSE));
		}
	}
}
