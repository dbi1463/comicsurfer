/* SoftwareUpdateUIController.java created on 2012/9/9
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.ui;

import static java.lang.System.exit;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.YES_NO_OPTION;
import static javax.swing.JOptionPane.YES_OPTION;
import static javax.swing.JOptionPane.showConfirmDialog;
import static javax.swing.JOptionPane.showMessageDialog;
import static tw.funymph.comicsurfer.LanguageKeys.LANG_KEY_RESTART_MESSAGE;
import static tw.funymph.comicsurfer.LanguageKeys.LANG_KEY_TRY_AGAIN_LATER;
import static tw.funymph.comicsurfer.LanguageKeys.LANG_KEY_UPDATE_COMPLETED;

import javax.swing.DefaultBoundedRangeModel;

import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;
import tw.funymph.comicsurfer.update.UpdateDescriptor;
import tw.funymph.comicsurfer.update.UpdateProgressListener;
import tw.jsway.ApplicationRestarter;
import tw.jsway.ProgressModel;

/**
 * The UI controller to monitor the update progress and update the UI.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.0
 */
public class SoftwareUpdateUIController extends DefaultBoundedRangeModel implements UpdateProgressListener, ProgressModel {

	private static final String PROGRAM_ENTRY = "ComicSurfer.jar";

	private ComicSurferFrame systemFrame;
	private UpdateDescriptor lastDescriptor;
	private ApplicationRestarter restarter;
	private ComicSurferDesktopModel systemModel;

	private String latestMessage;

	/**
	 * Construct a <code>SoftwareUpdateUIController</code> to monitor the update progress
	 * from the model and update the UI.
	 * 
	 * @param frame the UI to be updated
	 * @param model the model to be monitored
	 */
	public SoftwareUpdateUIController(ComicSurferFrame frame, ComicSurferDesktopModel model) {
		systemFrame = frame;
		systemModel = model;
		restarter = new ApplicationRestarter();
	}

	@Override
	public void errorOccurred(String message) {
		String localizedMessage = systemModel.getLanguageManager().getLocalizedText(message);
		String localizedTitle = systemModel.getLanguageManager().getLocalizedText(LANG_KEY_TRY_AGAIN_LATER);
		showMessageDialog(systemFrame, localizedMessage, localizedTitle, ERROR_MESSAGE);
		updateNewsMenuStatus();
	}

	@Override
	public void checkFinished(UpdateDescriptor newDescriptor) {
		lastDescriptor = newDescriptor;
		updateNewsMenuStatus();
	}

	@Override
	public void updateFinished(boolean success) {
		if(success) {
			systemFrame.setNewsMenuEnabled(false);
			String localizedMessage = systemModel.getLanguageManager().getLocalizedText(LANG_KEY_RESTART_MESSAGE);
			String localizedTitle = systemModel.getLanguageManager().getLocalizedText(LANG_KEY_UPDATE_COMPLETED);
			int option = showConfirmDialog(systemFrame, localizedMessage, localizedTitle, YES_NO_OPTION);
			if(option == YES_OPTION) {
				systemModel.releaseResources();
				String entry = PROGRAM_ENTRY;
				String arguments[] = {};
				try {
					restarter.exitAndLaunchApplication(entry, arguments, null);
				} catch (Exception e) {
					e.printStackTrace();
					exit(-1);
				}
			}
		}
	}

	@Override
	public void progressUpdated(String info, String extra, int start, int current, int end) {
		latestMessage = systemModel.getLanguageManager().getLocalizedText(info) + " " + extra;
		setRangeProperties(current, 0, start, end, true);
	}

	@Override
	public String getProgressInfo() {
		return latestMessage;
	}

	/**
	 * Update the News menu in the window.
	 */
	private void updateNewsMenuStatus() {
		if(lastDescriptor != null) {
			systemFrame.setNewsMenuEnabled(lastDescriptor.hasNewVersion());
		}
	}
}
