/* CountLabel.java created on Oct 14, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.ui;

import tw.funymph.comicsurfer.core.events.NavigationEvent;
import tw.funymph.comicsurfer.core.events.NavigationEventListener;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/**
 * A class that display the number of pages in the current comic book
 * or the number of volumes in the current suite.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.1
 */
public class CountLabel extends SystemLabel {

	private static final String SEPARATOR = " / ";

	/**
	 * A factory method to create a page count label.
	 * 
	 * @param ComicSurferDesktopModel the model
	 * @return the created label
	 */
	public static CountLabel createPageCountLabel(ComicSurferDesktopModel model) {
		CountLabel label = new CountLabel(model);
		CountLabelController controller = new PageCountLabelController(label);
		model.getNavigator().addNavigationEventListener(controller);
		controller.updateLabel(model.getNavigator().getPageCount(), model.getNavigator().getVolumeCount());
		return label;
	}

	/**
	 * A factory method to create a volume count label.
	 * 
	 * @param ComicSurferDesktopModel the model
	 * @return the created label
	 */
	public static CountLabel createVolumeCountLabel(ComicSurferDesktopModel model) {
		CountLabel label = new CountLabel(model);
		CountLabelController controller = new VolumeCountLabelController(label);
		model.getNavigator().addNavigationEventListener(controller);
		controller.updateLabel(model.getNavigator().getPageCount(), model.getNavigator().getVolumeCount());
		return label;
	}

	/**
	 * Disable the construction of <code>CountLabel</code>. Use the static
	 * factory methods instead.
	 * 
	 * @param model the model in MVC pattern
	 */
	protected CountLabel(ComicSurferDesktopModel model) {
		super(model);
	}

	@Override
	public void updateLabel() {}

	/**
	 * A class that implements both <code>ComicSurerEventListener</code> to update
	 * the text of the label.
	 * 
	 * @author Pin-Ying Tu
	 * @version 3.0
	 * @since 1.3
	 */
	private static abstract class CountLabelController implements NavigationEventListener {

		protected CountLabel label;

		/**
		 * Construct a <code>CountLabelController</code> instance by its concrete classes.
		 * 
		 * @param theLabel the controlled label
		 */
		protected CountLabelController(CountLabel theLabel) {
			label = theLabel;
		}

		@Override
		public void pageChanged(NavigationEvent event) {
			updateLabel(event.getPageCount(), event.getVolumeCount());
		}

		@Override
		public void volumeChanging(NavigationEvent event) {}

		@Override
		public void volumeChanged(NavigationEvent event) {
			updateLabel(event.getPageCount(), event.getVolumeCount());
		}

		@Override
		public void suiteClosed(NavigationEvent event) {
			updateLabel(event.getPageCount(), event.getVolumeCount());
		}

		@Override
		public void suiteOpening(NavigationEvent event) {}

		@Override
		public void suiteOpened(NavigationEvent event) {
			updateLabel(event.getPageCount(), event.getVolumeCount());
		}

		@Override
		public void suiteOpenFailed() {}

		@Override
		public void suiteClosing(NavigationEvent event) {}

		/**
		 * Update the label with the new count value from the source
		 * 
		 * @param pageCount the page count of the current suite
		 * @param volumeCount the volume count of the current suite
		 */
		protected abstract void updateLabel(int pageCount, int volumeCount);
	}

	/**
	 * A class that controls the label with the page count.
	 * 
	 * @author Pin-Ying Tu
	 * @version 1.3
	 * @since 1.3
	 */
	private static class PageCountLabelController extends CountLabelController {

		/**
		 * Construct a <code>PageCountLabelController</code> instance.
		 * 
		 * @param theLabel the controlled label
		 */
		public PageCountLabelController(CountLabel theLabel) {
			super(theLabel);
		}

		@Override
		protected void updateLabel(int pageCount, int volumeCount) {
			label.setText(SEPARATOR + pageCount);
		}
	}

	/**
	 * A class that controls the label with the volume count.
	 * 
	 * @author Pin-Ying Tu
	 * @version 1.3
	 * @since 1.3
	 */
	private static class VolumeCountLabelController extends CountLabelController {

		/**
		 * Construct a <code>VolumeCountLabelController</code> instance.
		 * 
		 * @param theLabel the controlled label
		 */
		public VolumeCountLabelController(CountLabel theLabel) {
			super(theLabel);
		}

		@Override
		protected void updateLabel(int pageCount, int volumeCount) {
			label.setText(SEPARATOR + volumeCount);
		}
	}
}
