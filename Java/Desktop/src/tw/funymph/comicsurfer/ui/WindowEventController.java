/* WindowEventController.java created on 2013/1/6
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.ui;

import static java.lang.Boolean.parseBoolean;
import static tw.funymph.comicsurfer.model.ComicSurferDesktopPreferences.KEY_SHOW_OPEN_DIALOG;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Action;

import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/**
 * The default window event controller.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class WindowEventController extends WindowAdapter {

	private Action openAction;
	private ComicSurferDesktopModel systemModel;

	/**
	 * Construct a <code>WindowEventController</code> instance with the comic
	 * surfer model and the action to open file chooser.
	 * 
	 * @param model the comic surfer model
	 * @param action the action to open the file chooser
	 */
	public WindowEventController(ComicSurferDesktopModel model, Action action) {
		systemModel = model;
		openAction = action;
	}

	@Override
	public void windowClosing(WindowEvent e) {
		systemModel.exit();
	}

	@Override
	public void windowOpened(WindowEvent e) {
		if(parseBoolean(systemModel.getPreferences().getPreference(KEY_SHOW_OPEN_DIALOG))) {
			openAction.actionPerformed(null);
		}
	}
}
