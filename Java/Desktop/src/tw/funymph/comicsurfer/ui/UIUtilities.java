/* UIUtilities.java created on Oct 24, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.ui;

import static javax.swing.JFileChooser.APPROVE_OPTION;
import static javax.swing.JFileChooser.FILES_AND_DIRECTORIES;
import static tw.funymph.comicsurfer.preferences.Preferences.KEY_LAST_OPEN_PATH;

import java.io.File;
import java.util.Collection;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import tw.funymph.comicsurfer.actions.OpenLastViewedSuiteAction;
import tw.funymph.comicsurfer.actions.SystemAction;
import tw.funymph.comicsurfer.ComicSurferModel;
import tw.funymph.comicsurfer.history.ViewHistory;
import tw.funymph.comicsurfer.history.ViewRecord;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;
import tw.funymph.comicsurfer.model.FileFilterAdapter;

/**
 * A helper class to facilitate creating/updating GUI widgets.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public class UIUtilities {

	private static final String LANG_KEY_DIALOG_TITLE = "Open comic books";

	/**
	 * Create a tool bar button and register the action as the mouse event listener.
	 * 
	 * @param action the action to create a button (also a mouse listener)
	 * @return the created button
	 */
	public static JButton createToolBarButton(Action action) {
		JButton button = new JButton(action);
		button.setHideActionText(true);
		button.setRolloverEnabled(false);
		if(action instanceof SystemAction) {
			button.addMouseListener((SystemAction)action);
		}
		return button;
	}

	/**
	 * Create a menu item and register the action as the mouse event listener.
	 * 
	 * @param action the action to create a menu item (also a mouse listener)
	 * @return the created menu item
	 */
	public static JMenuItem createMenuItem(Action action) {
		JMenuItem item = new JMenuItem(action);
		if(action instanceof SystemAction) {
			item.addMouseListener((SystemAction)action);
		}
		return item;
	}

	/**
	 * Show the file chooser and get the selected file.
	 * 
	 * @param model the comic surfer model
	 * @return the selected file; null if the user cancel the dialog
	 */
	public static File showFileChooser(ComicSurferModel model) {
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle(model.getLanguageManager().getLocalizedText(LANG_KEY_DIALOG_TITLE));
		chooser.setFileSelectionMode(FILES_AND_DIRECTORIES);
		chooser.setCurrentDirectory(new File(model.getPreferences().getPreference(KEY_LAST_OPEN_PATH)));
		FileFilterAdapter filter = new FileFilterAdapter(model);
		chooser.addChoosableFileFilter(filter);
		chooser.removeChoosableFileFilter(chooser.getFileFilter());
		chooser.setFileFilter(filter);
		File selectedFile = null;
		if(chooser.showOpenDialog(null) == APPROVE_OPTION) {
			selectedFile = chooser.getSelectedFile();
		}
		chooser = null;
		return selectedFile;
	}

	/**
	 * Update the history menu.
	 * 
	 * @param menu the menu to be updated
	 * @param model the comic surfer model
	 */
	public static void updateHistoryMenu(JMenu menu, ComicSurferDesktopModel model) {
		ViewHistory history = model.getViewHistory();
		menu.setEnabled(history.getRecordCount() > 0);
		menu.removeAll();
		Collection<ViewRecord> records = history.getRecords();
		int index = 0;
		for(ViewRecord record : records) {
			menu.add(new OpenLastViewedSuiteAction(model, ++index, record.getURI(), record.getName()));
		}
	}
}
