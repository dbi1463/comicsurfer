/* ImagePanelSurfingController.java created on Oct 14, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.ui;

import static java.awt.event.MouseEvent.BUTTON1;
import static java.awt.event.MouseEvent.BUTTON3;

import static tw.funymph.comicsurfer.core.MotionController.ACCESSORY_ACTION;
import static tw.funymph.comicsurfer.core.MotionController.PRIMARY_ACTION;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import tw.funymph.comicsurfer.ComicSurferModel;

/**
 * A class that handles the mouse events from the <code>ImagePanel</code>.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.2
 */
public class ImagePanelSurfingController implements MouseListener, MouseMotionListener, MouseWheelListener {

	private ImagePanel imagePanel;
	private ComicSurferModel systemModel;

	/**
	 * Construct a <code>ImagePanelSurfingController</code> instance with the model
	 * and the controlled panel.
	 * 
	 * @param panel the controlled panel
	 * @param model the model in MVC pattern.
	 */
	public ImagePanelSurfingController(ImagePanel panel, ComicSurferModel model) {
		imagePanel = panel;
		systemModel = model;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if(e.getButton() == BUTTON1) {
			imagePanel.showPopupMenu(e.getX(), e.getY());
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		int action = (e.getButton() == BUTTON3)? ACCESSORY_ACTION : PRIMARY_ACTION;
		systemModel.getMotionController().pressed(e.getX(), e.getY(), action);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		int action = (e.getButton() == BUTTON3)? ACCESSORY_ACTION : PRIMARY_ACTION;
		systemModel.getMotionController().released(e.getX(), e.getY(), action);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		imagePanel.requestFocus();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		systemModel.getMotionController().moved(e.getX(), e.getY());
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		if(e.getWheelRotation() < 0) {
			systemModel.getNavigator().previousPage();
		}
		else {
			systemModel.getNavigator().nextPage();
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mouseDragged(MouseEvent e) {
		systemModel.getMotionController().dragged(e.getX(), e.getY());
	}
}
