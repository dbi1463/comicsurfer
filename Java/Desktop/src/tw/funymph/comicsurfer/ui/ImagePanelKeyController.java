/* ImagePanelKeyController.java created on Oct 14, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.ui;

import static java.awt.event.InputEvent.CTRL_MASK;
import static java.awt.event.KeyEvent.*;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Hashtable;

import tw.funymph.comicsurfer.actions.*;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/**
 * A class that handles the key events from the <code>ImagePanel</code>.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public class ImagePanelKeyController implements KeyListener {

	/**
	 * A class can implement this interface to handle a specific key event.
	 * 
	 * @author Pin-Ying Tu
	 * @version 3.0
	 * @since 1.3
	 */
	public static interface KeyHandler {

		/** Invoked when a specific key event occurs.
		 */
		public void handle();
	}

	private static final int MOVEMENT = 48;

	private ComicSurferDesktopModel systemModel;
	private Hashtable<Integer, KeyHandler> handlers;
	private Hashtable<Integer, KeyHandler> ctrlHandlers;

	private int x;
	private int y;

	private boolean surfingStarted;

	/**
	 * Construct a <code>ImagePanelKeyController</code> instance with the model.
	 * 
	 * @param model the model in MVC pattern.
	 */
	public ImagePanelKeyController(ComicSurferDesktopModel model) {
		systemModel = model;
		handlers = new Hashtable<Integer, KeyHandler>();
		ctrlHandlers = new Hashtable<Integer, KeyHandler>();
		resetTotalMovedXY();
		registerMotionHandlers();
		registerNavigationHandler();
		handlers.put(VK_ESCAPE, new StopSlideShowAction(systemModel));
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int modifier = e.getModifiers();
		if((modifier & CTRL_MASK) > 0 && ctrlHandlers.containsKey(e.getKeyCode())) {
			ctrlHandlers.get(e.getKeyCode()).handle();
			return;
		}
		else if(handlers.containsKey(e.getKeyCode())) {
			handlers.get(e.getKeyCode()).handle();
			return;
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {}

	@Override
	public void keyTyped(KeyEvent e) {}

	/**
	 * Reset the total moved x and y offsets.
	 */
	public void resetTotalMovedXY() {
		x = 0;
		y = 0;
		surfingStarted = false;
	}

	/**
	 * Move the image.
	 * 
	 * @param offsetX the x offset to move
	 * @param offsetY the y offset to move
	 */
	private void moveImage(int offsetX, int offsetY) {
		if(!surfingStarted) {
			systemModel.getMotionController().moved(0, 0);
			surfingStarted = true;
		}
		x = x + offsetX;
		y = y + offsetY;
		systemModel.getMotionController().moved(x, y);
	}

	/**
	 * Register the handlers for navigation.
	 */
	private void registerNavigationHandler() {
		OpenAction open = new OpenAction(systemModel);
		handlers.put(VK_INSERT, open);
		handlers.put(VK_NUMPAD0, open);

		CloseAction close = new CloseAction(systemModel);
		handlers.put(VK_DELETE, close);
		handlers.put(VK_DECIMAL, close);

		PreviousPageAction previousPage = new PreviousPageAction(systemModel);
		handlers.put(VK_E, previousPage);
		handlers.put(VK_NUMPAD9, previousPage);
		handlers.put(VK_PAGE_UP, previousPage);
		ctrlHandlers.put(VK_LEFT, previousPage);

		PreviousVolumeAction previousVolume = new PreviousVolumeAction(systemModel);
		handlers.put(VK_Q, previousVolume);
		handlers.put(VK_NUMPAD7, previousVolume);
		handlers.put(VK_HOME, previousVolume);
		ctrlHandlers.put(VK_UP, previousVolume);

		NextPageAction nextPage = new NextPageAction(systemModel);
		handlers.put(VK_C, nextPage);
		handlers.put(VK_NUMPAD3, nextPage);
		handlers.put(VK_PAGE_DOWN, nextPage);
		ctrlHandlers.put(VK_RIGHT, nextPage);

		NextVolumeAction nextVolume = new NextVolumeAction(systemModel);
		handlers.put(VK_NUMPAD1, nextVolume);
		handlers.put(VK_END, nextVolume);
		handlers.put(VK_Z, nextVolume);
		ctrlHandlers.put(VK_DOWN, nextVolume);
	}

	/**
	 * Register the handlers for motion control.
	 */
	private void registerMotionHandlers() {
		MotionHandler left = new MotionHandler(this, -MOVEMENT, 0);
		handlers.put(VK_A, left);
		handlers.put(VK_LEFT, left);
		handlers.put(VK_NUMPAD4, left);
		handlers.put(VK_KP_LEFT, left);

		MotionHandler right = new MotionHandler(this, MOVEMENT, 0);
		handlers.put(VK_D, right);
		handlers.put(VK_RIGHT, right);
		handlers.put(VK_NUMPAD6, right);
		handlers.put(VK_KP_RIGHT, right);

		MotionHandler down = new MotionHandler(this, 0, -MOVEMENT);
		handlers.put(VK_X, down);
		handlers.put(VK_DOWN, down);
		handlers.put(VK_NUMPAD2, down);
		handlers.put(VK_KP_DOWN, down);

		MotionHandler up = new MotionHandler(this, 0, MOVEMENT);
		handlers.put(VK_W, up);
		handlers.put(VK_UP, up);
		handlers.put(VK_NUMPAD8, up);
		handlers.put(VK_KP_UP, up);
	}

	/**
	 * A class that handles the motion events.
	 * 
	 * @author Pin-Ying Tu
	 * @version 3.0
	 * @since 1.3
	 */
	private static class MotionHandler implements KeyHandler {

		private int movedX;
		private int movedY;

		private ImagePanelKeyController controller;

		/**
		 * Construct a <code>MotionHandler</code> instance.
		 * 
		 * @param keyController the parent controller
		 * @param offsetX the x offset to move
		 * @param offsetY the y offset to move
		 */
		public MotionHandler(ImagePanelKeyController keyController, int offsetX, int offsetY) {
			movedX = offsetX;
			movedY = offsetY;
			controller = keyController;
		}

		@Override
		public void handle() {
			controller.moveImage(movedX, movedY);
		}
	}
}
