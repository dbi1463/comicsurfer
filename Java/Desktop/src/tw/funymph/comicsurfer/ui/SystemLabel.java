/* SystemLabel.java created on Oct 14, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.ui;

import javax.swing.JLabel;

import tw.funymph.comicsurfer.ComicSurferModel;
import tw.funymph.comicsurfer.language.*;

/**
 * A class that implements <code>LanguageChangeListener</code> to automatically
 * change its displayed text when the language is changed.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.2
 */
public class SystemLabel extends JLabel implements LanguageChangeListener {

	protected ComicSurferModel systemModel;

	private String displayText;

	/**
	 * Construct an empty <code>SystemLabel</code> instance with the model.
	 * 
	 * @param model the model in the MVC pattern
	 */
	protected SystemLabel(ComicSurferModel model) {
		this(model, "");
	}

	/**
	 * Construct a <code>SystemLabel</code> instance with model and the
	 * initial text that will be translated according the current language.
	 * 
	 * @param model the model in the MVC pattern
	 * @param text the initial text
	 */
	public SystemLabel(ComicSurferModel model, String text) {
		displayText = text;
		systemModel = model;
		systemModel.getLanguageManager().addLanguageChangeListener(this);
		updateLabel();
	}

	@Override
	public void languageChanged(LocalizedTextProvider source) {
		updateLabel();
	}

	/**
	 * Update the label with the localized text.
	 */
	public void updateLabel() {
		setText(systemModel.getLanguageManager().getLocalizedText(displayText));
	}
}
