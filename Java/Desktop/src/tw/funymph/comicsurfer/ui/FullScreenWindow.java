/* FullScreenWindow.java created on 2013/1/6
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.ui;

import static java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment;
import static tw.funymph.commons.thread.ThreadUtilities.sleepSilently;

import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JFrame;

/**
 * The class that renders comic books in full screen mode.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class FullScreenWindow extends JFrame implements Runnable {

	private static final int DOUBLE_BUFFERED = 2;

	private Thread paintingThread;
	private AtomicBoolean running;

	/**
	 * Construct a <code>FullScreenWindow</code> instance.
	 */
	public FullScreenWindow() {
		setUndecorated(true);
		setIgnoreRepaint(true);
		setAutoRequestFocus(true);
		enableInputMethods(false);
		running = new AtomicBoolean(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

	/**
	 * Enter the full screen mode.
	 */
	public void enterFullScreen() {
		if(running.compareAndSet(false, true)) {
			paintingThread = new Thread(this);
			requestFocus();
			getLocalGraphicsEnvironment().getDefaultScreenDevice().setFullScreenWindow(this);
			createBufferStrategy(DOUBLE_BUFFERED);
			paintingThread.start();
		}
	}

	/**
	 * Leave the full screen mode.
	 */
	public void leaveFullScreen() {
		if(running.compareAndSet(true, false)) {
			paintingThread.interrupt();
			paintingThread = null;
			getLocalGraphicsEnvironment().getDefaultScreenDevice().setFullScreenWindow(null);
			dispose();
		}
	}

	@Override
	public void run() {
		BufferStrategy strategy = getBufferStrategy();
		while(running.get()) {
			Graphics g = strategy.getDrawGraphics();
			paint(g);
			strategy.show();
			g.dispose();
			sleepSilently(100);
		}
	}
}
