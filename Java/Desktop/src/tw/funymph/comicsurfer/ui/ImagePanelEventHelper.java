/* ImagePanelEventHelper.java created on 2012/8/19
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.ui;

import static tw.funymph.comicsurfer.LanguageKeys.LANG_KEY_RECENT_VIEWED;
import static tw.funymph.comicsurfer.preferences.Preferences.KEY_BACKGROUND_COLOR;
import static tw.funymph.comicsurfer.ui.UIUtilities.updateHistoryMenu;

import javax.swing.JMenu;

import tw.funymph.comicsurfer.history.ViewHistory;
import tw.funymph.comicsurfer.history.ViewHistoryListener;
import tw.funymph.comicsurfer.language.*;
import tw.funymph.comicsurfer.preferences.*;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;
import tw.funymph.comicsurfer.model.ComicSurferDesktopPreferences;
import tw.jsway.property.ColorProperty;

/**
 * A helper class that handles the events from the model.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.5
 */
public class ImagePanelEventHelper implements LanguageChangeListener, PreferencesListener, ViewHistoryListener {

	private ComicSurferDesktopModel systemModel;
	private ComicSurferDesktopPreferences preferences;

	private ImagePanel imagePanel;
	private JMenu historyMenu;
	private ColorProperty backgroundColor;

	/**
	 * Construct a <code>ImagePanelEventHelper</code> instance with the help target
	 * and the data source.
	 * 
	 * @param panel the help target
	 * @param model the data source
	 */
	ImagePanelEventHelper(ImagePanel panel, ComicSurferDesktopModel model) {
		imagePanel = panel;
		systemModel = model;
		systemModel.getViewHistory().addViewHistoryListener(this);
		preferences = (ComicSurferDesktopPreferences)systemModel.getPreferences();
		historyMenu = new JMenu(systemModel.getLanguageManager().getLocalizedText(LANG_KEY_RECENT_VIEWED));
		updateHistoryMenu(historyMenu, systemModel);
		backgroundColor = (ColorProperty)preferences.getEditableProperty(KEY_BACKGROUND_COLOR);
		imagePanel.setBackground(backgroundColor.getColor());
	}

	/**
	 * Get the history menu.
	 * 
	 * @return the history menu
	 */
	public JMenu getHistoryMenu() {
		return historyMenu;
	}

	@Override
	public void preferencesChanged(String key, Preferences preferences) {
		if(KEY_BACKGROUND_COLOR.equals(key)) {
			imagePanel.setBackground(backgroundColor.getColor());
		}
	}

	@Override
	public void languageChanged(LocalizedTextProvider source) {
		historyMenu.setText(source.getLocalizedText(LANG_KEY_RECENT_VIEWED));
	}

	@Override
	public void viewHistoryUpdated(ViewHistory history) {
		updateHistoryMenu(historyMenu, systemModel);
	}
}
