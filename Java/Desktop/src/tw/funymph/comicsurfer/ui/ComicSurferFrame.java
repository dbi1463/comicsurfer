/* ComicSurferFrame.java created on Oct 13, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.ui;

import java.awt.BorderLayout;
import java.util.Iterator;
import java.util.Set;

import javax.swing.ActionMap;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.KeyStroke;

import tw.funymph.comicsurfer.actions.ChangeLanguageAction;
import tw.funymph.comicsurfer.actions.SystemActions;
import tw.funymph.comicsurfer.history.ViewHistory;
import tw.funymph.comicsurfer.history.ViewHistoryListener;
import tw.funymph.comicsurfer.language.*;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

import tw.jsway.StyledToolBar;
import tw.jsway.memory.MemoryUsageMonitorBar;

import static java.awt.BorderLayout.SOUTH;
import static java.awt.Color.red;
import static javax.swing.Box.createHorizontalGlue;
import static tw.funymph.comicsurfer.ComicSurfer.FULL_PRODUCT_NAME;
import static tw.funymph.comicsurfer.LanguageKeys.*;
import static tw.funymph.comicsurfer.actions.SystemActions.*;
import static tw.funymph.comicsurfer.ui.CountLabel.*;
import static tw.funymph.comicsurfer.ui.Indexer.*;
import static tw.funymph.comicsurfer.ui.UIUtilities.*;
import static tw.jsway.utilities.ImageUtilities.loadIcon;

/**
 * A class that manages all components in the main window.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public class ComicSurferFrame extends JFrame implements LanguageChangeListener, ViewHistoryListener {

	private static final double SIZE_RATIO = 0.9;

	private static final String ICON_PATH = "icons/ComicSurferIcon.png";

	private JMenu fileMenu;
	private JMenu viewMenu;
	private JMenu helpMenu;
	private JMenu langMenu;
	private JMenu newsMenu;
	private JMenu historyMenu;
	private JMenuBar menubar;

	private ImagePanel panel;
	private StyledToolBar toolbar;
	private MemoryUsageMonitorBar monitor;

	private ActionMap actions;
	private ComicSurferDesktopModel systemModel;
	private WindowEventController eventController;
	private SoftwareUpdateUIController updateController;

	/**
	 * Construct a <code>ComicSurferFrame</code> instance with the model and actions.
	 * 
	 * @param model the model in the MVC pattern
	 * @param theActions the actions to create to menu bar and tool bar
	 */
	public ComicSurferFrame(ComicSurferDesktopModel model, ActionMap theActions) {
		super(FULL_PRODUCT_NAME);
		systemModel = model;
		actions = theActions;
		updateController = new SoftwareUpdateUIController(this, systemModel);
		eventController = new WindowEventController(systemModel, actions.get(OPEN));
		systemModel.getViewHistory().addViewHistoryListener(this);
		systemModel.getLanguageManager().addLanguageChangeListener(this);
		systemModel.getUpdateManager().addUpdateProgressListener(updateController);
		initializeComponents();
		initializeSizeAndBehavior();
	}

	/**
	 * Enable or disable the News menu. This is designed for internal use.
	 * 
	 * @param enabled true to enable the menu
	 */
	void setNewsMenuEnabled(boolean enabled) {
		newsMenu.setEnabled(enabled);
		actions.get(SystemActions.UPDATE_SOFTWARE).setEnabled(systemModel.getUpdateManager().hasNewVersion());
	}

	/**
	 * Get the image panel.
	 * 
	 * @return the image panel
	 */
	ImagePanel getImagePanel() {
		return panel;
	}

	/**
	 * Switch the frame into background: show as the full-screen mode
	 */
	public void switchToBack() {
		remove(panel);
		remove(toolbar);
	}

	/**
	 * Switch the frame back to foreground
	 */
	public void switchToFront() {
		add(panel, CENTER);
		add(toolbar, SOUTH);
	}

	@Override
	public void languageChanged(LocalizedTextProvider source) {
		fileMenu.setText(source.getLocalizedText(LANG_KEY_FILE_MENU));
		viewMenu.setText(source.getLocalizedText(LANG_KEY_VIEW_MENU));
		langMenu.setText(source.getLocalizedText(LANG_KEY_LANGUAGE_MENU));
		helpMenu.setText(source.getLocalizedText(LANG_KEY_HELP_MENU));
		newsMenu.setText(source.getLocalizedText(LANG_KEY_NEWS_MENU));
		historyMenu.setText(source.getLocalizedText(LANG_KEY_RECENT_VIEWED));
	}

	@Override
	public void viewHistoryUpdated(ViewHistory history) {
		updateHistoryMenu(historyMenu, systemModel);
	}

	/**
	 * Initialize all components.
	 */
	private void initializeComponents() {
		panel = new ImagePanel(systemModel, actions);

		initializeMenuBar();
		initializeToolBar();
		registerAction(START_SLIDE_SHOW, "F5");
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(panel, BorderLayout.CENTER);
		getContentPane().add(toolbar, BorderLayout.SOUTH);
	}

	/**
	 * Register the named action with the key stroke.
	 * 
	 * @param actionName the action name
	 * @param key the key stroke
	 */
	private void registerAction(String actionName, String key) {
		KeyStroke stroke = KeyStroke.getKeyStroke(key);
		getRootPane().getInputMap().put(stroke, key);
		getRootPane().getActionMap().put(key, actions.get(actionName));
	}

	/**
	 * Initialize the menu bar.
	 */
	private void initializeMenuBar() {
		menubar = new JMenuBar();
		initializeFileMenu();
		initializeViewMenu();
		initializeLanguageMenu();
		initializeHelpMenu();
		initializeNewsMenu();
		setJMenuBar(menubar);
	}

	/**
	 * Initialize the File menu.
	 */
	private void initializeFileMenu() {
		menubar.add(fileMenu = new JMenu(systemModel.getLanguageManager().getLocalizedText(LANG_KEY_FILE_MENU)));
		fileMenu.add(createMenuItem(actions.get(OPEN)));
		fileMenu.add(createMenuItem(actions.get(OPEN_FROM)));
		fileMenu.add(createMenuItem(actions.get(CLOSE)));
		fileMenu.addSeparator();
		fileMenu.add(createMenuItem(actions.get(SHOW_PREFERENCE_DIALOG)));
		fileMenu.addSeparator();
		fileMenu.add(historyMenu = new JMenu(systemModel.getLanguageManager().getLocalizedText(LANG_KEY_RECENT_VIEWED)));
		fileMenu.addSeparator();
		fileMenu.add(createMenuItem(actions.get(EXIT)));
		updateHistoryMenu(historyMenu, systemModel);
	}

	/**
	 * Initialize the View menu.
	 */
	private void initializeViewMenu() {
		menubar.add(viewMenu = new JMenu(systemModel.getLanguageManager().getLocalizedText(LANG_KEY_VIEW_MENU)));
		viewMenu.add(createMenuItem(actions.get(BACK)));
		viewMenu.addSeparator();
		viewMenu.add(createMenuItem(actions.get(PREVIOUS_PAGE)));
		viewMenu.add(createMenuItem(actions.get(NEXT_PAGE)));
		viewMenu.addSeparator();
		viewMenu.add(createMenuItem(actions.get(PREVIOUS_VOLUME)));
		viewMenu.add(createMenuItem(actions.get(NEXT_VOLUME)));
		viewMenu.addSeparator();
		viewMenu.add(actions.get(ACTUAL_SIZE));
		viewMenu.add(actions.get(FIT_WINDOW_WIDTH));
		viewMenu.add(actions.get(FIT_WINDOW_HEIGHT));
		viewMenu.add(actions.get(FIT_WINDOW_SIZE));
		viewMenu.addSeparator();
		viewMenu.add(actions.get(START_SLIDE_SHOW));
		viewMenu.addSeparator();
		viewMenu.add(actions.get(SET_BACKGROUND_COLOR));
	}

	/**
	 * Initialize the Language menu.
	 */
	private void initializeLanguageMenu() {
		menubar.add(langMenu = new JMenu(systemModel.getLanguageManager().getLocalizedText(LANG_KEY_LANGUAGE_MENU)));
		Set<String> names = systemModel.getLanguageManager().getAvailableLanguages();
		Iterator<String> iterator = names.iterator();
		while(iterator.hasNext()) {
			langMenu.add(new ChangeLanguageAction(systemModel, iterator.next()));
		}
	}

	/**
	 * Initialize the Help menu.
	 */
	private void initializeHelpMenu() {
		menubar.add(helpMenu = new JMenu(systemModel.getLanguageManager().getLocalizedText(LANG_KEY_HELP_MENU)));
		helpMenu.add(createMenuItem(actions.get(ABOUT)));
	}

	/**
	 * Initialize the News menu.
	 */
	private void initializeNewsMenu() {
		menubar.add(newsMenu = new JMenu(systemModel.getLanguageManager().getLocalizedText(LANG_KEY_NEWS_MENU)));
		newsMenu.add(createMenuItem(actions.get(UPDATE_SOFTWARE)));
		newsMenu.setEnabled(systemModel.getUpdateManager().hasNewVersion());
		newsMenu.setForeground(red);
	}

	/**
	 * Initialize the tool bar.
	 */
	private void initializeToolBar() {
		toolbar = new StyledToolBar();
		toolbar.setFloatable(false);
		toolbar.add(createHorizontalGlue());
		initializeOpenCloseControls();
		initializePageControls();
		initializeVolumeControls();
		toolbar.add(createHorizontalGlue());
		toolbar.add(monitor = new MemoryUsageMonitorBar());
		monitor.addMonitorProgress(updateController);
		toolbar.addSeparator();
	}

	/**
	 * Initialize the controls for Open and Close.
	 */
	private void initializeOpenCloseControls() {
		toolbar.addFirstPositionButton(createToolBarButton(actions.get(OPEN)));
		toolbar.addLastPositionButton(createToolBarButton(actions.get(CLOSE)));
		toolbar.addSeparator();
	}

	/**
	 * Initialize the controls for page navigation.
	 */
	private void initializePageControls() {
		toolbar.addFirstPositionButton(createToolBarButton(actions.get(PREVIOUS_PAGE)));
		toolbar.addSeparator();
		toolbar.add(createPageIndexer(systemModel.getNavigator()).getEditor());
		toolbar.add(createPageCountLabel(systemModel));
		toolbar.addSeparator();
		toolbar.addLastPositionButton(createToolBarButton(actions.get(NEXT_PAGE)));
		toolbar.addSeparator();
	}

	/**
	 * Initialize the controls for the volume navigation.
	 */
	private void initializeVolumeControls() {
		toolbar.addFirstPositionButton(createToolBarButton(actions.get(PREVIOUS_VOLUME)));
		toolbar.addSeparator();
		toolbar.add(createVolumeIndexer(systemModel.getNavigator()).getEditor());
		toolbar.add(createVolumeCountLabel(systemModel));
		toolbar.addSeparator();
		toolbar.addLastPositionButton(createToolBarButton(actions.get(NEXT_VOLUME)));
		toolbar.addSeparator();
	}

	/**
	 * Initialize the size the default behaviors.
	 */
	private void initializeSizeAndBehavior() {
		int width = (int)(getToolkit().getScreenSize().width * SIZE_RATIO);
		int height = (int)(getToolkit().getScreenSize().height * SIZE_RATIO);
		setSize(width, height);
		setLocationRelativeTo(null);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setIconImage(loadIcon(ICON_PATH).getImage());
		addWindowListener(eventController);
	}
}
