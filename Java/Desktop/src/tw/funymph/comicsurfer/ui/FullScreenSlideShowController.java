/* FullScreenSlideShowController.java created on 2013/1/6
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.ui;

import static java.awt.BorderLayout.CENTER;
import static java.lang.Integer.parseInt;
import static tw.funymph.comicsurfer.core.ImageDisplayModel.SLIDE_SHOW_MODE;
import static tw.funymph.comicsurfer.preferences.Preferences.KEY_SLIDESHOW_SPEED;
import static tw.funymph.commons.thread.ThreadUtilities.sleepSilently;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import tw.funymph.comicsurfer.core.events.NavigationEvent;
import tw.funymph.comicsurfer.core.events.NavigationEventListenerAdapter;
import tw.funymph.comicsurfer.core.slide.SlideShowController;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/**
 * The controller to monitor the time and key events to control every
 * things in the slide show mode.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class FullScreenSlideShowController extends NavigationEventListenerAdapter implements Runnable, SlideShowController, KeyListener {

	private static final int DEFAULT_SLEEP_TIME = 100;

	private Thread timingThread;
	private AtomicBoolean running;
	private AtomicBoolean isFullScreen;
	private AtomicInteger remainingTime;

	private int originalMode;
	private ComicSurferDesktopModel systemModel;

	private ImagePanel imagePanel;
	private ComicSurferFrame mainFrame;
	private FullScreenWindow fullScreen;

	/**
	 * Construct a <code>FullScreenSlideShowController</code> instance with the
	 * comic surfer model.
	 * 
	 * @param model the comic surfer model
	 */
	public FullScreenSlideShowController(ComicSurferDesktopModel model) {
		systemModel = model;
		running = new AtomicBoolean(false);
		isFullScreen = new AtomicBoolean(false);
		remainingTime = new AtomicInteger(0);
	}

	/**
	 * Set the controlled comic surfer frame.
	 * 
	 * @param frame the controlled frame
	 */
	public void setComicSurferFrame(ComicSurferFrame frame) {
		mainFrame = frame;
		imagePanel = frame.getImagePanel();
	}

	/**
	 * Reset the slide show time
	 */
	public void resetSlideShowTime() {
		remainingTime.set(getSlideShowSpeed());
	}

	@Override
	public void startSlideShow() {
		if(isFullScreen.compareAndSet(false, true)) {
			// Create a full-screen frame and move the image panel from the main frame to the full-screen frame
			fullScreen = new FullScreenWindow();
			mainFrame.switchToBack();
			fullScreen.add(imagePanel, CENTER);
			fullScreen.enterFullScreen();
			originalMode = systemModel.getDisplayModel().getDisplayMode();
			systemModel.getDisplayModel().setDisplayMode(SLIDE_SHOW_MODE);
			systemModel.getDisplayModel().setWindowSize(fullScreen.getWidth(), fullScreen.getHeight());
	
			// Register event listeners
			fullScreen.addKeyListener(this);
			systemModel.getNavigator().addNavigationEventListener(this);
	
			// Start the timer
			remainingTime.set(getSlideShowSpeed());
			running.set(true);
			timingThread = new Thread(this);
			timingThread.start();
		}
	}

	@Override
	public void stopSlideShow() {
		if(isFullScreen.compareAndSet(true, false)) {
			// Stop the timer
			running.set(false);
			timingThread.interrupt();
			fullScreen.leaveFullScreen();
			fullScreen.dispose();
			fullScreen.remove(imagePanel);
	
			// Remove the event listeners
			fullScreen.removeKeyListener(this);
			systemModel.getNavigator().removeNavigationEventListener(this);
	
			// Add the image panel back to the main frame
			mainFrame.switchToFront();
			mainFrame.setIgnoreRepaint(false);
			systemModel.getDisplayModel().setDisplayMode(originalMode);
			mainFrame.getRootPane().updateUI();
			fullScreen = null;
		}
	}

	@Override
	public boolean isSlideShowStarted() {
		return isFullScreen.get();
	}

	@Override
	public void run() {
		while(running.get()) {
			// If still has time, subtract the remaining time, and sleep a while
			if(remainingTime.get() > 0) {
				remainingTime.set(remainingTime.get() - DEFAULT_SLEEP_TIME);
				sleepSilently(DEFAULT_SLEEP_TIME);
			}
			// Time is up, go to the next page, and reset the remaining time
			else {
				systemModel.getNavigator().nextPage();
				resetSlideShowTime();
			}
		}
	}

	private int getSlideShowSpeed() {
		return parseInt(systemModel.getPreferences().getPreference(KEY_SLIDESHOW_SPEED));
	}

	@Override
	public void pageChanged(NavigationEvent event) {
		resetSlideShowTime();
	}

	@Override
	public void keyTyped(KeyEvent e) {}

	@Override
	public void keyPressed(KeyEvent e) {}

	@Override
	public void keyReleased(KeyEvent e) {
		// In the full screen mode, the image panel can not get the event from its container.
		// so forward the event to the image panel
		imagePanel.getKeyController().keyReleased(e);
	}
}
