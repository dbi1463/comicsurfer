/* Indexer.java created on Nov 29, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.ui;

import static javax.swing.border.EtchedBorder.LOWERED;

import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JFormattedTextField;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.text.NumberFormatter;

import tw.funymph.comicsurfer.core.NavigationModel;
import tw.funymph.comicsurfer.core.events.NavigationEvent;
import tw.funymph.comicsurfer.core.events.NavigationEventListener;

/**
 * A class that provides an editor with a controller to synchronize the editor
 * between the model value and the user's inputs.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.3
 */
public class Indexer {

	private static final int DEFAULT_EDITOR_VALUE = 0;
	private static final int DEFAULT_MINIMUM_VALUE = 0;
	private static final int DEFAULT_MAXIMUM_VALUE = 1;
	private static final int DEFAULT_EDITOR_WIDTH = 40;
	private static final int DEFAULT_EDITOR_HEIGHT = 25;
	private static final int DEFAULT_EDITOR_COLUMNS = 3;

	private static final String REGISTERED_PROPERTY = "value";

	// Model
	private NavigationModel navigator;

	// View member data
	private NumberFormatter formatter;
	private JFormattedTextField editor;

	// Controller
	private EditorController editorController;

	/**
	 * A factory method to create a page indexer.
	 * 
	 * @param theNavigator the model
	 * @return the created indexer
	 */
	public static Indexer createPageIndexer(NavigationModel theNavigator) {
		Indexer indexer = new Indexer(theNavigator);
		indexer.setIndexerController(new PageIndexController(indexer));
		return indexer;
	}

	/**
	 * A factory method to create a volume indexer.
	 * 
	 * @param theNavigator the model
	 * @return the created indexer
	 */
	public static Indexer createVolumeIndexer(NavigationModel theNavigator) {
		Indexer indexer = new Indexer(theNavigator);
		indexer.setIndexerController(new VolumeIndexController(indexer));
		return indexer;
	}

	/**
	 * Disable the direct construction of <code>Indexer</code>. Use the static
	 * factory method instead.
	 * 
	 * @param theNavigator the model
	 */
	protected Indexer(NavigationModel theNavigator) {
		navigator = theNavigator;
		createEditor();
	}

	/**
	 * Get the editor provided by the indexer
	 * 
	 * @return the editor
	 */
	public JFormattedTextField getEditor() {
		return editor;
	}

	/**
	 * Set the editor controller
	 * 
	 * @param controller the editor controller
	 */
	private void setIndexerController(EditorController controller) {
		if(editorController != null && editorController != controller) {
			editor.removeMouseListener(editorController);
			editor.removePropertyChangeListener(REGISTERED_PROPERTY, editorController);
			navigator.removeNavigationEventListener(editorController);
		}
		editorController = controller;
		if(editorController != null) {
			editor.addMouseListener(editorController);
			editor.addPropertyChangeListener(REGISTERED_PROPERTY, editorController);
			navigator.addNavigationEventListener(editorController);
		}
	}

	/**
	 * Create the editor with default settings.
	 */
	private void createEditor() {
		formatter = new NumberFormatter();
		formatter.setMinimum(DEFAULT_MINIMUM_VALUE);
		formatter.setMaximum(DEFAULT_MAXIMUM_VALUE);
		formatter.setCommitsOnValidEdit(false);
		editor = new JFormattedTextField(formatter);
		editor.setValue(DEFAULT_EDITOR_VALUE);
		editor.setColumns(DEFAULT_EDITOR_COLUMNS);
		editor.setMinimumSize(new Dimension(DEFAULT_EDITOR_WIDTH, DEFAULT_EDITOR_HEIGHT));
		editor.setMaximumSize(new Dimension(DEFAULT_EDITOR_WIDTH, DEFAULT_EDITOR_HEIGHT));
		editor.setBorder(null);
		editor.setHorizontalAlignment(JTextField.RIGHT);
		editor.setFocusLostBehavior(JFormattedTextField.COMMIT_OR_REVERT);
	}

	/**
	 * A class that implements both <code>ComicSurerEventListener</code> and
	 * <code>PropertyChangeListener</code> to synchorize the editor with the
	 * model value and the user's inputs.
	 * 
	 * @author Pin-Ying Tu
	 * @version 3.0
	 * @since 1.3
	 */
	private static abstract class EditorController implements NavigationEventListener, PropertyChangeListener, MouseListener {

		protected static final int INDEX_OFFSET = 1;

		protected Indexer indexer;
		protected EtchedBorder border;

		/** Construct a <code>EditorController</code> instance by its concrete classes.
		 * 
		 * @param theIndexer the controlled indexer
		 */
		protected EditorController(Indexer theIndexer) {
			indexer = theIndexer;
			border = new EtchedBorder(LOWERED);
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			indexer.getEditor().setBorder(border);
		}

		@Override
		public void mouseClicked(MouseEvent e) {}

		@Override
		public void mousePressed(MouseEvent e) {}

		@Override
		public void mouseReleased(MouseEvent e) {}

		@Override
		public void mouseExited(MouseEvent e) {
			indexer.getEditor().setBorder(null);
		}

		@Override
		public void volumeChanging(NavigationEvent event) {}

		@Override
		public void suiteClosing(NavigationEvent event) {}

		@Override
		public void suiteOpening(NavigationEvent event) {}

		@Override
		public void suiteOpenFailed() {}
	}

	/**
	 * A class that controls the index editor with the page index.
	 * 
	 * @author Pin-Ying Tu
	 * @version 3.0
	 * @since 1.3
	 */
	private static class PageIndexController extends EditorController {

		/**
		 * Construct a <code>PageIndexController</code> instance.
		 * 
		 * @param indexer the controlled indexer
		 */
		public PageIndexController(Indexer indexer) {
			super(indexer);
		}

		/**
		 * Update the editor with the new page index.
		 */
		private void indexUpdated(int index) {
			indexer.getEditor().setValue((index + INDEX_OFFSET));
		}

		@Override
		public void pageChanged(NavigationEvent event) {
			indexUpdated(event.getCurrentPageIndex());
		}

		@Override
		public void volumeChanged(NavigationEvent event) {
			indexUpdated(event.getCurrentPageIndex());
			indexer.formatter.setMaximum(event.getPageCount());
		}

		@Override
		public void suiteClosed(NavigationEvent event) {
			indexUpdated(event.getCurrentPageIndex());
		}

		@Override
		public void suiteOpened(NavigationEvent event) {
			indexUpdated(event.getCurrentPageIndex());
		}

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			indexer.navigator.jumpToPage((((Number)indexer.getEditor().getValue()).intValue() - INDEX_OFFSET));
		}
	}

	/**
	 * A class that controls the index editor with the volume index.
	 * 
	 * @author Pin-Ying Tu
	 * @version 3.0
	 * @since 1.3
	 */
	private static class VolumeIndexController extends EditorController {

		/**
		 * Construct a <code>VolumeIndexController</code> instance.
		 * 
		 * @param indexer the controlled indexer
		 */
		public VolumeIndexController(Indexer indexer) {
			super(indexer);
		}

		/**
		 * Update the editor with the next volume index.
		 */
		private void indexUpdated(int index) {
			indexer.getEditor().setValue((index + INDEX_OFFSET));
		}

		@Override
		public void pageChanged(NavigationEvent event) {}

		@Override
		public void volumeChanged(NavigationEvent event) {
			indexUpdated(event.getCurrentVolumeIndex());
		}

		@Override
		public void suiteClosed(NavigationEvent event) {
			indexUpdated(event.getCurrentVolumeIndex());
			indexer.formatter.setMaximum(event.getVolumeCount());
		}

		@Override
		public void suiteOpened(NavigationEvent event) {
			indexUpdated(event.getCurrentVolumeIndex());
			indexer.formatter.setMaximum(event.getVolumeCount());
		}

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			indexer.navigator.jumpToVolume((((Number)indexer.getEditor().getValue()).intValue() - INDEX_OFFSET));
		}
	}
}
