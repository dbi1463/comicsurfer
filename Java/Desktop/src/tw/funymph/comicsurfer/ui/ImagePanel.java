/* ImagePanel.java created on Oct 13, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.ui;

import static java.awt.Font.BOLD;
import static java.awt.RenderingHints.*;
import static tw.funymph.comicsurfer.actions.SystemActions.*;
import static tw.funymph.comicsurfer.ui.UIUtilities.createMenuItem;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Stroke;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

import javax.swing.ActionMap;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import tw.funymph.comicsurfer.core.ImageDisplayModel;
import tw.funymph.comicsurfer.core.MotionTrackPoint;
import tw.funymph.comicsurfer.core.events.ImageDisplayModelListener;
import tw.funymph.comicsurfer.core.events.NavigationEvent;
import tw.funymph.comicsurfer.core.events.NavigationEventListener;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/**
 * A class that shows the image of the current page in a comic book. Most
 * importantly, the class use multiple controllers to handle the mouse and
 * key events to interact with the end user.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public class ImagePanel extends JPanel implements ComponentListener, NavigationEventListener, ImageDisplayModelListener {

	private static final int MESSAGE_FONT_SIZE = 24;
	private static final int OVERLAPPING_IMAGE_MESSAGE_GAP = 10;

	private static final float DEFAULT_TRACK_THICKNESS = 2.5f;

	private JPopupMenu popupMenu;
	private Stroke trackStoke;

	private BufferedImage comicImage;
	private Image overlappingImage;
	private String overalppingMessage;

	private ComicSurferDesktopModel systemModel;
	private ImagePanelEventHelper eventHelper;
	private ImagePanelEventSynchronizer eventSynchronizer;

	/**
	 * Constant a <code>ImagePanel</code> instance with the model.
	 * 
	 * @param model the model in MVC pattern
	 */
	public ImagePanel(ComicSurferDesktopModel model, ActionMap actions) {
		systemModel = model;
		trackStoke = new BasicStroke(DEFAULT_TRACK_THICKNESS);
		eventHelper = new ImagePanelEventHelper(this, systemModel);
		eventSynchronizer = new ImagePanelEventSynchronizer(this, systemModel, actions);

		// Register all listeners
		addComponentListener(this);
		systemModel.getNavigator().addNavigationEventListener(this);
		systemModel.getDisplayModel().addImageDisplayModelListener(this);
		systemModel.getPreferences().addPreferencesListener(eventHelper);
		systemModel.getLanguageManager().addLanguageChangeListener(eventHelper);

		createPopupMenu(actions);
		setDoubleBuffered(true);
		setFont(new Font(getFont().getFamily(), BOLD, MESSAGE_FONT_SIZE));
	}

	/**
	 * Get the key controller.
	 * 
	 * @return the key controller
	 */
	KeyListener getKeyController() {
		return eventSynchronizer.getKeyController();
	}

	/**
	 * Set the image to be displayed.
	 * 
	 * @param image the image to be displayed
	 */
	public void setImage(BufferedImage image) {
		comicImage = image;
	}

	/**
	 * Set the image to be overlapped on the displayed image.
	 * 
	 * @param image the image to be overlapped
	 */
	public void setOverlappingImage(Image image) {
		overlappingImage = image;
	}

	/**
	 * Set the message to be overlapped on the displayed image.
	 * 
	 * @param message the message to be overlapped
	 */
	public void setOverlappingMessage(String message) {
		overalppingMessage = message;
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D)g.create(0, 0, getWidth(), getHeight());
		g2d.setBackground(getBackground());
		g2d.setRenderingHint(KEY_TEXT_ANTIALIASING, VALUE_TEXT_ANTIALIAS_ON);
		g2d.setRenderingHint(KEY_ANTIALIASING, VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(KEY_INTERPOLATION, VALUE_INTERPOLATION_BILINEAR);
		drawCurrentPage(g2d);
		resetSacle(g2d);
		drawOverlapping(g2d);
		drawMotionTrack(g2d);
		g2d.dispose();
	}

	@Override
	public void componentResized(ComponentEvent e) {
		systemModel.getDisplayModel().setWindowSize(getWidth(), getHeight());
		repaint();
	}

	@Override
	public void componentShown(ComponentEvent e) {
		repaint();
	}

	@Override
	public void componentMoved(ComponentEvent e) {}

	@Override
	public void componentHidden(ComponentEvent e) {}

	@Override
	public void pageChanged(NavigationEvent event) {
		eventSynchronizer.loadPage(systemModel.getNavigator().currentPage());
	}

	@Override
	public void modelUpdated(ImageDisplayModel model) {
		repaint();
	}

	@Override
	public void volumeChanging(NavigationEvent event) {
		eventSynchronizer.enterBusyState();
	}

	@Override
	public void volumeChanged(NavigationEvent event) {}

	@Override
	public void suiteClosed(NavigationEvent event) {
		comicImage = null;
		overlappingImage = null;
		overalppingMessage = null;
		systemModel.getDisplayModel().setImageSize(getWidth(), getHeight());
		repaint();
	}

	@Override
	public void suiteOpening(NavigationEvent event) {
		eventSynchronizer.enterBusyState();
	}

	@Override
	public void suiteClosing(NavigationEvent event) {}

	@Override
	public void suiteOpened(NavigationEvent event) {}

	@Override
	public void suiteOpenFailed() {
		eventSynchronizer.leaveBusyStateOnErrorOccurred();
	}

	/**
	 * Show a pop-up menu at the specified location.
	 * 
	 * @param x the x location to show pop-up menu
	 * @param y the y location to show pop-up menu
	 */
	public void showPopupMenu(int x, int y) {
		popupMenu.show(this, x, y);
	}

	/**
	 * Draw the current page.
	 * 
	 * @param g2d the graphics context
	 */
	private void drawCurrentPage(Graphics2D g2d) {
		if(comicImage != null) {
			double ratio = systemModel.getDisplayModel().getDisplayRatio();
			g2d.scale(ratio, ratio);
			int x = (int)((double)systemModel.getDisplayModel().getImageX() / ratio);
			int y = (int)((double)systemModel.getDisplayModel().getImageY() / ratio);
			g2d.drawImage(comicImage, x, y, this);
		}
	}

	/**
	 * Reset the scale
	 * 
	 * @param g2d the graphics context
	 */
	private void resetSacle(Graphics2D g2d) {
		double restRatio = 1.0f / systemModel.getDisplayModel().getDisplayRatio();
		g2d.scale(restRatio, restRatio);
	}

	/**
	 * Draw the overlapping image and message if they are existing
	 * 
	 * @param g2d the graphics context
	 */
	private void drawOverlapping(Graphics2D g2d) {
		Color oldColor = g2d.getColor();
		g2d.setColor(Color.gray);
		int imageWidth = 0, imageHeight = 0;
		int messageWidth = 0, messageHeight = 0;
		String message = null;
		if(overlappingImage != null) {
			imageWidth = overlappingImage.getWidth(this);
			imageHeight = overlappingImage.getHeight(this);
		}
		if(overalppingMessage != null) {
			message = systemModel.getLanguageManager().getLocalizedText(overalppingMessage);
			messageWidth = g2d.getFontMetrics().stringWidth(message);
			messageHeight = g2d.getFontMetrics().getAscent();
		}
		int overlappingWidth = imageWidth + messageWidth + OVERLAPPING_IMAGE_MESSAGE_GAP;
		int overlappingHeight = Math.max(imageHeight, messageHeight);
		int overlappingX = (getWidth() - overlappingWidth) / 2;
		int overlappingY = (getHeight() - overlappingHeight) / 2;
		if(overlappingImage != null) {
			g2d.drawImage(overlappingImage, overlappingX, overlappingY, this);
		}
		if(message != null) {
			int x = overlappingX + imageWidth + OVERLAPPING_IMAGE_MESSAGE_GAP;
			int y = overlappingY + messageHeight;
			g2d.drawString(message, x, y);
		}
		g2d.setColor(oldColor);
	}

	/**
	 * Draw the motion track line
	 * 
	 * @param g2d the graphics context
	 */
	private void drawMotionTrack(Graphics2D g2d) {
		MotionTrackPoint[] points = systemModel.getMotionController().getMotionTrack();
		if(points.length > 0) {
			g2d.setColor(Color.red);
			g2d.setStroke(trackStoke);
			for(int index = 0; index < points.length - 1; index++) {
				MotionTrackPoint from = points[index];
				MotionTrackPoint to = points[index + 1];
				g2d.drawLine(from.getX(), from.getY(), to.getX(), to.getY());
			}
		}
	}

	/**
	 * Use the system actions to create a pop-up menu.
	 * 
	 * @param actions the system actions
	 */
	private void createPopupMenu(ActionMap actions) {
		popupMenu = new JPopupMenu();
		popupMenu.add(createMenuItem(actions.get(OPEN)));
		popupMenu.add(createMenuItem(actions.get(OPEN_FROM)));
		popupMenu.add(createMenuItem(actions.get(CLOSE)));
		popupMenu.addSeparator();
		popupMenu.add(createMenuItem(actions.get(BACK)));
		popupMenu.addSeparator();
		popupMenu.add(createMenuItem(actions.get(PREVIOUS_PAGE)));
		popupMenu.add(createMenuItem(actions.get(NEXT_PAGE)));
		popupMenu.addSeparator();
		popupMenu.add(createMenuItem(actions.get(PREVIOUS_VOLUME)));
		popupMenu.add(createMenuItem(actions.get(NEXT_VOLUME)));
		popupMenu.addSeparator();
		popupMenu.add(actions.get(ACTUAL_SIZE));
		popupMenu.add(actions.get(FIT_WINDOW_WIDTH));
		popupMenu.add(actions.get(FIT_WINDOW_HEIGHT));
		popupMenu.add(actions.get(FIT_WINDOW_SIZE));
		popupMenu.addSeparator();
		popupMenu.add(actions.get(SET_BACKGROUND_COLOR));
		popupMenu.addSeparator();
		popupMenu.add(eventHelper.getHistoryMenu());
		popupMenu.addSeparator();
		popupMenu.add(createMenuItem(actions.get(EXIT)));
	}
}
