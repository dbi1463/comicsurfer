/* ImagePanelEventSynchronizer.java created on 2012/9/6
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.ui;

import static tw.funymph.comicsurfer.LanguageKeys.*;
import static tw.funymph.comicsurfer.actions.SystemActions.*;
import static tw.funymph.commons.thread.ThreadUtilities.sleepSilently;
import static tw.jsway.utilities.ImageUtilities.loadIcon;

import java.awt.Image;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.imageio.ImageIO;
import javax.swing.ActionMap;

import tw.funymph.comicsurfer.cache.CachedPage;
import tw.funymph.comicsurfer.core.Page;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;
import tw.funymph.commons.thread.ThreadedWorker;

/**
 * A class that synchronizes the event between different dispatching threads
 * when a page or a comic book is on loading.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.0
 */
public class ImagePanelEventSynchronizer implements Runnable {

	private static final int MONITOR_PERIOD = 30;

	private static final String ICON_EXTENSION = ".png";
	private static final String ICON_PATH = "icons/";
	private static final String ICON_NAME = "Error";

	private AtomicBoolean busyRightNow;

	private Page page;
	private Image errorIcon;
	private ImagePanel imagePanel;
	private ActionMap systemActions;
	private ProgressUpdater progressUpdater;

	private ThreadedWorker imageLoadingThread;
	private ThreadedWorker progressUpdateThread;

	private ComicSurferDesktopModel systemModel;
	private ImagePanelKeyController keyController;
	private ImagePanelSurfingController surfingController;

	/**
	 * Construct a <code>ImagePanelEventSynchronizer</code> instance.
	 * 
	 * @param panel the panel to be synchronized
	 * @param model the model to perform task
	 */
	public ImagePanelEventSynchronizer(ImagePanel panel, ComicSurferDesktopModel model, ActionMap actions) {
		imagePanel = panel;
		systemModel = model;
		systemActions = actions;

		errorIcon = loadIcon(ICON_PATH + ICON_NAME + ICON_EXTENSION).getImage();
		busyRightNow = new AtomicBoolean(false);
		progressUpdater = new ProgressUpdater(imagePanel);
		imageLoadingThread = new ThreadedWorker("Image Load Thread");
		progressUpdateThread = new ThreadedWorker("Progress Update Thread");
		imageLoadingThread.start();
		progressUpdateThread.start();

		keyController = new ImagePanelKeyController(systemModel);
		surfingController = new ImagePanelSurfingController(imagePanel, systemModel);
		imagePanel.addKeyListener(keyController);
		imagePanel.addMouseListener(surfingController);
		imagePanel.addMouseWheelListener(surfingController);
		imagePanel.addMouseMotionListener(surfingController);
	}

	/**
	 * Load the page for the image panel asynchronously. Note that before the page
	 * loading completed, all events from the Swing event dispatching thread will
	 * be ignored.
	 * 
	 * @param newPage the page to be load.
	 */
	public void loadPage(Page newPage) {
		page = newPage;
		enterBusyState();
		imageLoadingThread.addTask(this);
	}

	/**
	 * Enter the busy state so that all events from the Swing event dispatching thread
	 * will be ignored until {@link #leaveBusyState()} has been called. During the busy
	 * state, the image panel will show a busy animation and message.
	 */
	public synchronized void enterBusyState() {
		if(busyRightNow.compareAndSet(false, true)) {
			disableActions();
			imagePanel.removeKeyListener(keyController);
			imagePanel.removeMouseListener(surfingController);
			imagePanel.removeMouseWheelListener(surfingController);
			progressUpdateThread.addTask(progressUpdater);
		}
	}

	/**
	 * Leave the busy state so that the image panel can receive the Swing events again,
	 * and the busy animation and message will disappear.
	 */
	public synchronized void leaveBusyState() {
		if(busyRightNow.compareAndSet(true, false)) {
			enableActions();
			imagePanel.addKeyListener(keyController);
			imagePanel.addMouseListener(surfingController);
			imagePanel.addMouseWheelListener(surfingController);
		}
	}

	/**
	 * Leave the busy state due to error occurred so that the image panel can receive
	 * the Swing events again, and the busy animation and message will disappear.
	 */
	public synchronized void leaveBusyStateOnErrorOccurred() {
		updateImagePanel(null, errorIcon, LANG_KEY_ERROR_MSG_ON_LOADING_IMAGE);
	}

	@Override
	public void run() {
		BufferedImage image = null;
		Image overlappingImage = null;
		String message = null;
		waitUntilPageReady();
		try {
			image = ImageIO.read(page.getContent());
			if(image == null) {
				message = LANG_KEY_ERROR_MSG_ON_LOADING_IMAGE;
				overlappingImage = errorIcon;
			}
		} catch (Exception e) {
			message = LANG_KEY_ERROR_MSG_ON_LOADING_IMAGE;
			overlappingImage = errorIcon;
		}
		updateImagePanel(image, overlappingImage, message);
	}

	KeyListener getKeyController() {
		return keyController;
	}

	/**
	 * Update the image panel with the specified image, overlapping image, and message.
	 * 
	 * @param image the basic image to be displayed
	 * @param overlappingImage the image that will be overlapped on the basic image
	 * @param message the message to be displayed (also overlapped on the basic image)
	 */
	private void updateImagePanel(BufferedImage image, Image overlappingImage, String message) {
		leaveBusyState();
		imagePanel.setImage(image);
		imagePanel.setOverlappingMessage(message);
		imagePanel.setOverlappingImage(overlappingImage);
		keyController.resetTotalMovedXY();
		int width = image != null? image.getWidth() : 0;
		int height = image != null? image.getHeight() : 0;
		systemModel.getDisplayModel().setImageSize(width, height);
		imagePanel.repaint();
	}

	/**
	 * Wait the page cache until the content is ready. This is a blocking (by sleeping)
	 * method and should NOT be called in Swing event dispatching thread.
	 */
	private void waitUntilPageReady() {
		if(!(page instanceof CachedPage)) {
			return;
		}
		while(!((CachedPage)page).isReady()) {
			sleepSilently(MONITOR_PERIOD);
		}
	}

	/**
	 * Disable the actions that may cause inconsistency.
	 */
	private void disableActions() {
		systemActions.get(OPEN).setEnabled(false);
		systemActions.get(CLOSE).setEnabled(false);
		systemActions.get(NEXT_PAGE).setEnabled(false);
		systemActions.get(NEXT_VOLUME).setEnabled(false);
		systemActions.get(PREVIOUS_PAGE).setEnabled(false);
		systemActions.get(PREVIOUS_VOLUME).setEnabled(false);
	}

	/**
	 * According the model, enable the actions that are disabled by {@link #disableActions()}.
	 */
	private void enableActions() {
		systemActions.get(OPEN).setEnabled(true);
		systemActions.get(CLOSE).setEnabled(systemModel.getNavigator().currentPage() != null);
		systemActions.get(NEXT_PAGE).setEnabled(systemModel.getNavigator().hasNextPage());
		systemActions.get(NEXT_VOLUME).setEnabled(systemModel.getNavigator().hasNextVolume());
		systemActions.get(PREVIOUS_PAGE).setEnabled(systemModel.getNavigator().hasPreviousPage());
		systemActions.get(PREVIOUS_VOLUME).setEnabled(systemModel.getNavigator().hasPreviousVolume());
	}

	/**
	 * A class that update the progress animation periodically.
	 * 
	 * @author Pin-Ying Tu
	 * @version 3.0
	 * @since 2.0
	 */
	private class ProgressUpdater implements Runnable {

		private static final int PROGRESS_IMAGES = 8;
		private static final int PROGRESS_TIMER_PERIOD = 50;

		private static final String PROGRESS_ICON_NAME = "Loading";

		private int progress;

		private ImagePanel imagePanel;
		private Image[] progressIcons;

		/**
		 * Construct <code>ProgressUpdater</code> instance with the panel that actually
		 * display the progress animation.
		 * 
		 * @param panel the panel that actually display the progress animation
		 */
		public ProgressUpdater(ImagePanel panel) {
			imagePanel = panel;
			progressIcons = new Image[PROGRESS_IMAGES];
			for(int index = 0; index < PROGRESS_IMAGES; index++) {
				progressIcons[index] = loadIcon(ICON_PATH + PROGRESS_ICON_NAME + index + ICON_EXTENSION).getImage();
			}
		}

		@Override
		public void run() {
			progress = -1;
			while(busyRightNow.get()) {
				sleepSilently(PROGRESS_TIMER_PERIOD);
				if(busyRightNow.get()) {
					progress++;
					imagePanel.setOverlappingImage(progressIcons[(progress % PROGRESS_IMAGES)]);
					imagePanel.setOverlappingMessage(LANG_KEY_LOADING_MSG);
					imagePanel.repaint();
				}
			}
		}
	}
}
