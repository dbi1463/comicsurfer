/* OpenComicBookTask.java created on 2013/1/6
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.model;

import java.net.URI;

import tw.funymph.comicsurfer.cache.CachedComicBookSuite;
import tw.funymph.comicsurfer.core.ComicBookSuite;
import tw.funymph.comicsurfer.core.NavigationModel;
import tw.funymph.comicsurfer.readers.ComicBookSuiteReaderManager;

/**
 * The parallel task to open comic book.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class OpenComicBookTask implements Runnable {

	private URI uri;
	private NavigationModel navigationModel;
	private ComicBookSuiteReaderManager readerManager;

	/**
	 * Construct a <code>OpenComicBookTask</code> instance with the reader manager, the
	 * navigation model, and the URI of the suite.
	 * 
	 * @param manager the manager that read the comic book suite
	 * @param navigator the navigator to open the suite
	 * @param suiteURI the URI of the suite
	 */
	public OpenComicBookTask(ComicBookSuiteReaderManager manager, NavigationModel navigator, URI suiteURI) {
		uri = suiteURI;
		readerManager = manager;
		navigationModel = navigator;
	}

	@Override
	public void run() {
		ComicBookSuite suite = null;
		try {
			suite = readerManager.read(uri);
			navigationModel.openComicBook(new CachedComicBookSuite(suite));
		}
		catch(Throwable e) {
			e.printStackTrace();
			suite = null;
		}
	}
}
