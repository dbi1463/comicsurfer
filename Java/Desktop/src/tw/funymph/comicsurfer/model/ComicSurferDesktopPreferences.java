/* ComicSurferDesktopPreferences.java created on Dec 7, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.model;

import static tw.funymph.comicsurfer.core.ImageDisplayModel.ACTUAL_IMAGE_SIZE;
import static tw.funymph.comicsurfer.core.ImageDisplayModel.FIT_WINDOW_HEIGHT;
import static tw.funymph.comicsurfer.core.ImageDisplayModel.FIT_WINDOW_SIZE;
import static tw.funymph.comicsurfer.core.ImageDisplayModel.FIT_WINDOW_WIDTH;

import java.awt.Color;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import tw.funymph.comicsurfer.ComicSurferModel;
import tw.funymph.comicsurfer.LanguageKeys;
import tw.funymph.comicsurfer.preferences.Preferences;
import tw.funymph.comicsurfer.preferences.PreferencesListener;
import tw.funymph.comicsurfer.preferences.PreferencesListenerList;

import tw.jsway.MultiLanguageSupport;
import tw.jsway.property.*;
import tw.jsway.property.codecs.*;
import tw.jsway.utilities.ObjectUtilities;

/**
 * A class that implements <code>Preferences</code> to manage the
 * user's preferences in the desktop PC platform.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.3
 */
public class ComicSurferDesktopPreferences implements LanguageKeys, Preferences, EditablePropertyListener {

	public static final String KEY_SHOW_OPEN_DIALOG = "show-open-dialog";
	public static final String CATEGORY_BEHAVIOR = "Behavior";
	public static final String CATEGORY_APPEARANCE = "Appearance";

	private static final String DEFAULT_LANGUAGE = "English, United States";
	private static final String DEFAULT_LAST_OPEN_PATH = System.getProperty("user.home");

	private ComicSurferModel systemModel;
	private PreferencesListenerList listeners;
	private Hashtable<String, EditableProperty> allProperties;
	private Hashtable<String, EditablePropertyCodec> codecs;
	private CategorizedEditableProperties categorizedProperties;

	/**
	 * Construct a <code>ComicSurferDesktopPreferences</code> instance.
	 * 
	 * @param model the comic surfer model 
	 */
	public ComicSurferDesktopPreferences(ComicSurferModel model) {
		systemModel = ObjectUtilities.requireNonNull(model, "Cannot create preferences without the running model!");

		listeners = new PreferencesListenerList(this);
		codecs = new Hashtable<String, EditablePropertyCodec>();
		allProperties = new Hashtable<String, EditableProperty>();
		categorizedProperties = new DefaultCategorizedEditableProperties(LANG_KEY_PREFERENCES, getLangSupport(LANG_KEY_PREFERENCES));

		createBehaviorProperties();
		createAppearanceProperties();
		createOtherProperties();
	}

	/**
	 * Get the categorized editable properties.
	 * 
	 * @return the categorized editable properties
	 */
	public CategorizedEditableProperties getCategorizedEditableProperties() {
		return categorizedProperties;
	}

	/**
	 * Get the editable property specified by the name. All property names are defined
	 * as public string constants in this class (including {@link Preferences}) with the
	 * KEY_ prefix, e.g., {@link #KEY_SHOW_OPEN_DIALOG}, and can be retrieved by calling
	 * {@link #getPreferenceKeys()}. 
	 * 
	 * @param name the name of the property
	 * @return the editable property
	 */
	public EditableProperty getEditableProperty(String name) {
		return allProperties.get(name);
	}

	@Override
	public void propertyChanged(EditablePropertyEvent e) {
		String propertyName = e.getSource().getName();
		if(KEY_LANGUAGE.equals(propertyName)) {
			systemModel.getLanguageManager().setCurrentLanguage(e.getCurrentValue().toString());
		}
		else if(KEY_DISPLAY_MODE.equals(propertyName) && EditableProperty.KEY_VALUE.equals(e.getKey())) {
			systemModel.getDisplayModel().setDisplayMode(((Number)e.getSource().getValue()).intValue());
		}
		if(EditableProperty.KEY_VALUE.equals(e.getKey())) {
			listeners.firePreferencesChangedEvent(e.getSource().getName());
		}
	}

	@Override
	public void setPreference(String key, String value) {
		if(allProperties.containsKey(key)) {
			EditableProperty property = allProperties.get(key);
			property.setValue(codecs.containsKey(key)? codecs.get(key).decode(value) : (String)value);
		}
	}

	@Override
	public String getPreference(String key) {
		if(allProperties.containsKey(key)) {
			EditableProperty property = allProperties.get(key);
			return (codecs.containsKey(key))? codecs.get(key).encode(property.getValue()) : (String)property.getValue();
		}
		return null;
	}

	@Override
	public String[] getPreferenceKeys() {
		return allProperties.keySet().toArray(new String[0]);
	}

	@Override
	public void addPreferencesListener(PreferencesListener listener) {
		listeners.addPreferencesListener(listener);
	}

	@Override
	public void removePreferencesListener(PreferencesListener listener) {
		listeners.removePreferencesListener(listener);
	}

	/**
	 * Create the comic surfer multiple language support with the language key.
	 * 
	 * @param langKey the language key
	 * @return the comic surfer multiple language support
	 */
	private MultiLanguageSupport getLangSupport(String langKey) {
		return new ComicSurferMultiLanguageSupport(langKey, systemModel.getLanguageManager());
	}

	/**
	 * Create the appearance-related properties.
	 */
	private void createAppearanceProperties() {
		createColorOptions();
		createInternationalizationProperties();
	}

	/**
	 * Create the color-related options (properties) which belongs to the appearance category.
	 */
	private void createColorOptions() {
		EditableProperties properties = new DefaultEditableProperties(LANG_KEY_COLORS, getLangSupport(LANG_KEY_COLORS));
		registerProperty(properties, new ColorProperty(KEY_BACKGROUND_COLOR, getLangSupport(LANG_KEY_BACKGROUND_COLOR), Color.black), new ColorPropertyCodec());
		categorizedProperties.addProperties(CATEGORY_APPEARANCE, getLangSupport(LANG_KEY_APPEARANCE), properties);
	}

	/**
	 * Create the internationalization properties which belongs to the appearance category.
	 */
	private void createInternationalizationProperties() {
		List<Choice> choices = new LinkedList<Choice>();
		Set<String> languages = systemModel.getLanguageManager().getAvailableLanguages();
		Iterator<String> iterator = languages.iterator();
		while(iterator.hasNext()) {
			choices.add(new SimpleChoice(iterator.next()));
		}
		ChoicesProperty property = new ChoicesProperty(KEY_LANGUAGE, getLangSupport(LANG_KEY_LANGUAGE), DEFAULT_LANGUAGE, choices);
		EditableProperties properties = new DefaultEditableProperties(LANG_KEY_INTERNATIONALIZATION, getLangSupport(LANG_KEY_INTERNATIONALIZATION));
		registerProperty(properties, property, null);
		categorizedProperties.addProperties(CATEGORY_APPEARANCE, getLangSupport(LANG_KEY_APPEARANCE), properties);
	}

	/**
	 * Create the behavior-related properties.
	 */
	private void createBehaviorProperties() {
		createImageOptions();
		createDialogOptions();
	}

	/**
	 * Create the image-related options (properties) which belongs to the behavior category.
	 */
	private void createImageOptions() {
		EditableProperties properties = new DefaultEditableProperties(LANG_KEY_IMAGE_OPTIONS, getLangSupport(LANG_KEY_IMAGE_OPTIONS));
		registerProperty(properties, createDisplayModelProperty(), new NumberPropertyCodec());
		registerProperty(properties, new BooleanProperty(KEY_TRACK_MOUSE, getLangSupport(LANG_KEY_TRACK_MOUSE), true), new BooleanPropertyCodec());
		registerProperty(properties, new NumberProperty(KEY_SLIDESHOW_SPEED, getLangSupport(LANG_KEY_SLIDESHOW_SPEED), 3000, 500, "ms"), new NumberPropertyCodec());
		categorizedProperties.addProperties(CATEGORY_BEHAVIOR, getLangSupport(LANG_KEY_BEHAVIOR), properties);
	}

	/**
	 * Create the dialog-related options (properties) which belongs to the behavior category.
	 */
	private void createDialogOptions() {
		EditableProperties properties = new DefaultEditableProperties(LANG_KEY_DIALOG_OPTIONS, getLangSupport(LANG_KEY_DIALOG_OPTIONS));
		registerProperty(properties, new BooleanProperty(KEY_SHOW_OPEN_DIALOG, getLangSupport(LANG_KEY_SHOW_OPEN_DIALOG), true), new BooleanPropertyCodec());
		categorizedProperties.addProperties(CATEGORY_BEHAVIOR, getLangSupport(LANG_KEY_BEHAVIOR), properties);
	}

	/**
	 * Create the display model property (is a <code>ChoicesProperty</code>).
	 * 
	 * @return the display model property
	 */
	private EditableProperty createDisplayModelProperty() {
		List<Choice> choices = new LinkedList<Choice>();
		choices.add(new SimpleChoice(ACTUAL_IMAGE_SIZE, getLangSupport(LANG_KEY_ACTUAL_IMAGE_SIZE)));
		choices.add(new SimpleChoice(FIT_WINDOW_WIDTH, getLangSupport(LANG_KEY_FIT_WINDOW_WIDTH)));
		choices.add(new SimpleChoice(FIT_WINDOW_HEIGHT, getLangSupport(LANG_KEY_FIT_WINDOW_HEIGHT)));
		choices.add(new SimpleChoice(FIT_WINDOW_SIZE, getLangSupport(LANG_KEY_FIT_WINDOW_SIZE)));
		return new ChoicesProperty(KEY_DISPLAY_MODE, getLangSupport(LANG_KEY_DISPLAY_MODE), ACTUAL_IMAGE_SIZE, choices);
	}

	/**
	 * Create other properties that will not be shown in the Preferences dialog.
	 */
	private void createOtherProperties() {
		TextProperty property = new TextProperty(KEY_LAST_OPEN_PATH, DEFAULT_LAST_OPEN_PATH);
		property.addEditablePropertyListener(this);
		allProperties.put(KEY_LAST_OPEN_PATH, property);
	}

	/**
	 * Register the property into the property group and the global property group.
	 * 
	 * @param propertyGroup the target group
	 * @param property the property to be registered
	 * @param codec the codec used by the global property group
	 */
	private void registerProperty(EditableProperties propertyGroup, EditableProperty property, EditablePropertyCodec codec) {
		propertyGroup.addEditableProperty(property);
		allProperties.put(property.getName(), property);
		if(codec != null) {
			codecs.put(property.getName(), codec);
		}
		property.addEditablePropertyListener(this);
	}
}
