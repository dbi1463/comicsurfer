/* ComicSurferMultiLanguageSupport.java created on 2012/10/16
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.model;

import static tw.jsway.utilities.ObjectUtilities.requireNonNull;

import java.util.ArrayList;

import tw.funymph.comicsurfer.language.LanguageChangeListener;
import tw.funymph.comicsurfer.language.LanguageManager;
import tw.funymph.comicsurfer.language.LocalizedTextProvider;

import tw.jsway.MultiLanguageSupport;
import tw.jsway.MultiLanguageSupportListener;

/**
 * A class that adapts the JavaSway multiple language support and the
 * Comic Surfer language manager.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class ComicSurferMultiLanguageSupport implements MultiLanguageSupport, LanguageChangeListener {

	private String langKey;
	private LanguageManager languageManager;
	private ArrayList<MultiLanguageSupportListener> listeners;

	/**
	 * Construct a <code>ComicSurferMultiLanguageSupport</code> instance with
	 * the language key that can be used to retrieve the real display text from
	 * the specified language manager.
	 * 
	 * @param key the key to retrieve the real display text
	 * @param manager the language manager
	 */
	public ComicSurferMultiLanguageSupport(String key, LanguageManager manager) {
		langKey = requireNonNull(key, "Cannot create a multiple language support without lang key!");
		languageManager = requireNonNull(manager, "Cannot create a multiple language support without language manager!");
		listeners = new ArrayList<MultiLanguageSupportListener>();
		languageManager.addLanguageChangeListener(this);
	}

	@Override
	public String getDisplayText() {
		return languageManager.getLocalizedText(langKey);
	}

	@Override
	public void addMultiLanguageSupportListener(MultiLanguageSupportListener listener) {
		if(!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	@Override
	public void removeMultiLanguageSupportListener(MultiLanguageSupportListener listener) {
		listeners.remove(listener);
	}

	@Override
	public void languageChanged(LocalizedTextProvider source) {
		notifyMultiLanguageSupportListeners();
	}

	/**
	 * Notify all registered multiple language support listeners.
	 */
	private void notifyMultiLanguageSupportListeners() {
		for(MultiLanguageSupportListener listener : listeners) {
			listener.displayTextChanged(this);
		}
	}
}
