/* DesktopPreferencesManager.java created on 2012/09/27
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer Core project.
 */
package tw.funymph.comicsurfer.model;

import static java.lang.System.out;
import static tw.funymph.commons.assertion.AssertUtilities.isNotBlank;

import java.io.File;
import java.util.Hashtable;
import java.util.Map;

import tw.funymph.comicsurfer.compatibility.PreferencesV1DecoderNode;
import tw.funymph.comicsurfer.compatibility.ViewRecordV1DecoderNode;
import tw.funymph.comicsurfer.history.ViewHistory;
import tw.funymph.comicsurfer.preferences.Preferences;
import tw.funymph.comicsurfer.preferences.PreferencesManager;
import tw.funymph.commons.xml.AbstractXMLCodecTreeNode;
import tw.funymph.commons.xml.AbstractXMLDecoderTreeNode;
import tw.funymph.commons.xml.AbstractXMLEncoderTreeNode;
import tw.funymph.commons.xml.XMLDecoder;
import tw.funymph.commons.xml.XMLEncoder;

/**
 * A class can offer the implementation of {@link PreferencesManager} for {@link ComicSurferDesktopPreferences}
 * using the standard SAX API.
 *
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class DesktopPreferencesManager implements PreferencesManager {

	private static final String ELEMENT_ROOT = "preferences";
	private static final String ATTRIBUTE_VERSION = "version";
	private static final String DEFAULT_PATH = "preferences.xml";

	private static final int COMPATIBLE_MODE = 1;
	private static final int CURRENT_VERSION = 2;

	private XMLEncoder encoder;
	private XMLDecoder decoder;
	private ViewHistory history;

	/**
	 * Construct a <code>DesktopPreferencesManager</code> instance with the view history.
	 * The view history is needed because of the compatibility issue.
	 * 
	 * @param theHistory the view history
	 */
	public DesktopPreferencesManager(ViewHistory theHistory) {
		encoder = new XMLEncoder();
		decoder = new XMLDecoder();
		history = theHistory;
	}

	@Override
	public void loadPreferences(Preferences preferences) {
		File file = new File(DEFAULT_PATH);
		PreferencesDecodeTreeRootNode root = new PreferencesDecodeTreeRootNode(preferences, history);
		if(file.exists()) {
			decoder.decode(root, file);
		}
		else {
			out.println("    The preferences file does not exist! Use the default values.");
		}
	}

	@Override
	public void savePreferences(Preferences preferences) {
		PreferencesEncodeTreeRootNode root = new PreferencesEncodeTreeRootNode(preferences);
		encoder.encode(root, new File(DEFAULT_PATH));
	}

	/**
	 * The decoder that converts the XML nodes into the user's preferences.
	 * 
	 * @author Pin-Ying Tu
	 * @version 3.0
	 * @since 2.1
	 */
	private class PreferencesDecodeTreeRootNode extends AbstractXMLDecoderTreeNode {

		private int version;
		private ViewHistory history;
		private Preferences preferences;

		/**
		 * Construct a <code>PreferencesDecodeTreeRootNode</code> instance with the
		 * preference container and the view history. The view history is needed because
		 * of the compatibility issue.
		 * 
		 * @param thePreferences the preference container
		 * @param theHistory the view history
		 */
		public PreferencesDecodeTreeRootNode(Preferences thePreferences, ViewHistory theHistory) {
			super(ELEMENT_ROOT);
			history = theHistory;
			preferences = thePreferences;
			addChildNode(new PreferenceEntryCodecNode(preferences));
		}

		@Override
		public void processAttributes(Map<String, String> attributes) {
			version = attributes.containsKey(ATTRIBUTE_VERSION)? Integer.parseInt(attributes.get(ATTRIBUTE_VERSION)) : COMPATIBLE_MODE;
			if(version == COMPATIBLE_MODE) {
				addChildNode(new PreferencesV1DecoderNode(Preferences.KEY_LANGUAGE, preferences));
				addChildNode(new PreferencesV1DecoderNode(Preferences.KEY_TRACK_MOUSE, preferences));
				addChildNode(new PreferencesV1DecoderNode(Preferences.KEY_DISPLAY_MODE, preferences));
				addChildNode(new PreferencesV1DecoderNode(Preferences.KEY_LAST_OPEN_PATH, preferences));
				addChildNode(new PreferencesV1DecoderNode(Preferences.KEY_BACKGROUND_COLOR, preferences));
				addChildNode(new ViewRecordV1DecoderNode(history));
			}
		}
	}

	/**
	 * The encoder that converts the user's preferences into the XML nodes.
	 * 
	 * @author Pin-Ying Tu
	 * @version 3.0
	 * @since 2.1
	 */
	private class PreferencesEncodeTreeRootNode extends AbstractXMLEncoderTreeNode {

		/**
		 * Construct a <code>PreferencesEncodeTreeRootNode</code> instance with
		 * the preference container to be converted.
		 * 
		 * @param preferences the preference container
		 */
		public PreferencesEncodeTreeRootNode(Preferences preferences) {
			super(ELEMENT_ROOT);
			addChildEncoder(new PreferenceEntryCodecNode(preferences));
		}

		@Override
		public Map<String, String> getAttributes(int index) {
			Map<String, String> attributes = new Hashtable<String, String>();
			attributes.put(ATTRIBUTE_VERSION, String.valueOf(CURRENT_VERSION));
			return attributes;
		}
	}

	/**
	 * The decoder and encoder that convert the data between the user's preferences
	 * and the XML nodes. This is used in the new version XML format in which each
	 * preference is recorded as an entry.
	 * 
	 * @author Pin-Ying Tu
	 * @version 3.0
	 * @since 2.1
	 */
	private class PreferenceEntryCodecNode extends AbstractXMLCodecTreeNode {

		private static final String ELEMENT_ENTRY = "entry";
		private static final String ATTRIBUTE_NAME = "name";
		private static final String ATTRIBUTE_VALUE = "value";

		private String[] keys;
		private Preferences preferences;

		/**
		 * Construct a <code>PreferenceEntryCodecNode</code> instance with the
		 * preference container.
		 * 
		 * @param thePreferences the preference container
		 */
		public PreferenceEntryCodecNode(Preferences thePreferences) {
			super(ELEMENT_ENTRY);
			preferences = thePreferences;
			keys = preferences.getPreferenceKeys();
		}

		@Override
		public void processAttributes(Map<String, String> attributes) {
			String key = attributes.get(ATTRIBUTE_NAME);
			String value = attributes.get(ATTRIBUTE_VALUE);
			if(isNotBlank(key) && isNotBlank(value)) {
				preferences.setPreference(key, value);
			}
		}

		@Override
		public int getRepeatCount() {
			return keys.length;
		}

		@Override
		public Map<String, String> getAttributes(int index) {
			Map<String, String> attributes = new Hashtable<String, String>();
			attributes.put(ATTRIBUTE_NAME, keys[index]);
			attributes.put(ATTRIBUTE_VALUE, preferences.getPreference(keys[index]));
			return attributes;
		}
	}
}
