/* FileFilterAdapter.java created on Nov 5, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.model;

import static tw.funymph.comicsurfer.LanguageKeys.LANG_KEY_FILE_FILTER_DESCRIPTION;
import static tw.funymph.commons.file.FileUtilities.isImageFile;

import java.io.File;

import javax.swing.filechooser.FileFilter;

import tw.funymph.comicsurfer.ComicSurferModel;
import tw.funymph.comicsurfer.readers.ComicBookSuiteReaderManager;

/**
 * A class that transfers the comic book reader manager interface to
 * the Java Swing <code>FileFilter</code> interface.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.2
 */
public class FileFilterAdapter extends FileFilter {

	private ComicSurferModel systemModel;
	private ComicBookSuiteReaderManager readerManager;

	/**
	 * Construct a <code>FileFilterAdapter</code> instance that adapts
	 * the interfaces of <code>ComicBookSuiteReaderManager</code>.
	 * 
	 * @param manager the comic book suite reader manager
	 */
	public FileFilterAdapter(ComicSurferModel model) {
		systemModel = model;
		readerManager = systemModel.getReaderManager();
	}

	@Override
	public boolean accept(File file) {
		boolean result = false;
		if(file.isFile()) {
			return readerManager.getBestFitReader(file.toURI()) != null;
		}
		else {
			File children[] = file.listFiles();
			for(File child : children) {
				if(child.isDirectory() || isImageFile(child) || readerManager.getBestFitReader(child.toURI()) != null) {
					return true;
				}
			}
		}
		return result;
	}

	@Override
	public String getDescription() {
		return systemModel.getLanguageManager().getLocalizedText(LANG_KEY_FILE_FILTER_DESCRIPTION);
	}
}
