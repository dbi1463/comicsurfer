/* ComicSurferDesktopModel.java created on 2011/10/13
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.model;

import java.net.URI;
import java.util.concurrent.LinkedBlockingQueue;

import tw.funymph.comicsurfer.ComicSurfer;
import tw.funymph.comicsurfer.ComicSurferModel;

import tw.funymph.comicsurfer.core.ImageDisplayModel;
import tw.funymph.comicsurfer.core.MotionController;
import tw.funymph.comicsurfer.core.NavigationModel;
import tw.funymph.comicsurfer.core.paginal.PaginalComicBookDisplayController;
import tw.funymph.comicsurfer.core.paginal.PaginalComicBookNavigator;
import tw.funymph.comicsurfer.core.slide.SlideShowController;
import tw.funymph.comicsurfer.history.LastViewedRecordUpdater;
import tw.funymph.comicsurfer.history.RecentViewHistory;
import tw.funymph.comicsurfer.history.SecondaryLevelViewHistory;
import tw.funymph.comicsurfer.history.SimpleLastViewedComicBookInformer;
import tw.funymph.comicsurfer.history.ViewHistory;
import tw.funymph.comicsurfer.history.ViewHistoryManager;
import tw.funymph.comicsurfer.history.ViewHistoryManagerProvider;
import tw.funymph.comicsurfer.history.XMLViewHistoryManagerProvider;
import tw.funymph.comicsurfer.language.LanguageManager;
import tw.funymph.comicsurfer.language.XMLLanguageManager;
import tw.funymph.comicsurfer.preferences.DefaultTrackMouseInformer;
import tw.funymph.comicsurfer.preferences.Preferences;
import tw.funymph.comicsurfer.readers.ComicBookSuiteReaderManager;
import tw.funymph.comicsurfer.readers.DefaultComicBookSuiteReaderManager;
import tw.funymph.comicsurfer.ui.FullScreenSlideShowController;
import tw.funymph.comicsurfer.update.DefaultUpdateManager;
import tw.funymph.commons.thread.ThreadPool;
import tw.funymph.commons.zip.MultiCharsetZipFileFactory;
import tw.funymph.commons.zip.UTF8ZipFileFactory;
import tw.funymph.commons.zip.ZipFileFactory;

/**
 * The model that manages all the business logics of the ComicSurfer. This
 * is also the Facade class for all system actions.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 1.0
 */
public class ComicSurferDesktopModel implements ComicSurferModel {

	private static final int DEFAULT_THREAD_POOL_SIZE = 5;
	private static final int DEFAULT_HISTORY_CAPACITY = 10;

	private static final String DEFAULT_READERS_FOLDER = "readers";
	private static final String DEFAULT_LANGUAGES_FOLDER = "languages";

	private ThreadPool threadPool;
	private NavigationModel navigator;
	private LanguageManager languages;
	private ZipFileFactory zipFileFactory;
	private RecentViewHistory viewHistory;
	private ViewHistoryManager historyManager;
	private DefaultUpdateManager updateManager;
	private ComicBookSuiteReaderManager readers;
	private SlideShowController slideShowController;
	private ComicSurferDesktopPreferences preferences;
	private LinkedBlockingQueue<Runnable> parallelTasks;
	private DesktopPreferencesManager preferencesManager;
	private SecondaryLevelViewHistory secondaryLevelViewHistory;
	private PaginalComicBookDisplayController displayController;

	/**
	 * Construct a <code>ComicSurferModel</code> instance.
	 */
	public ComicSurferDesktopModel(boolean isJVM7) {
		languages = new XMLLanguageManager();
		ViewHistoryManagerProvider provider = new XMLViewHistoryManagerProvider();
		secondaryLevelViewHistory = new SecondaryLevelViewHistory(provider);
		historyManager = provider.getViewHistoryManager(false);
		zipFileFactory = isJVM7? new MultiCharsetZipFileFactory() : new UTF8ZipFileFactory();
		readers = new DefaultComicBookSuiteReaderManager(zipFileFactory);
		parallelTasks = new LinkedBlockingQueue<Runnable>();
		updateManager = DefaultUpdateManager.getDefaultManager();
		viewHistory = new RecentViewHistory(DEFAULT_HISTORY_CAPACITY);
		navigator = new PaginalComicBookNavigator(new SimpleLastViewedComicBookInformer(viewHistory));
		slideShowController = new FullScreenSlideShowController(this);
		threadPool = new ThreadPool(DEFAULT_THREAD_POOL_SIZE, parallelTasks);
	}

	/**
	 * Release all resources.
	 */
	public void releaseResources() {
		if(slideShowController.isSlideShowStarted()) {
			slideShowController.stopSlideShow();
		}
		navigator.closeComicBook();
		historyManager.saveHistory(viewHistory);
		preferencesManager.savePreferences(preferences);
		slideShowController.stopSlideShow();
		threadPool.dismiss();
		System.out.println("Saved your preferences. Bye!!");
	}

	/**
	 * Get the update manager.
	 * 
	 * @return the update manager
	 */
	public DefaultUpdateManager getUpdateManager() {
		return updateManager;
	}

	@Override
	public void initialize() {
		threadPool.recruitAndWait();
		System.out.println("  Loading third-party readers... ");
		readers.loadThirdPartyReaders(DEFAULT_READERS_FOLDER);
		System.out.println("  There are " + threadPool.getIdleThreads() + " idle threads ready for use.");
		System.out.println("  Use the " + zipFileFactory.getClass().getSimpleName() + " to load ZIP files.");
		System.out.println("  Loading language definition files in the \"" + DEFAULT_LANGUAGES_FOLDER + "\" folder...");
		languages.initialize(DEFAULT_LANGUAGES_FOLDER);
		System.out.println("  Loading view history... ");
		historyManager.loadHistory(viewHistory);
		viewHistory.setReference(secondaryLevelViewHistory);
		System.out.println("  Loading your preferences...");
		preferences = new ComicSurferDesktopPreferences(this);
		displayController = new PaginalComicBookDisplayController(navigator, new DefaultTrackMouseInformer(preferences));
		preferencesManager = new DesktopPreferencesManager(viewHistory);
		preferencesManager.loadPreferences(preferences);
		System.out.println("    Background Color: " + preferences.getPreference(Preferences.KEY_BACKGROUND_COLOR));
		System.out.println("    Display Language: " + preferences.getPreference(Preferences.KEY_LANGUAGE));
		System.out.println("    Image Track Mouse: " + (Boolean.parseBoolean(preferences.getPreference(Preferences.KEY_TRACK_MOUSE))? "On" : "Off"));
		System.out.println("  Starting version check at background...");
		navigator.addNavigationEventListener(new LastViewedRecordUpdater(viewHistory, preferences));
		languages.setCurrentLanguage(preferences.getPreference(Preferences.KEY_LANGUAGE));
		updateManager.startVersionCheck(ComicSurfer.MAJOR_VERSION, ComicSurfer.MINOR_VERSION, ComicSurfer.BUILD_VERSION);
	}

	public void open(final URI uri) {
		if(isReadyToReadComicBooks()) {
			parallelTasks.add(new OpenComicBookTask(readers, navigator, uri));
		}
	}

	private boolean isReadyToReadComicBooks() {
		return navigator != null && readers != null && parallelTasks != null;
	}

	@Override
	public void exit() {
		releaseResources();
		System.exit(0);
	}

	@Override
	public LanguageManager getLanguageManager() {
		return languages;
	}

	@Override
	public NavigationModel getNavigator() {
		return navigator;
	}

	@Override
	public ComicBookSuiteReaderManager getReaderManager() {
		return readers;
	}

	@Override
	public ImageDisplayModel getDisplayModel() {
		return displayController;
	}

	@Override
	public MotionController getMotionController() {
		return displayController;
	}

	@Override
	public Preferences getPreferences() {
		return preferences;
	}

	@Override
	public ViewHistory getViewHistory() {
		return viewHistory;
	}

	@Override
	public SlideShowController getSlideShowController() {
		return slideShowController;
	}
}
