/* ShowNextPageAction.java created on Oct 13, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static java.awt.event.KeyEvent.VK_N;
import static javax.swing.KeyStroke.getKeyStroke;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import tw.funymph.comicsurfer.core.events.NavigationEvent;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/** An action that changes the page to the next page.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.0
 */
public class NextPageAction extends NavigationAction {

	private static final String NEXT_PAGE_ICON_NAME = "NextPage";

	/** Construct a <code>NextPageAction</code> instance.
	 * 
	 * @param model the model that offers the related method to be invoked
	 */
	public NextPageAction(ComicSurferDesktopModel model) {
		super(model, NEXT_PAGE_ICON_NAME);
		setEnabled(systemModel.getNavigator().hasNextPage());
		putValue(ACCELERATOR_KEY, getKeyStroke(VK_N, KeyEvent.CTRL_MASK));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		systemModel.getNavigator().nextPage();
	}

	@Override
	protected void updateDisplayedContents() {
		putValue(NAME, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_NEXT_PAGE));
		putValue(SHORT_DESCRIPTION, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_NEXT_PAGE_DESCRIPTION));
	}

	@Override
	public void pageChanged(NavigationEvent event) {
		setEnabled(systemModel.getNavigator().hasNextPage());
	}
}
