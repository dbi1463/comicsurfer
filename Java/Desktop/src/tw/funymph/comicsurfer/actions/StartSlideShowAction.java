/* StartSlideShowAction.java created on 2013/1/6
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static javax.swing.KeyStroke.getKeyStroke;

import java.awt.event.ActionEvent;

import tw.funymph.comicsurfer.ComicSurfer;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;
import tw.funymph.comicsurfer.ui.FullScreenSlideShowController;

/** An action that start the slide show mode.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class StartSlideShowAction extends SystemAction {

	/** Construct a <code>StartSlideShowAction</code> instance.
	 * 
	 * @param model the model that offers the related method to be invoked
	 */
	public StartSlideShowAction(ComicSurferDesktopModel model) {
		super(model);
		putValue(ACCELERATOR_KEY, getKeyStroke("F5"));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		FullScreenSlideShowController controller = (FullScreenSlideShowController)systemModel.getSlideShowController();
		controller.setComicSurferFrame(ComicSurfer.getMainFrame());
		controller.startSlideShow();
	}

	@Override
	protected void updateDisplayedContents() {
		putValue(NAME, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_START_SLIDE_SHOW));
		putValue(SHORT_DESCRIPTION, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_START_SLIDE_SHOW));
	}
}
