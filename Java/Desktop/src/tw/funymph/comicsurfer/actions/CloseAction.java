/* CloseAction.java created on Oct 14, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static java.awt.event.KeyEvent.VK_W;
import static javax.swing.KeyStroke.getKeyStroke;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import tw.funymph.comicsurfer.core.events.NavigationEvent;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;
import tw.funymph.comicsurfer.ui.ImagePanelKeyController.KeyHandler;

/** An action that closes the opened comic book suite.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.0
 */
public class CloseAction extends NavigationAction implements KeyHandler {

	private static final String CLOSE_ICON_NAME = "Close";

	/** Construct a <code>CloseAction</code> instance
	 * 
	 * @param model the model that offers the related method to be invoked
	 */
	public CloseAction(ComicSurferDesktopModel model) {
		super(model, CLOSE_ICON_NAME);
		setEnabled(systemModel.getNavigator().currentPage() != null);
		putValue(ACCELERATOR_KEY, getKeyStroke(VK_W, KeyEvent.CTRL_MASK));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		systemModel.getNavigator().closeComicBook();
	}

	@Override
	protected void updateDisplayedContents() {
		putValue(NAME, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_CLOSE));
		putValue(SHORT_DESCRIPTION, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_CLOSE_DESCRIPTION));
	}

	@Override
	public void pageChanged(NavigationEvent event) {
		setEnabled(systemModel.getNavigator().currentPage() != null);
	}

	@Override
	public void suiteClosed(NavigationEvent event) {
		setEnabled(false);
	}
}
