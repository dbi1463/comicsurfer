/* OpenAction.java created on Oct 13, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static java.awt.event.KeyEvent.VK_O;
import static javax.swing.KeyStroke.getKeyStroke;
import static tw.funymph.comicsurfer.ui.UIUtilities.showFileChooser;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;

import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;
import tw.funymph.comicsurfer.ui.ImagePanelKeyController.KeyHandler;

/** An action that opens a file chooser with all available file filters from model.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.0
 */
public class OpenAction extends SystemAction implements KeyHandler {

	private static final String OPEN_ICON_NAME = "Open";

	/** Construct a <code>OpenAction</code> instance.
	 * 
	 * @param model the model that offers the related method to be invoked
	 */
	public OpenAction(ComicSurferDesktopModel model) {
		super(model, OPEN_ICON_NAME);
		putValue(ACCELERATOR_KEY, getKeyStroke(VK_O, KeyEvent.CTRL_MASK));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		File file = showFileChooser(systemModel);
		if(file != null) {
			systemModel.open(file.toURI());
		}
	}

	@Override
	public void handle() {
		actionPerformed(null);
	}

	@Override
	protected void updateDisplayedContents() {
		putValue(NAME, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_OPEN));
		putValue(SHORT_DESCRIPTION, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_OPEN_DESCRIPTION));
	}
}
