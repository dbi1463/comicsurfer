/* ExitAction.java created on Oct 13, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static java.awt.event.KeyEvent.VK_Q;
import static javax.swing.KeyStroke.getKeyStroke;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/** An action that closes the ComicSurfer application.
 * 
 * @author Pin-Ying Tu
 * @version 2.0
 * @since 1.0
 */
public class ExitAction extends SystemAction {

	private static final String EXIT_ICON_NAME = "Exit";

	/** Construct <code>ExitAction</code> instance.
	 * 
	 * @param model the model that offers the related method to be invoked
	 */
	public ExitAction(ComicSurferDesktopModel model) {
		super(model, EXIT_ICON_NAME);
		putValue(ACCELERATOR_KEY, getKeyStroke(VK_Q, KeyEvent.CTRL_MASK));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		systemModel.exit();
	}

	@Override
	protected void updateDisplayedContents() {
		putValue(NAME, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_EXIT));
		putValue(SHORT_DESCRIPTION, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_EXIT_DESCRIPTION));
	}
}
