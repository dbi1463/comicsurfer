/* AboutAction.java created on Oct 13, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static java.awt.Color.white;
import static javax.swing.KeyStroke.getKeyStroke;
import static tw.funymph.comicsurfer.ComicSurfer.FULL_PRODUCT_VERSION;
import static tw.funymph.comicsurfer.ComicSurfer.getMainFrame;
import static tw.jsway.utilities.ImageUtilities.loadIcon;

import java.awt.event.ActionEvent;

import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;
import tw.jsway.ImageAboutDialog;

/** An action that shows the about dialog.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.0
 */
public class AboutAction extends SystemAction {

	private static final int VER_INFO_LOCATON_X = 197;
	private static final int VER_INFO_LOCATON_Y = 182;

	private static final String ABOUT_ICON_NAME = "About";
	private static final String IMAGE_PATH = "icons/ComicSurfer.png";

	/** Construct an <code>AboutAction</code> instance.
	 * 
	 * @param model the comic surfer model
	 */
	public AboutAction(ComicSurferDesktopModel model) {
		super(model, ABOUT_ICON_NAME);
		putValue(ACCELERATOR_KEY, getKeyStroke("F1"));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ImageAboutDialog dialog = new ImageAboutDialog(getMainFrame(), loadIcon(IMAGE_PATH));
		dialog.addOverlappingTex(FULL_PRODUCT_VERSION, VER_INFO_LOCATON_X, VER_INFO_LOCATON_Y, white);
		dialog.setVisible(true);
	}

	@Override
	protected void updateDisplayedContents() {
		putValue(NAME, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_ABOUT_DESCRIPTION));
		putValue(SHORT_DESCRIPTION, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_ABOUT_DESCRIPTION));
	}
}
