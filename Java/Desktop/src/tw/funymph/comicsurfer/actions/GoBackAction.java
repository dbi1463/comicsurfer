/* GoBackAction.java created on April 5, 2012
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static java.awt.event.KeyEvent.VK_Z;
import static javax.swing.KeyStroke.getKeyStroke;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import tw.funymph.comicsurfer.core.events.NavigationEvent;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/** An action that goes back to the previous viewed page.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.4
 */
public class GoBackAction extends NavigationAction {

	private static final String BACK_ICON_NAME = "Back";

	public GoBackAction(ComicSurferDesktopModel model) {
		super(model, BACK_ICON_NAME);
		setEnabled(systemModel.getNavigator().canGoBack());
		putValue(ACCELERATOR_KEY, getKeyStroke(VK_Z, KeyEvent.CTRL_MASK));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		systemModel.getNavigator().back();
	}

	@Override
	protected void updateDisplayedContents() {
		putValue(NAME, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_GO_BACK));
		putValue(SHORT_DESCRIPTION, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_GO_BAKC_DESCRIPTION));
	}

	@Override
	public void pageChanged(NavigationEvent event) {
		setEnabled(systemModel.getNavigator().canGoBack());
	}
}
