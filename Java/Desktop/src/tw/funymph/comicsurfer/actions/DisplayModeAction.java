/* DisplayModeAction.java created on Dec 5, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static tw.funymph.comicsurfer.preferences.Preferences.KEY_DISPLAY_MODE;
import static tw.jsway.utilities.ImageUtilities.loadIcon;

import java.awt.event.ActionEvent;

import javax.swing.Icon;

import tw.funymph.comicsurfer.core.ImageDisplayModel;
import tw.funymph.comicsurfer.core.events.ImageDisplayModelListener;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/** A base class of actions for changing four image display modes.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.3
 */
public abstract class DisplayModeAction extends SystemAction implements ImageDisplayModelListener {

	private static final Icon CHECKED_ICON = loadIcon("icons/Selected.png");
	private static final Icon UNCHECKED_ICON = loadIcon("icons/Unselected.png");

	protected int mode;

	/** Construct a <code>DisplayModeAction</code> instance by its
	 * inherited concrete classes.
	 * 
	 * @param model the comic surfer model
	 * @param dipslayMode the mode to be set
	 */
	protected DisplayModeAction(ComicSurferDesktopModel model, int dipslayMode) {
		super(model);
		mode = dipslayMode;
		updateDisplayedContents();
		modelUpdated(systemModel.getDisplayModel());
		systemModel.getDisplayModel().addImageDisplayModelListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		systemModel.getPreferences().setPreference(KEY_DISPLAY_MODE, String.valueOf(mode));
	}

	@Override
	public void modelUpdated(ImageDisplayModel model) {
		putValue(SMALL_ICON, ((model.getDisplayMode() == mode)? CHECKED_ICON : UNCHECKED_ICON));
	}
}
