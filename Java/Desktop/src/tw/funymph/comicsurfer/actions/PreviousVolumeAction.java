/* PreviousVolumeAction.java created on Oct 25, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static java.awt.event.KeyEvent.VK_P;
import static javax.swing.KeyStroke.getKeyStroke;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import tw.funymph.comicsurfer.core.events.NavigationEvent;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/** An action that changes the current page back to the first
 * page of the previous volume.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.1
 */
public class PreviousVolumeAction extends NavigationAction {

	private static final String PREVIOUS_VOLUME_ICON_NAME = "PreviousVolume";

	/** Construct a <code>PreviousVolumeAction</code> instance.
	 * 
	 * @param model the model that offers the related method to be invoked
	 */
	public PreviousVolumeAction(ComicSurferDesktopModel model) {
		super(model, PREVIOUS_VOLUME_ICON_NAME);
		setEnabled(systemModel.getNavigator().hasPreviousVolume());
		putValue(ACCELERATOR_KEY, getKeyStroke(VK_P, KeyEvent.CTRL_MASK | KeyEvent.SHIFT_MASK));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		systemModel.getNavigator().previousVolume(false);
	}

	@Override
	protected void updateDisplayedContents() {
		putValue(NAME, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_PREVIOUS_VOLUME));
		putValue(SHORT_DESCRIPTION, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_PREVIOUS_VOLUME_DESCRIPTION));
	}

	@Override
	public void pageChanged(NavigationEvent event) {
		setEnabled(systemModel.getNavigator().hasPreviousVolume());
	}
}
