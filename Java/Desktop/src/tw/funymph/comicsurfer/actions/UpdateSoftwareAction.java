/* UpdateSoftwareAction.java created on 2012/9/9
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import java.awt.event.ActionEvent;

import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/** An action that start up the software update.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.0
 */
public class UpdateSoftwareAction extends SystemAction {

	private static final String UPDATE_ICON_NAME = "Update";

	/** Construct an <code>UpdateSoftwareAction</code> instance.
	 * 
	 * @param model the comic surfer model
	 */
	public UpdateSoftwareAction(ComicSurferDesktopModel model) {
		super(model, UPDATE_ICON_NAME);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		systemModel.getUpdateManager().startUpdate();
	}

	@Override
	protected void updateDisplayedContents() {
		putValue(NAME, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_UPDATE_COMIC_SURFER));
		putValue(SHORT_DESCRIPTION, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_UPDATE_COMIC_SURFER));
	}
}
