/* StopSlideShowAction.java created on 2013/1/6
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static javax.swing.KeyStroke.getKeyStroke;

import java.awt.event.ActionEvent;

import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;
import tw.funymph.comicsurfer.ui.ImagePanelKeyController.KeyHandler;

/** An action that stop (leave) the slide show mode.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class StopSlideShowAction extends SystemAction implements KeyHandler {

	/** Construct a <code>StopSlideShowAction</code> instance.
	 * 
	 * @param model the model that offers the related method to be invoked
	 */
	public StopSlideShowAction(ComicSurferDesktopModel model) {
		super(model);
		putValue(ACCELERATOR_KEY, getKeyStroke("ESCAPE"));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		handle();
	}

	@Override
	protected void updateDisplayedContents() {
		putValue(NAME, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_STOP_SLIDE_SHOW));
		putValue(SHORT_DESCRIPTION, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_STOP_SLIDE_SHOW));
	}

	@Override
	public void handle() {
		systemModel.getSlideShowController().stopSlideShow();		
	}
}
