/* SetBackgroundColorAction.java created on Oct 26, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static java.awt.event.KeyEvent.VK_B;
import static javax.swing.KeyStroke.getKeyStroke;
import static tw.funymph.comicsurfer.preferences.Preferences.KEY_BACKGROUND_COLOR;
import static tw.jsway.color.ColorUtilities.toHexString;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import tw.funymph.comicsurfer.language.LanguageChangeListener;
import tw.funymph.comicsurfer.language.LocalizedTextProvider;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;
import tw.funymph.comicsurfer.model.ComicSurferDesktopPreferences;
import tw.jsway.color.ShowColorChooserAction;
import tw.jsway.property.ColorProperty;

/** An action that shows the color chooser for the user to change
 * the background color.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.0
 */
public class SetBackgroundColorAction extends ShowColorChooserAction implements LanguageChangeListener {

	/** Construct a <code>SetBackgroundColorAction</code> instance.
	 * 
	 * @param model the model that manages the background color
	 */
	public SetBackgroundColorAction(ComicSurferDesktopModel model) {
		super((ColorProperty)((ComicSurferDesktopPreferences)model.getPreferences()).getEditableProperty(KEY_BACKGROUND_COLOR));
		setUseColorInfoAsActionName(false);
		updateDisplayContents();
		putValue(ACCELERATOR_KEY, getKeyStroke(VK_B, KeyEvent.CTRL_MASK));
	}

	@Override
	public void languageChanged(LocalizedTextProvider source) {
		updateDisplayContents();
	}

	@Override
	protected void postActionPerformed(ActionEvent e) {
		updateDisplayContents();
	}

	/** Update the displayed contents when the current language is changed.
	 */
	private void updateDisplayContents() {
		String hexColor = toHexString(_color.getColor());
		putValue(NAME, _color.getDisplayText());
		putValue(SHORT_DESCRIPTION, _color.getDisplayText() + " - " + hexColor);
		setColorChooserTitle(_color.getDisplayText());
	}
}
