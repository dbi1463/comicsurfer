/* OpenFromAction.java created on 2012/9/20
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static java.awt.event.KeyEvent.VK_O;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import static javax.swing.JOptionPane.showInputDialog;
import static javax.swing.KeyStroke.getKeyStroke;
import static tw.funymph.commons.io.IOUtilities.convertFrom;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.net.URI;

import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/** An action that opens a input dialog for the user to input the comic book location.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class OpenFromAction extends SystemAction {

	private static final String OPEN_ICON_NAME = "OpenFrom";

	/** Construct a <code>OpenFromAction</code> instance.
	 * 
	 * @param model the model that offers the related method to be invoked
	 */
	public OpenFromAction(ComicSurferDesktopModel model) {
		super(model, OPEN_ICON_NAME);
		putValue(ACCELERATOR_KEY, getKeyStroke(VK_O, KeyEvent.CTRL_DOWN_MASK | KeyEvent.SHIFT_DOWN_MASK));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String location = null;
		String title = systemModel.getLanguageManager().getLocalizedText(LANG_KEY_OPEN_FROM_DIALOG_TITLE);
		String message = systemModel.getLanguageManager().getLocalizedText(LANG_KEY_OPEN_FROM_DIALOG_MSG);
        location = showInputDialog(null, message, title, INFORMATION_MESSAGE);
		final URI uri = convertFrom(location);
		if(uri != null) {
			systemModel.open(uri);
		}
	}

	@Override
	protected void updateDisplayedContents() {
		putValue(NAME, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_OPEN_FROM));
		putValue(SHORT_DESCRIPTION, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_OPEN_FROM_DESCRIPTION));
	}
}
