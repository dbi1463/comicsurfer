/* SetActualImageSizeAction.java created on Dec 5, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static java.awt.event.KeyEvent.VK_1;
import static javax.swing.KeyStroke.getKeyStroke;
import static tw.funymph.comicsurfer.core.ImageDisplayModel.ACTUAL_IMAGE_SIZE;

import java.awt.event.KeyEvent;

import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/** An action that change the current display mode to 
 * <code>ImageDisplayModel.ACTUAL_IMAGE_SIZE</code>.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.3
 */
public class SetActualImageSizeAction extends DisplayModeAction {

	/** Construct a <code>SetActualImageSizeAction</code> instance.
	 * 
	 * @param model the comic surfer model
	 */
	public SetActualImageSizeAction(ComicSurferDesktopModel model) {
		super(model, ACTUAL_IMAGE_SIZE);
		putValue(ACCELERATOR_KEY, getKeyStroke(VK_1, KeyEvent.CTRL_MASK));
	}

	@Override
	protected void updateDisplayedContents() {
		putValue(NAME, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_ACTUAL_IMAGE_SIZE));
		putValue(SHORT_DESCRIPTION, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_ACTUAL_IMAGE_SIZE_DESCRIPTION));
	}
}
