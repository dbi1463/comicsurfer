/* ChangeLanguageAction.java created on Oct 26, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static tw.funymph.comicsurfer.preferences.Preferences.KEY_LANGUAGE;
import static tw.jsway.utilities.ImageUtilities.loadIcon;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;

import tw.funymph.comicsurfer.language.LanguageChangeListener;
import tw.funymph.comicsurfer.language.LocalizedTextProvider;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/** An action that change the current displayed language.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.2
 */
public class ChangeLanguageAction extends AbstractAction implements LanguageChangeListener {

	private static final Icon CHECKED_ICON = loadIcon("icons/Selected.png");
	private static final Icon UNCHECKED_ICON = loadIcon("icons/Unselected.png");

	private String language;
	private ComicSurferDesktopModel sysModel;

	/** Construct a <code>ChangeLanguageAction</code> instance
	 * 
	 * @param model the model that offers the related method to be invoked
	 * @param languageName the language to be changed
	 */
	public ChangeLanguageAction(ComicSurferDesktopModel model, String languageName) {
		super(languageName);
		sysModel = model;
		language = languageName;
		sysModel.getLanguageManager().addLanguageChangeListener(this);
		putValue(NAME, languageName);
		putValue(SHORT_DESCRIPTION, languageName);
		updateDisplayedContents();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		sysModel.getPreferences().setPreference(KEY_LANGUAGE, language);
	}

	@Override
	public void languageChanged(LocalizedTextProvider source) {
		updateDisplayedContents();
	}

	/** Update the displayed icon.
	 */
	private void updateDisplayedContents() {
		if(sysModel.getLanguageManager().getCurrentLanguage().equalsIgnoreCase(language)) {
			putValue(SMALL_ICON, CHECKED_ICON);
		}
		else {
			putValue(SMALL_ICON, UNCHECKED_ICON);
		}
	}
}
