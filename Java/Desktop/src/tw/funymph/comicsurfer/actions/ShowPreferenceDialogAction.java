/* ShowPreferenceDialogAction.java created on 2012/9/25
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static java.awt.event.KeyEvent.VK_COMMA;
import static javax.swing.KeyStroke.getKeyStroke;
import static tw.funymph.comicsurfer.ComicSurfer.getMainFrame;
import static tw.funymph.comicsurfer.ui.PreferenceDialog.showPreferenceDialog;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/** An action that opens a dialog for editing preferences.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 2.1
 */
public class ShowPreferenceDialogAction extends SystemAction {

	private static final String PREFERENCES_ICON_NAME = "Preferences";

	/** Construct a <code>ShowPreferenceDialogAction</code> instance.
	 * 
	 * @param model the model that offers the related method to be invoked
	 */
	public ShowPreferenceDialogAction(ComicSurferDesktopModel model) {
		super(model, PREFERENCES_ICON_NAME);
		putValue(ACCELERATOR_KEY, getKeyStroke(VK_COMMA, KeyEvent.CTRL_DOWN_MASK));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		showPreferenceDialog(getMainFrame(), systemModel);
	}

	@Override
	protected void updateDisplayedContents() {
		putValue(NAME, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_PREFERENCES));
		putValue(SHORT_DESCRIPTION, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_PREFERENCES_DESCRIPTION));
	}
}
