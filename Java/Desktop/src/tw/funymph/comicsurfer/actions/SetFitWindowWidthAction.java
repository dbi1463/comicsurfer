/* SetFitScreenWidthAction.java created on Dec 5, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static java.awt.event.KeyEvent.VK_2;
import static javax.swing.KeyStroke.getKeyStroke;
import static tw.funymph.comicsurfer.core.ImageDisplayModel.FIT_WINDOW_WIDTH;

import java.awt.event.KeyEvent;

import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/** An action that change the current display mode to 
 * <code>ImageDisplayModel.FIT_WINDOW_WIDTH</code>.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.3
 */
public class SetFitWindowWidthAction extends DisplayModeAction {

	/** Construct a <code>SetFitWindowWidthAction</code> instance.
	 * 
	 * @param model the comic surfer model
	 */
	public SetFitWindowWidthAction(ComicSurferDesktopModel model) {
		super(model, FIT_WINDOW_WIDTH);
		putValue(ACCELERATOR_KEY, getKeyStroke(VK_2, KeyEvent.CTRL_MASK));
	}

	@Override
	protected void updateDisplayedContents() {
		putValue(NAME, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_FIT_WINDOW_WIDTH));
		putValue(SHORT_DESCRIPTION, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_FIT_WINDOW_WIDTH_DESCRIPTION));
	}
}
