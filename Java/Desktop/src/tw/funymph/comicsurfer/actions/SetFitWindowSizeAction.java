/* SetFitWindowSizeAction.java created on Dec 5, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static java.awt.event.KeyEvent.VK_4;
import static javax.swing.KeyStroke.getKeyStroke;
import static tw.funymph.comicsurfer.core.ImageDisplayModel.FIT_WINDOW_SIZE;

import java.awt.event.KeyEvent;

import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/** An action that change the current display mode to 
 * <code>ImageDisplayModel.FIT_WINDOW_SIZE</code>.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.3
 */
public class SetFitWindowSizeAction extends DisplayModeAction {

	/** Construct a <code>SetFitWindowSizeAction</code> instance.
	 * 
	 * @param model the comic surfer model
	 */
	public SetFitWindowSizeAction(ComicSurferDesktopModel model) {
		super(model, FIT_WINDOW_SIZE);
		putValue(ACCELERATOR_KEY, getKeyStroke(VK_4, KeyEvent.CTRL_MASK));
	}

	@Override
	protected void updateDisplayedContents() {
		putValue(NAME, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_FIT_WINDOW_SIZE));
		putValue(SHORT_DESCRIPTION, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_FIT_WINDOW_SIZE_DESCRIPTION));
	}
}
