/* NextVolumeAction.java created on Oct 25, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static java.awt.event.KeyEvent.VK_N;
import static javax.swing.KeyStroke.getKeyStroke;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import tw.funymph.comicsurfer.core.events.NavigationEvent;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/** An action that changes the page to the first page of the next volume.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.1
 */
public class NextVolumeAction extends NavigationAction {

	private static final String NEXT_VOLUME_ICON_NAME = "NextVolume";

	/** Construct a <code>NextVolumeAction</code> instance.
	 * 
	 * @param model the model that offers the related method to be invoked
	 */
	public NextVolumeAction(ComicSurferDesktopModel model) {
		super(model, NEXT_VOLUME_ICON_NAME);
		setEnabled(systemModel.getNavigator().hasNextVolume());
		putValue(ACCELERATOR_KEY, getKeyStroke(VK_N, KeyEvent.CTRL_MASK | KeyEvent.SHIFT_MASK));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		systemModel.getNavigator().nextVolume();
	}

	@Override
	protected void updateDisplayedContents() {
		putValue(NAME, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_NEXT_VOLUME));
		putValue(SHORT_DESCRIPTION, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_NEXT_VOLUME_DESCRIPTION));
	}

	@Override
	public void pageChanged(NavigationEvent event) {
		setEnabled(systemModel.getNavigator().hasNextVolume());
	}
}
