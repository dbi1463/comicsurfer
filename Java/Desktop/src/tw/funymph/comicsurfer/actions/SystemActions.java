/* SystemActions.java created on Oct 13, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static java.lang.Class.forName;

import java.lang.reflect.Constructor;

import javax.swing.Action;
import javax.swing.ActionMap;

import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/** A class that loads (using Java Reflection API) all actions that extended the
 * abstract <code>SystemAction</code> class.
 * 
 * @author Pin-Ying Tu
 * @version 1.3
 * @since 1.0
 */
public class SystemActions extends ActionMap {

	public static final String EXIT = "ExitAction";
	public static final String OPEN = "OpenAction";
	public static final String CLOSE = "CloseAction";
	public static final String ABOUT = "AboutAction";
	public static final String BACK = "GoBackAction";
	public static final String OPEN_FROM = "OpenFromAction";
	public static final String NEXT_PAGE = "NextPageAction";
	public static final String NEXT_VOLUME = "NextVolumeAction";
	public static final String PREVIOUS_PAGE = "PreviousPageAction";
	public static final String PREVIOUS_VOLUME = "PreviousVolumeAction";
	public static final String ACTUAL_SIZE = "SetActualImageSizeAction";
	public static final String UPDATE_SOFTWARE = "UpdateSoftwareAction";
	public static final String START_SLIDE_SHOW = "StartSlideShowAction";
	public static final String FIT_WINDOW_SIZE = "SetFitWindowSizeAction";
	public static final String FIT_WINDOW_WIDTH = "SetFitWindowWidthAction";
	public static final String FIT_WINDOW_HEIGHT = "SetFitWindowHeightAction";
	public static final String SET_BACKGROUND_COLOR = "SetBackgroundColorAction";
	public static final String SHOW_PREFERENCE_DIALOG = "ShowPreferenceDialogAction";

	private static final String ACTIONS[] = { EXIT, OPEN, CLOSE, ABOUT, NEXT_PAGE,
		NEXT_VOLUME, PREVIOUS_PAGE, PREVIOUS_VOLUME, ACTUAL_SIZE, FIT_WINDOW_WIDTH,
		FIT_WINDOW_HEIGHT, FIT_WINDOW_SIZE, SET_BACKGROUND_COLOR, BACK, OPEN_FROM,
		UPDATE_SOFTWARE, SHOW_PREFERENCE_DIALOG, START_SLIDE_SHOW };
	private static final String PACKAGE_PATH = "tw.funymph.comicsurfer.actions.";

	private static SystemActions actions;

	/** Get the loaded actions. Note that if the actions is not available, the
	 * method will call {@link #loadAction(String, ComicSurferDesktopModel)} method to
	 * load all actions.
	 * 
	 * @param model the instance needed when loading actions
	 * @return all loaded actions
	 */
	public static ActionMap getActions(ComicSurferDesktopModel model) {
		if(actions == null) {
			actions = new SystemActions();
			for(String name : ACTIONS) {
				actions.loadAction(name, model);
			}
		}
		return actions;
	}

	/** Load (using Java Reflection API) all action that extended the abstract
	 * <code>SystemAction</code> class.
	 * 
	 * @param name the action name
	 * @param model the model instance needed by <code>SystemAction</code>
	 */
	private void loadAction(String name, ComicSurferDesktopModel model) {
		try {
			Class<?> parameterTypes[] = { ComicSurferDesktopModel.class };
			Object parameters[] = { model };
			Constructor<?> constructor = forName(PACKAGE_PATH + name).getConstructor(parameterTypes);
			put(name, (Action)constructor.newInstance(parameters));
		} catch (Exception e) {
			System.out.println("[SystemActions] Unable load the action: " + name);
		}
	}
}
