/* ShowPreviousPageAction.java created on Oct 13, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static java.awt.event.KeyEvent.VK_P;
import static javax.swing.KeyStroke.getKeyStroke;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import tw.funymph.comicsurfer.core.events.NavigationEvent;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/** An action that changes the current page back to the previous page.
 * 
 * @author Pin-Ying Tu
 * @version 2.0
 * @since 1.0
 */
public class PreviousPageAction extends NavigationAction {

	private static final String PREVIOUS_PAGE_ICON_NAME = "PreviousPage";

	/** Construct a <code>PreviousPageAction</code> instance.
	 * 
	 * @param model the model that offers the related method to be invoked
	 */
	public PreviousPageAction(ComicSurferDesktopModel model) {
		super(model, PREVIOUS_PAGE_ICON_NAME);
		setEnabled(systemModel.getNavigator().hasPreviousPage());
		putValue(ACCELERATOR_KEY, getKeyStroke(VK_P, KeyEvent.CTRL_MASK));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		systemModel.getNavigator().previousPage();
	}

	@Override
	protected void updateDisplayedContents() {
		putValue(NAME, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_PREVIOUS_PAGE));
		putValue(SHORT_DESCRIPTION, systemModel.getLanguageManager().getLocalizedText(LANG_KEY_PREVIOUS_PAGE_DESCRIPTION));
	}

	@Override
	public void pageChanged(NavigationEvent event) {
		setEnabled(systemModel.getNavigator().hasPreviousPage());
	}
}
