/* SystemAction.java created on Oct 13, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import static tw.jsway.utilities.ImageUtilities.loadIcon;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.AbstractAction;
import javax.swing.Icon;

import tw.funymph.comicsurfer.LanguageKeys;
import tw.funymph.comicsurfer.language.LanguageChangeListener;
import tw.funymph.comicsurfer.language.LocalizedTextProvider;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/** A class can extend the abstract class that implements <code>LanguageChangeListener</code>
 * to update the displayed text when system language is changed. Note that any displayed text
 * must be declared as private string constants with the name starting by <code>LANG_KEY_</code>.
 * The <code>LanguageEditor</code> will use Java Reflection API to scan these constants and
 * offer the user a GUI to add a language definition file.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.0
 */
public abstract class SystemAction extends AbstractAction implements LanguageKeys, LanguageChangeListener, MouseListener {

	public static final String DEFAULT_ICON_EXTENSION = ".png";
	public static final String BASE_DIR = "icons/";

	private static final String HOVERED_ICON_POSFIX = "_Hovered";
	private static final String PRESSED_ICON_POSFIX = "_Pressed";

	protected Icon hoveredIcon;
	protected Icon normalIcon;
	protected Icon pressedIcon;
	protected ComicSurferDesktopModel systemModel;

	/** Construct a <code>SystemAction</code> instance by its inherited
	 * concrete classes.
	 * 
	 * @param sysModel the comic surfer model
	 */
	protected SystemAction(ComicSurferDesktopModel sysModel) {
		this(sysModel, "");
	}

	/** Construct a <code>SystemAction</code> instance by its inherited
	 * concrete classes with the specified icon.
	 * 
	 * @param sysModel the comic surfer model
	 * @param small the small icon
	 */
	protected SystemAction(ComicSurferDesktopModel sysModel, Icon small) {
		this(sysModel, small, null, null);
	}

	/** Construct a <code>SystemAction</code> instance by its inherited
	 * concrete classes with the specified small, hovered, and pressed icon.
	 * 
	 * @param sysModel the comic surfer model
	 * @param small the small icon
	 * @param hovered the hovered icon
	 * @param pressed the pressed icon
	 */
	protected SystemAction(ComicSurferDesktopModel sysModel, Icon small, Icon hovered, Icon pressed) {
		systemModel = sysModel;
		normalIcon = small;
		hoveredIcon = hovered;
		pressedIcon = pressed;
		updateDisplayedContents();
		systemModel.getLanguageManager().addLanguageChangeListener(this);
	}

	/** Construct a <code>SystemAction</code> instance by its inherited
	 * concrete classes with the specified icon name. This will load the
	 * small, hovered, and pressed icons from the default base directory
	 * using the default file extension. 
	 * 
	 * @param sysModel the comic surfer model
	 * @param iconName the small icon name
	 */
	protected SystemAction(ComicSurferDesktopModel sysModel, String iconName) {
		this(sysModel, iconName, BASE_DIR, DEFAULT_ICON_EXTENSION);
	}

	/** Construct a <code>SystemAction</code> instance by its inherited
	 * concrete classes with the specified icon name. This will load the
	 * small, hovered, and pressed icons from the specified base directory
	 * using the specified file extension.
	 * 
	 * @param sysModel the comic surfer model
	 * @param iconName the small icon name
	 * @param baseDir the base direction to load icons
	 * @param extension the file extension
	 */
	protected SystemAction(ComicSurferDesktopModel sysModel, String iconName, String baseDir, String extension) {
		systemModel = sysModel;
		systemModel.getLanguageManager().addLanguageChangeListener(this);
		setSmallIcon(loadIcon(baseDir + iconName + extension));
		setHoveredIcon(loadIcon(baseDir + iconName + HOVERED_ICON_POSFIX + extension));
		setPressedIcon(loadIcon(baseDir + iconName + PRESSED_ICON_POSFIX + extension));
		updateDisplayedContents();
	}

	/** Set the small icon.
	 * 
	 * @param icon the small icon
	 */
	public void setSmallIcon(Icon icon) {
		normalIcon = icon;
		putValue(SMALL_ICON, normalIcon);
	}

	/** Set the icon displayed when the mouse is hovered.
	 * 
	 * @param icon the hovered icon
	 */
	public void setHoveredIcon(Icon icon) {
		hoveredIcon = icon;
	}

	/** Set the icon displayed when any button of the mouse is pressed.
	 * 
	 * @param icon the pressed icon
	 */
	public void setPressedIcon(Icon icon) {
		pressedIcon = icon;
	}

	@Override
	public void languageChanged(LocalizedTextProvider source) {
		updateDisplayedContents();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		if(hoveredIcon != null) {
			putValue(SMALL_ICON, hoveredIcon);
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		if(getValue(SMALL_ICON) != normalIcon) {
			putValue(SMALL_ICON, normalIcon);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if(pressedIcon != null) {
			putValue(SMALL_ICON, pressedIcon);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if(getValue(SMALL_ICON) != normalIcon) {
			putValue(SMALL_ICON, normalIcon);
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {}

	/** Update the displayed contents when the system language is changed.
	 */
	protected abstract void updateDisplayedContents();
}
