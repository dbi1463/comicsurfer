/* NavigationAction.java created on Dec 7, 2011
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import tw.funymph.comicsurfer.core.events.NavigationEvent;
import tw.funymph.comicsurfer.core.events.NavigationEventListener;
import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;
import tw.funymph.comicsurfer.ui.ImagePanelKeyController.KeyHandler;

/** An abstract class that implements parts of <code>NavigationEventListener</code>.
 *  
 * @author Pin-Ying Tu
 * @version 2.0
 * @since 1.3
 */
public abstract class NavigationAction extends SystemAction implements NavigationEventListener, KeyHandler {

	/** Construct a <code>NavigationAction</code> instance by its
	 * inherited concrete classes.
	 * 
	 * @param model the comic surfer model
	 * @param iconName the name of the icon
	 */
	protected NavigationAction(ComicSurferDesktopModel model, String iconName) {
		super(model, iconName);
		systemModel.getNavigator().addNavigationEventListener(this);
	}

	@Override
	public void handle() {
		actionPerformed(null);
	}

	@Override
	public void volumeChanging(NavigationEvent event) {}

	@Override
	public void volumeChanged(NavigationEvent event) {}

	@Override
	public void suiteClosing(NavigationEvent event) {}

	@Override
	public void suiteClosed(NavigationEvent event) {
		setEnabled(false);
	}

	@Override
	public void suiteOpening(NavigationEvent event) {}

	@Override
	public void suiteOpened(NavigationEvent event) {}

	@Override
	public void suiteOpenFailed() {}
}
