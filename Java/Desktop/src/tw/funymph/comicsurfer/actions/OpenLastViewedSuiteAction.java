/* OpenLastViewedSuiteAction.java created on 2012/4/5
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer.actions;

import java.awt.event.ActionEvent;
import java.net.URI;

import tw.funymph.comicsurfer.model.ComicSurferDesktopModel;

/** The action to open the last viewed suites.
 * 
 * @author Pin-Ying Tu
 * @version 2.1
 * @since 1.4
 */
public class OpenLastViewedSuiteAction extends SystemAction {

	private URI _uri;

	public OpenLastViewedSuiteAction(ComicSurferDesktopModel model, int index, URI uri, String name) {
		super(model);
		putValue(NAME, index + ". " + name);
		putValue(SHORT_DESCRIPTION, name);
		_uri = uri;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		systemModel.open(_uri);
	}

	@Override
	protected void updateDisplayedContents() {}
}
