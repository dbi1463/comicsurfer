/* XMLLanguageFileGenerator.java created on 2013/2/10
 *
 * Copyright (C) Funymph all rights reserved.
 *
 * This file is a part of the Comic Surfer project.
 */
package tw.funymph.comicsurfer;

import static java.lang.String.format;
import static java.lang.reflect.Modifier.isStatic;

import java.io.File;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import tw.funymph.commons.xml.AbstractXMLEncoderTreeNode;
import tw.funymph.commons.xml.XMLEncoder;

/**
 * The class extracts the language keys from {@link LanguageKeys} and then
 * generate a XML language file.
 * 
 * @author Pin-Ying Tu
 * @version 3.0
 * @since 2.1
 */
public class XMLLanguageFileGenerator extends AbstractXMLEncoderTreeNode {

	private static final String FILE_NAME_TEMPLATE = "%1s.xml";

	private static final String ROOT_NODE_NAME = "language";
	private static final String ROOT_NODE_ATTRIBUTE_NAME = "name";
	private static final String ROOT_NODE_ATTRIBUTE_AUTHORS = "authors";

	private XMLEncoder encoder;
	private HashMap<String, String> attributes;

	/**
	 * Construct a <code>XMLLanguageFileGenerator</code> instance.
	 */
	public XMLLanguageFileGenerator() {
		super(ROOT_NODE_NAME);
		encoder = new XMLEncoder();
		attributes = new HashMap<String, String>();
		addChildEncoder(new XMLLanguageKeyExtracter());
	}

	/**
	 * Generate the language file.
	 * 
	 * @param locale the locale of the language file
	 * @param languageName the display name of the language
	 * @param author the author's name
	 */
	public void generate(String locale, String languageName, String author) {
		attributes.put(ROOT_NODE_ATTRIBUTE_NAME, languageName);
		attributes.put(ROOT_NODE_ATTRIBUTE_AUTHORS, author);
		encoder.encode(this, new File(format(FILE_NAME_TEMPLATE, locale)));
	}

	public Map<String, String> getAttributes(int index) {
		return attributes;
	}

	/**
	 * The class extract the language keys from {@link LanguageKeys}.
	 * 
	 * @author Pin-Ying Tu
	 * @version 3.0
	 * @since 2.1
	 */
	private class XMLLanguageKeyExtracter extends AbstractXMLEncoderTreeNode {

		private static final String TEXT_NODE_NAME = "text";
		private static final String LANG_KEY_PREFIX = "LANG_KEY";
		private static final String TEXT_NODE_ATTRIBUTE_KEY = "key";
		private static final String TEXT_NODE_ATTRIBUTE_VALUE = "value";

		private List<String> keys;

		/**
		 * Construct a <code>XMLLanguageKeyExtracter</code> instance.
		 */
		public XMLLanguageKeyExtracter() {
			super(TEXT_NODE_NAME);
			keys = new LinkedList<String>();
			extractKeys();
		}

		/**
		 * Extract all keys in the {@link LanguageKeys}.
		 */
		private void extractKeys() {
			Field[] fields = LanguageKeys.class.getDeclaredFields();
			for(Field field : fields) {
				if(isStatic(field.getModifiers()) && field.getName().startsWith(LANG_KEY_PREFIX)) {
					field.setAccessible(true);
					try {
						keys.add((String)field.get(null));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}

		@Override
		public int getRepeatCount() {
			return keys.size();
		}

		@Override
		public Map<String, String> getAttributes(int index) {
			HashMap<String, String> attributes = new HashMap<String, String>();
			String key = keys.get(index);
			attributes.put(TEXT_NODE_ATTRIBUTE_KEY, key);
			attributes.put(TEXT_NODE_ATTRIBUTE_VALUE, key);
			return attributes;
		}
	}
}
